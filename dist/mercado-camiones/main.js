(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--The content below is only a placeholder and can be replaced.-->\n\n<!-- LOADER -->\n<!--<div id=\"loader\">\n  <div class=\"position-center-center\">\n    <div class=\"ldr\"></div>\n  </div>\n</div>\n-->\n<!-- Wrap -->\n<div id=\"wrap\">\n<app-header></app-header>\n\n<router-outlet></router-outlet>\n\n<app-footer></app-footer>\n\n</div>\n<script src=\"./assets/js/jquery-1.11.3.min.js\"></script>\n<script src=\"./assets/js/bootstrap.min.js\"></script>\n<script src=\"./assets/js/own-menu.js\"></script>\n<script src=\"./assets/js/jquery.lighter.js\"></script>\n<script src=\"./assets/js/owl.carousel.min.js\"></script>\n\n<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->\n<script type=\"text/javascript\" src=\"./assets/rs-plugin/js/jquery.tp.t.min.js\"></script>\n<script type=\"text/javascript\" src=\"./assets/rs-plugin/js/jquery.tp.min.js\"></script>\n<script src=\"./assets/js/main.js\"></script>\n<script src=\"./assets/js/main.js\"></script>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/busqueda/busqueda.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/busqueda/busqueda.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header-breadcump \ntitulo='Resultados para: \"{{stringParam}}\"' \ndescripcion=\"Podes buscar por número de pieza o nombre\" \nbg=\"assets/img/bg1.png\">\n</header-breadcump>\n\n<section class=\"shop-page padding-top-100 padding-bottom-100\">\n  <div class=\"container\">\n      <div class=\"papular-block\" #allElements>\n        \n          <list-productos [productos]=\"productos\"></list-productos>\n\n      </div>\n\n\n      <div *ngIf=\"loading\" class=\"text-center\"> cargando... </div>\n\n\n      <div #moreElementsDiv class=\"moreelements text-center\">\n        <button *ngIf=\"getMore\"  (click)=\"loadmore()\" class=\"btn\">Cargas más</button>\n      </div>\n  </div>\n\n  <div *ngIf=\"!getMore && !loading\"  class=\"row\">\n    <div class=\"col-md-12 text-center\">\n      <h2 >¿No encontras el producto que estas buscando?</h2>\n      <h4 >Seguro lo tenemos</h4>\n      <p >Dejano un mensaje en el siguiente link con el producto que necesitas y nos comunicaremos cuanto antes.</p>\n      <p ><a href=\"/contacto\" class=\"btn btn-outline\">Contacto</a></p>\n    </div>\n  </div>\n\n</section>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/carro-compra/carro-compra.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/carro-compra/carro-compra.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("  <!-- Content -->\n  <div class=\"container padding-top-20\"><br>\n    <h2>CARRITO</h2>\n\n    <section *ngIf=\" ( getCantidad() == 0 ) && ( !enviado ) \"  class=\"padding-top-100 padding-bottom-100 pages-in chart-page\">\n      <div class=\"container text-center\">\n        <h2>El carrito de compras se encuentra vacío.</h2>\n        <p>Agregue productos para poder final la compra.</p>\n        <a class=\"btn\" href=\"/repuestos\"> Repuestos</a>\n      </div>        \n    </section>\n\n\n    <section *ngIf=\" enviado \"  class=\"padding-top-100 padding-bottom-100 pages-in chart-page\">\n      <div class=\"container text-center\">\n        <h3>Pedido Enviado.</h3>\n        <p>En un breve tiempo uno de nuestros asistentes se pondrá en contacto con ud. para coordinar la forma de pago y la recepcíon del producto.</p>\n        <a class=\"btn margin-top-100 margin-bottom-100\" href=\"#.\"> Inicio</a>\n      </div>        \n    </section>\n\n    <!--======= PAGES INNER =========-->\n    <section *ngIf=\" (getCantidad() > 0 )  && ( !enviado )\"  class=\"padding-top-20 pages-in chart-page\">\n      <div class=\"container\">\n\n        <!-- Payments Steps -->\n        <div class=\"shopping-cart text-center\">\n          <div class=\"cart-head\">\n            <ul class=\"row\">\n              <!-- PRODUCTS -->\n              <li class=\"col-sm-2 text-left\">\n                <h6>PRODUCTO</h6>\n              </li>\n              <!-- NAME -->\n              <li class=\"col-sm-4 text-left\">\n                <h6>NOMBRE</h6>\n              </li>\n              <!-- PRICE -->\n              <li class=\"col-sm-2\">\n                <h6>PRECIO</h6>\n              </li>\n              <!-- QTY -->\n              <li class=\"col-sm-1\">\n                <h6>CANTIDAD</h6>\n              </li>\n\n              <!-- TOTAL PRICE -->\n              <li class=\"col-sm-2\">\n                <h6>TOTAL</h6>\n              </li>\n              <li class=\"col-sm-1\"> </li>\n            </ul>\n          </div>\n\n          <!-- Cart Details -->\n          <ul class=\"row cart-details\" *ngFor=\"let producto of products;let i = index\">\n            <li class=\"col-sm-2 col-2\">            \n              <div class=\"position-center-center\">\n                   <a href=\"{{producto.info.image_url}}\" class=\"item-img\">\n                     <img src=\"{{producto.info.image_url}}\" width=\"70\" height=\"90\" alt=\"\"> \n                    </a> \n                  </div>\n              </li>\n              <li class=\"col-sm-4  col-10\"> \n                  <div class=\"position-center-center\">\n                    <h5>{{producto.info.replacement_name}}</h5>\n                    <p>{{producto.info.replacement_description}}</p>\n                  </div>\n              </li>\n              <li class=\"col-sm-2 col-3\">\n                <div class=\"position-center-center\"> <span class=\"price\"><small>$</small>{{producto.info.replacement_price}}</span> </div>\n              </li>\n              <li class=\"col-sm-1 col-3\">\n                <div class=\"position-center-center\">\n                    <select [(ngModel)]=\"products[i].amount\" (change)=\"modifyProd(i)\">\n                      <option *ngFor=\"let c of [1,2,3,4,5,6,7,8,9,10]\" [ngValue]=\"c\">{{c}}</option>\n                    </select>\n                  </div>\n              </li>\n            <li class=\"col-sm-2 col-3\">\n              <div class=\"position-center-center\"> <span class=\"price\"><small>$</small>{{producto.total}}</span> </div>\n            </li>\n            <li class=\"col-sm-1 col-3\">\n              <div class=\"position-center-center\"> <a (click)=\"removeProd(i)\" href=\"\"><i class=\"icon-close\"></i></a> </div>\n            </li>\n          </ul>\n          <!-- Total de compra-->\n          <div class=\"row\">\n            <div class=\"col-md-12 text-mutted text-right\">\n              <!-- SUB TOTAL -->\n              SUBTOTAL (SIN IVA) <span> $ {{totalSinIva}}</span>\n            </div>\n            <div class=\"col-md-12 text-right total\">\n                  <!-- SUB TOTAL -->\n                  TOTAL <span> $ {{ total }}</span>\n            </div>\n          </div>\n\n\n\n        </div>\n      </div> <!-- Cierra div container-->\n    </section>\n\n    <section *ngIf=\"getCantidad() > 0 && ( !enviado )\" class=\"chart-page  padding-bottom-20\">\n      <form #signupForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"enviarpedido(signupForm)\">\n        <div class=\"container\">\n          <!-- Payments Steps -->\n          <div class=\"shopping-cart\">\n\n            <div class=\"cart-ship-info\">\n              <div class=\"\">\n                <!-- ESTIMATE SHIPPING & TAX -->\n                <div class=\"\">\n                  <h6>DATOS DEL COMPRADOR</h6>\n                    <ul class=\"row\">\n                      <!-- Name -->\n                      <li class=\"col-md-6\">\n                        <label> *NOMBRE Y APELLIDO\n                          <input ngModel type=\"text\" name=\"carrito_nombre\" id=\"carrito_nombre\" value=\"\" placeholder=\"\" ngModel required>\n                        </label>\n                      </li>\n                      <li class=\"col-md-6\">\n                        <!-- ADDRESS -->\n                        <label>*DIRECCION (Calle y ciudad)\n                          <input ngModel type=\"text\" name=\"carrito_direccion\" id=\"carrito_direccion\" value=\"\" placeholder=\"\" ngModel required>\n                        </label>\n                      </li>\n\n                      <!-- PHONE -->\n                      <li class=\"col-md-6\">\n                        <label> *TELEFONO\n                          <input ngModel type=\"text\" name=\"carrito_telefono\" id=\"carrito_telefono\" value=\"\" placeholder=\"\" ngModel required>\n                        </label>\n                      </li>\n\n                      <!-- EMAIL ADDRESS -->\n                      <li class=\"col-md-6\">\n                        <label> *DIRECCION DE EMAIL\n                          <input ngModel type=\"text\" name=\"carrito_email\" id=\"carrito_email\" value=\"\" placeholder=\"\" ngModel required>\n                        </label>\n                      </li>\n                    </ul>\n                </div>\n              </div>\n            </div>\n\n            <!-- SHOPPING INFORMATION -->\n            <div class=\"cart-ship-info\">\n              <div class=\"\">\n                <!-- ESTIMATE SHIPPING & TAX -->\n                <div class=\"\">\n                  <h6>DETALLES DE FACTURACION</h6>\n                    <ul class=\"row\">\n                      <!-- Name -->\n                      <li class=\"col-md-6\">\n                        <label> *RAZON SOCIAL\n                          <input ngModel type=\"text\" name=\"carrito_razon\" id=\"carrito_razon\" value=\"\" placeholder=\"\" ngModel required>\n                        </label>\n                      </li>\n\n                      <li class=\"col-md-6\">\n                        <!-- COMPANY NAME -->\n                        <label>*CUIT\n                          <input ngModel type=\"text\" name=\"carrito_cuit\" id=\"carrito_cuit\" value=\"\" placeholder=\"\" ngModel required>\n                        </label>\n                      </li>\n                    </ul>\n                </div>\n              </div>\n            </div>\n\n            <div class=\"cart-ship-info\">\n              <!-- SUB TOTAL -->\n              <div class=\"col-12\">\n                <h6>FORMA DE PAGO</h6>\n                <div class=\"order-place\">\n                  <div class=\"pay-meth\">\n                    <ul>\n                      <li>\n                        <div class=\"radio\">\n                          <input ngModel type=\"radio\" name=\"carrito_pago\" id=\"radio1\" value=\"Transferencia Bancaria\" checked>\n                          <label for=\"radio1\"> TRANSFERENCIA BANCARIA </label>\n                        </div>\n                        <p>Se le facilitarán los datos bancarios para la transferencia.</p>\n                      </li>\n                      <li>\n                        <div class=\"radio\">\n                          <input type=\"radio\" ngModel name=\"carrito_pago\" id=\"radio2\" value=\"Efectivo o tarjeta en agencia \">\n                          <label for=\"radio2\"> EFECTIVO O TARJETA EN AGENCIA</label>\n                        </div>\n                        <p>Se acordará la fecha de pago.</p>\n                      </li>\n                      <li>\n                        <div class=\"radio\">\n                          <input type=\"radio\" name=\"carrito_pago\" id=\"radio3\" ngModel value=\"Cheque\">\n                          <label for=\"radio3\"> CHEQUE EN AGENCIA </label>\n                        </div>\n                        <p>Se le solicitará información de los mismos.</p>\n                      </li>\n                    </ul>\n\n                  </div>\n                </div>\n              </div>\n            </div>\n\n            <div class=\"cart-ship-info\">\n              <!-- SUB TOTAL -->\n              <div class=\"col-12\">\n                <h6>RECEPCIÓN DEL PRODUCTO</h6>\n                <div class=\"order-place\">\n                  <div class=\"pay-meth\">\n                    <ul>\n                      <li>\n                        <div class=\"radio\">\n                          <input type=\"radio\" name=\"carrito_envio\" id=\"radio4\" ngModel value=\"Envio a domicilio\" >\n                          <label for=\"radio4\">ENVIO A DOMICILIO</label>\n                        </div>\n                        <p>Se le solicitará información adicional sobre la recepción del pedido.</p>\n                      </li>\n                      <li>\n                        <div class=\"radio\">\n                          <input type=\"radio\" name=\"carrito_envio\" id=\"radio5\" ngModel value=\"Retiro en sucursal\" checked>\n                          <label for=\"radio5\">RETIRO EN SUCURSAL</label>\n                        </div>\n                        <p>Podrá elegir entre cualquiera de nuestras sucursales.</p>\n                      </li>\n                    </ul>\n\n                  </div>\n                </div>\n              </div>\n            </div>\n\n          </div>\n        </div>\n        <div class=\"container padding-top-50 padding-bottom-50\">\n          <div class=\"text-right\">\n            <button type=\"submit\" class=\"btn btn-primary\">Finalizar</button>\n          </div>\n        </div>\n      </form>\n    </section>\n\n\n\n\n  </div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/categoria/categoria.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/categoria/categoria.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"buscador-usados\"><input name=\"term\" placeholder=\"Buscá por marca, numero de pieza, categoria...\" /></div>\n\n<div class=\"container\"> \n  <h2>{{categoria?.type_name}}</h2>\n\n  <div *ngIf=\"categoria?.hijos\">\n    <list-categoria [categorias]=\"categoria?.hijos\" [titulo]=\"tituloSubcat\"></list-categoria>\n  </div>\n\n  <div class=\"row shop-page padding-top-50 padding-bottom-50\">\n      <div class=\"col-2\">\n        Categorias\n      </div>\n      <div class=\"col-10\">\n        Resultado: 200\n        <div class=\"papular-block\">\n          <list-productos [productos]=\"productos\"></list-productos> \n        </div>\n      </div>\n      \n      <div #moreElementsDiv class=\"col-12 moreelements text-center\">\n        <button *ngIf=\"getMore\"  (click)=\"loadmore()\" class=\"btn\">Cargas más</button>\n      </div>\n  </div>\n  \n</div>\n\n<div *ngIf=\"!getMore\">\n  <div class=\"text-center\">\n    <h2 >¿No encontras el producto que estas buscando?</h2>\n    <h4 >Seguro lo tenemos</h4>\n    <p >Dejano un mensaje en el siguiente link con el producto que necesitas y nos comunicaremos cuanto antes.</p>\n    <p ><a href=\"/contacto\" class=\"btn btn-outline\">Contacto</a></p>\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/filtro-elegidos/filtro-elegidos.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/common/filtro-elegidos/filtro-elegidos.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"filtros.length > 0\">\nFiltros:\n<ng-container *ngFor=\"let filtro of filtros\" >\n\n   <div class=\"option\" *ngFor=\"let combo of filtro.selected\" (click)=\"removeThisFilter(combo, filtro.type)\">\n      {{ combo.label }} <span class=\"remove\"> x </span>\n   </div>\n\n</ng-container>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/filtro-select/filtro-select.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/common/filtro-select/filtro-select.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h3 (click)=\"visible = !visible\">\n  <span>{{label}}</span>\n\n  <div class=\"icon\" *ngIf=\"!visible\" >\n    <i class=\"fas fa-chevron-down\"></i>\n  </div>\n  <div class=\"icon\" *ngIf=\"visible\" >\n    <i class=\"fas fa-chevron-up\"></i>\n  </div>\n\n\n</h3>\n<ul *ngIf=\"visible\" [@slideInOut]>\n    <li class=\"option\" *ngFor=\"let dato of datos\" (click)=\"select(dato)\" [ngClass]=\"{ active: isSelected(dato)}\" >\n        {{dato.label}}\n    </li>\n</ul>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/galery/galery.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/common/galery/galery.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"images-slider\">\n\n  <div class=\"slider-thumb\">\n    <ol id=\"listImag\" class=\"flex-control-thumbs\">\n      <li *ngFor=\"let imagen of imagenes;let i = index\" class=\"\">\n        <img [ngClass]=\"{'flex-active': this.indice==i}\"  (click)=\"setThisFocus(i)\" src=\"{{ imagen.image_url }}\" draggable=\"false\">\n      </li>\n    </ol>\n  </div>\n\n  <div class=\"flecha-prev\" (click)=\"changePrevFocus()\">\n    <i class=\"fa fa-arrow-left\" aria-hidden=\"true\"></i>\n  </div>\n\n  <div class=\"slider-container\">\n    <div class=\"slides\">\n      <div class=\"flex-active-slide\" data-thumb-alt=\"\" style=\"width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 1;\"> <img class=\"img-responsive\" src=\"{{imagenes[this.indice].image_url}}\" alt=\"\" draggable=\"false\" width=\"100%\" > </div>\n    </div>\n  </div>\n\n  <div class=\"flecha-next\" (click)=\"changeNextFocus()\">\n    <i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i>\n  </div>\n\n\n\n\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/contacto/contacto.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contacto/contacto.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n  <div class=\"container-fluid padding-top-50 padding-bottom-50\">\n    <div class=\"contact-form\">\n      <h5>Complete el formulario</h5>\n      <div class=\"row\">\n        <div class=\"col-md-8\">\n\n          <!--======= Success Msg =========-->\n          <div *ngIf=\" enviado \" class=\"padding-top-50 padding-bottom-50\" >\n            <div id=\"contact_message\" class=\"success-msg\"> \n              <i class=\"fa fa-paper-plane-o\"></i>Gracias por su interes. Su mensaje ha sido enviado correctamente, en instantes un operador se pondra en contacto.\n            </div>\n\n            <a class=\"btn margin-top-100 margin-bottom-50\" href=\"/\"> VOLVER AL INICIO</a>\n\n          </div>\n          <!-- ============== UNIDAD ==================-->\n\n          <div *ngIf=\" unidad && !enviado \" class=\"col-md-12\">\n            <section  class=\" padding-bottom-20 pages-in chart-page\">\n              <div class=\"container\">\n        \n                <!-- Payments Steps -->\n                <div class=\"shopping-cart text-center\">\n                  <div class=\"cart-head\">\n                    <ul class=\"row\">\n                      <!-- PRODUCTS -->\n                      <li class=\"col-sm-3 text-center\">\n                        <h6>CAMION</h6>\n                      </li>\n                      <!-- NAME -->\n                      <li class=\"col-sm-4 text-center\">\n                        <h6>MODELO</h6>\n                      </li>\n                      <!-- PRICE -->\n                      <li class=\"col-sm-3\">\n                        <h6>MARCA</h6>\n                      </li>\n                      <!-- QTY -->\n                      <li class=\"col-sm-2\">\n                        <h6>AÑO</h6>\n                      </li>\n                    </ul>\n                  </div>\n        \n                  <!-- Cart Details -->\n                  <ul class=\"row cart-details\">\n                    <li class=\"col-sm-3 col-3\">            \n                      <div class=\"position-center-center\">\n                           <a href=\"{{usado.vehicle_thumbnail_url}}\" class=\"item-img\">\n                             <img src=\"{{usado.vehicle_thumbnail_url}}\" width=\"70\" height=\"90\" alt=\"\"> \n                            </a> \n                          </div>\n                      </li>\n                      <li class=\"col-sm-4  col-10\"> \n                          <div class=\"position-center-center\">\n                            <p>{{usado.vehicle_model}}</p>\n                          </div>\n                      </li>\n                      <li class=\"col-sm-3 col-3\">\n                        <div class=\"position-center-center\">\n                          <p>{{usado.brand_name}}</p> \n                        </div>\n                      </li>\n                      <li class=\"col-sm-2 col-3\">\n                        <div class=\"position-center-center\">\n                          <p>{{usado.vehicle_year}}</p> \n                        </div>\n                      </li>\n                  </ul>        \n                </div>\n              </div> <!-- Cierra div container-->\n            </section>\n          </div>\n\n\n          <!--======= FORM  =========-->\n          <form #signupForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"contacto(signupForm)\">\n            <div  *ngIf=\" ! enviado \" >\n              <ul class=\"row\">\n                <li class=\"col-sm-6\">\n                  <label>Nombre Completo\n                    <input type=\"hidden\" ngModel name=\"contact_type\" id=\"contact_type\"  value=\"Contacto\">\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_name' type=\"text\" class=\"form-control\" ngModel required name=\"contact_name\" id=\"contact_name\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel required name=\"contact_name\" id=\"contact_name\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-6\">\n                  <label>Email\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_email' type=\"text\" class=\"form-control\" ngModel name=\"contact_email\" id=\"contact_email\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel name=\"contact_email\" id=\"contact_email\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-6\">\n                  <label>Teléfono\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_phone' type=\"text\" class=\"form-control\" ngModel required name=\"contact_phone\" id=\"contact_phone\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel required name=\"contact_phone\" id=\"contact_phone\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-6\">\n                  <label>Ciudad\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_city' type=\"text\" class=\"form-control\" ngModel name=\"contact_city\" id=\"contact_city\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel name=\"contact_city\" id=\"contact_city\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-12\">\n                  <label>Mensaje\n                    <textarea class=\"form-control\" name=\"contact_msj\" [(ngModel)]='msj' required id=\"contact_msj\" rows=\"5\" placeholder=\"\"></textarea>\n                  </label>\n                </li>\n                <li class=\"col-sm-12\">\n                  <button type=\"submit\" value=\"Enviar Email\" class=\"btn\" id=\"btn_submit\">ENVIAR</button>\n                </li>\n              </ul>\n            </div>\n          </form>\n        </div>\n\n        <!--======= ADDRESS INFO  =========-->\n        <div class=\"col-md-4\">\n          <div class=\"contact-info\">\n            <h6>NUESTRA DIRECCIÓN</h6>\n            <ul>\n              <li> <i class=\"icon-map-pin\"></i> Camino Parque Sesquicentenario<br> RN3 Km 696<br>\n                B8000 Bahía Blanca, <br>Buenos Aires - Argentina</li>\n              <li> <i class=\"icon-call-end\"></i> 0291 454-7043</li>\n              <li> <i class=\"icon-envelope\"></i> <a href=\"mailto:info@mercadocamiones.com.ar\" target=\"_top\">info@mercadocamiones.com.ar</a> </li>\n            </ul>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/ajuste/ajuste.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/ajuste/ajuste.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\n  ajuste works!\n</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/cotizaciones/cotizaciones.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/cotizaciones/cotizaciones.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\n  cotizaciones works!\n</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/cotizador.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/cotizador.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"sub-bnr\" data-stellar-background-ratio=\"0.5\" style=\"background-position: 0% BOTTOM; background-image: url(assets/img/seguros.jpg);\">\n  <div class=\"position-center-center\">\n    <div class=\"container\">\n      <h4>ASEGURÁ CON NOSOTROS</h4>\n    </div>\n  </div>\n</section>\n\n<section class=\" padding-top-100\">\n    <div class=\"container\">\n\n      <div class=\"contact-form\">\n\n        <h2>Cotizamos tu seguro</h2>\n\n        <h6><b>¿Queres tener una referencia, saber si pagas de más o mejorar la poliza de tus seguros? Envianos un mensaje y nuestro asesor se pondrá en contacto con vos.</b></h6>\n\n\n        <p>Te ofrecemos una cobertura total que garantice el amparo del patrimonio de tu empresa, protegiendo los bienes ante los riesgos que puedan afectarlos.</p>\n        <p>Conocemos la necesidad e importancia que implica contar con vehículos seguros para el desarrollo de su actividad. Garantizamos protección total.</p>\n        <hr>\n        <div class=\"row\" >\n\n          <div class=\"col-md-8\">\n\n            <!--======= Success Msg =========-->\n            <div *ngIf=\" enviado \" >\n              <div id=\"contact_message\" class=\"success-msg\"> <i class=\"fa fa-paper-plane-o\"></i>Gracias por confiar en nosotros. Su mensaje ha sido enviado correctamente.</div>\n            </div>\n            <!--======= FORM  =========-->\n            <form #signupForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"contacto(signupForm)\">\n              <div *ngIf=\" ! enviado \">\n              <ul class=\"row\">\n                <li class=\"col-sm-6\">\n                  <label>Nombre Completo\n                    <input type=\"hidden\" ngModel name=\"contact_type\" id=\"contact_type\"  value=\"Seguro\">\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_name' type=\"text\" class=\"form-control\" ngModel required name=\"contact_name\" id=\"name\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel required name=\"contact_name\" id=\"name\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-6\">\n                  <label>Email\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_email' type=\"text\" class=\"form-control\" ngModel name=\"contact_email\" id=\"email\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel name=\"contact_email\" id=\"email\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-6\">\n                  <label>Teléfono\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_phone' type=\"text\" class=\"form-control\" ngModel required name=\"contact_phone\" id=\"telefono\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel required name=\"contact_phone\" id=\"telefono\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-6\">\n                  <label>Ciudad\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_city' type=\"text\" class=\"form-control\" ngModel name=\"contact_city\" id=\"contact_city\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel name=\"contact_city\" id=\"contact_city\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-12\">\n                  <label>Detalle de la cotización (Marca + Modelo + Año )\n                    <textarea class=\"form-control\" name=\"contact_msj\" ngModel required id=\"contact_msj\" rows=\"5\" placeholder=\"\"></textarea>\n                  </label>\n                </li>\n                <li class=\"col-sm-12\">\n                  <button type=\"submit\" value=\"Enviar Email\" class=\"btn\" id=\"btn_submit\">ENVIAR EMAIL</button>\n                </li>\n              </ul>\n              </div>\n            </form>\n          </div>\n\n          <!--======= ADDRESS INFO  =========-->\n          <div class=\"col-md-4\">\n            <div class=\"contact-info\">\n              <h6>NUESTRA DIRECCIÓN</h6>\n              <ul>\n                <li> <i class=\"icon-map-pin\"></i> Camino Parque Sesquicentenario<br> RN3 Km 696<br>\n                  B8000 Bahía Blanca, <br>Buenos Aires - Argentina</li>\n                <li> <i class=\"icon-call-end\"></i> 0291 454-7043</li>\n                <li> <i class=\"icon-envelope\"></i> <a href=\"mailto:info@mercadocamiones.com.ar\" target=\"_top\">info@mercadocamiones.com.ar</a> </li>\n              </ul>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n\n\n  </section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/solicitud/solicitud.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/solicitud/solicitud.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\n  solicitud works!\n</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/vehiculo/vehiculo.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/vehiculo/vehiculo.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\n  vehiculo works!\n</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/financiamiento/financiamiento.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/financiamiento/financiamiento.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header-breadcump\n  titulo='Fianciamiento Exclusivo'\n  descripcion=\"Brindamos finacionamiento a máxima velocidad\"\n  bg=\"assets/img/bg3.png\">\n</header-breadcump>\n\n\n<section class=\"contact padding-top-100 padding-bottom-100\">\n  <div class=\"container\">\n    <div class=\"contact-form\">\n      <h5>Complete el formulario</h5>\n      <div class=\"row\">\n        <div class=\"col-md-8\">\n\n          <!--======= Success Msg =========-->\n          <div *ngIf=\" enviado \" >\n            <div id=\"contact_message\" class=\"success-msg\"> <i class=\"fa fa-paper-plane-o\"></i>Gracias por confiar en nosotros. Su mensaje ha sido enviado correctamente.</div>\n          </div>\n          <!--======= FORM  =========-->\n          <form #signupForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"contacto(signupForm)\">\n            <div  *ngIf=\" ! enviado \" >\n              <ul class=\"row\">\n                <li class=\"col-sm-6\">\n                  <label>Nombre Completo\n                    <input type=\"hidden\" ngModel name=\"contact_type\" id=\"contact_type\"  value=\"Contacto\">\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_name' type=\"text\" class=\"form-control\" ngModel required name=\"contact_name\" id=\"contact_name\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel required name=\"contact_name\" id=\"contact_name\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-6\">\n                  <label>Email\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_email' type=\"text\" class=\"form-control\" ngModel name=\"contact_email\" id=\"contact_email\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel name=\"contact_email\" id=\"contact_email\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-6\">\n                  <label>Teléfono\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_phone' type=\"text\" class=\"form-control\" ngModel required name=\"contact_phone\" id=\"contact_phone\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel required name=\"contact_phone\" id=\"contact_phone\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-6\">\n                  <label>Ciudad\n                    <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_city' type=\"text\" class=\"form-control\" ngModel name=\"contact_city\" id=\"contact_city\" placeholder=\"\">\n                    <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel name=\"contact_city\" id=\"contact_city\" placeholder=\"\">\n                  </label>\n                </li>\n                <li class=\"col-sm-12\">\n                  <label>Mensaje\n                    <textarea class=\"form-control\" name=\"contact_msj\" ngModel required id=\"contact_msj\" rows=\"5\" placeholder=\"\"></textarea>\n                  </label>\n                </li>\n                <li class=\"col-sm-12\">\n                  <button type=\"submit\" value=\"Enviar Email\" class=\"btn\" id=\"btn_submit\">ENVIAR EMAIL</button>\n                </li>\n              </ul>\n            </div>\n          </form>\n        </div>\n\n        <!--======= ADDRESS INFO  =========-->\n        <div class=\"col-md-4\">\n          <div class=\"contact-info\">\n            <h6>NUESTRA DIRECCIÓN</h6>\n            <ul>\n              <li> <i class=\"icon-map-pin\"></i> Camino Parque Sesquicentenario<br> RN3 Km 696<br>\n                B8000 Bahía Blanca, <br>Buenos Aires - Argentina</li>\n              <li> <i class=\"icon-call-end\"></i> 0291 454-7043</li>\n              <li> <i class=\"icon-envelope\"></i> <a href=\"mailto:info@mercadocamiones.com.ar\" target=\"_top\">info@mercadocamiones.com.ar</a> </li>\n            </ul>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/footer/footer.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/footer/footer.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<app-newsletter></app-newsletter>\n\n<footer>\n  <div class=\"container\">\n    <div class=\"row\">\n\n\n    <div class=\"col-md-4  col-sm-12  col-12  order-1 text-left\">\n      <div class=\"text-center\">\n        <img class=\"margin-bottom-30\" src=\"./assets/img/logo1.png\" width=\"100\" alt=\"\" >\n      </div>\n      <p>Lorem ipsum dolor sit amet, consectetur\n        adipiscing elit. Pellen tesque lacinia dolor in lacus u\n        llamcorper laoreet.</p>\n    </div>\n\n    <div class=\"col-md-2 order-2\"> </div>\n\n    <div class=\"col-md-2 col-sm-4 col-6 text-left  order-3\">\n      <h3>EXPLORAR</h3>\n          <ul class=\"link\">\n          <li><a routerLink=\"/\" > Inicio</a></li>\n          <li><a routerLink=\"/contacto\" > Contacto</a></li>\n          <li><a routerLink=\"/carrito\" > Carrito</a></li>\n          <li><a routerLink=\"/buscar\" > Buscar</a></li>\n        </ul>\n    </div>\n\n    <div class=\"col-md-2 col-sm-4  col-6 text-left  order-4\">\n      <h3>INFO</h3>\n      <ul class=\"link\">\n        <li><a routerLink=\"/usados\" > Usados</a></li>\n        <li><a routerLink=\"/repuestos\" > Repuestos</a></li>\n        <li><a routerLink=\"/servicios\" > Servicios</a></li>\n        <li><a routerLink=\"/seguros\" > Seguros</a></li>\n      </ul>\n    </div>\n\n    <div class=\"col-md-2 col-sm-4 text-left  order-5\">\n      <h3>CONTACTO</h3>\n      <ul class=\"link\">\n        <li><a href=\"#\">RN3 Km 696</a></li>\n        <li><a href=\"#\">B8000 Bahía Blanca</a></li>\n        <li><a href=\"#\">Buenos Aires</a></li>\n        <li><a href=\"#\">Argentina</a></li>\n      </ul>\n    </div>\n  </div>\n    <!-- Rights -->\n\n\n  </div>\n\n\n    <!-- About -->\n    <section class=\"small-about\">\n      <div class=\"container\">\n          <div class=\"row\">\n            <div class=\"col-md-6 col-sm-12 col-12 text-left\">\n              <ul class=\"social_icons\">\n                <li><a href=\"#\">Decker Camiones</a></li>\n                <li> <a href=\"#\">Privacidad</a></li>\n                <li> <a href=\"#\">Terminos de uso</a></li>\n              </ul>\n            </div>\n            <div class=\"col-md-6 col-sm-12 col-12 text-right\">\n              <ul class=\"social_icons\">\n                <li><a href=\"#.\"><i class=\"icon-social-facebook\"></i></a></li>\n                <li><a href=\"#.\"><i class=\"icon-social-twitter\"></i></a></li>\n                <li><a href=\"#.\"><i class=\"icon-social-tumblr\"></i></a></li>\n                <li><a href=\"#.\"><i class=\"icon-social-youtube\"></i></a></li>\n                <li><a href=\"#.\"><i class=\"icon-social-dribbble\"></i></a></li>\n              </ul>\n            </div>\n          </div>\n      </div>\n    </section>\n</footer>\n<div class=\"rights\">\n  <p>©  2020  Mercado Camiones -  All right reserved. </p>\n  <div class=\"scroll\"> <span (click)=\"totop()\" class=\"go-up\"><i class=\"fas fa-angle-up\"></i></span> </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/header-breadcump/header-breadcump.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/header-breadcump/header-breadcump.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"sub-bnr\" data-stellar-background-ratio=\"0.5\" [ngStyle]=\"{'background-image': 'url('+mybg+')'}\">\n  <div class=\"position-center-center\">\n    <div class=\"container\">\n      <h4>{{titulo}}</h4>\n      <p class=\"parrafo-blanco\">{{descripcion}}</p>\n    </div>\n  </div>\n</section>\n\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/header/buscador/buscador.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/header/buscador/buscador.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"buscador-usados\">\n\n  <input name=\"term\" placeholder=\"Buscá por marca, modelo, año, color...\" />\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header>\n\n  <div class=\"container-fluid\">\n\n\n    <div class=\"top-nav\">\n\n      <!--<div class=\"nav-buscador\">\n        <form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\" >\n          <input [(ngModel)]=\"textoDeInput\" name=\"search\" class=\"input-search\" type=\"search\" placeholder=\"Buscar producto o número de pieza\" aria-label=\"Search\">\n          <button class=\"btn-search\" type=\"submit\"> <i class=\" icon-magnifier\"></i></button>\n        </form>\n      </div>-->\n\n      <div class=\"nav-botones\">\n\n\n        <ul class=\"nav-header-mobile\">\n          <!--<li class=\"nav-carrito\">\n            <a href=\"/carrito\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"true\">\n              <i class=\"icon-basket-loaded\" *ngIf=\"getCantidad() > 0\"></i>\n              <i class=\"icon-basket\" *ngIf=\"getCantidad() == 0\"></i>\n              <span *ngIf=\"getCantidad() > 0\"  class=\"mybadge\">{{ getCantidad() }}</span>\n            </a>\n          </li>-->\n          <li>\n            <img src=\"./assets/img/logo0.png\" alt=\"Logo\" class=\"logo-mobile\" >\n          </li>\n          <li class=\"nav-toggle\">\n            <button class=\"navbar-toggler\" (click)=\"clickEventMenu()\" type=\"button\" data-toggle=\"collapse\" data-target=\"#nav-open-btn\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n              <i class=\"fas fa-bars\"></i>\n            </button>\n          </li>\n\n        </ul>\n      </div>\n\n    </div>\n\n    <nav class=\"navbar navbar-expand-md \">\n      <div class=\"nav-logo\">\n        <a href=\"#\">\n          <img src=\"./assets/img/logo0.png\" alt=\"Logo\" class=\"logo-full\">\n          <img src=\"./assets/img/logo0.png\" alt=\"Logo\" class=\"logo-responsive\" >\n        </a>\n      </div>\n\n      <div class=\"collapse navbar-collapse navbar-expand-md\" [ngClass]=\"statusMenu ? 'show' : ''\" id=\"nav-open-btn\">\n        <ul class=\"nav nav-options\">\n          <li class=\"nav-item\"> <a routerLink=\"/vender\">Vendé tu camión</a> </li>\n          <li class=\"nav-item\"> <a routerLink=\"/comprar\">Comprá tu camión</a> </li>\n          <li class=\"nav-item\"> <a routerLink=\"/financiamiento\">Financiá tu camión</a></li>\n          <li class=\"nav-item\"> <a routerLink=\"/repuestos\">Repuestos</a> </li>\n          <li class=\"nav-item\"> <a routerLink=\"/nosotros\">Nosotros</a> </li>\n          <!--<li class=\"nav-item\"> <a routerLink=\"/contacto\">Contacto</a> </li>-->\n          <li class=\"nav-item\"> <a routerLink=\"/carrito\"><i class=\"icon-basket-loaded\" *ngIf=\"getCantidad() > 0 \"></i>\n            <i class=\"icon-basket\" *ngIf=\"getCantidad() == 0\"></i> Mi Cuenta</a> </li>\n\n          <!--<li class=\"nav-item\"> <a routerLink=\"/cotizador\">App DeckerCamiones</a></li>\n            <li class=\"nav-item\">\n              <a class=\"nav-link\"  (click)=\"clickEventCategorias()\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                Repuestos <i class=\"icon-arrow-down\"></i>\n              </a>\n              <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\"  [ngClass]=\"statusCategoria ? 'show' : '' \" >\n                <a *ngFor=\"let item of categorias\" routerLink=\"/categoria/{{item.type_id}}\"  class=\"dropdown-item\" href=\"#\">{{item.type_name}}</a>\n              </div>\n            </li>\n            <li class=\"nav-item\">\n              <a class=\"nav-link\" (click)=\"clickEventNosotros()\" id=\"navbarDropdownMenuLink\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                Nosotros <i class=\"icon-arrow-down\"></i>\n              </a>\n              <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\" >\n                <a routerLink=\"/sedes/\"  class=\"dropdown-item\" href=\"#\">Sedes</a>\n                <a routerLink=\"/historia/\"  class=\"dropdown-item\" href=\"#\">Historia</a>\n                <a routerLink=\"/contacto/\"  class=\"dropdown-item\" href=\"#\">Contacto</a>\n              </div>\n            </li>\n\n            <li class=\"nav-item carrito-responsive\">\n              <a routerLink=\"/carrito\">\n                <i class=\"icon-basket-loaded\" *ngIf=\"getCantidad() > 0 \"></i>\n                <i class=\"icon-basket\" *ngIf=\"getCantidad() == 0\"></i>\n                Ver carrito ({{ getCantidad() }})\n              </a>\n            </li>-->\n          </ul>\n        </div>\n      </nav>\n\n    </div>\n\n\n    </header>\n\n\n\n    <app-buscador></app-buscador>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/buy-truck/buy-truck.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/buy-truck/buy-truck.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1>TENEMOS LOS MEJORES PRECIOS DEL MERCADO</h1>\n\n\n\n<div class=\"container\">\n\n  <div class=\"row mejores\">\n\n    <div class=\"col-md-3 col-sm-6 col-6\">\n      <div class=\"card\">\n        <img src=\"./assets/images/valores2.png\" alt=\"\">\n        <p>REACONDICIONAMOS<BR>NUESTROS CAMIONES</p>\n      </div></div>\n\n\n      <div class=\"col-md-3 col-sm-6 col-6\">\n      <div class=\"card\">\n        <img src=\"./assets/images/valores1.png\" alt=\"\">\n        <p>SOMOS CONFIABLES<BR>LO QUE DECIMOS<BR>LO HACEMOS</p>\n      </div></div>\n\n      <div class=\"col-md-3 col-sm-6 col-6\">\n      <div class=\"card\">\n        <img src=\"./assets/images/valores3.png\" alt=\"\">\n        <p>SOMOS CONFIABLES<BR>LO QUE DECIMOS<BR>LO HACEMOS</p>\n      </div></div>\n\n      <div class=\"col-md-3 col-sm-6 col-6\">\n      <div class=\"card\">\n        <img src=\"./assets/images/valores4.png\" alt=\"\">\n        <p>SOMOS CONFIABLES<BR>LO QUE DECIMOS<BR>LO HACEMOS</p>\n      </div></div>\n\n\n\n    </div>\n  </div>\n\n\n\n\n<div class=\"button-yellow\">\n\n  <a href=\"\">Comprá un camión</a>\n\n</div>\n\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/buyorsell/buyorsell.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/buyorsell/buyorsell.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"buy-your-truck\">\n\n  <img src=\"./assets/images/camion.png\" alt=\"\">\n\n</section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/frontproducts/frontproducts.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/frontproducts/frontproducts.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("  <!-- Knowledge Share -->\n  <section class=\"testimonial padding-top-100 padding-bottom-50\">\n    <div class=\"container\"> \n      \n      <!-- Main Heading -->\n      <div class=\"heading text-center\">\n        <h4>USADOS DESTACADOS</h4>\n        <span>Todas las Marcas y Modelos encontralos en Decker Camiones ¡Contactate Hoy! Los Mejores Planes, Precios y Promociones los encontrás acá. Tenemos El Modelo Que Buscás.\n        </span> </div>\n      \n        \n\n          <div class=\"row padding-bottom-20\">\n\n            <div *ngFor=\"let d of destacados | slice:0:1;  let i = index \" class=\"col-md-12 col-sm-12 col-xs-12\">\n              <one-destacado [destacado]=\"destacados[0]\" ></one-destacado>\n            </div>\n\n          </div>\n        \n          <div class=\"papular-block block-slide\">\n            <div class=\"row\">\n              <div  *ngFor=\"let d of destacados | slice:1;  let i = index \" class=\"col-md-4 col-sm-4 col-xs-4 col-6\">\n                <one-destacado-card [destacado]=\"d\" ></one-destacado-card>\n              </div>\n            </div>\n          </div>\n          \n      \n          \n    </div>\n  </section>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home-seguro/home-seguro.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home-seguro/home-seguro.component.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"img-home-seguro\">\n    <a routerLink=\"/cotizador\">\n        <img src=\"../assets/img/seguro.png\" width=\"100%\" alt=\"\" /></a>\n</div>   \n    ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-slider></app-slider>\n<home-logos></home-logos>\n<div id=\"content\">\n\n  <buy-truck></buy-truck>\n  <!--<list-oferta [ofertas]=\"ofertas\"></list-oferta>-->\n  <!--<home-seguro></home-seguro>-->\n<!--<popular-products></popular-products>-->\n  <!--<list-categoria [categorias]=\"categorias\" [titulo]=\"tituloCategorias\"></list-categoria>-->\n  <!--<app-frontproducts></app-frontproducts>-->\n</div>\n<app-repuestos-mujer></app-repuestos-mujer>\n<app-buyorsell></app-buyorsell>\n<app-sedes></app-sedes>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/logos/logos.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/logos/logos.component.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"logos\">\n\n  <img src=\"./assets/images/logo-volvo.png\" alt=\"\">\n  <img src=\"./assets/images/logo-renaul.png\" alt=\"\">\n  <img src=\"./assets/images/logo-scania.png\" alt=\"\">\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/newsletter/newsletter.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/newsletter/newsletter.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"news-letter padding-top-150 padding-bottom-150\">\n  <div class=\"container\">\n\n    <div class=\"header-text\">\n      <h4>Brindamos financiamiento <br>\n      a <span>máxima velocidad</span>\n    </h4>\n    <p>Tenemos propuestas excelentes para todos los clientes.</p>\n    </div>\n\n      <form #signupForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"suscripcion(signupForm)\">\n        <div *ngIf=\" ! enviado \" class=\"inputs\" >\n          <input ngModel required type=\"email\" name=\"contact_email\" placeholder=\"Nombre\" >\n          <input ngModel required type=\"email\" name=\"contact_email\" placeholder=\"Correo electrónico\" >\n\n          <button type=\"submit\">Obtener financiamiento</button>\n        </div>\n      </form>\n\n      <p class=\"lema\">Garantizamos 100% su privacidad. Su información no será compartida.</p>\n      <div *ngIf=\" enviado \" >\n        <div class=\"alert alert-success\">Tus datos han sido enviados correctamente. Muchas gracias por inscribirte.</div>\n      </div>\n\n  </div>\n\n</section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/popularproducts/popularproducts.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/popularproducts/popularproducts.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("    <!-- Popular Products -->\n    <section class=\"padding-top-50 padding-bottom-150\">\n        <div class=\"container\">\n\n          <!-- Main Heading -->\n          <div class=\"heading text-center\">\n            <h4>Productos destacados</h4>\n            <span>¡Encontrá precios increíbles cada día!</span> </div>\n\n          <!-- Popular Item Slide -->\n          <div class=\"papular-block block-slide\">\n            <div class=\"row\">\n\n                <div class=\"col-md-3 col-sm-6 col-6\" *ngFor=\"let producto of productos\">\n                  <one-producto [producto]=\"producto\"></one-producto>\n                </div>\n\n\n            </div>\n\n\n\n\n          </div>\n        </div>\n      </section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/repuestos/repuestos.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/repuestos/repuestos.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"repuestos\">\n<img src=\"./assets/images/mujer.png\" alt=\"\">\n</section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/sedes/sedes.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/sedes/sedes.component.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"sedes\">\n      <img src=\"../../../assets/img/sedes.png\" alt=\"\">\n</section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/slider/slider.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/slider/slider.component.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--<section class=\"home-slider\">\n  <div class=\"tp-banner-container\">\n    <div class=\"tp-banner\">\n      <ul>\n        <li data-transition=\"random\" data-slotamount=\"7\" data-masterspeed=\"300\"  data-saveperformance=\"off\" >\n          <img src=\"./assets/images/slide-bg-1.jpg\" width=\"100%\"  alt=\"slider\"  data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\">\n        </li>\n      </ul>\n    </div>\n  </div>\n</section>-->\n\n<owl-carousel-o [options]=\"customOptions\">\n\n  <ng-template carouselSlide>\n      <div class=\"slide\">\n          <img src=\"./assets/images/header1.png\">\n      </div>\n  </ng-template>\n\n  <ng-template carouselSlide>\n      <div class=\"slide\">\n          <img src=\"./assets/images/header1.png\">\n      </div>\n  </ng-template>\n\n</owl-carousel-o>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/list-categoria/list-categoria.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/list-categoria/list-categoria.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n <!-- Testimonial -->\n <section class=\"light-gray-bg padding-top-150 padding-bottom-150\">\n\n  <div class=\"container\">\n\n    <!-- Main Heading -->\n    <div class=\"heading text-center\">\n      <h4>{{titulo}}</h4>\n      <span>Encontrá el producto que buscás dentro de estas categorías</span>\n    </div>\n\n    <!-- Categories -->\n    <div class=\"papular-block\">\n      <ul class=\"row\">\n         <li class=\"col-sm-3 col-4 col-md-2\" *ngFor=\"let categoria of categorias\">\n            <one-categoria [categoria]=\"categoria\"></one-categoria>\n         </li>\n      </ul>\n    </div>\n  </div>\n</section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/list-oferta/list-oferta.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/list-oferta/list-oferta.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"shop-page padding-top-100 padding-bottom-100\">\n  <div class=\"container\">\n\n        <div class=\"heading text-center\">\n          <h4>Ofertas del mes</h4>\n          <span>Descubrí todos los meses las ofertas, promociones y descuentos especiales que tenemos para vos. Comprá online y aprovechá los beneficios.</span> \n        </div>\n\n      <div class=\"papular-block row\">\n        <div class=\"col-6 col-sm-6 col-md-3 \" *ngFor=\"let oferta of ofertas\">\n            <one-oferta [oferta]=\"oferta\"></one-oferta>\n        </div>\n      </div>\n\n  </div>\n    </section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/nosotros/nosotros.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nosotros/nosotros.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fluid\"><br>\n    <h1>DECKER</h1>\n    <h3>UNA FAMILIA DEDICADA AL SERVICIO DEL TRANSPORTE</h3>\n    <div class=\"row\">\n        <div class=\"col-6\">\n            <p>ROBERTO DECKER, UN TRABAJADOR INCANSABLE</p>\n            <p>Roberto Decker fue una de esas personas que se puede decir que nació trabajando. Cuando tenía 8 años empezó en una fábrica de baterías. Luego junto a su hermano Rubén y su papá hicieron un equipo al que ningún camionero superaba en cargar y descargar camiones, ellos con un camioncito viejo entraban a los campos llenos de barro a los que nadie se animaba a entrar por miedo a quedarse encajado… pero el trío Decker allí estaba..</p>\n            <p>Consiguieron cambiar su camioncito viejo por el famoso amuleto de Roberto… el guerrero que todavía conservamos. Se fue en búsqueda de nuevos horizontes, pasó por Bahía Blanca, llegó a La Pampa hasta que volvió a Necochea por el año 1968 con el dinero para comprar el terreno y hacer un pequeño galpón para abrir la tan ansiada gomería en la entrada a la ciudad.</p>\n        </div>\n        <div class=\"col-6\">\n            <img src=\"https://decker.ar/wp-content/uploads/2015/12/Composicion.png\" alt=\"\" width=\"100%\">\n        </div>\n        <div class=\"col-12\">\n            <h2>DESDE EL VIEJO GUERRERO A DECKER CAMIONES</h2>\n            <p>Roberto fue una de esas personas que se puede decir que nacio trabajando. Cuando tenia 8 años empezó a trabajar en una fábrica de baterías. “Baterias Corbaz” en Necochea. Estuvo allí hasta su adolescencia que junto a su hermano Rubén y su papa hicieron un equipo al que ningún camionero superaba en cargar y descargar camiones, ellos con un camioncito viejo entraba a los campos llenos de barro a los que nadie se animaba a entrar por miedo a quedarse encajado… pero el trio Decker allí estaba, fue así que los propietarios los contrataban.</p>\n            <p>\n                Consiguieron cambiar su camioncito viejo por el famoso amuleto de Roberto… el guerrero que todavía conservamos y que es parte de nuestra familia, el que no se vende a ningún precio. Se fue en búsqueda de nuevos horizontes, pasó por Bahía Blanca y llegó a La Pampa donde se quedó tres años hasta que juntó el dinero suficiente para poder cumplir su sueño y el de su hermano… Tener su negocio propio.\n            </p>\n            <p>\n                Volvió a Necochea por el año 1968 con el dinero para comprar el terreno y hacer un pequeño galpón para abrir la tan ansiada gomería en la entrada a la ciudad. Era famosa porque estaba abierta las 24 hs. Todos los dueños de los campos a los que ellos les habían trabajado hacía un par de años fueron sus clientes, les compraban las gomas nuevas, arreglaban las de los tractores y estaban llenos de trabajo, hacían recapados, vulcanizados y hasta inventaron una correo para una maquina transportadora de piedra para una cantera de la ciudad de Batán. Trabajaban en familia y le daban trabajo a la gente del barrio.</p>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/oferta/oferta.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/oferta/oferta.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<header-breadcump \ntitulo='{{oferta?.offer_name}}' \ndescripcion=\"{{oferta?.offer_description}}\" \nbg=\"assets/img/bg0.png\">\n</header-breadcump>\n\n\n<section class=\"shop-page padding-top-50 padding-bottom-50\">\n    <div class=\"container\">\n      <div class=\"papular-block\">\n          <list-productos [productos]=\"productos\"></list-productos>\n      </div>\n\n  <!--<div #moreElementsDiv class=\"moreelements text-center\">\n    <button *ngIf=\"getMore\"  (click)=\"loadmore()\" class=\"btn\">Cargas más</button>\n  </div>-->\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/ofertas/ofertas.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/ofertas/ofertas.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header-breadcump \ntitulo='Ofertas' \ndescripcion=\"Encontrá aquí todas las ofertas de este mes.\" \nbg=\"assets/img/bg1.png\">\n</header-breadcump>\n\n\n\n<section class=\"shop-page padding-top-50 padding-bottom-50\">\n  <div class=\"container\">\n\n        <div class=\"heading text-center\">\n          <span>Descubrí todos los meses las ofertas, promociones y descuentos especiales que tenemos para vos. Comprá online y aprovechá los beneficios.</span> \n        </div>\n\n      <div class=\"papular-block row\">\n        <div class=\"col-6 col-sm-6 col-md-3 \" *ngFor=\"let oferta of ofertas\">\n            <one-oferta [oferta]=\"oferta\"></one-oferta>\n        </div>\n      </div>\n\n  </div>\n    </section>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/producto/description/description.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/producto/description/description.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"item-decribe\">\n    \n    <ul class=\"nav nav-tabs animate fadeInUp\" data-wow-delay=\"0.4s\" role=\"tablist\">\n      <li role=\"presentation\" class=\"active\"><a>DESCRIPCIÓN</a></li>\n    </ul>\n\n    \n    <div class=\"tab-content animate fadeInUp\" data-wow-delay=\"0.4s\">\n    \n      <div role=\"tabpanel\" class=\"tab-pane fade in active\" id=\"descr\">\n        <p>{{ descripcion }}</p>\n      </div>\n\n    </div>\n  </div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/producto/detalle/detalle.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/producto/detalle/detalle.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\n  detalle works!\n</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/producto/producto.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/producto/producto.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<section class=\"padding-top-50 padding-bottom-50\">\n\n  <div class=\"container-fluid\">\n    <div class=\"shop-detail\">\n        <div class=\"row\">\n\n\n          <div class=\"col-md-8 col-sm-12\">\n            <app-galery [imagenes]=\"imagenes\"></app-galery>\n          </div>\n\n\n          <div class=\"col-md-4  col-sm-12 \">\n            <h4>{{producto?.replacement_name}}</h4>\n              <p class=\"anio\">{{producto?.brand_name}}</p>\n\n            <div class=\"price\"><span>Precio:</span> ${{getOldPrice()}}.-</div>\n\n\n\n\n            <div class=\"text-left\">\n                <ul>\n                  <li class=\"col-12\">Cantidad:\n                    <select [(ngModel)]=\"cantSelected\" class=\"myselect\">\n                      <option *ngFor=\"let c of [1,2,3,4,5,6,7,8,9,10]\" [ngValue]=\"c\">{{c}}</option>\n                    </select>\n                  </li>\n                  <li class=\"col-12\"><button type=\"button\" (click)=\"addToCart()\" class=\"btn-black\">Agregar al carrito</button> </li>\n                  <li class=\"col-12\"><a href=\"/carrito\" class=\"\">Ir al carrito</a> </li>\n                </ul>\n              </div>\n\n              <div *ngIf=\"showMessage\" id=\"contact_message\" class=\"alert alert-success\"> <i class=\"fa fa-paper-plane-o\"></i> Producto agregado al carrito.</div>\n\n            </div>\n          </div>\n        </div>\n      </div>\n  </section>\n\n\n\n<section class=\"caracteristicas\">\n\n  <div class=\"container-fluid\">\n      <h3>Características</h3>\n\n    <h4>General</h4>\n    <div class=\"row\">\n      <div class=\"col-md-12 col-sm-12\" >\n        <p class=\"info\"><span>Descripcion:</span><span>{{ producto?.replacement_description }}</span></p>\n\n\n        <p class=\"info\"><span>Modelo</span><span>{{ producto.replacement_model}}</span></p>\n        <p class=\"info\"><span>Código:</span><span>{{ producto?.replacement_part_number}}</span></p>\n        <p class=\"info\"><span>Altura:</span><span>{{ producto?.replacement_height}}</span></p>\n        <p class=\"info\"><span>Ancho:</span><span>{{ producto?.replacement_width}}</span></p>\n        <p class=\"info\"><span>Profundidad:</span><span>{{ producto?.replacement_depth}}</span></p>\n        <p class=\"info\"><span>Diámetro:</span><span>{{ producto?.replacement_diameter}}</span></p>\n        <p class=\"info\"><span>Color:</span><span>{{ producto?.replacement_color}}</span></p>\n\n\n      </div>\n    </div>\n\n\n\n  </div>\n\n\n\n</section>\n\n\n  <!--<section *ngIf=\"producto?.replacement_stock == 0\" class=\"contact padding-top-100 padding-bottom-100\">\n    <div class=\"container\">\n      <div class=\"contact-form\">\n        <div class=\"row\">\n          <h3>No contamos con stock online de este producto por el momento</h3>\n          <h5>Dejanos tus datos y nos contactaremos en breve para informarte sobre la disponibilidad de este producto en alguna de nuestras agencias</h5>\n          <p>Pieza: {{producto?.replacement_code}} - {{producto?.replacement_name}} </p>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-md-8\">\n\n            <div *ngIf=\" enviado \" >\n              <div id=\"contact_message\" class=\"success-msg\"> <i class=\"fa fa-paper-plane-o\"></i>Gracias por confiar en nosotros. Su mensaje ha sido enviado correctamente.</div>\n            </div>\n\n            <form #signupForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"contacto(signupForm)\">\n              <div  *ngIf=\" ! enviado \" >\n                <ul class=\"row\">\n                  <li class=\"col-sm-6\">\n                    <label>Nombre Completo\n                      <input type=\"hidden\" ngModel name=\"contact_type\" id=\"contact_type\"  value=\"Contacto\">\n                      <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_name' type=\"text\" class=\"form-control\" ngModel required name=\"contact_name\" id=\"contact_name\" placeholder=\"\">\n                      <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel required name=\"contact_name\" id=\"contact_name\" placeholder=\"\">\n                    </label>\n                  </li>\n                  <li class=\"col-sm-6\">\n                    <label>Email\n                      <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_email' type=\"text\" class=\"form-control\" ngModel name=\"contact_email\" id=\"contact_email\" placeholder=\"\">\n                      <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel name=\"contact_email\" id=\"contact_email\" placeholder=\"\">\n                    </label>\n                  </li>\n                  <li class=\"col-sm-6\">\n                    <label>Teléfono\n                      <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_phone' type=\"text\" class=\"form-control\" ngModel required name=\"contact_phone\" id=\"contact_phone\" placeholder=\"\">\n                      <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel required name=\"contact_phone\" id=\"contact_phone\" placeholder=\"\">\n                    </label>\n                  </li>\n                  <li class=\"col-sm-6\">\n                    <label>Ciudad\n                      <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_city' type=\"text\" class=\"form-control\" ngModel name=\"contact_city\" id=\"contact_city\" placeholder=\"\">\n                      <input *ngIf=\"!infoClient\" type=\"text\" class=\"form-control\" ngModel name=\"contact_city\" id=\"contact_city\" placeholder=\"\">\n                    </label>\n                  </li>\n\n                  <input type=\"hidden\" class=\"form-control\" name=\"contact_msj\" ngModel  id=\"contact_msj\"  placeholder=\"\">\n\n                  <li class=\"col-sm-12\">\n                    <button type=\"submit\" value=\"Enviar Email\" class=\"btn\" id=\"btn_submit\">ENVIAR EMAIL</button>\n                  </li>\n                </ul>\n              </div>\n            </form>\n          </div>\n\n\n          <div class=\"col-md-4\">\n            <div class=\"contact-info\">\n              <h6>NUESTRA DIRECCIÓN</h6>\n              <ul>\n                <li> <i class=\"icon-map-pin\"></i> Camino Parque Sesquicentenario<br> RN3 Km 696<br>\n                  B8000 Bahía Blanca, <br>Buenos Aires - Argentina</li>\n                <li> <i class=\"icon-call-end\"></i> 0291 454-7043</li>\n                <li> <i class=\"icon-envelope\"></i> <a href=\"mailto:info@mercadocamiones.com.ar\" target=\"_top\">info@mercadocamiones.com.ar</a> </li>\n              </ul>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>-->\n\n <!-- <list-oferta [ofertas]=\"ofertas\"></list-oferta>-->\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/repuestos/filtro-repuestos/filtro-repuestos.component.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/repuestos/filtro-repuestos/filtro-repuestos.component.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<filtro-select [datos]=\"categorias\" [label]=\"'Categorias'\" [type]=\"'type'\" (filtrar)=\"addItem($event)\" ></filtro-select>\n<filtro-select [datos]=\"marcas\" [label]=\"'Marcas'\" [type]=\"'brand'\" (filtrar)=\"addItem($event)\" ></filtro-select>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/repuestos/list-productos/list-productos.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/repuestos/list-productos/list-productos.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"row\">\n  <div [ngClass]=\"showFiltros ? 'col-xl-4' : 'col-xl-3' \"  class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 col-12\" *ngFor=\"let producto of productos\">    \n    <one-producto [producto]=\"producto\"></one-producto>\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/repuestos/repuestos.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/repuestos/repuestos.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fluid\"><br>\n  <p>REPUESTOS</p>\n\n\n  <div class=\"row\">\n    <div class=\"col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12\">\n        <p class=\"showhidefilter\" *ngIf=\"showFiltros\" (click)=\"showFiltros = !showFiltros\" >Ocultar Filtros</p>\n        <p class=\"showhidefilter\" *ngIf=\"!showFiltros\" (click)=\"showFiltros = !showFiltros\">Mostrar Filtros</p>\n        <filtro-repuestos *ngIf=\"showFiltros\" (buscar)=\"obtenerProductos($event)\"></filtro-repuestos>\n      </div>\n      <div [ngClass]=\"showFiltros ? 'col-xl-9 col-lg-9 col-md-9 col-sm-9 col-12' : 'col-12'\">\n        <filtro-elegidos [filtros]=\"filtros\" (remove)=\"removeFilter($event)\"  ></filtro-elegidos>\n\n        <div class=\"papular-block\" *ngIf=\"productos.length > 0; else more\">\n          <list-productos [showFiltros]=\"showFiltros\" [productos]=\"productos\"></list-productos>\n        </div>\n\n        <ng-template #more>\n          <div class=\"text-center\">\n            <h2 >¿No encontras el producto que estas buscando?</h2>\n            <h4 >Seguro lo tenemos</h4>\n            <p >Dejano un mensaje en el siguiente link con el producto que necesitas y nos comunicaremos cuanto antes.</p>\n            <p ><a href=\"/contacto\" class=\"btn btn-outline\">Contacto</a></p>\n          </div>\n        </ng-template>\n\n      </div>\n\n  </div>\n\n</div>\n\n\n<div #moreElementsDiv class=\"col-12 moreelements text-right\">\n  <div class=\"container-fluid\">\n  <button *ngIf=\"getMore\"  (click)=\"loadmore()\" class=\"btn-load-more\">Siguiente ></button>\n  </div>\n</div>\n\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rutaclub/mispuntos/mispuntos.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rutaclub/mispuntos/mispuntos.component.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\n  mispuntos works!\n</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rutaclub/rutaclub.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rutaclub/rutaclub.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"sub-bnr\" data-stellar-background-ratio=\"0.5\" style=\"background-position: 0% -69px; background-image: url(assets/img/rutaclub.png);\">\n  <div class=\"position-center-center\">\n    <div class=\"container\">\n<!--      <h4>RUTA CLUB</h4>\n      <ol class=\"breadcrumb\">\n        <li><a href=\"#\">Home</a></li>\n        <li class=\"active\">Data</li>\n      </ol>-->\n    </div>\n  </div>\n</section>\n\n<section class=\"testimonial padding-top-100\">\n  <div class=\"container\"> \n    \n    <!-- Main Heading -->\n    <div class=\"heading text-center\">\n      <h4>RUTA CLUB</h4>\n      <p class=\"subtitle\">El club de beneficios de Ruta Sur Trucks que te da puntos por tus compras <br>y podes canjearlos por descuentos en productos y servicios.</p> \n    </div>\n    <div class=\"knowledge-share\">\n      \n      \n\n\n    </div>\n  </div>\n</section>\n\n<section class=\"testimonial \">\n  <div class=\"container\"> \n    \n    \n    <div class=\"body-rutaclub padding-bottom-50 \">\n\n\n      <h4>¿Cómo funciona?</h4>\n\n      <p>El usuario de RUTACLUB, puede consultar los descuentos vigentes en los productos en el sitio web de Mercado Camiones o en nuestras distitas agencias, luego al momento de comprar debe presentar la tarjeta de RUTACLUB y su DOCUMENTO DE IDENTIDAD para su validación. Nosotros aplicaremos el beneficio, descuento o promoción al asociado de RUTACLUB.</p>\n     \n     <h4>¿Cómo obtengo mi tarjeta?</h4>\n     \n     <p>La Tarjeta se obtiene solicitandola con la compra de productos en nuestras agencias o con la compra de nuestras unidades o enviando un mail a rutaclub@mecadocamiones.com.ar</p>\n     \n     <h4> ¿Cuánto sale?</h4>\n     \n     <p>La Tarjeta de RUTACLUB no tiene costo adicional, es un beneficio incluido para todos nuestros clientes. </p>\n       \n\n    </div>\n\n\n  </div>\n</section>\n\n\n\n\n<section class=\"testimonial padding-top-100 yellow-box\">\n  <div class=\"container\"> \n    \n    <!-- Main Heading -->\n    <div class=\"heading text-center\">\n      <h4>Consultá tus puntos</h4>\n      <span>Ingresá el numero de socio que figura en tu tarjeta para saber cuantos puntos llevas acumulados.</span> \n    </div>\n    \n    <div align=\"center\">\n    <input type=\"text\" class=\"buscador\" [(ngModel)]=\"idSocio\" placeholder=\"Nº de Socio\">\n      <button (click)=\"agregar()\">Agregar</button>\n\n      <button (click)=\"consultar()\">Consultar</button>\n      <button (click)=\"consultarId()\">Consultar x ID</button>\n    </div>  \n\n    <div *ngIf=\"consultado\" >\n      <h3>Cantidad de puntos: {{ puntos }}</h3>\n    </div> \n\n      <br>\n   \n  </div>\n</section>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/servicios/servicios.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/servicios/servicios.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("  <header-breadcump\ntitulo='Servicios'\ndescripcion=\"Agendá con nosotros tu próximo servicio preventivo.\"\nbg=\"bg1.png\">\n</header-breadcump>\n\n<div id=\"content\">\n\n<section class=\"chart-page padding-top-20 padding-bottom-20\">\n    <form #signupForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"contacto(signupForm)\">\n      <div *ngIf=\" ! enviado \" >\n\n      <div class=\"container\">\n        <!-- Payments Steps -->\n        <div class=\"shopping-cart\">\n\n          <div class=\"cart-ship-info\">\n            <div class=\"\">\n              <!-- ESTIMATE SHIPPING & TAX -->\n              <div class=\"\">\n                <h6>DATOS DEL CONTACTO</h6>\n                  <ul class=\"row\">\n                    <!-- Name -->\n                    <li class=\"col-md-6\">\n                      <label> *NOMBRE Y APELLIDO\n                        <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_name' ngModel required type=\"text\" name=\"service_name\" value=\"\" placeholder=\"\">\n                        <input *ngIf=\"!infoClient\" ngModel required type=\"text\" name=\"service_name\" value=\"\" placeholder=\"\">\n                      </label>\n                    </li>\n                    <li class=\"col-md-6\">\n                      <!-- ADDRESS -->\n                      <label>*CIUDAD\n                        <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_city' ngModel type=\"text\" name=\"service_city\" value=\"\" placeholder=\"\">\n                        <input *ngIf=\"!infoClient\" ngModel type=\"text\" name=\"service_city\" value=\"\" placeholder=\"\">\n                      </label>\n                    </li>\n\n                    <!-- PHONE -->\n                    <li class=\"col-md-6\">\n                      <label> *TELEFONO\n                        <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_phone' ngModel required type=\"text\" name=\"service_phone\" value=\"\" placeholder=\"\">\n                        <input *ngIf=\"!infoClient\" ngModel required type=\"text\" name=\"service_phone\" value=\"\" placeholder=\"\">\n                      </label>\n                    </li>\n\n                    <!-- EMAIL ADDRESS -->\n                    <li class=\"col-md-6\">\n                      <label> DIRECCIÓN DE EMAIL\n                        <input *ngIf=\"infoClient\" [(ngModel)]='infoClient.contact_email' ngModel type=\"text\" name=\"service_email\" value=\"\" placeholder=\"\">\n                        <input *ngIf=\"!infoClient\" ngModel type=\"text\" name=\"service_email\" value=\"\" placeholder=\"\">\n                      </label>\n                    </li>\n                  </ul>\n              </div>\n            </div>\n          </div>\n\n\n          <div class=\"cart-ship-info\">\n            <div class=\"\">\n              <!-- ESTIMATE SHIPPING & TAX -->\n              <div class=\"\">\n                <h6>DATOS DEL VEHÍCULO</h6>\n                  <ul class=\"row\">\n                    <!-- Name -->\n                    <li class=\"col-md-6\">\n                      <label> *CHASIS\n                        <input ngModel type=\"text\" name=\"service_chassis\" value=\"\" placeholder=\"\">\n                      </label>\n                    </li>\n                    <li class=\"col-md-6\">\n                      <!-- ADDRESS -->\n                      <label>*PATENTE\n                        <input ngModel required type=\"text\" name=\"service_patent\" value=\"\" placeholder=\"\">\n                      </label>\n                    </li>\n\n                    <!-- PHONE -->\n                    <li class=\"col-md-6\">\n                      <label> *KILOMETROS\n                        <input ngModel type=\"text\" name=\"service_km\" value=\"\" placeholder=\"\">\n                      </label>\n                    </li>\n\n                    <!-- EMAIL ADDRESS -->\n                    <li class=\"col-md-6\">\n                      <label> EMPRESA\n                        <input ngModel type=\"text\" name=\"service_business\" value=\"\" placeholder=\"\">\n                      </label>\n                    </li>\n                  </ul>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"cart-ship-info\">\n            <div class=\"\">\n              <div class=\"\">\n                <h6>TIPO DE SERVICIO</h6>\n                <ul class=\"row\">\n                    <li class=\"col-md-12\">\n                    <label> TIPO DE SERVICIO\n                        <input ngModel required type=\"text\" name=\"service_service\" value=\"\" placeholder=\"\">\n                    </label>\n                    </li>\n                    <li class=\"col-sm-12\">\n                        <label>DESCRIBA EL SERVICIO REQUERIDO\n                        <textarea class=\"form-control\" name=\"service_description\" ngModel required id=\"descripcion\" rows=\"5\" placeholder=\"\"></textarea>\n                        </label>\n                    </li>\n                </ul>\n            </div>\n        </div></div>\n\n\n        </div>\n      </div>\n      <div class=\"container padding-top-50 padding-bottom-50\">\n        <div class=\"text-right\">\n          <button type=\"submit\" class=\"btn btn-primary\">Finalizar</button>\n        </div>\n      </div>\n      </div>\n    </form>\n    <div *ngIf=\" enviado \" class=\"margin-top-50\" >\n\n      <div id=\"contact_message\" class=\"success-msg\"> <i class=\"fa fa-paper-plane-o\"></i>Gracias por confiar en nosotros. Su mensaje ha sido enviado correctamente.</div>\n\n  </div>\n  </section>\n  </div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-categoria/one-categoria.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-categoria/one-categoria.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"item\">\n    <a routerLink=\"/categoria/{{categoria.type_id}}\">\n        <img src=\"{{categoria.type_image}}\" class=\"item-img\" alt=\"\">\n    </a>\n\n    <div class=\"price\">\n        <a routerLink=\"/categoria/{{categoria.type_id}}\">\n        {{categoria.type_name}}\n        </a>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-destacado-card/one-destacado-card.component.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-destacado-card/one-destacado-card.component.html ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"item\" routerLink=\"/usado/{{destacado.vehicle_id}}\">\n    <div class=\"img\">\n      <img src=\"{{ destacado.vehicle_thumbnail_url }}\" alt=\"\">\n   </div>\n\n\n    <div class=\"descipcion\">\n       <h2>{{ destacado.brand_name}} {{ destacado.vehicle_model}}</h2>\n       <p>\n          <span>{{destacado.vehicle_year}}</span>\n          <span *ngIf=\" destacado.vehicle_year &&  destacado.vehicle_km\"> - </span>\n          <span *ngIf=\"destacado.vehicle_km\">{{destacado.vehicle_km}} km</span>\n          <span *ngIf=\" (destacado.company_name &&  destacado.vehicle_km) || (destacado.vehicle_year &&  destacado.company_name)  \"> - </span>\n          <span>{{ destacado.company_name}}</span>\n      </p>\n    </div>\n    <div class=\"price\">\n\n          Ver unidad\n\n     </div>\n     <div>\n       <img src=\"./assets/images/fondo-one.png\" alt=\"\">\n     </div>\n </div>\n\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-destacado/one-destacado.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-destacado/one-destacado.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div class=\"item-usado\" style=\"background-image: url({{ destacado.vehicle_thumbnail_url | replaceThumbForOriginal : 'original' }});\">\n\n    <div class=\"item-usado-titulo\">\n        <div class=\"item-img-anio\">\n            <img src=\"/assets/img/decker.png\" alt=\"\">\n            <div>\n                {{destacado.brand_name | uppercase }}<br> {{destacado.vehicle_model}}\n            </div>\n        </div>\n        <div class=\"price\">{{destacado.vehicle_km}}</div>\n        <div class=\"item-info\">\n            <p>Año: {{destacado.vehicle_year}}</p>\n            <p>Km: {{destacado.vehicle_km}}</p>\n            <p>Tracción: {{destacado.vehicle_traction}} </p>\n        </div>\n\n        \n        <a routerLink=\"/usado/{{destacado.vehicle_id}}\" class=\"btn-mas-info\">\n            Conocelo\n        </a>\n        \n    </div> \n\n\n </div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-oferta/one-oferta.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-oferta/one-oferta.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"item\">\n   <div class=\"item-img bgw\"> \n\n<img *ngIf=\"oferta.offer_image_url != '0' \" class=\"img-1\" src=\"{{oferta.offer_image_url}}\" alt=\"\">\n<img *ngIf=\"oferta.offer_image_url != '0' \" class=\"img-2\" src=\"{{oferta.offer_image_url}}\" alt=\"\">\n\n<img *ngIf=\"oferta.offer_image_url == '' \" class=\"img-1\" src=\"./assets/img/oferta.png\" alt=\"\">\n<img *ngIf=\"oferta.offer_image_url == '' \" class=\"img-2\" src=\"./assets/img/oferta.png\" alt=\"\">\n\n<img *ngIf=\"oferta.offer_image_url == '0' \" class=\"img-1\" src=\"./assets/img/oferta.png\" alt=\"\">\n<img *ngIf=\"oferta.offer_image_url == '0' \" class=\"img-2\" src=\"./assets/img/oferta.png\" alt=\"\">\n\n\n   <div class=\"overlay\">\n\n    <div class=\"offer\">{{oferta.offer_percentage}}%</div>\n    \n   \n\n      <p class=\"descripcion-overlay\">{{oferta.offer_description}}</p>\n\n      <div class=\"position-center-center\">\n         <div class=\"inn\">\n            <a  routerLink=\"/oferta/{{oferta.offer_id}}\" data-lighter>\n            <i class=\"icon-magnifier\"></i> Ver oferta</a>   \n         </div>\n\n       </div>\n\n\n    </div>\n\n   </div>\n    <div class=\"price\">\n         <a routerLink=\"/oferta/{{oferta.offer_id}}\" >{{oferta.offer_name}}</a>\n     \n    </div>\n</div>\n\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-producto/one-producto.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-producto/one-producto.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n\n<div class=\"item\" routerLink=\"/producto/{{producto.replacement_id}}\">\n  <div class=\"img\">\n    <img src=\"{{ producto.replacement_thumbnail_url }}\" alt=\"\">\n  </div>\n\n\n  <div class=\"descipcion\">\n     <h2>{{ producto.replacement_name}}</h2>\n     <p>\n        <span>{{producto.replacement_state}}</span>\n        <span *ngIf=\" producto.replacement_state\"> - </span>\n        <span *ngIf=\"producto.brand_name\">{{producto.brand_name}}</span>\n        <span *ngIf=\" (producto.type_name) || (producto.replacement_state &&  producto.type_name)  \"> - </span>\n        <span>{{ producto.type_name}}</span>\n    </p>\n  </div>\n  <div class=\"price\">\n\n    ${{getOldPrice()}}\n\n   </div>\n   <div>\n     <img src=\"./assets/images/fondo-one.png\" alt=\"\">\n   </div>\n</div>\n\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/terminos/terminos.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/terminos/terminos.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<section class=\"contact padding-top-100 padding-bottom-100\">\n    <div class=\"container\">\n\n\n<h1>TÉRMINOS Y CONDICIONES</h1>\n\n\n<ul>\n\n<li>\n<h2>1. Información General</h2>\n\n<p>Estos Términos y Condiciones de pedido (en adelante, los “Términos y Condiciones”) se aplican\n     a toda orden de pedido de un cliente (en adelante, el “Cliente”) efectuado a MERCADO CAMIONES (en adelante, “MC”) \n     en el sitio web URL: http://www.mercadocamiones.com.ar/ (en adelante, la “Web Volvo Repuestos”).</p>\n\n<p>Por navegar en la Web Mercado Camiones y/o efectuar una orden de pedido, \n    el Cliente acepta irrevocablemente los Términos y Condiciones.\n     Es responsabilidad del Cliente leer y comprender los Términos \n     y Condiciones antes de efectuar una orden de pedido. MC tiene el \n     derecho de realizar cambios a estos Términos y Condiciones de tanto\n      en tanto sin necesidad de efectuar una notificación al Cliente. \n      Los Términos y Condiciones son los aplicables a la orden de pedido\n       efectuado por el Cliente al momento de realizar el mismo.</p>\n\n<p>Toda disputa respecto de los Términos y Condiciones\n     será definida bajo las leyes de la República Argentina \n     y ante los Tribunales de la Ciudad Autónoma de Buenos Aires.</p>\n\n\n</li>\n<li>\n<h2>2. Costos y órdenes de pedido</h2>\n<p>Los precios de los productos que se encuentran en la Web Mercado Camiones \n    no incluyen ningún impuesto que pudiera ser \n    aplicable (incluyendo, pero no limitando, IVA, a \n    derechos de importación, tasas aduaneras o portuarias, etc.) ni costos de entregas.</p>\n\n\n\n</li>\n<li>\n<h2>3. Entrega.</h2>\n<p>Los productos seleccionados deberán ser retirados por la red de Concesionarios RUTA SUR TRUCKS S.A. y\n     Talleres Oficiales de RUTASURTRUCKS en la República Argentina o acuerdo de envio con los mismos. \n     Al momento de realizar la orden de pedido, \n     el Cliente deberá seleccionar el punto de retiro de los productos seleccionados.\n      El punto de retiro no podrá ser luego modificado por el Cliente. \n      Una vez confirmado el pedido por MC, el mismo deberá ser retirado \n      por el punto de entrega confirmado según la confirmación de la orden \n      de pedido del Cliente. MC realizará sus mayores esfuerzos para que la\n       entrega se efectúe dentro de los 5 (cinco) días hábiles de enviada la \n       confirmación de la orden de pedido.</p>\n\n\n</li>\n<li>\n<h2>4. Modalidad de pago.</h2>\n<p>El pago de los productos se realizará en el momento de retiro de los mismo, \n    en la red de Concesionarios y Talleres Oficiales de RUTA SUR TRUCKS en la República Argentina.</p>\n\n</li>\n<li>\n<h2>5. Cancelación de órdenes de pedido.</h2>\n<p>El Cliente sólo podrá cancelar una orden de pedido, contactándose \n    con el Concesionario o Taller Oficial de RUTA SUR TRUCKS en la República Argentina \n    que fue seleccionado para efectuar el retiro, \n    con el número de orden de pedido. \n    Podrán encontrarse los teléfonos de cada Concesionario y \n    Taller Oficial en la República Argentina en www.rutasurtrucks.com.ar</p>\n\n</li>\n<li>\n<h2>6. Reproducciones en la Web Mercado Camiones.</h2>\n<p>Las imágenes de los productos que se encuentran en la Web Mercado \n    Camiones podrían no representar el producto en detalle.\n     MC no se responsabiliza por las potenciales diferencias entre los\n      colores mostrados en la Web Mercado Camiones en la computadora \n      del Cliente y los reales colores del producto.</p>\n\n</li>\n<li>\n<h2>7. Responsabilidad.</h2>\n<p>MC bajo ninguna circunstancia será responsable con respecto al Cliente o a \n    cualquier tercero por daños directos, indirectos o pérdidas, pudieran \n    o no haberse previsto, o por pérdidas de ganancias, pérdidas de ahorros\n     o consecuentes daños derivados del uso del producto por el Cliente o por\n      la inhabilidad del Cliente para usar el producto o causados por un incumplimiento \n      de cualquier contrato por parte del Cliente, aun cuando MC pudiera haber sido \n      informada del riesgo de daños o pérdidas de este tipo.</p>\n\n</li>\n<li>\n<h2>8. Propiedad intelectual</h2>\n<p>Todo el contenido de la Web Mercado Camiones, incluyendo todas las marcas,\n     derechos de autor y los derechos de propiedad intelectual relativas a los productos,\n      pertenece a Volvo Trademark Holding AB, AB Volvo, una subsidiaria de AB Volvo o un proveedor de MC.\n       En consecuencia, el contenido está protegido por la legislación aplicable en la materia.\n        Esto significa que marcas, nombres de empresas, nombres de productos e información acerca de productos, \n        tales como descripciones de mercancías, pesos, ilustraciones, cuadros, gráficos, diseños, diseño\n         y otros contenidos de estas páginas, no pueden bajo ninguna circunstancia ser descargados, \n         copiados o utilizados sin el consentimiento escrito explícito de MC.</p>\n\n<p>El Cliente acepta abstenerse de: (i) compartir nombres de usuarios, \n    contraseñas u otra información de acceso que permita el ingreso a las porciones protegidas \n    de la Web Mercado Camiones por parte de un tercero no autorizado; (ii) publicar, distribuir \n    o transmitir publicidad o materiales promocionales no autorizados, correos en cadena, correo \n    no deseado o cualquier otro tipo de correo electrónico masivo no solicitado, a individuos o \n    entidades que no han convenido formar parte de dicha correspondencia; (iii) omitir, eliminar, \n    falsificar o falsear información sobre transmisiones, incluyendo encabezados, correo devuelto \n    y direcciones de protocolo de Internet, o bien, de cualquier otra forma manipular los identificadores\n     para ocultar el origen de cualquier contenido; (iv) hacerse pasar por cualquier individuo o entidad,\n      o bien, declarar falsamente o de cualquier otra forma falsear una afiliación con cualquier individuo \n      o entidad, u omitir, eliminar, falsificar o falsear intencionalmente, información sobre transmisiones,\n       incluyendo encabezados, correo devuelto y direcciones de protocolo de Internet, o bien, de cualquier \n       otra forma manipular los identificadores para ocultar el origen de cualquier contenido transmitido a \n       o por medio de la Web Mercado Camiones; y (v) distribuir o transmitir cualquier contenido sin derecho \n       a hacerlo; (vi) enviar mensajes de acoso y/o de amenaza a otros.</p>\n\n</li>\n<li>\n<h2>9. Comportamiento Ilegal</h2>\n<p>El Cliente acepta no: (i) utilizar la Web Mercado Camiones para cualquier fin ilícito, \n    en violación a las leyes y regulaciones aplicables; (ii) publicar páginas web que contengan vínculos \n    que inicien descargas de material en franca infracción de propiedad intelectual o que sea ilícito; (iii) \n    acosar u hostigar de manera electrónica a otra persona; (iv) participar en cualquier actividad de Internet\n     que viole los derechos de privacidad de otras personas, incluyendo sin limitación, recabar y distribuir \n     información sobre usuarios de Internet sin su autorización, con excepción de lo que permita la ley, o (v)\n      realizar rifas, sorteos o concursos por Internet que violen las leyes aplicables.</p>\n\n</li>\n<li>\n<h2>10. Declaración de Limitación de Responsabilidad</h2>\n<p>El Cliente acepta expresamente que el uso de la Web Mercado Camiones es bajo propio riesgo. \n    MC no garantiza que el uso de la Web Mercado Camiones será sin interrupciones o estará libre de errores.\n    MC tiene el derecho de modificar, eliminar y/o dejar sin efecto en cualquier momento cualquier ofrecimiento\n     de productos de la Web Mercado Camiones a su exclusiva discreción. MC no garantiza la certeza, integridad \n     o la exhaustividad del contenido presentado en la Web Mercado Camiones o de los productos allí ofrecidos. \n     MC no es responsable bajo circunstancia alguna de los daños y perjuicios directos, indirectos, incidentales, \n     especiales o consecuenciales, causados por el uso o inhabilidad para usar la Web Mercado Camiones, incluyendo \n     sin limitación, que el Cliente se apoye en cualquier información obtenida de la Web Mercado Camiones que resulten \n     ser errores, omisiones, interrupciones, supresiones o corrupción de archivos, virus, retraso de operaciones o \n     transmisiones, o bien, cualquier incumplimiento. La anterior limitación de responsabilidad aplicará en caso de \n     ejercicio de acciones derivadas del Contrato, por negligencia u otra responsabilidad, incluso cuando un \n     representante autorizado de MC ha sido asesorado respecto a o debiera tener conocimiento sobre la posibilidad \n     de dichos daños y perjuicios.</p>\n    </li>\n</ul>\n\n\n    </div></section>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/usado/usado.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/usado/usado.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--<header-breadcump\ntitulo=\"{{ usado?.brand_name }} {{ usado?.vehicle_model }}\"\ndescripcion=\"Año: {{ usado?.vehicle_year }}\"\nbg=\"{{ usado.vehicle_thumbnail_url | replaceThumbForOriginal }}\" >\n</header-breadcump>-->\n\n\n<section class=\"padding-top-50 padding-bottom-50\">\n    <div class=\"container-fluid\">\n\n      <!-- SHOP DETAIL -->\n      <div class=\"shop-detail\">\n        <div class=\"row\">\n\n          <!-- Popular Images Slider -->\n          <div class=\"col-lg-8 col-md-8 col-sm-12 \">\n            <app-galery [imagenes]=\"imagenes\"></app-galery>\n          </div>\n\n          <!-- COntent -->\n          <div class=\"col-md-4  col-sm-12 \">\n            <h4>{{ usado?.brand_name }} {{ usado?.vehicle_model }}</h4>\n\n\n\n\n              <p class=\"anio\">{{usado?.vehicle_year}} - {{usado?.vehicle_km}} km</p>\n\n\n            <div class=\"price\"><span>Precio:</span> ${{usado?.vehicle_km}}.-</div>\n            <div class=\"text-left\">\n              <button type=\"button\"  routerLink=\"/contacto/{{usado?.vehicle_id}}\" class=\"btn-black\">¡Estoy interesado!</button>\n            </div>\n          </div>\n        </div>\n      </div>\n\n\n\n  </div>\n</section>\n\n\n\n<section class=\"caracteristicas\">\n\n  <div class=\"container-fluid\">\n      <h3>Características</h3>\n\n    <h4>General</h4>\n    <div class=\"row\">\n      <div class=\"col-md-4 col-sm-12\" >\n        <p class=\"info\"><span>Combustible</span><span>{{ usado?.vehicle_fuel}}</span></p>\n        <p class=\"info\"><span>Motor</span><span>{{ usado?.vehicle_motor}}</span></p>\n        <p class=\"info\"><span>Tracción</span><span>{{ usado?.vehicle_power}}</span></p>\n\n      </div>\n      <div class=\"col-md-4 col-sm-12\" >\n        <p class=\"info\"><span>Tracción</span><span>{{ usado?.vehicle_traction}}</span></p>\n        <p class=\"info\"><span>Dirección</span><span>{{ usado?.vehicle_direction}}</span></p>\n\n      </div>\n      <div class=\"col-md-4 col-sm-12\" >\n        <p class=\"info\"><span>Agencia</span><span>{{ usado?.company_name}}</span></p>\n        <p class=\"info\"><span>Ubicación</span><span>{{ usado?.company_address}}</span></p>\n\n\n      </div>\n    </div>\n\n    <h4>Exterior</h4>\n    <div class=\"row\">\n      <div class=\"col-md-4 col-sm-12\" >\n        <p class=\"info\"><span>Largo</span><span>{{ usado?.vehicle_long}}</span></p>\n      </div>\n      <div class=\"col-md-4 col-sm-12\" >\n        <p class=\"info\"><span>Color</span><span>{{ usado?.vehicle_color}}</span></p>\n      </div>\n      <div class=\"col-md-4 col-sm-12\" >\n        <p class=\"info\"><span>Puertas</span><span>{{ usado?.vehicle_puertas }}</span></p>\n      </div>\n    </div>\n\n    <h4>Seguridad</h4>\n    <div class=\"row\">\n      <div class=\"col-md-4 col-sm-12\" >\n        <p class=\"info\"><span>Frenos</span><span>{{ usado?.vehicle_brake}}</span></p>\n      </div>\n      <div class=\"col-md-4 col-sm-12\" >\n        <p class=\"info\"><span>Airbag</span><span>{{ usado?.vehicle_airbag }}</span></p>\n      </div>\n    </div>\n\n\n    <h4>Equipamiento</h4>\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n\n          <p class=\"info\"><span>{{ usado?.vehicle_equipment}}</span></p>\n\n\n\n      </div>\n    </div>\n\n  </div>\n\n\n\n</section>\n\n\n<!--<hr>\n\n<section class=\"contact padding-top-50 padding-bottom-50\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-md-6\">\n        <h1>Consultá por tu financiamiento en 2 minutos</h1>\n        <button type=\"button\" routerLink=\"/financiamiento\" class=\"btn-black\">Aplicar a financiamiento</button>\n      </div>\n      <div class=\"col-md-6\">\n        <img src=\"./assets/images/red.png\" width=\"100%\" alt=\"\">\n      </div>\n    </div>\n  </div>\n</section>-->\n\n<!--\n<list-oferta [ofertas]=\"ofertas\"></list-oferta>\n-->\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/usados/filtro-usados/filtro-usados.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/usados/filtro-usados/filtro-usados.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<filtro-select [datos]=\"categorias\" [label]=\"'Categorias'\" [type]=\"'type'\" ></filtro-select>\n<filtro-select [datos]=\"marcas\" [label]=\"'Marcas'\" [type]=\"'brand'\" ></filtro-select>\n<filtro-select [datos]=\"colores\" [label]=\"'Colores'\" [type]=\"'color'\" ></filtro-select>\n<filtro-select [datos]=\"largo\" [label]=\"'Largo'\" [type]=\"'long'\" ></filtro-select>\n<filtro-select [datos]=\"anios\" [label]=\"'Año'\" [type]=\"'year'\" ></filtro-select>\n<filtro-select [datos]=\"modelos\" [label]=\"'Modelo'\" [type]=\"'model'\" ></filtro-select>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/usados/lista-usados/lista-usados.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/usados/lista-usados/lista-usados.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("  <div class=\"row\">\n    <div [ngClass]=\"showFiltros ? 'col-xl-4' : 'col-xl-3' \"  class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 col-12\" *ngFor=\"let destacado of datos\">\n      <one-destacado-card [destacado]=\"destacado\"></one-destacado-card>\n    </div>\n  </div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/usados/usados.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/usados/usados.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n  <div class=\"container-fluid\"><br>\n      <p>UNIDADES DESTACADAS</p>\n      <div class=\"row\">\n\n        <div class=\"col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12\">\n          <p class=\"showhidefilter\" *ngIf=\"showFiltros\" (click)=\"showFiltros = !showFiltros\" >Ocultar Filtros</p>\n          <p class=\"showhidefilter\" *ngIf=\"!showFiltros\" (click)=\"showFiltros = !showFiltros\">Mostrar Filtros</p>\n\n          <filtro-usados *ngIf=\"showFiltros\"></filtro-usados>\n        </div>\n        <div class=\"\" [ngClass]=\"showFiltros ? 'col-xl-9 col-lg-9 col-md-9 col-sm-9 col-12' : 'col-12'     \"   >\n          <filtro-elegidos [filtros]=\"filtros\"></filtro-elegidos>\n          <div class=\"papular-block\" *ngIf=\"usados.length > 0; else more\">\n            <lista-usados [datos]=\"usados\" [showFiltros]=\"showFiltros\"></lista-usados>\n          </div>\n          <ng-template #more>\n            <div class=\"text-center\">\n              <h2 >¿No encontras el producto que estas buscando?</h2>\n              <h4 >Seguro lo tenemos</h4>\n              <p >Dejano un mensaje en el siguiente link con el producto que necesitas y nos comunicaremos cuanto antes.</p>\n              <p ><a href=\"/contacto\" class=\"btn btn-outline\">Contacto</a></p>\n            </div>\n          </ng-template>\n\n\n        </div>\n\n      </div>\n\n    </div>\n\n\n      <div #moreElementsDiv class=\"col-12 moreelements text-right\">\n        <div class=\"container-fluid\">\n\n        <button *ngIf=\"getMore\"  (click)=\"loadmore()\" class=\"btn-load-more\">Siguiente ></button>\n        </div>\n      </div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/vender/vender.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/vender/vender.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<img src=\"assets/images/vende.png\" width=\"100%\" alt=\"\">\n\n\n\n<h2>\n  Completá tus datos para que podamos enviarte la oferta:\n</h2>\n\n\n\n<section class=\"chart-page  padding-bottom-20\">\n  <form [formGroup]=\"myForm\" (ngSubmit)=\"enviarCamion()\">\n    <div class=\"container\">\n      <!-- Payments Steps -->\n      <div class=\"shopping-cart\">\n\n        <div class=\"cart-ship-info\">\n          <div class=\"\">\n            <!-- ESTIMATE SHIPPING & TAX -->\n            <div class=\"\">\n              <h6>Tus datos de contacto</h6>\n                <ul class=\"row\">\n                  <!-- formControlName -->\n                  <li class=\"col-md-4\">\n                    <label>\n                      <input  type=\"text\" formControlName=\"nombre\" id=\"nombre\" value=\"\" placeholder=\"Nombre completo * \"  >\n                    </label>\n                  </li>\n                  <li class=\"col-md-4\">\n                    <!-- ADDRESS -->\n                    <label>\n                      <input  type=\"text\" formControlName=\"email\" id=\"email\" value=\"\" placeholder=\"Email * \"  >\n                    </label>\n                  </li>\n\n                  <!-- PHONE -->\n                  <li class=\"col-md-4\">\n                    <label>\n                      <input  type=\"text\" formControlName=\"telefono\" id=\"telefono\" value=\"\" placeholder=\"Teléfono * \"  >\n                    </label>\n                  </li>\n                </ul>\n\n                <h6>Tu usado</h6>\n\n                <ul class=\"row\">\n                  <!-- formControlName -->\n                  <li class=\"col-md-4\">\n                    <label>\n                      <input type=\"text\" formControlName=\"marca\" id=\"marca\" value=\"\" placeholder=\"Marca y modelo * \"  >\n                    </label>\n                  </li>\n                  <li class=\"col-md-4\">\n                    <!-- ADDRESS -->\n                    <label>\n                      <input  type=\"text\" formControlName=\"anio\" id=\"anio\" value=\"\" placeholder=\"Año * \"  >\n                    </label>\n                  </li>\n\n                  <!-- PHONE -->\n                  <li class=\"col-md-4\">\n                    <label>\n                      <input  type=\"text\" formControlName=\"kilometros\" id=\"kilometros\" value=\"\" placeholder=\"Kilómetros\"  >\n                    </label>\n                  </li>\n                  <li class=\"col-md-12\">\n                    <label>\n                      <input  type=\"text\" formControlName=\"observaciones\" id=\"observaciones\" value=\"\" placeholder=\"Observaciones\"  >\n                    </label>\n                  </li>\n\n                </ul>\n\n\n            </div>\n          </div>\n        </div>\n\n        <!-- SHOPPING INFORMATION -->\n        <div class=\"cart-ship-info\">\n          <div class=\"row\">\n\n            <div class=\"col-md-1\"></div>\n            <div class=\"col-md-3\">\n\n              <h3>No es obligatorio,\n                pero tener las fotos\n                de tu unidad\n                nos ayuda a tener\n                mayor detalle\n                </h3>\n                <h4 >Clickeá en cada campo y envianos la foto sugerida.</h4>\n\n            </div>\n            <div class=\"col-md-1\"></div>\n            <div class=\"col-md-6\">\n\n              <div class=\"camiones\">\n                <div class=\"col\">\n                  <div>\n                    <img src=\"assets/images/lado2.png\"  alt=\"\">\n                    <label class=\"custom-file-upload\" for=\"foto2\" [ngClass]=\"foto2 ? 'selected' : '' \">\n                      <input type=\"file\" id=\"foto2\" aria-label=\"2\" formControlName=\"foto2\" (change)=\"onFileChange($event)\" />\n                      LATERAL IZQ <span>Subir</span>\n                    </label>\n                  </div>\n                  <div>\n                    <img src=\"assets/images/lado3.png\"  alt=\"\" >\n                    <label class=\"custom-file-upload\" for=\"foto3\" [ngClass]=\"foto3 ? 'selected' : '' \">\n                      <input type=\"file\" id=\"foto3\" aria-label=\"3\" formControlName=\"foto3\" (change)=\"onFileChange($event)\" />\n                      FRENTE <span>Subir</span>\n                    </label>\n\n                  </div>\n                </div>\n                <div class=\"col\">\n                  <div>\n                    <img src=\"assets/images/lado1.png\"  alt=\"\">\n                    <label class=\"custom-file-upload\" for=\"foto1\" [ngClass]=\"foto1 ? 'selected' : '' \">\n                      <input type=\"file\" id=\"foto1\" aria-label=\"1\" formControlName=\"foto1\" (change)=\"onFileChange($event)\" />\n                      LATERAL DER <span>Subir</span>\n                    </label>\n                  </div>\n                  <div>\n                    <img src=\"assets/images/lado4.png\"  alt=\"\">\n                    <label class=\"custom-file-upload\" for=\"foto4\" [ngClass]=\"foto4 ? 'selected' : '' \">\n                      <input type=\"file\" id=\"foto4\" aria-label=\"4\" formControlName=\"foto4\"  (change)=\"onFileChange($event)\" />\n                      ATRÁS <span>Subir</span>\n                    </label>\n                  </div>\n                </div>\n\n              </div>\n\n\n            </div>\n            <div class=\"col-md-1\"></div>\n\n\n          </div>\n        </div>\n\n\n\n\n\n      </div>\n    </div>\n    <div class=\"container padding-top-50 padding-bottom-50\">\n      <div class=\"text-right\">\n\n        <button type=\"submit\" [disabled]=\"!status()\" class=\"btn btn-primary\">Enviar</button>\n        <p *ngIf=\"!status()\">Complete los campos requeridos</p>\n      </div>\n    </div>\n  </form>\n</section>\n");

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ofertas_ofertas_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ofertas/ofertas.component */ "./src/app/ofertas/ofertas.component.ts");
/* harmony import */ var _usados_usados_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./usados/usados.component */ "./src/app/usados/usados.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm5/router.js");
/* harmony import */ var _oferta_oferta_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./oferta/oferta.component */ "./src/app/oferta/oferta.component.ts");
/* harmony import */ var _categoria_categoria_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./categoria/categoria.component */ "./src/app/categoria/categoria.component.ts");
/* harmony import */ var _producto_producto_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./producto/producto.component */ "./src/app/producto/producto.component.ts");
/* harmony import */ var _rutaclub_rutaclub_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./rutaclub/rutaclub.component */ "./src/app/rutaclub/rutaclub.component.ts");
/* harmony import */ var _cotizador_cotizador_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./cotizador/cotizador.component */ "./src/app/cotizador/cotizador.component.ts");
/* harmony import */ var _contacto_contacto_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./contacto/contacto.component */ "./src/app/contacto/contacto.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _busqueda_busqueda_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./busqueda/busqueda.component */ "./src/app/busqueda/busqueda.component.ts");
/* harmony import */ var _carro_compra_carro_compra_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./carro-compra/carro-compra.component */ "./src/app/carro-compra/carro-compra.component.ts");
/* harmony import */ var _usado_usado_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./usado/usado.component */ "./src/app/usado/usado.component.ts");
/* harmony import */ var _servicios_servicios_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./servicios/servicios.component */ "./src/app/servicios/servicios.component.ts");
/* harmony import */ var _terminos_terminos_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./terminos/terminos.component */ "./src/app/terminos/terminos.component.ts");
/* harmony import */ var _financiamiento_financiamiento_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./financiamiento/financiamiento.component */ "./src/app/financiamiento/financiamiento.component.ts");
/* harmony import */ var _vender_vender_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./vender/vender.component */ "./src/app/vender/vender.component.ts");
/* harmony import */ var _repuestos_repuestos_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./repuestos/repuestos.component */ "./src/app/repuestos/repuestos.component.ts");
/* harmony import */ var _nosotros_nosotros_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./nosotros/nosotros.component */ "./src/app/nosotros/nosotros.component.ts");





















var routes = [
    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"] },
    { path: 'oferta/:id', component: _oferta_oferta_component__WEBPACK_IMPORTED_MODULE_5__["OfertaComponent"] },
    { path: 'categoria', component: _categoria_categoria_component__WEBPACK_IMPORTED_MODULE_6__["CategoriaComponent"] },
    { path: 'categoria/:id', component: _categoria_categoria_component__WEBPACK_IMPORTED_MODULE_6__["CategoriaComponent"] },
    { path: 'producto/:id', component: _producto_producto_component__WEBPACK_IMPORTED_MODULE_7__["ProductoComponent"] },
    { path: 'rutaclub', component: _rutaclub_rutaclub_component__WEBPACK_IMPORTED_MODULE_8__["RutaclubComponent"] },
    { path: 'cotizador', component: _cotizador_cotizador_component__WEBPACK_IMPORTED_MODULE_9__["CotizadorComponent"] },
    { path: 'seguros', component: _cotizador_cotizador_component__WEBPACK_IMPORTED_MODULE_9__["CotizadorComponent"] },
    { path: 'contacto', component: _contacto_contacto_component__WEBPACK_IMPORTED_MODULE_10__["ContactoComponent"] },
    { path: 'contacto/:id', component: _contacto_contacto_component__WEBPACK_IMPORTED_MODULE_10__["ContactoComponent"] },
    { path: 'busqueda/', component: _busqueda_busqueda_component__WEBPACK_IMPORTED_MODULE_12__["BusquedaComponent"] },
    { path: 'carrito', component: _carro_compra_carro_compra_component__WEBPACK_IMPORTED_MODULE_13__["CarroCompraComponent"] },
    { path: 'usado/:id', component: _usado_usado_component__WEBPACK_IMPORTED_MODULE_14__["UsadoComponent"] },
    { path: 'comprar', component: _usados_usados_component__WEBPACK_IMPORTED_MODULE_2__["UsadosComponent"] },
    { path: 'servicios', component: _servicios_servicios_component__WEBPACK_IMPORTED_MODULE_15__["ServiciosComponent"] },
    { path: 'ofertas', component: _ofertas_ofertas_component__WEBPACK_IMPORTED_MODULE_1__["OfertasComponent"] },
    { path: 'terminos', component: _terminos_terminos_component__WEBPACK_IMPORTED_MODULE_16__["TerminosComponent"] },
    { path: 'financiamiento', component: _financiamiento_financiamiento_component__WEBPACK_IMPORTED_MODULE_17__["FinanciamientoComponent"] },
    { path: 'vender', component: _vender_vender_component__WEBPACK_IMPORTED_MODULE_18__["VenderComponent"] },
    { path: 'repuestos', component: _repuestos_repuestos_component__WEBPACK_IMPORTED_MODULE_19__["RepuestosComponent"] },
    { path: 'nosotros', component: _nosotros_nosotros_component__WEBPACK_IMPORTED_MODULE_20__["NosotrosComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot(routes, {
                    scrollPositionRestoration: 'enabled',
                    onSameUrlNavigation: 'reload'
                })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'mercado-camiones';
    }
    AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm5/animations.js");
/* harmony import */ var ngx_owl_carousel_o__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-owl-carousel-o */ "./node_modules/ngx-owl-carousel-o/__ivy_ngcc__/fesm5/ngx-owl-carousel-o.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _home_slider_slider_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./home/slider/slider.component */ "./src/app/home/slider/slider.component.ts");
/* harmony import */ var _home_frontproducts_frontproducts_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./home/frontproducts/frontproducts.component */ "./src/app/home/frontproducts/frontproducts.component.ts");
/* harmony import */ var _home_popularproducts_popularproducts_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./home/popularproducts/popularproducts.component */ "./src/app/home/popularproducts/popularproducts.component.ts");
/* harmony import */ var _common_galery_galery_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./common/galery/galery.component */ "./src/app/common/galery/galery.component.ts");
/* harmony import */ var _producto_description_description_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./producto/description/description.component */ "./src/app/producto/description/description.component.ts");
/* harmony import */ var _categoria_categoria_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./categoria/categoria.component */ "./src/app/categoria/categoria.component.ts");
/* harmony import */ var _oferta_oferta_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./oferta/oferta.component */ "./src/app/oferta/oferta.component.ts");
/* harmony import */ var _list_categoria_list_categoria_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./list-categoria/list-categoria.component */ "./src/app/list-categoria/list-categoria.component.ts");
/* harmony import */ var _list_oferta_list_oferta_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./list-oferta/list-oferta.component */ "./src/app/list-oferta/list-oferta.component.ts");
/* harmony import */ var _home_home_seguro_home_seguro_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./home/home-seguro/home-seguro.component */ "./src/app/home/home-seguro/home-seguro.component.ts");
/* harmony import */ var _producto_detalle_detalle_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./producto/detalle/detalle.component */ "./src/app/producto/detalle/detalle.component.ts");
/* harmony import */ var _repuestos_list_productos_list_productos_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./repuestos/list-productos/list-productos.component */ "./src/app/repuestos/list-productos/list-productos.component.ts");
/* harmony import */ var _singles_one_oferta_one_oferta_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./singles/one-oferta/one-oferta.component */ "./src/app/singles/one-oferta/one-oferta.component.ts");
/* harmony import */ var _singles_one_categoria_one_categoria_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./singles/one-categoria/one-categoria.component */ "./src/app/singles/one-categoria/one-categoria.component.ts");
/* harmony import */ var _singles_one_producto_one_producto_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./singles/one-producto/one-producto.component */ "./src/app/singles/one-producto/one-producto.component.ts");
/* harmony import */ var _singles_one_destacado_one_destacado_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./singles/one-destacado/one-destacado.component */ "./src/app/singles/one-destacado/one-destacado.component.ts");
/* harmony import */ var _home_newsletter_newsletter_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./home/newsletter/newsletter.component */ "./src/app/home/newsletter/newsletter.component.ts");
/* harmony import */ var _rutaclub_rutaclub_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./rutaclub/rutaclub.component */ "./src/app/rutaclub/rutaclub.component.ts");
/* harmony import */ var _rutaclub_mispuntos_mispuntos_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./rutaclub/mispuntos/mispuntos.component */ "./src/app/rutaclub/mispuntos/mispuntos.component.ts");
/* harmony import */ var _cotizador_cotizador_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./cotizador/cotizador.component */ "./src/app/cotizador/cotizador.component.ts");
/* harmony import */ var _cotizador_vehiculo_vehiculo_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./cotizador/vehiculo/vehiculo.component */ "./src/app/cotizador/vehiculo/vehiculo.component.ts");
/* harmony import */ var _cotizador_ajuste_ajuste_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./cotizador/ajuste/ajuste.component */ "./src/app/cotizador/ajuste/ajuste.component.ts");
/* harmony import */ var _cotizador_cotizaciones_cotizaciones_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./cotizador/cotizaciones/cotizaciones.component */ "./src/app/cotizador/cotizaciones/cotizaciones.component.ts");
/* harmony import */ var _cotizador_solicitud_solicitud_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./cotizador/solicitud/solicitud.component */ "./src/app/cotizador/solicitud/solicitud.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _producto_producto_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./producto/producto.component */ "./src/app/producto/producto.component.ts");
/* harmony import */ var _contacto_contacto_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./contacto/contacto.component */ "./src/app/contacto/contacto.component.ts");
/* harmony import */ var _header_buscador_buscador_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./header/buscador/buscador.component */ "./src/app/header/buscador/buscador.component.ts");
/* harmony import */ var _busqueda_busqueda_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./busqueda/busqueda.component */ "./src/app/busqueda/busqueda.component.ts");
/* harmony import */ var _carro_compra_carro_compra_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./carro-compra/carro-compra.component */ "./src/app/carro-compra/carro-compra.component.ts");
/* harmony import */ var _usado_usado_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./usado/usado.component */ "./src/app/usado/usado.component.ts");
/* harmony import */ var _usados_usados_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./usados/usados.component */ "./src/app/usados/usados.component.ts");
/* harmony import */ var _header_breadcump_header_breadcump_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./header-breadcump/header-breadcump.component */ "./src/app/header-breadcump/header-breadcump.component.ts");
/* harmony import */ var _servicios_servicios_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./servicios/servicios.component */ "./src/app/servicios/servicios.component.ts");
/* harmony import */ var _ofertas_ofertas_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./ofertas/ofertas.component */ "./src/app/ofertas/ofertas.component.ts");
/* harmony import */ var _terminos_terminos_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./terminos/terminos.component */ "./src/app/terminos/terminos.component.ts");
/* harmony import */ var _pipes_replace_thumb_for_original_pipe__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./pipes/replace-thumb-for-original.pipe */ "./src/app/pipes/replace-thumb-for-original.pipe.ts");
/* harmony import */ var _singles_one_destacado_card_one_destacado_card_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./singles/one-destacado-card/one-destacado-card.component */ "./src/app/singles/one-destacado-card/one-destacado-card.component.ts");
/* harmony import */ var _home_logos_logos_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./home/logos/logos.component */ "./src/app/home/logos/logos.component.ts");
/* harmony import */ var _home_buy_truck_buy_truck_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./home/buy-truck/buy-truck.component */ "./src/app/home/buy-truck/buy-truck.component.ts");
/* harmony import */ var _home_buyorsell_buyorsell_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./home/buyorsell/buyorsell.component */ "./src/app/home/buyorsell/buyorsell.component.ts");
/* harmony import */ var _home_sedes_sedes_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./home/sedes/sedes.component */ "./src/app/home/sedes/sedes.component.ts");
/* harmony import */ var _home_repuestos_repuestos_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./home/repuestos/repuestos.component */ "./src/app/home/repuestos/repuestos.component.ts");
/* harmony import */ var _repuestos_repuestos_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./repuestos/repuestos.component */ "./src/app/repuestos/repuestos.component.ts");
/* harmony import */ var _financiamiento_financiamiento_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./financiamiento/financiamiento.component */ "./src/app/financiamiento/financiamiento.component.ts");
/* harmony import */ var _vender_vender_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./vender/vender.component */ "./src/app/vender/vender.component.ts");
/* harmony import */ var _repuestos_filtro_repuestos_filtro_repuestos_component__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./repuestos/filtro-repuestos/filtro-repuestos.component */ "./src/app/repuestos/filtro-repuestos/filtro-repuestos.component.ts");
/* harmony import */ var _common_filtro_select_filtro_select_component__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./common/filtro-select/filtro-select.component */ "./src/app/common/filtro-select/filtro-select.component.ts");
/* harmony import */ var _common_filtro_elegidos_filtro_elegidos_component__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./common/filtro-elegidos/filtro-elegidos.component */ "./src/app/common/filtro-elegidos/filtro-elegidos.component.ts");
/* harmony import */ var _usados_filtro_usados_filtro_usados_component__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ./usados/filtro-usados/filtro-usados.component */ "./src/app/usados/filtro-usados/filtro-usados.component.ts");
/* harmony import */ var _usados_lista_usados_lista_usados_component__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./usados/lista-usados/lista-usados.component */ "./src/app/usados/lista-usados/lista-usados.component.ts");
/* harmony import */ var _nosotros_nosotros_component__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! ./nosotros/nosotros.component */ "./src/app/nosotros/nosotros.component.ts");







//import { OwlCarouselDemoComponent } from './components/owl-carousel-demo/owl-carousel-demo.component';

























































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__["FooterComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_11__["HeaderComponent"],
                _home_slider_slider_component__WEBPACK_IMPORTED_MODULE_12__["SliderComponent"],
                _home_frontproducts_frontproducts_component__WEBPACK_IMPORTED_MODULE_13__["FrontproductsComponent"],
                _home_popularproducts_popularproducts_component__WEBPACK_IMPORTED_MODULE_14__["PopularproductsComponent"],
                _common_galery_galery_component__WEBPACK_IMPORTED_MODULE_15__["GaleryComponent"],
                _producto_description_description_component__WEBPACK_IMPORTED_MODULE_16__["DescriptionComponent"],
                _categoria_categoria_component__WEBPACK_IMPORTED_MODULE_17__["CategoriaComponent"],
                _oferta_oferta_component__WEBPACK_IMPORTED_MODULE_18__["OfertaComponent"],
                _list_categoria_list_categoria_component__WEBPACK_IMPORTED_MODULE_19__["ListCategoriaComponent"],
                _list_oferta_list_oferta_component__WEBPACK_IMPORTED_MODULE_20__["ListOfertaComponent"],
                _home_home_seguro_home_seguro_component__WEBPACK_IMPORTED_MODULE_21__["HomeSeguroComponent"],
                _producto_detalle_detalle_component__WEBPACK_IMPORTED_MODULE_22__["DetalleComponent"],
                _repuestos_list_productos_list_productos_component__WEBPACK_IMPORTED_MODULE_23__["ListProductosComponent"],
                _singles_one_oferta_one_oferta_component__WEBPACK_IMPORTED_MODULE_24__["OneOfertaComponent"],
                _singles_one_categoria_one_categoria_component__WEBPACK_IMPORTED_MODULE_25__["OneCategoriaComponent"],
                _singles_one_producto_one_producto_component__WEBPACK_IMPORTED_MODULE_26__["OneProductoComponent"],
                _singles_one_destacado_one_destacado_component__WEBPACK_IMPORTED_MODULE_27__["OneDestacadoComponent"],
                _home_newsletter_newsletter_component__WEBPACK_IMPORTED_MODULE_28__["NewsletterComponent"],
                _rutaclub_rutaclub_component__WEBPACK_IMPORTED_MODULE_29__["RutaclubComponent"],
                _rutaclub_mispuntos_mispuntos_component__WEBPACK_IMPORTED_MODULE_30__["MispuntosComponent"],
                _cotizador_cotizador_component__WEBPACK_IMPORTED_MODULE_31__["CotizadorComponent"],
                _cotizador_vehiculo_vehiculo_component__WEBPACK_IMPORTED_MODULE_32__["VehiculoComponent"],
                _cotizador_ajuste_ajuste_component__WEBPACK_IMPORTED_MODULE_33__["AjusteComponent"],
                _cotizador_cotizaciones_cotizaciones_component__WEBPACK_IMPORTED_MODULE_34__["CotizacionesComponent"],
                _cotizador_solicitud_solicitud_component__WEBPACK_IMPORTED_MODULE_35__["SolicitudComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_36__["HomeComponent"],
                _producto_producto_component__WEBPACK_IMPORTED_MODULE_37__["ProductoComponent"],
                _contacto_contacto_component__WEBPACK_IMPORTED_MODULE_38__["ContactoComponent"],
                _header_buscador_buscador_component__WEBPACK_IMPORTED_MODULE_39__["BuscadorComponent"],
                _busqueda_busqueda_component__WEBPACK_IMPORTED_MODULE_40__["BusquedaComponent"],
                _carro_compra_carro_compra_component__WEBPACK_IMPORTED_MODULE_41__["CarroCompraComponent"],
                _usado_usado_component__WEBPACK_IMPORTED_MODULE_42__["UsadoComponent"],
                _usados_usados_component__WEBPACK_IMPORTED_MODULE_43__["UsadosComponent"],
                _header_breadcump_header_breadcump_component__WEBPACK_IMPORTED_MODULE_44__["HeaderBreadcumpComponent"],
                _servicios_servicios_component__WEBPACK_IMPORTED_MODULE_45__["ServiciosComponent"],
                _ofertas_ofertas_component__WEBPACK_IMPORTED_MODULE_46__["OfertasComponent"],
                _terminos_terminos_component__WEBPACK_IMPORTED_MODULE_47__["TerminosComponent"],
                _pipes_replace_thumb_for_original_pipe__WEBPACK_IMPORTED_MODULE_48__["ReplaceThumbForOriginalPipe"],
                _singles_one_destacado_card_one_destacado_card_component__WEBPACK_IMPORTED_MODULE_49__["OneDestacadoCardComponent"],
                _repuestos_repuestos_component__WEBPACK_IMPORTED_MODULE_55__["RepuestosComponent"],
                _home_logos_logos_component__WEBPACK_IMPORTED_MODULE_50__["LogosComponent"],
                _home_buy_truck_buy_truck_component__WEBPACK_IMPORTED_MODULE_51__["BuyTruckComponent"],
                _home_buyorsell_buyorsell_component__WEBPACK_IMPORTED_MODULE_52__["BuyorsellComponent"],
                _home_sedes_sedes_component__WEBPACK_IMPORTED_MODULE_53__["SedesComponent"],
                _financiamiento_financiamiento_component__WEBPACK_IMPORTED_MODULE_56__["FinanciamientoComponent"],
                _vender_vender_component__WEBPACK_IMPORTED_MODULE_57__["VenderComponent"],
                _home_repuestos_repuestos_component__WEBPACK_IMPORTED_MODULE_54__["RepuestosMujerComponent"],
                _repuestos_filtro_repuestos_filtro_repuestos_component__WEBPACK_IMPORTED_MODULE_58__["FiltroRepuestosComponent"],
                _common_filtro_select_filtro_select_component__WEBPACK_IMPORTED_MODULE_59__["FiltroSelectComponent"],
                _common_filtro_elegidos_filtro_elegidos_component__WEBPACK_IMPORTED_MODULE_60__["FiltroElegidosComponent"],
                _usados_filtro_usados_filtro_usados_component__WEBPACK_IMPORTED_MODULE_61__["FiltroUsadosComponent"],
                _usados_lista_usados_lista_usados_component__WEBPACK_IMPORTED_MODULE_62__["ListaUsadosComponent"],
                _nosotros_nosotros_component__WEBPACK_IMPORTED_MODULE_63__["NosotrosComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"],
                ngx_owl_carousel_o__WEBPACK_IMPORTED_MODULE_8__["CarouselModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/busqueda/busqueda.component.scss":
/*!**************************************************!*\
  !*** ./src/app/busqueda/busqueda.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2J1c3F1ZWRhL2J1c3F1ZWRhLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/busqueda/busqueda.component.ts":
/*!************************************************!*\
  !*** ./src/app/busqueda/busqueda.component.ts ***!
  \************************************************/
/*! exports provided: BusquedaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusquedaComponent", function() { return BusquedaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm5/router.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");




var BusquedaComponent = /** @class */ (function () {
    function BusquedaComponent(rutaActiva, replacementService, router) {
        this.rutaActiva = rutaActiva;
        this.replacementService = replacementService;
        this.router = router;
        this.calls = 0;
        this.offset = 0;
        this.cantidad = 24;
        this.productos = [];
        this.getMore = true;
        this.loading = false;
        this.nextBreakpoint = 0;
        this.heigthDiv = 0;
    }
    BusquedaComponent.prototype.handleScroll = function (event) {
        if (this.nextBreakpoint == 0) {
            this.nextBreakpoint = parseInt(this.moreElementsDiv.nativeElement.offsetTop);
            this.heigthDiv = parseInt(this.allElements.nativeElement.offsetHeight);
        }
        var positionScroll = parseInt(document.documentElement.scrollTop.toFixed(0));
        var viewportHeight = parseInt(window.innerWidth.toFixed(0));
        if ((viewportHeight + positionScroll) > this.nextBreakpoint) {
            this.loadmore();
            this.nextBreakpoint = this.nextBreakpoint + this.heigthDiv;
        }
    };
    BusquedaComponent.prototype.loadmore = function () {
        this.loading = true;
        if (this.getMore && !this.loading) {
            this.offset = this.offset + this.cantidad;
            this.obtenerResultado();
        }
    };
    BusquedaComponent.prototype.obtenerResultado = function () {
        var _this = this;
        this.calls++;
        console.log(this.calls + '. Next bp: ' + this.nextBreakpoint);
        this.replacementService.getSearch(this.stringParam, this.cantidad, this.offset).subscribe(function (response) {
            var _a;
            (_a = _this.productos).push.apply(_a, response);
            _this.loading = false;
            if (response.length < _this.cantidad)
                _this.getMore = false;
        }, function (error) {
            _this.loading = false;
            _this.getMore = false;
            console.log(JSON.stringify(error));
        });
    };
    BusquedaComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.stringParam = this.rutaActiva.snapshot.queryParams.query;
        this.rutaActiva.queryParams.subscribe(function (params) {
            _this.stringParam = params.query;
            _this.productos = [];
            _this.offset = 0;
            _this.loading = false;
            _this.getMore = true;
            _this.obtenerResultado();
        });
    };
    BusquedaComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('moreElementsDiv'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], BusquedaComponent.prototype, "moreElementsDiv", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('allElements'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], BusquedaComponent.prototype, "allElements", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:scroll', ['$event']),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Function),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [Object]),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:returntype", void 0)
    ], BusquedaComponent.prototype, "handleScroll", null);
    BusquedaComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-busqueda',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./busqueda.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/busqueda/busqueda.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./busqueda.component.scss */ "./src/app/busqueda/busqueda.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], BusquedaComponent);
    return BusquedaComponent;
}());



/***/ }),

/***/ "./src/app/carro-compra/carro-compra.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/carro-compra/carro-compra.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".total {\n  padding: 20px;\n  font-size: 2em;\n}\n.total span {\n  font-weight: bold;\n}\n.media-object {\n  border: 1px solid #EFEFEF;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9jYXJyby1jb21wcmEvY2Fycm8tY29tcHJhLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jYXJyby1jb21wcmEvY2Fycm8tY29tcHJhLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksYUFBQTtFQUNBLGNBQUE7QUNBSjtBREVJO0VBQ0ksaUJBQUE7QUNBUjtBRElBO0VBQ0kseUJBQUE7QUNESiIsImZpbGUiOiJzcmMvYXBwL2NhcnJvLWNvbXByYS9jYXJyby1jb21wcmEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi50b3RhbHtcbiAgICBwYWRkaW5nOiAyMHB4O1xuICAgIGZvbnQtc2l6ZTogMmVtO1xuXG4gICAgc3BhbntcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgfVxufVxuXG4ubWVkaWEtb2JqZWN0e1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFRkVGRUY7XG4gICAgXG59IiwiLnRvdGFsIHtcbiAgcGFkZGluZzogMjBweDtcbiAgZm9udC1zaXplOiAyZW07XG59XG4udG90YWwgc3BhbiB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ubWVkaWEtb2JqZWN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgI0VGRUZFRjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/carro-compra/carro-compra.component.ts":
/*!********************************************************!*\
  !*** ./src/app/carro-compra/carro-compra.component.ts ***!
  \********************************************************/
/*! exports provided: CarroCompraComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarroCompraComponent", function() { return CarroCompraComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _shop_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shop-cart.service */ "./src/app/shop-cart.service.ts");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");
/* harmony import */ var _service_contactos_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/contactos.service */ "./src/app/service/contactos.service.ts");





var CarroCompraComponent = /** @class */ (function () {
    function CarroCompraComponent(cs, shopCart, replacServ) {
        this.cs = cs;
        this.shopCart = shopCart;
        this.replacServ = replacServ;
        this.enviado = false;
        this.amounts = [];
    }
    CarroCompraComponent.prototype.ngOnInit = function () {
        this.getProducts();
        this.shopCart.getProducts();
        this.getTotal();
        //console.log(this.products);
    };
    CarroCompraComponent.prototype.getCantidad = function () {
        return this.shopCart.getCantidad();
    };
    CarroCompraComponent.prototype.getProducts = function () {
        this.products = this.shopCart.getProducts();
        if (this.products != undefined) {
            for (var _i = 0, _a = this.products; _i < _a.length; _i++) {
                var elem = _a[_i];
                this.amounts.push(elem);
            }
        }
    };
    CarroCompraComponent.prototype.getTotal = function () {
        this.totalSinIva = this.shopCart.getTotal();
        this.total = this.totalSinIva + (this.totalSinIva * 21 / 100);
        this.total = this.total.toFixed(2);
    };
    CarroCompraComponent.prototype.removeProd = function (index) {
        this.products.splice(index, 1);
        this.shopCart.removeFromCart(index);
        this.getTotal();
        return false;
    };
    CarroCompraComponent.prototype.modifyProd = function (index) {
        this.products[index].total = parseFloat(this.products[index].info.replacement_price) * this.products[index].amount;
        this.shopCart.modifyCantProd(index, this.products[index].amount);
        this.getTotal();
    };
    CarroCompraComponent.prototype.enviarpedido = function (form) {
        var _this = this;
        form.value.contact_type = "Pedido";
        form.value.contact_desde = "Pedido desde Mercado Camiones";
        form.value.productos = this.products;
        form.value.subtotal = this.totalSinIva;
        form.value.total = this.total;
        //console.log(form.value);
        this.cs.sendCarrito(form.value).subscribe(function (data) {
            form.reset();
            _this.enviado = true;
            _this.shopCart.reset();
            alert("Pedido enviado correctamente");
        }, function (error) {
            console.log(error);
            alert("Intente mas tarde");
        });
    };
    CarroCompraComponent.ctorParameters = function () { return [
        { type: _service_contactos_service__WEBPACK_IMPORTED_MODULE_4__["ContactosService"] },
        { type: _shop_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShopCartService"] },
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"] }
    ]; };
    CarroCompraComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-carro-compra',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./carro-compra.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/carro-compra/carro-compra.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./carro-compra.component.scss */ "./src/app/carro-compra/carro-compra.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_contactos_service__WEBPACK_IMPORTED_MODULE_4__["ContactosService"], _shop_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShopCartService"], _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"]])
    ], CarroCompraComponent);
    return CarroCompraComponent;
}());



/***/ }),

/***/ "./src/app/categoria/categoria.component.scss":
/*!****************************************************!*\
  !*** ./src/app/categoria/categoria.component.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhdGVnb3JpYS9jYXRlZ29yaWEuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/categoria/categoria.component.ts":
/*!**************************************************!*\
  !*** ./src/app/categoria/categoria.component.ts ***!
  \**************************************************/
/*! exports provided: CategoriaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriaComponent", function() { return CategoriaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm5/router.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");




var CategoriaComponent = /** @class */ (function () {
    function CategoriaComponent(rutaActiva, replacementService) {
        this.rutaActiva = rutaActiva;
        this.replacementService = replacementService;
        this.offset = 0;
        this.cantidad = 24;
        this.idParam = null;
        this.productos = [];
        this.getMore = true;
        this.loading = false;
        this.nextBreakpoint = 0;
    }
    CategoriaComponent.prototype.handleScroll = function (event) {
        if (this.nextBreakpoint == 0)
            this.nextBreakpoint = parseInt(this.moreElementsDiv.nativeElement.offsetTop);
        var positionScroll = parseInt(document.documentElement.scrollTop.toFixed(0));
        var viewportHeight = parseInt(window.innerWidth.toFixed(0));
        if ((viewportHeight + positionScroll) > this.nextBreakpoint) {
            this.loadmore();
            this.nextBreakpoint = parseInt(this.moreElementsDiv.nativeElement.offsetTop);
        }
    };
    CategoriaComponent.prototype.loadmore = function () {
        if (this.getMore && !this.loading) {
            this.loading = true;
            this.offset = this.offset + this.cantidad;
            this.obtenerProductos(this.idParam);
        }
    };
    CategoriaComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.rutaActiva.params.subscribe(function (params) {
            if (params.id != undefined) {
                _this.idParam = params.id;
                _this.obtenerCategoria(_this.idParam);
            }
            _this.obtenerProductos(_this.idParam);
            _this.tituloSubcat = "SUBCATEGORIAS";
        });
    };
    CategoriaComponent.prototype.obtenerCategoria = function (idParam) {
        var _this = this;
        this.replacementService.getCategory(idParam).subscribe(function (resultado) {
            _this.categoria = resultado;
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    CategoriaComponent.prototype.obtenerProductos = function (idParam) {
        var _this = this;
        this.replacementService.getProducts(idParam, this.cantidad, this.offset).subscribe(function (response) {
            _this.productos.push(response);
            console.log(_this.productos);
            _this.loading = false;
            if (response.length == 0)
                _this.getMore = false;
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    CategoriaComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('moreElementsDiv'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], CategoriaComponent.prototype, "moreElementsDiv", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:scroll', ['$event']),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Function),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [Object]),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:returntype", void 0)
    ], CategoriaComponent.prototype, "handleScroll", null);
    CategoriaComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-categoria',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./categoria.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/categoria/categoria.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./categoria.component.scss */ "./src/app/categoria/categoria.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"]])
    ], CategoriaComponent);
    return CategoriaComponent;
}());



/***/ }),

/***/ "./src/app/common/filtro-elegidos/filtro-elegidos.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/common/filtro-elegidos/filtro-elegidos.component.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".option {\n  background-color: white;\n  border: 1px solid #333;\n  color: #333;\n  padding: 5px 10px;\n  border-radius: 5px;\n  display: inline-block;\n  margin: 0 5px 5px 0;\n  cursor: pointer;\n}\n\n.remove {\n  font-size: 12px;\n  color: red;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9jb21tb24vZmlsdHJvLWVsZWdpZG9zL2ZpbHRyby1lbGVnaWRvcy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tbW9uL2ZpbHRyby1lbGVnaWRvcy9maWx0cm8tZWxlZ2lkb3MuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ0NKOztBREVBO0VBQVMsZUFBQTtFQUFpQixVQUFBO0FDRzFCIiwiZmlsZSI6InNyYy9hcHAvY29tbW9uL2ZpbHRyby1lbGVnaWRvcy9maWx0cm8tZWxlZ2lkb3MuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIub3B0aW9ue1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XG4gICAgY29sb3I6ICMzMzM7XG4gICAgcGFkZGluZzogNXB4IDEwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW46IDAgNXB4IDVweCAwO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLnJlbW92ZXsgZm9udC1zaXplOiAxMnB4OyBjb2xvcjogcmVkO30iLCIub3B0aW9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XG4gIGNvbG9yOiAjMzMzO1xuICBwYWRkaW5nOiA1cHggMTBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbjogMCA1cHggNXB4IDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLnJlbW92ZSB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6IHJlZDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/common/filtro-elegidos/filtro-elegidos.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/common/filtro-elegidos/filtro-elegidos.component.ts ***!
  \*********************************************************************/
/*! exports provided: FiltroElegidosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltroElegidosComponent", function() { return FiltroElegidosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var src_app_service_filtro_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/filtro.service */ "./src/app/service/filtro.service.ts");



var FiltroElegidosComponent = /** @class */ (function () {
    function FiltroElegidosComponent(fs) {
        this.fs = fs;
        this.filtros = [];
        this.remove = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    FiltroElegidosComponent.prototype.ngOnInit = function () {
    };
    FiltroElegidosComponent.prototype.removeThisFilter = function (combo, type) {
        var element = {
            type: type,
            combo: combo
        };
        this.remove.emit(element);
        this.fs.remove(type, combo);
    };
    FiltroElegidosComponent.ctorParameters = function () { return [
        { type: src_app_service_filtro_service__WEBPACK_IMPORTED_MODULE_2__["FiltroService"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Array)
    ], FiltroElegidosComponent.prototype, "filtros", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], FiltroElegidosComponent.prototype, "remove", void 0);
    FiltroElegidosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'filtro-elegidos',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./filtro-elegidos.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/filtro-elegidos/filtro-elegidos.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./filtro-elegidos.component.scss */ "./src/app/common/filtro-elegidos/filtro-elegidos.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [src_app_service_filtro_service__WEBPACK_IMPORTED_MODULE_2__["FiltroService"]])
    ], FiltroElegidosComponent);
    return FiltroElegidosComponent;
}());



/***/ }),

/***/ "./src/app/common/filtro-select/filtro-select.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/common/filtro-select/filtro-select.component.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("h3 {\n  margin-top: 0.5rem;\n  margin-bottom: 0.5rem;\n  border-radius: 0rem;\n  font-size: 1rem;\n  background-color: #FFF;\n  font-weight: 400;\n  display: flex;\n  align-items: center;\n  position: relative;\n  padding: 20px;\n  border: none;\n  width: 100%;\n  text-align: left;\n  overflow-wrap: break-word;\n  line-height: 1.25;\n  cursor: pointer;\n  display: flex;\n  justify-content: space-between;\n  border-bottom: 2px solid #000;\n}\nh3 .icon svg {\n  color: #f3840d;\n  fill: #f3840d;\n}\n.option {\n  background-color: white;\n  border: 1px solid #333;\n  color: #333;\n  padding: 5px 10px;\n  border-radius: 5px;\n  display: inline-block;\n  margin: 0 5px 5px 0;\n  cursor: pointer;\n  font-size: 14px;\n  font-weight: 100;\n}\n.option:hover {\n  background-color: #F5F5F5;\n}\n.active {\n  background-color: blue;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9jb21tb24vZmlsdHJvLXNlbGVjdC9maWx0cm8tc2VsZWN0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21tb24vZmlsdHJvLXNlbGVjdC9maWx0cm8tc2VsZWN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0Usa0JBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSw2QkFBQTtBQ0FGO0FERUU7RUFDRSxjQUFBO0VBQ0EsYUFBQTtBQ0FKO0FESUE7RUFDSSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0RKO0FETUE7RUFDRSx5QkFBQTtBQ0hGO0FETUE7RUFDSSxzQkFBQTtFQUNBLFlBQUE7QUNISiIsImZpbGUiOiJzcmMvYXBwL2NvbW1vbi9maWx0cm8tc2VsZWN0L2ZpbHRyby1zZWxlY3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbmgze1xuICBtYXJnaW4tdG9wOiAwLjVyZW07XG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMHJlbTtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGIDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBwYWRkaW5nOiAyMHB4O1xuICBib3JkZXI6IG5vbmU7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBvdmVyZmxvdy13cmFwOiBicmVhay13b3JkO1xuICBsaW5lLWhlaWdodDogMS4yNTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjMDAwO1xuXG4gIC5pY29uIHN2Z3tcbiAgICBjb2xvcjogI2YzODQwZDtcbiAgICBmaWxsOiAjZjM4NDBkO1xuICB9XG59XG5cbi5vcHRpb257XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzMzMztcbiAgICBjb2xvcjogIzMzMztcbiAgICBwYWRkaW5nOiA1cHggMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogMCA1cHggNXB4IDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBmb250LXdlaWdodDogMTAwO1xuXG59XG5cblxuLm9wdGlvbjpob3ZlcntcbiAgYmFja2dyb3VuZC1jb2xvcjogI0Y1RjVGNTtcbn1cblxuLmFjdGl2ZXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cbiIsImgzIHtcbiAgbWFyZ2luLXRvcDogMC41cmVtO1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XG4gIGJvcmRlci1yYWRpdXM6IDByZW07XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRjtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBwYWRkaW5nOiAyMHB4O1xuICBib3JkZXI6IG5vbmU7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBvdmVyZmxvdy13cmFwOiBicmVhay13b3JkO1xuICBsaW5lLWhlaWdodDogMS4yNTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjMDAwO1xufVxuaDMgLmljb24gc3ZnIHtcbiAgY29sb3I6ICNmMzg0MGQ7XG4gIGZpbGw6ICNmMzg0MGQ7XG59XG5cbi5vcHRpb24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyOiAxcHggc29saWQgIzMzMztcbiAgY29sb3I6ICMzMzM7XG4gIHBhZGRpbmc6IDVweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAwIDVweCA1cHggMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG59XG5cbi5vcHRpb246aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjVGNUY1O1xufVxuXG4uYWN0aXZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZTtcbiAgY29sb3I6IHdoaXRlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/common/filtro-select/filtro-select.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/common/filtro-select/filtro-select.component.ts ***!
  \*****************************************************************/
/*! exports provided: FiltroSelectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltroSelectComponent", function() { return FiltroSelectComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/__ivy_ngcc__/fesm5/animations.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var src_app_service_filtro_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/filtro.service */ "./src/app/service/filtro.service.ts");




var FiltroSelectComponent = /** @class */ (function () {
    function FiltroSelectComponent(fs) {
        this.fs = fs;
        this.visible = false;
        this.filtrar = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
    }
    FiltroSelectComponent.prototype.ngOnInit = function () {
        this.filtro = {
            type: this.type,
            selected: []
        };
        this.fs.addItem(this.filtro);
    };
    FiltroSelectComponent.prototype.select = function (dato) {
        if ((this.filtro.selected.find(function (e) { return (e.id === dato.id) && (e.label == dato.label); })) === undefined) {
            this.filtro.selected.push(dato);
            this.filtrar.emit(this.filtro);
            this.updateObserver();
        }
    };
    FiltroSelectComponent.prototype.isSelected = function (dato) {
        var found = this.filtro.selected.find(function (e) { return e.id == dato.id && e.label == dato.label; });
        return found ? true : false;
    };
    FiltroSelectComponent.prototype.updateObserver = function () {
        this.fs.update();
    };
    FiltroSelectComponent.ctorParameters = function () { return [
        { type: src_app_service_filtro_service__WEBPACK_IMPORTED_MODULE_3__["FiltroService"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Array)
    ], FiltroSelectComponent.prototype, "datos", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", String)
    ], FiltroSelectComponent.prototype, "label", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", String)
    ], FiltroSelectComponent.prototype, "type", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], FiltroSelectComponent.prototype, "filtrar", void 0);
    FiltroSelectComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'filtro-select',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./filtro-select.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/filtro-select/filtro-select.component.html")).default,
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('slideInOut', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('200ms ease-in', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ overflow: 'hidden', height: 'auto' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('200ms ease-in', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ overflow: 'hidden', height: '0px' }))
                    ])
                ])
            ],
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./filtro-select.component.scss */ "./src/app/common/filtro-select/filtro-select.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [src_app_service_filtro_service__WEBPACK_IMPORTED_MODULE_3__["FiltroService"]])
    ], FiltroSelectComponent);
    return FiltroSelectComponent;
}());



/***/ }),

/***/ "./src/app/common/galery/galery.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/common/galery/galery.component.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".images-slider {\n  width: 100%;\n  display: flex;\n  justify-content: space-around;\n  flex-direction: row;\n  margin: 0;\n  padding: 0;\n  max-height: 450px;\n  overflow: hidden;\n}\n.images-slider .slider-thumb {\n  width: 100px;\n  margin-right: 10px;\n}\n.images-slider .slider-thumb .flex-control-thumbs {\n  height: 100%;\n  padding: 0px;\n  justify-content: flex-start;\n}\n.images-slider .slider-thumb .flex-control-thumbs li {\n  width: 100%;\n  padding: 0px;\n  margin-bottom: 10px;\n}\n.images-slider .flecha-prev, .images-slider .flecha-next {\n  background-color: #CCC;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  color: #FFF;\n  padding: 10px;\n}\n.images-slider .slider-container {\n  width: 100%;\n  background-color: #CCC;\n}\n.images-slider .slider-container .slides {\n  height: 600px;\n  max-height: 600px;\n  overflow: hidden;\n}\n.images-slider .flex-control-nav .col-6 {\n  margin: 0px;\n  padding: 0px;\n}\n.images-slider .flex-control-nav .col-6:first-child {\n  padding-right: 2px;\n}\n.images-slider .flex-control-nav .col-6:last-child {\n  padding-left: 2px;\n}\n.images-slider .btn-outline {\n  width: 100%;\n  color: #666;\n  border: #EFEFEF 1px solid;\n  padding: 0px;\n  margin: 0;\n}\n.images-slider .btn-outline:hover {\n  color: #FFF;\n  border: #333 1px solid;\n  background-color: #666;\n}\n@media (max-width: 600px) {\n  .images-slider {\n    max-height: auto;\n    margin-bottom: 30px;\n    margin: 15px;\n    width: auto;\n  }\n  .images-slider .slider-container .slides {\n    height: 280px;\n    max-height: 280px;\n    overflow: hidden;\n  }\n  .images-slider .slider-container .flex-control-nav {\n    margin: 0;\n    padding: 0;\n  }\n  .images-slider .slider-thumb {\n    display: none;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9jb21tb24vZ2FsZXJ5L2dhbGVyeS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tbW9uL2dhbGVyeS9nYWxlcnkuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNDSjtBRElFO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0FDRko7QURJSTtFQUVFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsMkJBQUE7QUNITjtBREtNO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ0hSO0FEVUU7RUFDRSxzQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7QUNSSjtBRGFFO0VBQ0UsV0FBQTtFQUNBLHNCQUFBO0FDWEo7QURZTTtFQUNFLGFBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDVlI7QURlSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDYlI7QURlSTtFQUNJLGtCQUFBO0FDYlI7QURlSTtFQUNJLGlCQUFBO0FDYlI7QURlSTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtBQ2JSO0FEZUk7RUFDSSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtBQ2JSO0FEb0JBO0VBRUU7SUFDRSxnQkFBQTtJQUNBLG1CQUFBO0lBQ0EsWUFBQTtJQUNBLFdBQUE7RUNsQkY7RUR1QkE7SUFDRSxhQUFBO0lBQ0EsaUJBQUE7SUFDQSxnQkFBQTtFQ3JCRjtFRHdCQTtJQUNFLFNBQUE7SUFDQSxVQUFBO0VDdEJGO0VEMkJBO0lBQ0UsYUFBQTtFQ3pCRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvY29tbW9uL2dhbGVyeS9nYWxlcnkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW1hZ2VzLXNsaWRlcntcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgbWFyZ2luOiAwO1xuICAgIHBhZGRpbmc6IDA7XG4gICAgbWF4LWhlaWdodDogNDUwcHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcblxuXG5cblxuICAuc2xpZGVyLXRodW1ie1xuICAgIHdpZHRoOiAxMDBweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG5cbiAgICAuZmxleC1jb250cm9sLXRodW1ic3tcblxuICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgcGFkZGluZzogMHB4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuXG4gICAgICBsaXtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIHBhZGRpbmc6IDBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgIH1cblxuICAgIH1cblxuICB9XG5cbiAgLmZsZWNoYS1wcmV2LCAgIC5mbGVjaGEtbmV4dHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjQ0NDO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBjb2xvcjogI0ZGRjtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICB9XG5cblxuXG4gIC5zbGlkZXItY29udGFpbmVye1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNDQ0M7XG4gICAgICAuc2xpZGVze1xuICAgICAgICBoZWlnaHQ6IDYwMHB4O1xuICAgICAgICBtYXgtaGVpZ2h0OiA2MDBweDtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgIH1cblxuICB9XG5cbiAgICAuZmxleC1jb250cm9sLW5hdiAuY29sLTZ7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBwYWRkaW5nOiAwcHg7XG4gICAgfVxuICAgIC5mbGV4LWNvbnRyb2wtbmF2IC5jb2wtNjpmaXJzdC1jaGlsZHtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMnB4O1xuICAgIH1cbiAgICAuZmxleC1jb250cm9sLW5hdiAuY29sLTY6bGFzdC1jaGlsZHtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAycHg7XG4gICAgfVxuICAgIC5idG4tb3V0bGluZXtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGNvbG9yOiAjNjY2O1xuICAgICAgICBib3JkZXI6ICNFRkVGRUYgMXB4IHNvbGlkO1xuICAgICAgICBwYWRkaW5nOiAwcHg7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICB9XG4gICAgLmJ0bi1vdXRsaW5lOmhvdmVye1xuICAgICAgICBjb2xvcjogI0ZGRjtcbiAgICAgICAgYm9yZGVyOiAjMzMzIDFweCBzb2xpZDtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzY2NjtcblxuICAgIH1cbn1cblxuXG5cbkBtZWRpYSAobWF4LXdpZHRoOiA2MDBweCkge1xuXG4gIC5pbWFnZXMtc2xpZGVye1xuICAgIG1heC1oZWlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcbiAgICBtYXJnaW46IDE1cHg7XG4gICAgd2lkdGg6IGF1dG87XG5cblxuXG4gIC5zbGlkZXItY29udGFpbmVye1xuICAuc2xpZGVze1xuICAgIGhlaWdodDogMjgwcHg7XG4gICAgbWF4LWhlaWdodDogMjgwcHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgfVxuXG4gIC5mbGV4LWNvbnRyb2wtbmF2e1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG5cbn1cblxuICAuc2xpZGVyLXRodW1ie1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbn1cblxufVxuIiwiLmltYWdlcy1zbGlkZXIge1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbiAgbWF4LWhlaWdodDogNDUwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uaW1hZ2VzLXNsaWRlciAuc2xpZGVyLXRodW1iIHtcbiAgd2lkdGg6IDEwMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4uaW1hZ2VzLXNsaWRlciAuc2xpZGVyLXRodW1iIC5mbGV4LWNvbnRyb2wtdGh1bWJzIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwYWRkaW5nOiAwcHg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbn1cbi5pbWFnZXMtc2xpZGVyIC5zbGlkZXItdGh1bWIgLmZsZXgtY29udHJvbC10aHVtYnMgbGkge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLmltYWdlcy1zbGlkZXIgLmZsZWNoYS1wcmV2LCAuaW1hZ2VzLXNsaWRlciAuZmxlY2hhLW5leHQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjQ0NDO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgY29sb3I6ICNGRkY7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG4uaW1hZ2VzLXNsaWRlciAuc2xpZGVyLWNvbnRhaW5lciB7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjQ0NDO1xufVxuLmltYWdlcy1zbGlkZXIgLnNsaWRlci1jb250YWluZXIgLnNsaWRlcyB7XG4gIGhlaWdodDogNjAwcHg7XG4gIG1heC1oZWlnaHQ6IDYwMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmltYWdlcy1zbGlkZXIgLmZsZXgtY29udHJvbC1uYXYgLmNvbC02IHtcbiAgbWFyZ2luOiAwcHg7XG4gIHBhZGRpbmc6IDBweDtcbn1cbi5pbWFnZXMtc2xpZGVyIC5mbGV4LWNvbnRyb2wtbmF2IC5jb2wtNjpmaXJzdC1jaGlsZCB7XG4gIHBhZGRpbmctcmlnaHQ6IDJweDtcbn1cbi5pbWFnZXMtc2xpZGVyIC5mbGV4LWNvbnRyb2wtbmF2IC5jb2wtNjpsYXN0LWNoaWxkIHtcbiAgcGFkZGluZy1sZWZ0OiAycHg7XG59XG4uaW1hZ2VzLXNsaWRlciAuYnRuLW91dGxpbmUge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6ICM2NjY7XG4gIGJvcmRlcjogI0VGRUZFRiAxcHggc29saWQ7XG4gIHBhZGRpbmc6IDBweDtcbiAgbWFyZ2luOiAwO1xufVxuLmltYWdlcy1zbGlkZXIgLmJ0bi1vdXRsaW5lOmhvdmVyIHtcbiAgY29sb3I6ICNGRkY7XG4gIGJvcmRlcjogIzMzMyAxcHggc29saWQ7XG4gIGJhY2tncm91bmQtY29sb3I6ICM2NjY7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA2MDBweCkge1xuICAuaW1hZ2VzLXNsaWRlciB7XG4gICAgbWF4LWhlaWdodDogYXV0bztcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICAgIG1hcmdpbjogMTVweDtcbiAgICB3aWR0aDogYXV0bztcbiAgfVxuICAuaW1hZ2VzLXNsaWRlciAuc2xpZGVyLWNvbnRhaW5lciAuc2xpZGVzIHtcbiAgICBoZWlnaHQ6IDI4MHB4O1xuICAgIG1heC1oZWlnaHQ6IDI4MHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gIH1cbiAgLmltYWdlcy1zbGlkZXIgLnNsaWRlci1jb250YWluZXIgLmZsZXgtY29udHJvbC1uYXYge1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG4gIC5pbWFnZXMtc2xpZGVyIC5zbGlkZXItdGh1bWIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbn0iXX0= */");

/***/ }),

/***/ "./src/app/common/galery/galery.component.ts":
/*!***************************************************!*\
  !*** ./src/app/common/galery/galery.component.ts ***!
  \***************************************************/
/*! exports provided: GaleryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GaleryComponent", function() { return GaleryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var GaleryComponent = /** @class */ (function () {
    function GaleryComponent() {
    }
    GaleryComponent.prototype.ngOnInit = function () {
        this.indice = 0;
    };
    GaleryComponent.prototype.changePrevFocus = function () {
        var size = this.imagenes.length;
        this.indice = this.indice - 1;
        if (this.indice < 0) {
            this.indice = size - 1;
        }
    };
    GaleryComponent.prototype.changeNextFocus = function () {
        var size = this.imagenes.length;
        this.indice = this.indice + 1;
        if (this.indice == size) {
            this.indice = 0;
        }
    };
    GaleryComponent.prototype.setThisFocus = function (i) {
        var size = this.imagenes.length;
        this.indice = i;
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Array)
    ], GaleryComponent.prototype, "imagenes", void 0);
    GaleryComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-galery',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./galery.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/common/galery/galery.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./galery.component.scss */ "./src/app/common/galery/galery.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], GaleryComponent);
    return GaleryComponent;
}());



/***/ }),

/***/ "./src/app/contacto/contacto.component.scss":
/*!**************************************************!*\
  !*** ./src/app/contacto/contacto.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".sub-bnr {\n  background-image: url(\"/assets/img/bg1.png\");\n  background-position: bottom;\n}\n\n.text-area {\n  width: 100%;\n}\n\n.cart-head {\n  padding-bottom: 0;\n  margin-bottom: 0;\n}\n\n.cart-details {\n  padding-bottom: 0;\n  margin-bottom: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9jb250YWN0by9jb250YWN0by5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29udGFjdG8vY29udGFjdG8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQSw0Q0FBQTtFQUNBLDJCQUFBO0FDQ0E7O0FER0E7RUFDSSxXQUFBO0FDQUo7O0FER0E7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0FDQUo7O0FER0E7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0FDQUoiLCJmaWxlIjoic3JjL2FwcC9jb250YWN0by9jb250YWN0by5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zdWItYm5ye1xuYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL2ltZy9iZzEucG5nJyk7XG5iYWNrZ3JvdW5kLXBvc2l0aW9uOiBib3R0b207XG59XG5cblxuLnRleHQtYXJlYXtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmNhcnQtaGVhZHtcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG4uY2FydC1kZXRhaWxze1xuICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG59IiwiLnN1Yi1ibnIge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ltZy9iZzEucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBib3R0b207XG59XG5cbi50ZXh0LWFyZWEge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmNhcnQtaGVhZCB7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG4uY2FydC1kZXRhaWxzIHtcbiAgcGFkZGluZy1ib3R0b206IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59Il19 */");

/***/ }),

/***/ "./src/app/contacto/contacto.component.ts":
/*!************************************************!*\
  !*** ./src/app/contacto/contacto.component.ts ***!
  \************************************************/
/*! exports provided: ContactoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoComponent", function() { return ContactoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _forms_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../forms.service */ "./src/app/forms.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _service_contactos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/contactos.service */ "./src/app/service/contactos.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm5/router.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");






var ContactoComponent = /** @class */ (function () {
    function ContactoComponent(rs, cs, fs, route) {
        this.rs = rs;
        this.cs = cs;
        this.fs = fs;
        this.route = route;
        this.enviado = false;
        this.unidad = false;
        this.msj = '';
    }
    ContactoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getInfoContacto();
        var id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.rs.getUsado(id).subscribe(function (data) {
                _this.usado = data;
                _this.unidad = true;
                _this.msj = 'Me interesa la siguiente unidad: ' + _this.usado.vehicle_model + ', me podrian pasar información? \nMuchas gracias.';
            }, function (error) { console.log(error); });
        }
    };
    ContactoComponent.prototype.contacto = function (form) {
        var _this = this;
        form.value.contact_type = "Contacto";
        form.value.contact_desde = "Contacto desde Mercado Camiones";
        if (this.unidad) {
            form.value.contact_unidad = this.usado.vehicle_id;
        }
        this.cs.sendContact(form.value).subscribe(function (data) {
            form.reset();
            _this.enviado = true;
            //alert("Mensaje enviado correctamente");
        }, function (error) {
            console.log(error);
            alert("Intente mas tarde");
        });
        var info = {
            "contact_name": form.value.contact_name,
            "contact_email": form.value.contact_email,
            "contact_phone": form.value.contact_phone,
            "contact_city": form.value.contact_city
        };
        this.fs.saveInfo(info);
    };
    ContactoComponent.prototype.getInfoContacto = function () {
        this.infoClient = this.fs.getInfo();
    };
    ContactoComponent.ctorParameters = function () { return [
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_5__["ReplacementService"] },
        { type: _service_contactos_service__WEBPACK_IMPORTED_MODULE_3__["ContactosService"] },
        { type: _forms_service__WEBPACK_IMPORTED_MODULE_1__["FormsService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
    ]; };
    ContactoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-contacto',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contacto.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/contacto/contacto.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contacto.component.scss */ "./src/app/contacto/contacto.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_replacement_service__WEBPACK_IMPORTED_MODULE_5__["ReplacementService"], _service_contactos_service__WEBPACK_IMPORTED_MODULE_3__["ContactosService"], _forms_service__WEBPACK_IMPORTED_MODULE_1__["FormsService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], ContactoComponent);
    return ContactoComponent;
}());



/***/ }),

/***/ "./src/app/cotizador/ajuste/ajuste.component.scss":
/*!********************************************************!*\
  !*** ./src/app/cotizador/ajuste/ajuste.component.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvdGl6YWRvci9hanVzdGUvYWp1c3RlLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/cotizador/ajuste/ajuste.component.ts":
/*!******************************************************!*\
  !*** ./src/app/cotizador/ajuste/ajuste.component.ts ***!
  \******************************************************/
/*! exports provided: AjusteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AjusteComponent", function() { return AjusteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var AjusteComponent = /** @class */ (function () {
    function AjusteComponent() {
    }
    AjusteComponent.prototype.ngOnInit = function () {
    };
    AjusteComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ajuste',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./ajuste.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/ajuste/ajuste.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./ajuste.component.scss */ "./src/app/cotizador/ajuste/ajuste.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], AjusteComponent);
    return AjusteComponent;
}());



/***/ }),

/***/ "./src/app/cotizador/cotizaciones/cotizaciones.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/cotizador/cotizaciones/cotizaciones.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvdGl6YWRvci9jb3RpemFjaW9uZXMvY290aXphY2lvbmVzLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/cotizador/cotizaciones/cotizaciones.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/cotizador/cotizaciones/cotizaciones.component.ts ***!
  \******************************************************************/
/*! exports provided: CotizacionesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CotizacionesComponent", function() { return CotizacionesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var CotizacionesComponent = /** @class */ (function () {
    function CotizacionesComponent() {
    }
    CotizacionesComponent.prototype.ngOnInit = function () {
    };
    CotizacionesComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cotizaciones',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./cotizaciones.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/cotizaciones/cotizaciones.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./cotizaciones.component.scss */ "./src/app/cotizador/cotizaciones/cotizaciones.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], CotizacionesComponent);
    return CotizacionesComponent;
}());



/***/ }),

/***/ "./src/app/cotizador/cotizador.component.scss":
/*!****************************************************!*\
  !*** ./src/app/cotizador/cotizador.component.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".sub-bnr {\n  background-image: url(\"/assets/img/bg1.png\");\n  background-position: bottom;\n}\n\n.text-area {\n  width: 100%;\n}\n\n.contact-form {\n  margin-bottom: 45px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9jb3RpemFkb3IvY290aXphZG9yLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb3RpemFkb3IvY290aXphZG9yLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNENBQUE7RUFDQSwyQkFBQTtBQ0NKOztBREdBO0VBQ0ksV0FBQTtBQ0FKOztBREdBO0VBQ0ksbUJBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2NvdGl6YWRvci9jb3RpemFkb3IuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3ViLWJucntcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9hc3NldHMvaW1nL2JnMS5wbmcnKTtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBib3R0b207XG4gICAgfVxuICAgIFxuICAgIFxuLnRleHQtYXJlYXtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmNvbnRhY3QtZm9ybXtcbiAgICBtYXJnaW4tYm90dG9tOiA0NXB4O1xufSIsIi5zdWItYm5yIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWcvYmcxLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogYm90dG9tO1xufVxuXG4udGV4dC1hcmVhIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5jb250YWN0LWZvcm0ge1xuICBtYXJnaW4tYm90dG9tOiA0NXB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/cotizador/cotizador.component.ts":
/*!**************************************************!*\
  !*** ./src/app/cotizador/cotizador.component.ts ***!
  \**************************************************/
/*! exports provided: CotizadorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CotizadorComponent", function() { return CotizadorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _forms_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../forms.service */ "./src/app/forms.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");




var CotizadorComponent = /** @class */ (function () {
    function CotizadorComponent(rs, fs) {
        this.rs = rs;
        this.fs = fs;
        this.enviado = false;
    }
    CotizadorComponent.prototype.ngOnInit = function () {
        this.getInfoContacto();
    };
    CotizadorComponent.prototype.contacto = function (form) {
        var _this = this;
        form.value.contact_type = "Seguro";
        //console.log(form.value);
        this.rs.enviarmensaje(form.value).subscribe(function (data) {
            form.reset();
            _this.enviado = true;
            //alert("Mensaje enviado")
        }, function (error) {
            console.log(error);
            alert("Intente mas tarde");
        });
        var info = {
            "contact_name": form.value.contact_name,
            "contact_email": form.value.contact_email,
            "contact_phone": form.value.contact_phone,
            "contact_city": form.value.contact_city
        };
        this.fs.saveInfo(info);
    };
    CotizadorComponent.prototype.getInfoContacto = function () {
        this.infoClient = this.fs.getInfo();
    };
    CotizadorComponent.ctorParameters = function () { return [
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"] },
        { type: _forms_service__WEBPACK_IMPORTED_MODULE_1__["FormsService"] }
    ]; };
    CotizadorComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-cotizador',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./cotizador.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/cotizador.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./cotizador.component.scss */ "./src/app/cotizador/cotizador.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"], _forms_service__WEBPACK_IMPORTED_MODULE_1__["FormsService"]])
    ], CotizadorComponent);
    return CotizadorComponent;
}());



/***/ }),

/***/ "./src/app/cotizador/solicitud/solicitud.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/cotizador/solicitud/solicitud.component.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvdGl6YWRvci9zb2xpY2l0dWQvc29saWNpdHVkLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/cotizador/solicitud/solicitud.component.ts":
/*!************************************************************!*\
  !*** ./src/app/cotizador/solicitud/solicitud.component.ts ***!
  \************************************************************/
/*! exports provided: SolicitudComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SolicitudComponent", function() { return SolicitudComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var SolicitudComponent = /** @class */ (function () {
    function SolicitudComponent() {
    }
    SolicitudComponent.prototype.ngOnInit = function () {
    };
    SolicitudComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-solicitud',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./solicitud.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/solicitud/solicitud.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./solicitud.component.scss */ "./src/app/cotizador/solicitud/solicitud.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], SolicitudComponent);
    return SolicitudComponent;
}());



/***/ }),

/***/ "./src/app/cotizador/vehiculo/vehiculo.component.scss":
/*!************************************************************!*\
  !*** ./src/app/cotizador/vehiculo/vehiculo.component.scss ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvdGl6YWRvci92ZWhpY3Vsby92ZWhpY3Vsby5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/cotizador/vehiculo/vehiculo.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/cotizador/vehiculo/vehiculo.component.ts ***!
  \**********************************************************/
/*! exports provided: VehiculoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehiculoComponent", function() { return VehiculoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var VehiculoComponent = /** @class */ (function () {
    function VehiculoComponent() {
    }
    VehiculoComponent.prototype.ngOnInit = function () {
    };
    VehiculoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-vehiculo',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./vehiculo.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cotizador/vehiculo/vehiculo.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./vehiculo.component.scss */ "./src/app/cotizador/vehiculo/vehiculo.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], VehiculoComponent);
    return VehiculoComponent;
}());



/***/ }),

/***/ "./src/app/financiamiento/financiamiento.component.sass":
/*!**************************************************************!*\
  !*** ./src/app/financiamiento/financiamiento.component.sass ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2lhbWllbnRvL2ZpbmFuY2lhbWllbnRvLmNvbXBvbmVudC5zYXNzIn0= */");

/***/ }),

/***/ "./src/app/financiamiento/financiamiento.component.ts":
/*!************************************************************!*\
  !*** ./src/app/financiamiento/financiamiento.component.ts ***!
  \************************************************************/
/*! exports provided: FinanciamientoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanciamientoComponent", function() { return FinanciamientoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _forms_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../forms.service */ "./src/app/forms.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");




var FinanciamientoComponent = /** @class */ (function () {
    function FinanciamientoComponent(rs, fs) {
        this.rs = rs;
        this.fs = fs;
        this.enviado = false;
    }
    FinanciamientoComponent.prototype.ngOnInit = function () {
        this.getInfoContacto();
    };
    FinanciamientoComponent.prototype.contacto = function (form) {
        var _this = this;
        form.value.contact_type = "Contacto";
        //console.log(form.value);
        this.rs.enviarmensaje(form.value).subscribe(function (data) {
            form.reset();
            _this.enviado = true;
            //alert("Mensaje enviado")
        }, function (error) {
            alert("Intente mas tarde");
        });
        var info = {
            "contact_name": form.value.contact_name,
            "contact_email": form.value.contact_email,
            "contact_phone": form.value.contact_phone,
            "contact_city": form.value.contact_city
        };
        this.fs.saveInfo(info);
    };
    FinanciamientoComponent.prototype.getInfoContacto = function () {
        this.infoClient = this.fs.getInfo();
    };
    FinanciamientoComponent.ctorParameters = function () { return [
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"] },
        { type: _forms_service__WEBPACK_IMPORTED_MODULE_1__["FormsService"] }
    ]; };
    FinanciamientoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-financiamiento',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./financiamiento.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/financiamiento/financiamiento.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./financiamiento.component.sass */ "./src/app/financiamiento/financiamiento.component.sass")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"], _forms_service__WEBPACK_IMPORTED_MODULE_1__["FormsService"]])
    ], FinanciamientoComponent);
    return FinanciamientoComponent;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.scss":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n:host footer p {\n  font-size: 12px;\n  line-height: 26px;\n  color: #999999;\n}\n:host footer h6 {\n  color: #fff;\n  margin-top: 0px;\n  font-size: 14px;\n  margin-bottom: 42px;\n  letter-spacing: 1.5px;\n}\n:host footer {\n  padding: 120px 0;\n  padding-bottom: 60px;\n  background: #403f3f;\n  position: relative;\n  z-index: 1;\n}\n:host .about-footer i {\n  width: 20px;\n  display: inline-block;\n}\n:host .about-footer p {\n  margin-bottom: 20px;\n}\n:host footer li a {\n  font-size: 12px;\n  color: #999999;\n  line-height: 26px;\n  display: inline-block;\n  width: 100%;\n  letter-spacing: 0px;\n}\n:host footer li a:hover {\n  color: #fff;\n}\n:host .rights .go-up {\n  border: 1px solid #454545;\n  color: #949494;\n  height: 30px;\n  width: 30px;\n  float: right;\n  position: absolute;\n  right: 0px;\n  left: 0px;\n  margin: 0 auto;\n  bottom: -60px;\n  text-align: center;\n  line-height: 30px;\n  display: inline-block;\n}\n:host .rights .go-up:hover {\n  background: #fff;\n  color: #333;\n}\n:host .rights {\n  position: relative;\n  text-align: center;\n  display: inline-block;\n  width: 100%;\n  margin-top: 0;\n  background-color: #000;\n  padding: 30px;\n  color: #969595;\n}\n:host .rights p {\n  margin: 0px;\n  font-size: 10px;\n  line-height: 26px;\n  color: #999999;\n}\n:host .light-rights {\n  background: #eceaea;\n}\n:host footer .social_icons {\n  display: inline-block;\n  margin-top: 20px;\n}\n:host footer .social_icons li {\n  margin-right: 5px;\n}\n:host footer .social_icons a {\n  font-size: 14px;\n  margin-right: 20px;\n  border-radius: 50%;\n  line-height: 34px;\n  color: #949494;\n  display: inline-block;\n  text-align: center;\n}\n:host .footer-info h6 {\n  font-size: 12px;\n  text-transform: uppercase;\n  color: #fff;\n  letter-spacing: 2px;\n  font-weight: 600;\n  margin-bottom: 30px;\n}\n:host .news-letter p {\n  font-size: 13px;\n}\n:host footer .subscribe {\n  border-bottom: 1px solid rgba(255, 255, 255, 0.1);\n  padding-bottom: 20px;\n  margin-top: 10px;\n  display: inline-block;\n  width: 100%;\n}\n:host footer .subscribe button {\n  background: #965452 !important;\n  border: none;\n  float: right;\n  margin-top: -45px;\n  height: 40px;\n  color: #fff;\n  padding: 0 20px;\n  line-height: 40px;\n  z-index: 9;\n  position: relative;\n}\n:host footer .subscribe input {\n  width: 100%;\n  border: none;\n  height: 40px;\n  font-size: 12px;\n  font-family: \"Montserrat\", sans-serif;\n  padding: 0 10px;\n  margin-bottom: 5px;\n}\n:host .footer-info .links li {\n  position: relative;\n}\n:host .footer-info .links li a:before {\n  content: \"\";\n  font-family: FontAwesome;\n  color: #8e8e8e;\n  position: absolute;\n  left: 0px;\n  font-size: 12px;\n  padding: 0px;\n  font-weight: 100;\n}\n:host .footer-info .links li a {\n  color: #999999;\n  padding-left: 15px;\n  font-size: 12px;\n  font-weight: 500;\n  text-transform: uppercase;\n}\n:host .footer-info .links li a:hover {\n  color: #fff;\n}\n:host .footer-info .quote .form-control {\n  background: none;\n  border-radius: 0px;\n  border: none;\n  border-bottom: 1px solid #45515d;\n  margin-bottom: 20px;\n  height: 40px;\n  font-size: 13px;\n}\n:host .footer-info .quote textarea.form-control {\n  height: 80px;\n}\n:host .footer-info .personal-info {\n  margin-top: 30px;\n}\n:host .footer-info .personal-info li {\n  color: #999999;\n  font-size: 12px;\n  line-height: 24px;\n  padding: 10px 0;\n  padding-top: 0px;\n  margin-bottom: 10px;\n  letter-spacing: 0.5px;\n  display: inline-block;\n  width: 100%;\n  font-weight: 500;\n  border-bottom: 1px solid #414141;\n}\n:host .footer-info .flicker {\n  margin: 0 -3px;\n  margin-top: 20px;\n}\n:host .footer-info .flicker a:hover {\n  opacity: 0.5;\n}\n:host .footer-info .flicker .owl-dots {\n  text-align: center;\n}\n:host .footer-info .flicker .owl-dots .owl-dot {\n  display: inline-block;\n  height: 5px;\n  width: 5px;\n  margin: 0 2px;\n  background: rgba(255, 255, 255, 0.2);\n  border-radius: 4px;\n}\n:host .footer-info .flicker .owl-dots .owl-dot.active {\n  background: #fff;\n}\n:host .footer-info .personal-info li i {\n  font-size: 16px;\n  color: #929292;\n  margin-right: 10px;\n  float: left;\n  width: 30px;\n  margin-top: 3px;\n  margin-bottom: 8px;\n}\n:host .foot-info-con {\n  display: inline-block;\n  width: 100%;\n  margin-top: 80px;\n}\n:host .foot-info-con li a {\n  font-size: 13px;\n  text-transform: uppercase;\n  font-weight: 400;\n}\n:host .foot-info-con .icon {\n  height: 90px;\n  width: 90px;\n  color: #fff;\n  background: #3f4955;\n  text-align: center;\n  line-height: 90px;\n  display: inline-block;\n  border-radius: 0px;\n  font-size: 20px;\n  margin-right: 10px;\n  border-radius: 50%;\n}\n:host .foot-info-con p {\n  font-size: 16px;\n  margin: 0px;\n}\n:host .foot-info-con h6 {\n  font-weight: normal;\n  font-family: \"Ubuntu\";\n  color: #fff;\n  font-weight: bold;\n  margin-top: 20px;\n  font-size: 15px;\n  text-transform: none;\n  margin-bottom: 4px;\n  letter-spacing: 2px;\n}\n:host .footer-light {\n  background: #f9f9f9;\n}\n:host .footer-light h6 {\n  color: #333333;\n}\n:host .footer-light .rights {\n  margin-top: 60px;\n  background: #efeeee;\n}\n:host .scroll {\n  cursor: pointer;\n}\n:host h3 {\n  font-size: 18px;\n  font-weight: 100;\n  color: #FFFFFF;\n}\n:host .small-about {\n  border-top: 1px solid #d1d5db;\n}\n:host .small-about .social_icons {\n  text-align: center;\n  font-size: 15px;\n}\n:host .small-about li {\n  display: inline-block;\n}\n:host .small-about li a {\n  font-size: 14px;\n  margin: 0 5px;\n  color: #adacac;\n}\n:host .small-about li a:hover {\n  color: #ffe333;\n}\n:host .small-about p {\n  font-family: \"Playfair Display\", serif;\n  color: #adacac;\n  font-size: 14px;\n  line-height: 20px;\n  max-width: 900px;\n  margin: 0 auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyIsIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ0NkO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBRENKO0FDQ0U7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0FEQ0o7QUNDRTtFQUNFLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBRENKO0FDQ0U7RUFDRSxXQUFBO0VBQ0EscUJBQUE7QURDSjtBQ0NFO0VBQ0UsbUJBQUE7QURDSjtBQ0NFO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FEQ0o7QUNDRTtFQUNFLFdBQUE7QURDSjtBQ0NFO0VBQ0UseUJBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLGNBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0FEQ0o7QUNDRTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtBRENKO0FDQ0U7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QURDSjtBQ0NFO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QURDSjtBQ0NFO0VBQ0UsbUJBQUE7QURDSjtBQ0NFO0VBQ0UscUJBQUE7RUFDQSxnQkFBQTtBRENKO0FDQ0U7RUFDRSxpQkFBQTtBRENKO0FDQ0U7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7QURDSjtBQ0NFO0VBQ0UsZUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBRENKO0FDQ0U7RUFDRSxlQUFBO0FEQ0o7QUNDRTtFQUNFLGlEQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtBRENKO0FDQ0U7RUFDRSw4QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtBRENKO0FDQ0U7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EscUNBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QURDSjtBQ0NFO0VBQ0Usa0JBQUE7QURDSjtBQ0NFO0VBQ0UsWUFBQTtFQUNBLHdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QURDSjtBQ0NFO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7QURDSjtBQ0NFO0VBQ0UsV0FBQTtBRENKO0FDQ0U7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGdDQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBRENKO0FDQ0U7RUFDRSxZQUFBO0FEQ0o7QUNDRTtFQUNFLGdCQUFBO0FEQ0o7QUNDRTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQ0FBQTtBRENKO0FDQ0U7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7QURDSjtBQ0NFO0VBQ0UsWUFBQTtBRENKO0FDQ0U7RUFDRSxrQkFBQTtBRENKO0FDQ0U7RUFDRSxxQkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7QURDSjtBQ0NFO0VBQ0UsZ0JBQUE7QURDSjtBQ0NFO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FEQ0o7QUNDRTtFQUNFLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FEQ0o7QUNDRTtFQUNFLGVBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0FEQ0o7QUNDRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBRENKO0FDQ0U7RUFDRSxlQUFBO0VBQ0EsV0FBQTtBRENKO0FDQ0U7RUFDRSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FEQ0o7QUNDRTtFQUNFLG1CQUFBO0FEQ0o7QUNDRTtFQUNFLGNBQUE7QURDSjtBQ0NFO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBRENKO0FDQ0k7RUFDSSxlQUFBO0FEQ1I7QUNFSTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QURBTjtBQ0dJO0VBQ0UsNkJBQUE7QURETjtBQ0dJO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0FERE47QUNHSTtFQUNFLHFCQUFBO0FERE47QUNHSTtFQUNFLGVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBREROO0FDR0k7RUFDRSxjQUFBO0FERE47QUNHSTtFQUNFLHNDQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBREROIiwiZmlsZSI6InNyYy9hcHAvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbjpob3N0IGZvb3RlciBwIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBsaW5lLWhlaWdodDogMjZweDtcbiAgY29sb3I6ICM5OTk5OTk7XG59XG46aG9zdCBmb290ZXIgaDYge1xuICBjb2xvcjogI2ZmZjtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi1ib3R0b206IDQycHg7XG4gIGxldHRlci1zcGFjaW5nOiAxLjVweDtcbn1cbjpob3N0IGZvb3RlciB7XG4gIHBhZGRpbmc6IDEyMHB4IDA7XG4gIHBhZGRpbmctYm90dG9tOiA2MHB4O1xuICBiYWNrZ3JvdW5kOiAjNDAzZjNmO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHotaW5kZXg6IDE7XG59XG46aG9zdCAuYWJvdXQtZm9vdGVyIGkge1xuICB3aWR0aDogMjBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuOmhvc3QgLmFib3V0LWZvb3RlciBwIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbjpob3N0IGZvb3RlciBsaSBhIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzk5OTk5OTtcbiAgbGluZS1oZWlnaHQ6IDI2cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIGxldHRlci1zcGFjaW5nOiAwcHg7XG59XG46aG9zdCBmb290ZXIgbGkgYTpob3ZlciB7XG4gIGNvbG9yOiAjZmZmO1xufVxuOmhvc3QgLnJpZ2h0cyAuZ28tdXAge1xuICBib3JkZXI6IDFweCBzb2xpZCAjNDU0NTQ1O1xuICBjb2xvcjogIzk0OTQ5NDtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMzBweDtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAwcHg7XG4gIGxlZnQ6IDBweDtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIGJvdHRvbTogLTYwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cbjpob3N0IC5yaWdodHMgLmdvLXVwOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgY29sb3I6ICMzMzM7XG59XG46aG9zdCAucmlnaHRzIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG4gIHBhZGRpbmc6IDMwcHg7XG4gIGNvbG9yOiAjOTY5NTk1O1xufVxuOmhvc3QgLnJpZ2h0cyBwIHtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgbGluZS1oZWlnaHQ6IDI2cHg7XG4gIGNvbG9yOiAjOTk5OTk5O1xufVxuOmhvc3QgLmxpZ2h0LXJpZ2h0cyB7XG4gIGJhY2tncm91bmQ6ICNlY2VhZWE7XG59XG46aG9zdCBmb290ZXIgLnNvY2lhbF9pY29ucyB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbjpob3N0IGZvb3RlciAuc29jaWFsX2ljb25zIGxpIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG46aG9zdCBmb290ZXIgLnNvY2lhbF9pY29ucyBhIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgbGluZS1oZWlnaHQ6IDM0cHg7XG4gIGNvbG9yOiAjOTQ5NDk0O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbjpob3N0IC5mb290ZXItaW5mbyBoNiB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgY29sb3I6ICNmZmY7XG4gIGxldHRlci1zcGFjaW5nOiAycHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG46aG9zdCAubmV3cy1sZXR0ZXIgcCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cbjpob3N0IGZvb3RlciAuc3Vic2NyaWJlIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4xKTtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwMCU7XG59XG46aG9zdCBmb290ZXIgLnN1YnNjcmliZSBidXR0b24ge1xuICBiYWNrZ3JvdW5kOiAjOTY1NDUyICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogbm9uZTtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW4tdG9wOiAtNDVweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogMCAyMHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgei1pbmRleDogOTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuOmhvc3QgZm9vdGVyIC5zdWJzY3JpYmUgaW5wdXQge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyOiBub25lO1xuICBoZWlnaHQ6IDQwcHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiLCBzYW5zLXNlcmlmO1xuICBwYWRkaW5nOiAwIDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn1cbjpob3N0IC5mb290ZXItaW5mbyAubGlua3MgbGkge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG46aG9zdCAuZm9vdGVyLWluZm8gLmxpbmtzIGxpIGE6YmVmb3JlIHtcbiAgY29udGVudDogXCLvhIVcIjtcbiAgZm9udC1mYW1pbHk6IEZvbnRBd2Vzb21lO1xuICBjb2xvcjogIzhlOGU4ZTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwcHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgcGFkZGluZzogMHB4O1xuICBmb250LXdlaWdodDogMTAwO1xufVxuOmhvc3QgLmZvb3Rlci1pbmZvIC5saW5rcyBsaSBhIHtcbiAgY29sb3I6ICM5OTk5OTk7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuOmhvc3QgLmZvb3Rlci1pbmZvIC5saW5rcyBsaSBhOmhvdmVyIHtcbiAgY29sb3I6ICNmZmY7XG59XG46aG9zdCAuZm9vdGVyLWluZm8gLnF1b3RlIC5mb3JtLWNvbnRyb2wge1xuICBiYWNrZ3JvdW5kOiBub25lO1xuICBib3JkZXItcmFkaXVzOiAwcHg7XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM0NTUxNWQ7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gIGhlaWdodDogNDBweDtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuOmhvc3QgLmZvb3Rlci1pbmZvIC5xdW90ZSB0ZXh0YXJlYS5mb3JtLWNvbnRyb2wge1xuICBoZWlnaHQ6IDgwcHg7XG59XG46aG9zdCAuZm9vdGVyLWluZm8gLnBlcnNvbmFsLWluZm8ge1xuICBtYXJnaW4tdG9wOiAzMHB4O1xufVxuOmhvc3QgLmZvb3Rlci1pbmZvIC5wZXJzb25hbC1pbmZvIGxpIHtcbiAgY29sb3I6ICM5OTk5OTk7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XG4gIHBhZGRpbmc6IDEwcHggMDtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXdlaWdodDogNTAwO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzQxNDE0MTtcbn1cbjpob3N0IC5mb290ZXItaW5mbyAuZmxpY2tlciB7XG4gIG1hcmdpbjogMCAtM3B4O1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuOmhvc3QgLmZvb3Rlci1pbmZvIC5mbGlja2VyIGE6aG92ZXIge1xuICBvcGFjaXR5OiAwLjU7XG59XG46aG9zdCAuZm9vdGVyLWluZm8gLmZsaWNrZXIgLm93bC1kb3RzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuOmhvc3QgLmZvb3Rlci1pbmZvIC5mbGlja2VyIC5vd2wtZG90cyAub3dsLWRvdCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgaGVpZ2h0OiA1cHg7XG4gIHdpZHRoOiA1cHg7XG4gIG1hcmdpbjogMCAycHg7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuOmhvc3QgLmZvb3Rlci1pbmZvIC5mbGlja2VyIC5vd2wtZG90cyAub3dsLWRvdC5hY3RpdmUge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuOmhvc3QgLmZvb3Rlci1pbmZvIC5wZXJzb25hbC1pbmZvIGxpIGkge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGNvbG9yOiAjOTI5MjkyO1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIGZsb2F0OiBsZWZ0O1xuICB3aWR0aDogMzBweDtcbiAgbWFyZ2luLXRvcDogM3B4O1xuICBtYXJnaW4tYm90dG9tOiA4cHg7XG59XG46aG9zdCAuZm9vdC1pbmZvLWNvbiB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDgwcHg7XG59XG46aG9zdCAuZm9vdC1pbmZvLWNvbiBsaSBhIHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogNDAwO1xufVxuOmhvc3QgLmZvb3QtaW5mby1jb24gLmljb24ge1xuICBoZWlnaHQ6IDkwcHg7XG4gIHdpZHRoOiA5MHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgYmFja2dyb3VuZDogIzNmNDk1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBsaW5lLWhlaWdodDogOTBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBib3JkZXItcmFkaXVzOiAwcHg7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG46aG9zdCAuZm9vdC1pbmZvLWNvbiBwIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBtYXJnaW46IDBweDtcbn1cbjpob3N0IC5mb290LWluZm8tY29uIGg2IHtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgZm9udC1mYW1pbHk6IFwiVWJ1bnR1XCI7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgbWFyZ2luLWJvdHRvbTogNHB4O1xuICBsZXR0ZXItc3BhY2luZzogMnB4O1xufVxuOmhvc3QgLmZvb3Rlci1saWdodCB7XG4gIGJhY2tncm91bmQ6ICNmOWY5Zjk7XG59XG46aG9zdCAuZm9vdGVyLWxpZ2h0IGg2IHtcbiAgY29sb3I6ICMzMzMzMzM7XG59XG46aG9zdCAuZm9vdGVyLWxpZ2h0IC5yaWdodHMge1xuICBtYXJnaW4tdG9wOiA2MHB4O1xuICBiYWNrZ3JvdW5kOiAjZWZlZWVlO1xufVxuOmhvc3QgLnNjcm9sbCB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbjpob3N0IGgzIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogMTAwO1xuICBjb2xvcjogI0ZGRkZGRjtcbn1cbjpob3N0IC5zbWFsbC1hYm91dCB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZDFkNWRiO1xufVxuOmhvc3QgLnNtYWxsLWFib3V0IC5zb2NpYWxfaWNvbnMge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cbjpob3N0IC5zbWFsbC1hYm91dCBsaSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cbjpob3N0IC5zbWFsbC1hYm91dCBsaSBhIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW46IDAgNXB4O1xuICBjb2xvcjogI2FkYWNhYztcbn1cbjpob3N0IC5zbWFsbC1hYm91dCBsaSBhOmhvdmVyIHtcbiAgY29sb3I6ICNmZmUzMzM7XG59XG46aG9zdCAuc21hbGwtYWJvdXQgcCB7XG4gIGZvbnQtZmFtaWx5OiBcIlBsYXlmYWlyIERpc3BsYXlcIiwgc2VyaWY7XG4gIGNvbG9yOiAjYWRhY2FjO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBtYXgtd2lkdGg6IDkwMHB4O1xuICBtYXJnaW46IDAgYXV0bztcbn0iLCI6aG9zdHtcbiAgZm9vdGVyIHAge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMjZweDtcbiAgICBjb2xvcjogIzk5OTk5OTtcbiAgfVxuICBmb290ZXIgaDYge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNDJweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMS41cHg7XG4gIH1cbiAgZm9vdGVyIHtcbiAgICBwYWRkaW5nOiAxMjBweCAwO1xuICAgIHBhZGRpbmctYm90dG9tOiA2MHB4O1xuICAgIGJhY2tncm91bmQ6ICM0MDNmM2Y7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHotaW5kZXg6IDE7XG4gIH1cbiAgLmFib3V0LWZvb3RlciBpIHtcbiAgICB3aWR0aDogMjBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIH1cbiAgLmFib3V0LWZvb3RlciBwIHtcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICB9XG4gIGZvb3RlciBsaSBhIHtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgY29sb3I6ICM5OTk5OTk7XG4gICAgbGluZS1oZWlnaHQ6IDI2cHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XG4gIH1cbiAgZm9vdGVyIGxpIGE6aG92ZXIge1xuICAgIGNvbG9yOiAjZmZmO1xuICB9XG4gIC5yaWdodHMgLmdvLXVwIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjNDU0NTQ1O1xuICAgIGNvbG9yOiAjOTQ5NDk0O1xuICAgIGhlaWdodDogMzBweDtcbiAgICB3aWR0aDogMzBweDtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAwcHg7XG4gICAgbGVmdDogMHB4O1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIGJvdHRvbTogLTYwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgfVxuICAucmlnaHRzIC5nby11cDpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBjb2xvcjogIzMzMztcbiAgfVxuICAucmlnaHRzIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG4gICAgcGFkZGluZzogMzBweDtcbiAgICBjb2xvcjogIzk2OTU5NTtcbiAgfVxuICAucmlnaHRzIHAge1xuICAgIG1hcmdpbjogMHB4O1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBsaW5lLWhlaWdodDogMjZweDtcbiAgICBjb2xvcjogIzk5OTk5OTtcbiAgfVxuICAubGlnaHQtcmlnaHRzIHtcbiAgICBiYWNrZ3JvdW5kOiAjZWNlYWVhO1xuICB9XG4gIGZvb3RlciAuc29jaWFsX2ljb25zIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgfVxuICBmb290ZXIgLnNvY2lhbF9pY29ucyBsaSB7XG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIH1cbiAgZm9vdGVyIC5zb2NpYWxfaWNvbnMgYSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIG1hcmdpbi1yaWdodDogMjBweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgbGluZS1oZWlnaHQ6IDM0cHg7XG4gICAgY29sb3I6ICM5NDk0OTQ7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICAuZm9vdGVyLWluZm8gaDYge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGxldHRlci1zcGFjaW5nOiAycHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICB9XG4gIC5uZXdzLWxldHRlciBwIHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gIH1cbiAgZm9vdGVyIC5zdWJzY3JpYmUge1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDI1NSwyNTUsMjU1LDAuMSk7XG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgZm9vdGVyIC5zdWJzY3JpYmUgYnV0dG9uIHtcbiAgICBiYWNrZ3JvdW5kOiAjOTY1NDUyICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBtYXJnaW4tdG9wOiAtNDVweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgcGFkZGluZzogMCAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICAgIHotaW5kZXg6IDk7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG4gIGZvb3RlciAuc3Vic2NyaWJlIGlucHV0IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQnLCBzYW5zLXNlcmlmO1xuICAgIHBhZGRpbmc6IDAgMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIH1cbiAgLmZvb3Rlci1pbmZvIC5saW5rcyBsaSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG4gIC5mb290ZXItaW5mbyAubGlua3MgbGkgYTpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXFxmMTA1IFwiO1xuICAgIGZvbnQtZmFtaWx5OiBGb250QXdlc29tZTtcbiAgICBjb2xvcjogIzhlOGU4ZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMHB4O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcbiAgfVxuICAuZm9vdGVyLWluZm8gLmxpbmtzIGxpIGEge1xuICAgIGNvbG9yOiAjOTk5OTk5O1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICB9XG4gIC5mb290ZXItaW5mbyAubGlua3MgbGkgYTpob3ZlciB7XG4gICAgY29sb3I6ICNmZmY7XG4gIH1cbiAgLmZvb3Rlci1pbmZvIC5xdW90ZSAuZm9ybS1jb250cm9sIHtcbiAgICBiYWNrZ3JvdW5kOiBub25lO1xuICAgIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM0NTUxNWQ7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICB9XG4gIC5mb290ZXItaW5mbyAucXVvdGUgdGV4dGFyZWEuZm9ybS1jb250cm9sIHtcbiAgICBoZWlnaHQ6IDgwcHg7XG4gIH1cbiAgLmZvb3Rlci1pbmZvIC5wZXJzb25hbC1pbmZvIHtcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xuICB9XG4gIC5mb290ZXItaW5mbyAucGVyc29uYWwtaW5mbyBsaSB7XG4gICAgY29sb3I6ICM5OTk5OTk7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xuICAgIHBhZGRpbmc6IDEwcHggMDtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjNDE0MTQxO1xuICB9XG4gIC5mb290ZXItaW5mbyAuZmxpY2tlciB7XG4gICAgbWFyZ2luOiAwIC0zcHg7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgfVxuICAuZm9vdGVyLWluZm8gLmZsaWNrZXIgYTpob3ZlciB7XG4gICAgb3BhY2l0eTogMC41O1xuICB9XG4gIC5mb290ZXItaW5mbyAuZmxpY2tlciAub3dsLWRvdHMge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICAuZm9vdGVyLWluZm8gLmZsaWNrZXIgLm93bC1kb3RzIC5vd2wtZG90IHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgaGVpZ2h0OiA1cHg7XG4gICAgd2lkdGg6IDVweDtcbiAgICBtYXJnaW46IDAgMnB4O1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LDI1NSwyNTUsMC4yKTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIH1cbiAgLmZvb3Rlci1pbmZvIC5mbGlja2VyIC5vd2wtZG90cyAub3dsLWRvdC5hY3RpdmUge1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gIH1cbiAgLmZvb3Rlci1pbmZvIC5wZXJzb25hbC1pbmZvIGxpIGkge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBjb2xvcjogIzkyOTI5MjtcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgd2lkdGg6IDMwcHg7XG4gICAgbWFyZ2luLXRvcDogM3B4O1xuICAgIG1hcmdpbi1ib3R0b206IDhweDtcbiAgfVxuICAuZm9vdC1pbmZvLWNvbiB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IDgwcHg7XG4gIH1cbiAgLmZvb3QtaW5mby1jb24gbGkgYSB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgfVxuICAuZm9vdC1pbmZvLWNvbiAuaWNvbiB7XG4gICAgaGVpZ2h0OiA5MHB4O1xuICAgIHdpZHRoOiA5MHB4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGJhY2tncm91bmQ6ICMzZjQ5NTU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGxpbmUtaGVpZ2h0OiA5MHB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIH1cbiAgLmZvb3QtaW5mby1jb24gcCB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIG1hcmdpbjogMHB4O1xuICB9XG4gIC5mb290LWluZm8tY29uIGg2IHtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtZmFtaWx5OiAnVWJ1bnR1JztcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgICBtYXJnaW4tYm90dG9tOiA0cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDJweDtcbiAgfVxuICAuZm9vdGVyLWxpZ2h0IHtcbiAgICBiYWNrZ3JvdW5kOiAjZjlmOWY5O1xuICB9XG4gIC5mb290ZXItbGlnaHQgaDYge1xuICAgIGNvbG9yOiAjMzMzMzMzO1xuICB9XG4gIC5mb290ZXItbGlnaHQgLnJpZ2h0cyB7XG4gICAgbWFyZ2luLXRvcDogNjBweDtcbiAgICBiYWNrZ3JvdW5kOiAjZWZlZWVlO1xuICB9XG4gICAgLnNjcm9sbHtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cblxuICAgIGgze1xuICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDEwMDtcbiAgICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgIH1cblxuICAgIC5zbWFsbC1hYm91dCB7XG4gICAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2QxZDVkYjtcbiAgICB9XG4gICAgLnNtYWxsLWFib3V0IC5zb2NpYWxfaWNvbnMge1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgIH1cbiAgICAuc21hbGwtYWJvdXQgbGkge1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIH1cbiAgICAuc21hbGwtYWJvdXQgbGkgYSB7XG4gICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICBtYXJnaW46IDAgNXB4O1xuICAgICAgY29sb3I6ICNhZGFjYWM7XG4gICAgfVxuICAgIC5zbWFsbC1hYm91dCBsaSBhOmhvdmVyIHtcbiAgICAgIGNvbG9yOiAjZmZlMzMzO1xuICAgIH1cbiAgICAuc21hbGwtYWJvdXQgcCB7XG4gICAgICBmb250LWZhbWlseTogJ1BsYXlmYWlyIERpc3BsYXknLCBzZXJpZjtcbiAgICAgIGNvbG9yOiAjYWRhY2FjO1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICBtYXgtd2lkdGg6IDkwMHB4O1xuICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgfVxufVxuIl19 */");

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent.prototype.totop = function () {
        var scrollToTop = window.setInterval(function () {
            var pos = window.pageYOffset;
            if (pos > 0) {
                window.scrollTo(0, pos - 20); // how far to scroll on each step
            }
            else {
                window.clearInterval(scrollToTop);
            }
        }, 5);
    };
    FooterComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/footer/footer.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./footer.component.scss */ "./src/app/footer/footer.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/forms.service.ts":
/*!**********************************!*\
  !*** ./src/app/forms.service.ts ***!
  \**********************************/
/*! exports provided: FormsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormsService", function() { return FormsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var FormsService = /** @class */ (function () {
    //infoClient: any;
    function FormsService() {
    }
    FormsService.prototype.saveInfo = function (info) {
        window.localStorage.setItem('infoClient', JSON.stringify(info));
    };
    FormsService.prototype.getInfo = function () {
        var info = JSON.parse(window.localStorage.getItem('infoClient'));
        //console.log(info);
        return info;
    };
    FormsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], FormsService);
    return FormsService;
}());



/***/ }),

/***/ "./src/app/header-breadcump/header-breadcump.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/header-breadcump/header-breadcump.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".sub-bnr {\n  background-position: center;\n  background-size: cover;\n}\n\n.sub-bnr h4 {\n  font-size: 30px;\n  text-shadow: 0px 5px 20px rgba(0, 0, 0, 0.7);\n}\n\n.sub-bnr p {\n  font-size: 18px;\n  font-family: \"Montserrat\", sans-serif;\n  color: #FFFFFf;\n  text-shadow: 0px 5px 20px rgba(0, 0, 0, 0.7);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9oZWFkZXItYnJlYWRjdW1wL2hlYWRlci1icmVhZGN1bXAuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2hlYWRlci1icmVhZGN1bXAvaGVhZGVyLWJyZWFkY3VtcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLDJCQUFBO0VBQ0Esc0JBQUE7QUNBSjs7QURLQTtFQUNJLGVBQUE7RUFDQSw0Q0FBQTtBQ0ZKOztBRE1BO0VBQ0ksZUFBQTtFQUNBLHFDQUFBO0VBQ0EsY0FBQTtFQUNBLDRDQUFBO0FDSEoiLCJmaWxlIjoic3JjL2FwcC9oZWFkZXItYnJlYWRjdW1wL2hlYWRlci1icmVhZGN1bXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3ViLWJucntcbiAgICBcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBcbiAgICBcbn1cblxuLnN1Yi1ibnIgaDR7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIHRleHQtc2hhZG93OiAwcHggNXB4IDIwcHggcmdiYSgwLDAsMCwwLjcpO1xuXG59XG5cbi5zdWItYm5yIHB7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCcsIHNhbnMtc2VyaWY7XG4gICAgY29sb3I6ICNGRkZGRmY7XG4gICAgdGV4dC1zaGFkb3c6IDBweCA1cHggMjBweCAgcmdiYSgwLDAsMCwwLjcpO1xufVxuIiwiLnN1Yi1ibnIge1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5zdWItYm5yIGg0IHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICB0ZXh0LXNoYWRvdzogMHB4IDVweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC43KTtcbn1cblxuLnN1Yi1ibnIgcCB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiLCBzYW5zLXNlcmlmO1xuICBjb2xvcjogI0ZGRkZGZjtcbiAgdGV4dC1zaGFkb3c6IDBweCA1cHggMjBweCByZ2JhKDAsIDAsIDAsIDAuNyk7XG59Il19 */");

/***/ }),

/***/ "./src/app/header-breadcump/header-breadcump.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/header-breadcump/header-breadcump.component.ts ***!
  \****************************************************************/
/*! exports provided: HeaderBreadcumpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderBreadcumpComponent", function() { return HeaderBreadcumpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var HeaderBreadcumpComponent = /** @class */ (function () {
    function HeaderBreadcumpComponent() {
    }
    HeaderBreadcumpComponent.prototype.ngOnInit = function () {
        this.mybg = this.bg;
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], HeaderBreadcumpComponent.prototype, "titulo", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], HeaderBreadcumpComponent.prototype, "bg", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], HeaderBreadcumpComponent.prototype, "descripcion", void 0);
    HeaderBreadcumpComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'header-breadcump',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./header-breadcump.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/header-breadcump/header-breadcump.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./header-breadcump.component.scss */ "./src/app/header-breadcump/header-breadcump.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], HeaderBreadcumpComponent);
    return HeaderBreadcumpComponent;
}());



/***/ }),

/***/ "./src/app/header/buscador/buscador.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/header/buscador/buscador.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".buscador-container {\n  width: 80vw;\n}\n\n.input-search {\n  font-family: \"Montserrat\", sans-serif;\n  color: #333;\n  font-size: 16px;\n  padding: 7px 60px 9px 15px;\n  border-radius: 5px 0px 0px 5px;\n  background-color: #fff;\n  border: 0 rgba(0, 0, 0, 0.2);\n  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.2);\n  height: 39px;\n}\n\n.btn-search {\n  background-color: #fff;\n  border: 0 rgba(0, 0, 0, 0.2);\n  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.2);\n  height: 39px;\n  padding: 0px 10px 0px 10px;\n  border-radius: 0px 5px 5px 0px;\n}\n\n.buscador-usados {\n  background-color: #000000;\n  width: 100%;\n  padding: 10px;\n  display: flex;\n  justify-items: center;\n  flex-direction: column;\n  align-items: center;\n  text-align: center;\n}\n\n.buscador-usados input {\n  width: 100%;\n  padding: 14px;\n  max-width: 1320px;\n  display: flex;\n  align-self: center;\n  font-size: 16px;\n  font-weight: 100;\n  border-radius: 5px;\n  border: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9oZWFkZXIvYnVzY2Fkb3IvYnVzY2Fkb3IuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2hlYWRlci9idXNjYWRvci9idXNjYWRvci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUNDSjs7QURDQTtFQUNJLHFDQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSwwQkFBQTtFQUNBLDhCQUFBO0VBRUEsc0JBQUE7RUFDQSw0QkFBQTtFQUNBLDBDQUFBO0VBQ0EsWUFBQTtBQ0NKOztBREdBO0VBQ0ksc0JBQUE7RUFDQSw0QkFBQTtFQUNBLDBDQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0EsOEJBQUE7QUNBSjs7QURHQTtFQUVDLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQyxhQUFBO0VBQ0EscUJBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNERjs7QURJQTtFQUVDLFdBQUE7RUFDQyxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7QUNGRiIsImZpbGUiOiJzcmMvYXBwL2hlYWRlci9idXNjYWRvci9idXNjYWRvci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idXNjYWRvci1jb250YWluZXJ7XG4gICAgd2lkdGg6IDgwdnc7XG59XG4uaW5wdXQtc2VhcmNoe1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCcsIHNhbnMtc2VyaWY7XG4gICAgY29sb3I6ICMzMzM7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIHBhZGRpbmc6IDdweCA2MHB4IDlweCAxNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweCAwcHggMHB4IDVweDtcblxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyOiAwIHJnYmEoMCwwLDAsLjIpO1xuICAgIGJveC1zaGFkb3c6IDAgMXB4IDJweCAwIHJnYmEoMCwwLDAsLjIpO1xuICAgIGhlaWdodDogMzlweDtcblxufVxuXG4uYnRuLXNlYXJjaHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICAgIGJvcmRlcjogMCByZ2JhKDAsMCwwLC4yKTtcbiAgICBib3gtc2hhZG93OiAwIDFweCAycHggMCByZ2JhKDAsMCwwLC4yKTtcbiAgICBoZWlnaHQ6IDM5cHg7XG4gICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4IDVweCA1cHggMHB4O1xufVxuXG4uYnVzY2Fkb3ItdXNhZG9ze1xuXG5cdGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDA7XG5cdHdpZHRoOiAxMDAlO1xuXHRwYWRkaW5nOiAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmJ1c2NhZG9yLXVzYWRvcyBpbnB1dHtcblxuXHR3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMTRweDtcbiAgbWF4LXdpZHRoOiAxMzIwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXdlaWdodDogMTAwO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJvcmRlcjogMDtcblxufVxuIiwiLmJ1c2NhZG9yLWNvbnRhaW5lciB7XG4gIHdpZHRoOiA4MHZ3O1xufVxuXG4uaW5wdXQtc2VhcmNoIHtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiLCBzYW5zLXNlcmlmO1xuICBjb2xvcjogIzMzMztcbiAgZm9udC1zaXplOiAxNnB4O1xuICBwYWRkaW5nOiA3cHggNjBweCA5cHggMTVweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4IDBweCAwcHggNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICBib3JkZXI6IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xuICBib3gtc2hhZG93OiAwIDFweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gIGhlaWdodDogMzlweDtcbn1cblxuLmJ0bi1zZWFyY2gge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICBib3JkZXI6IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xuICBib3gtc2hhZG93OiAwIDFweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gIGhlaWdodDogMzlweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDBweCA1cHggNXB4IDBweDtcbn1cblxuLmJ1c2NhZG9yLXVzYWRvcyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDA7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmJ1c2NhZG9yLXVzYWRvcyBpbnB1dCB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAxNHB4O1xuICBtYXgtd2lkdGg6IDEzMjBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24tc2VsZjogY2VudGVyO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYm9yZGVyOiAwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/header/buscador/buscador.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/header/buscador/buscador.component.ts ***!
  \*******************************************************/
/*! exports provided: BuscadorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuscadorComponent", function() { return BuscadorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var BuscadorComponent = /** @class */ (function () {
    function BuscadorComponent() {
        this.textoDeInput = null;
    }
    BuscadorComponent.prototype.ngOnInit = function () {
    };
    BuscadorComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-buscador',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./buscador.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/header/buscador/buscador.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./buscador.component.scss */ "./src/app/header/buscador/buscador.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], BuscadorComponent);
    return BuscadorComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.scss":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("header {\n  background-color: #FFFFFF;\n  position: -webkit-sticky;\n  position: sticky;\n  box-shadow: 0 0.063rem 0.313rem 0 rgba(70, 70, 70, 0.15), 0 0.125rem 0.125rem 0 rgba(70, 70, 70, 0.15), 0 0.188rem 0.125rem -0.125rem rgba(70, 70, 70, 0.15);\n}\n\n.top-nav {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-start;\n  /*border: 5px red solid;*/\n  padding-top: 10px;\n}\n\n.top-nav div {\n  /*border: 1px blue solid;*/\n  margin: 0px;\n  padding: 0px;\n}\n\n.nav-header-mobile {\n  width: 100%;\n  display: flex;\n  justify-content: space-around;\n}\n\n.nav-header-mobile li {\n  width: 50%;\n  margin: 0 20px;\n}\n\n.nav-header-mobile li:first-child {\n  text-align: left;\n}\n\n.nav-header-mobile li:last-child {\n  text-align: right;\n}\n\n.logo-mobile {\n  display: none;\n}\n\n.nav-logo {\n  width: 16vw;\n  text-align: left;\n  overflow: hidden;\n}\n\n.nav-buscador {\n  text-align: center;\n  width: 70vw;\n}\n\n.nav-botones {\n  text-align: center;\n  width: 100%;\n}\n\n.nav-item a {\n  font-weight: bold;\n  font-size: 16px;\n}\n\n.input-search {\n  font-family: \"Montserrat\", sans-serif;\n  color: #333;\n  font-size: 16px;\n  padding: 7px 60px 9px 15px;\n  border-radius: 5px 0px 0px 5px;\n  background-color: #fff;\n  border: 0 rgba(0, 0, 0, 0.2);\n  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.2);\n  height: 39px;\n  width: 90%;\n}\n\n.btn-search {\n  background-color: #fff;\n  border: 0 rgba(0, 0, 0, 0.2);\n  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.2);\n  height: 39px;\n  padding: 0px 10px 0px 10px;\n  border-radius: 0px 5px 5px 0px;\n}\n\n.mybadge {\n  display: inline-block;\n  color: white;\n  background-color: #333;\n  width: 20px;\n  font-size: 12px;\n  border-radius: 50%;\n  margin-left: 2px;\n}\n\n.navbar {\n  text-align: center !important;\n  padding: 14px 0;\n}\n\n.navbar-expand-md {\n  justify-content: flex-end !important;\n}\n\n.navbar .nav-options {\n  /*margin: auto;*/\n}\n\n.dropdown-menu {\n  background-color: #333;\n  padding: 0px;\n  transition: 0.5s ease-in-out;\n}\n\n.dropdown-item {\n  color: #ffffff;\n  display: block;\n  border-bottom: 1px solid #999;\n  padding: 4px 10px 4px 10px;\n  margin: 0px;\n}\n\n.dropdown-item:hover {\n  color: #333;\n  background-color: #fff;\n}\n\n.nav-options {\n  overflow: hidden;\n}\n\n@media (max-width: 770px) {\n  .nav-logo {\n    display: none;\n  }\n\n  .logo-mobile {\n    display: block;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSx3QkFBQTtFQUFBLGdCQUFBO0VBRUEsNEpBQUE7QUNBSjs7QURJQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtBQ0RKOztBRElBO0VBQ0ksMEJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ0RKOztBREtBO0VBQ0UsV0FBQTtFQUNBLGFBQUE7RUFDQSw2QkFBQTtBQ0ZGOztBREdFO0VBQUksVUFBQTtFQUFZLGNBQUE7QUNDbEI7O0FEQUU7RUFBZ0IsZ0JBQUE7QUNHbEI7O0FERkU7RUFBZSxpQkFBQTtBQ0tqQjs7QUREQTtFQUNFLGFBQUE7QUNJRjs7QUREQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDSUo7O0FEREE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7QUNJSjs7QUREQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtBQ0lKOztBRERBO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0FDSUY7O0FEREE7RUFDSSxxQ0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsMEJBQUE7RUFDQSw4QkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSwwQ0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0FDSUo7O0FEREE7RUFDSSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0EsMENBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSw4QkFBQTtBQ0lKOztBRERBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNJSjs7QURDQTtFQUNJLDZCQUFBO0VBQ0EsZUFBQTtBQ0VKOztBREVBO0VBQ0ksb0NBQUE7QUNDSjs7QURFQTtFQUNJLGdCQUFBO0FDQ0o7O0FESUE7RUFDSSxzQkFBQTtFQUNBLFlBQUE7RUFDQSw0QkFBQTtBQ0RKOztBRElBO0VBQ1EsY0FBQTtFQUNBLGNBQUE7RUFDQSw2QkFBQTtFQUNBLDBCQUFBO0VBQ0EsV0FBQTtBQ0RSOztBREtBO0VBQ0ksV0FBQTtFQUNBLHNCQUFBO0FDRko7O0FES0E7RUFDRSxnQkFBQTtBQ0ZGOztBREtBO0VBQ0U7SUFDRSxhQUFBO0VDRkY7O0VESUE7SUFDRSxjQUFBO0VDREY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoZWFkZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6I0ZGRkZGRjtcbiAgICBwb3NpdGlvbjogc3RpY2t5O1xuXG4gICAgYm94LXNoYWRvdzogMCAwLjA2M3JlbSAwLjMxM3JlbSAwIHJnYig3MCA3MCA3MCAvIDE1JSksIDAgMC4xMjVyZW0gMC4xMjVyZW0gMCByZ2IoNzAgNzAgNzAgLyAxNSUpLCAwIDAuMTg4cmVtIDAuMTI1cmVtIC0wLjEyNXJlbSByZ2IoNzAgNzAgNzAgLyAxNSUpO1xufVxuXG5cbi50b3AtbmF2e1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgLypib3JkZXI6IDVweCByZWQgc29saWQ7Ki9cbiAgICBwYWRkaW5nLXRvcDogMTBweDtcbn1cblxuLnRvcC1uYXYgZGl2e1xuICAgIC8qYm9yZGVyOiAxcHggYmx1ZSBzb2xpZDsqL1xuICAgIG1hcmdpbjogMHB4O1xuICAgIHBhZGRpbmc6IDBweDtcbn1cblxuXG4ubmF2LWhlYWRlci1tb2JpbGV7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgbGl7IHdpZHRoOiA1MCU7IG1hcmdpbjogMCAyMHB4O31cbiAgbGk6Zmlyc3QtY2hpbGR7IHRleHQtYWxpZ246IGxlZnQ7fVxuICBsaTpsYXN0LWNoaWxkeyB0ZXh0LWFsaWduOiByaWdodDt9XG5cbn1cblxuLmxvZ28tbW9iaWxle1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4ubmF2LWxvZ297XG4gICAgd2lkdGg6IDE2dnc7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4ubmF2LWJ1c2NhZG9ye1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogNzB2dztcbn1cblxuLm5hdi1ib3RvbmVze1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLm5hdi1pdGVtIGF7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5pbnB1dC1zZWFyY2h7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0Jywgc2Fucy1zZXJpZjtcbiAgICBjb2xvcjogIzMzMztcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgcGFkZGluZzogN3B4IDYwcHggOXB4IDE1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4IDBweCAwcHggNXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyOiAwIHJnYmEoMCwwLDAsLjIpO1xuICAgIGJveC1zaGFkb3c6IDAgMXB4IDJweCAwIHJnYmEoMCwwLDAsLjIpO1xuICAgIGhlaWdodDogMzlweDtcbiAgICB3aWR0aDogOTAlO1xufVxuXG4uYnRuLXNlYXJjaHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICAgIGJvcmRlcjogMCByZ2JhKDAsMCwwLC4yKTtcbiAgICBib3gtc2hhZG93OiAwIDFweCAycHggMCByZ2JhKDAsMCwwLC4yKTtcbiAgICBoZWlnaHQ6IDM5cHg7XG4gICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4IDVweCA1cHggMHB4O1xufVxuXG4ubXliYWRnZXtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzMzM7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBtYXJnaW4tbGVmdDogMnB4O1xuXG59XG5cblxuLm5hdmJhcntcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAxNHB4IDA7XG59XG5cblxuLm5hdmJhci1leHBhbmQtbWQge1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQgIWltcG9ydGFudDtcbn1cblxuLm5hdmJhciAubmF2LW9wdGlvbnN7XG4gICAgLyptYXJnaW46IGF1dG87Ki9cbn1cblxuXG5cbi5kcm9wZG93bi1tZW51e1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzMzM7XG4gICAgcGFkZGluZzogMHB4O1xuICAgIHRyYW5zaXRpb246IDAuNXMgZWFzZS1pbi1vdXQ7XG59XG5cbi5kcm9wZG93bi1pdGVte1xuICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjOTk5O1xuICAgICAgICBwYWRkaW5nOiA0cHggMTBweCA0cHggMTBweDtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG5cbn1cblxuLmRyb3Bkb3duLWl0ZW06aG92ZXJ7XG4gICAgY29sb3I6ICMzMzM7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn1cblxuLm5hdi1vcHRpb25ze1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogNzcwcHgpIHtcbiAgLm5hdi1sb2dvIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG4gIC5sb2dvLW1vYmlsZXtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxufVxuIiwiaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbiAgcG9zaXRpb246IHN0aWNreTtcbiAgYm94LXNoYWRvdzogMCAwLjA2M3JlbSAwLjMxM3JlbSAwIHJnYmEoNzAsIDcwLCA3MCwgMC4xNSksIDAgMC4xMjVyZW0gMC4xMjVyZW0gMCByZ2JhKDcwLCA3MCwgNzAsIDAuMTUpLCAwIDAuMTg4cmVtIDAuMTI1cmVtIC0wLjEyNXJlbSByZ2JhKDcwLCA3MCwgNzAsIDAuMTUpO1xufVxuXG4udG9wLW5hdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgLypib3JkZXI6IDVweCByZWQgc29saWQ7Ki9cbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG5cbi50b3AtbmF2IGRpdiB7XG4gIC8qYm9yZGVyOiAxcHggYmx1ZSBzb2xpZDsqL1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4O1xufVxuXG4ubmF2LWhlYWRlci1tb2JpbGUge1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG59XG4ubmF2LWhlYWRlci1tb2JpbGUgbGkge1xuICB3aWR0aDogNTAlO1xuICBtYXJnaW46IDAgMjBweDtcbn1cbi5uYXYtaGVhZGVyLW1vYmlsZSBsaTpmaXJzdC1jaGlsZCB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG4ubmF2LWhlYWRlci1tb2JpbGUgbGk6bGFzdC1jaGlsZCB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG4ubG9nby1tb2JpbGUge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4ubmF2LWxvZ28ge1xuICB3aWR0aDogMTZ2dztcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLm5hdi1idXNjYWRvciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDcwdnc7XG59XG5cbi5uYXYtYm90b25lcyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5uYXYtaXRlbSBhIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cblxuLmlucHV0LXNlYXJjaCB7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXRcIiwgc2Fucy1zZXJpZjtcbiAgY29sb3I6ICMzMzM7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgcGFkZGluZzogN3B4IDYwcHggOXB4IDE1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweCAwcHggMHB4IDVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgYm9yZGVyOiAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm94LXNoYWRvdzogMCAxcHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xuICBoZWlnaHQ6IDM5cHg7XG4gIHdpZHRoOiA5MCU7XG59XG5cbi5idG4tc2VhcmNoIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgYm9yZGVyOiAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm94LXNoYWRvdzogMCAxcHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xuICBoZWlnaHQ6IDM5cHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAwcHggNXB4IDVweCAwcHg7XG59XG5cbi5teWJhZGdlIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBjb2xvcjogd2hpdGU7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzMzM7XG4gIHdpZHRoOiAyMHB4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbn1cblxuLm5hdmJhciB7XG4gIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiAxNHB4IDA7XG59XG5cbi5uYXZiYXItZXhwYW5kLW1kIHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZCAhaW1wb3J0YW50O1xufVxuXG4ubmF2YmFyIC5uYXYtb3B0aW9ucyB7XG4gIC8qbWFyZ2luOiBhdXRvOyovXG59XG5cbi5kcm9wZG93bi1tZW51IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzMzMztcbiAgcGFkZGluZzogMHB4O1xuICB0cmFuc2l0aW9uOiAwLjVzIGVhc2UtaW4tb3V0O1xufVxuXG4uZHJvcGRvd24taXRlbSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBkaXNwbGF5OiBibG9jaztcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM5OTk7XG4gIHBhZGRpbmc6IDRweCAxMHB4IDRweCAxMHB4O1xuICBtYXJnaW46IDBweDtcbn1cblxuLmRyb3Bkb3duLWl0ZW06aG92ZXIge1xuICBjb2xvcjogIzMzMztcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn1cblxuLm5hdi1vcHRpb25zIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDc3MHB4KSB7XG4gIC5uYXYtbG9nbyB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIC5sb2dvLW1vYmlsZSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cbn0iXX0= */");

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm5/router.js");
/* harmony import */ var _shop_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../shop-cart.service */ "./src/app/shop-cart.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");





var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(scs, rs, router) {
        this.scs = scs;
        this.rs = rs;
        this.router = router;
        this.textoDeInput = '';
        this.statusMenu = true;
        this.statusCategoria = false;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.statusMenu = false;
        this.statusCategoria = false;
        this.getCategorias();
        this.router.events.subscribe(function (val) { return _this.closeDropDowns(val); });
    };
    HeaderComponent.prototype.closeDropDowns = function (val) {
        this.statusMenu = false;
        this.statusCategoria = false;
    };
    HeaderComponent.prototype.clickEventMenu = function () {
        this.statusMenu = !this.statusMenu;
    };
    HeaderComponent.prototype.clickEventCategorias = function () {
        this.statusCategoria = !this.statusCategoria;
    };
    HeaderComponent.prototype.getCantidad = function () {
        return this.scs.getCantidad();
    };
    HeaderComponent.prototype.getCategorias = function () {
        var _this = this;
        this.rs.getCategories().subscribe(function (data) { return _this.categorias = data; }, function (error) { return console.log(error); });
    };
    HeaderComponent.prototype.onSubmit = function (f) {
        this.router.navigateByUrl('/busqueda/?query=' + this.textoDeInput);
    };
    HeaderComponent.prototype.clickEventNosotros = function () {
    };
    HeaderComponent.ctorParameters = function () { return [
        { type: _shop_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShopCartService"] },
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_4__["ReplacementService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
    ]; };
    HeaderComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-header',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/header/header.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./header.component.scss */ "./src/app/header/header.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_shop_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShopCartService"],
            _service_replacement_service__WEBPACK_IMPORTED_MODULE_4__["ReplacementService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/home/buy-truck/buy-truck.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/home/buy-truck/buy-truck.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  background-color: #FFFFFF;\n  margin-bottom: 40px;\n}\n:host h1 {\n  text-align: center;\n  margin-bottom: 40px;\n}\n:host .mejores .card {\n  border: 0px;\n}\n:host .mejores .card p {\n  font-weight: bold;\n  font-size: 18px;\n  text-align: center;\n  color: black;\n}\n:host .button-yellow {\n  margin: 30px;\n  text-align: center;\n}\n:host .button-yellow a {\n  display: inline-block;\n  border-radius: 8px;\n  background-color: #f3840d;\n  color: #FFFFFF;\n  font-size: 18px;\n  padding: 10px 35px;\n  font-weight: 500;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9ob21lL2J1eS10cnVjay9idXktdHJ1Y2suY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2hvbWUvYnV5LXRydWNrL2J1eS10cnVjay5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVFLHlCQUFBO0VBQ0EsbUJBQUE7QUNBRjtBREVFO0VBQ0Usa0JBQUE7RUFDQSxtQkFBQTtBQ0FKO0FEUUk7RUFFRSxXQUFBO0FDUE47QURVTTtFQUFHLGlCQUFBO0VBQW1CLGVBQUE7RUFBaUIsa0JBQUE7RUFBb0IsWUFBQTtBQ0pqRTtBRFFFO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0FDTko7QURRSTtFQUNFLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ05OIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9idXktdHJ1Y2svYnV5LXRydWNrLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3R7XG5cbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcblxuICBoMXtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLWJvdHRvbTogNDBweDtcbiAgfVxuXG5cbiAgLm1lam9yZXN7XG5cblxuXG4gICAgLmNhcmR7XG5cbiAgICAgIGJvcmRlcjogMHB4O1xuXG5cbiAgICAgIHB7IGZvbnQtd2VpZ2h0OiBib2xkOyBmb250LXNpemU6IDE4cHg7IHRleHQtYWxpZ246IGNlbnRlcjsgY29sb3I6IGJsYWNrO31cbiAgICB9XG4gIH1cblxuICAuYnV0dG9uLXllbGxvd3tcbiAgICBtYXJnaW46IDMwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgYXtcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMzg0MGQ7XG4gICAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIHBhZGRpbmc6IDEwcHggMzVweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgfVxuXG4gIH1cbn1cbiIsIjpob3N0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcbn1cbjpob3N0IGgxIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xufVxuOmhvc3QgLm1lam9yZXMgLmNhcmQge1xuICBib3JkZXI6IDBweDtcbn1cbjpob3N0IC5tZWpvcmVzIC5jYXJkIHAge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiBibGFjaztcbn1cbjpob3N0IC5idXR0b24teWVsbG93IHtcbiAgbWFyZ2luOiAzMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG46aG9zdCAuYnV0dG9uLXllbGxvdyBhIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzg0MGQ7XG4gIGNvbG9yOiAjRkZGRkZGO1xuICBmb250LXNpemU6IDE4cHg7XG4gIHBhZGRpbmc6IDEwcHggMzVweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/home/buy-truck/buy-truck.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/home/buy-truck/buy-truck.component.ts ***!
  \*******************************************************/
/*! exports provided: BuyTruckComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuyTruckComponent", function() { return BuyTruckComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var BuyTruckComponent = /** @class */ (function () {
    function BuyTruckComponent() {
    }
    BuyTruckComponent.prototype.ngOnInit = function () {
    };
    BuyTruckComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'buy-truck',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./buy-truck.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/buy-truck/buy-truck.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./buy-truck.component.scss */ "./src/app/home/buy-truck/buy-truck.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], BuyTruckComponent);
    return BuyTruckComponent;
}());



/***/ }),

/***/ "./src/app/home/buyorsell/buyorsell.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/home/buyorsell/buyorsell.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".buy-your-truck {\n  background-color: #FFF;\n  text-align: center;\n}\n.buy-your-truck img {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9ob21lL2J1eW9yc2VsbC9idXlvcnNlbGwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2hvbWUvYnV5b3JzZWxsL2J1eW9yc2VsbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHNCQUFBO0VBQ0Esa0JBQUE7QUNDRjtBRENFO0VBQ0UsV0FBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9idXlvcnNlbGwvYnV5b3JzZWxsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ1eS15b3VyLXRydWNre1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgaW1ne1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59XG4iLCIuYnV5LXlvdXItdHJ1Y2sge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYnV5LXlvdXItdHJ1Y2sgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/home/buyorsell/buyorsell.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/home/buyorsell/buyorsell.component.ts ***!
  \*******************************************************/
/*! exports provided: BuyorsellComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuyorsellComponent", function() { return BuyorsellComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var BuyorsellComponent = /** @class */ (function () {
    function BuyorsellComponent() {
    }
    BuyorsellComponent.prototype.ngOnInit = function () {
    };
    BuyorsellComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-buyorsell',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./buyorsell.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/buyorsell/buyorsell.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./buyorsell.component.scss */ "./src/app/home/buyorsell/buyorsell.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], BuyorsellComponent);
    return BuyorsellComponent;
}());



/***/ }),

/***/ "./src/app/home/frontproducts/frontproducts.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/home/frontproducts/frontproducts.component.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvZnJvbnRwcm9kdWN0cy9mcm9udHByb2R1Y3RzLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/home/frontproducts/frontproducts.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/home/frontproducts/frontproducts.component.ts ***!
  \***************************************************************/
/*! exports provided: FrontproductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrontproductsComponent", function() { return FrontproductsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/replacement.service */ "./src/app/service/replacement.service.ts");



var FrontproductsComponent = /** @class */ (function () {
    function FrontproductsComponent(rs) {
        this.rs = rs;
    }
    FrontproductsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.rs.getDestacado().subscribe(function (data) {
            _this.destacados = data;
        }, function (error) { console.log(error); });
    };
    FrontproductsComponent.ctorParameters = function () { return [
        { type: src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"] }
    ]; };
    FrontproductsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-frontproducts',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./frontproducts.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/frontproducts/frontproducts.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./frontproducts.component.scss */ "./src/app/home/frontproducts/frontproducts.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"]])
    ], FrontproductsComponent);
    return FrontproductsComponent;
}());



/***/ }),

/***/ "./src/app/home/home-seguro/home-seguro.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/home/home-seguro/home-seguro.component.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".img-home-seguro {\n  padding: 20px;\n  background-color: #FFFFFF;\n}\n.img-home-seguro a img {\n  border: 1px #EFEFEF solid;\n  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.12);\n  border-radius: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9ob21lL2hvbWUtc2VndXJvL2hvbWUtc2VndXJvLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9ob21lL2hvbWUtc2VndXJvL2hvbWUtc2VndXJvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLHlCQUFBO0FDQ0o7QURDSTtFQUNJLHlCQUFBO0VBQ0EsMkNBQUE7RUFDQSxrQkFBQTtBQ0NSIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLXNlZ3Vyby9ob21lLXNlZ3Vyby5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbWctaG9tZS1zZWd1cm97XG4gICAgcGFkZGluZzogMjBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGRkZGO1xuXG4gICAgYSBpbWd7XG4gICAgICAgIGJvcmRlcjogMXB4ICNFRkVGRUYgc29saWQ7IFxuICAgICAgICBib3gtc2hhZG93OiAwIDFweCAycHggMCByZ2JhKDAsMCwwLC4xMik7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICB9XG5cbn0iLCIuaW1nLWhvbWUtc2VndXJvIHtcbiAgcGFkZGluZzogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbn1cbi5pbWctaG9tZS1zZWd1cm8gYSBpbWcge1xuICBib3JkZXI6IDFweCAjRUZFRkVGIHNvbGlkO1xuICBib3gtc2hhZG93OiAwIDFweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/home/home-seguro/home-seguro.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/home/home-seguro/home-seguro.component.ts ***!
  \***********************************************************/
/*! exports provided: HomeSeguroComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeSeguroComponent", function() { return HomeSeguroComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var HomeSeguroComponent = /** @class */ (function () {
    function HomeSeguroComponent() {
    }
    HomeSeguroComponent.prototype.ngOnInit = function () {
    };
    HomeSeguroComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'home-seguro',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home-seguro.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home-seguro/home-seguro.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home-seguro.component.scss */ "./src/app/home/home-seguro/home-seguro.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], HomeSeguroComponent);
    return HomeSeguroComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");



var HomeComponent = /** @class */ (function () {
    function HomeComponent(replacementService) {
        this.replacementService = replacementService;
    }
    /*
      obtenerOfertas(){
        this.replacementService.getOffers().subscribe( response => {
          this.ofertas = response;
        },
        error => {
          console.log(JSON.stringify(error));
        });
      }
    
      obtenerCategorias(){
        this.replacementService.getCategories().subscribe( resultado => {
          this.categorias = resultado;
        },
        error => {
          console.log(JSON.stringify(error));
        });
      }
    */
    HomeComponent.prototype.ngOnInit = function () {
        // this.obtenerOfertas();
        //  this.obtenerCategorias();
        //  this.tituloCategorias = "CATEGORIAS POPULARES";
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"] }
    ]; };
    HomeComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/home/logos/logos.component.scss":
/*!*************************************************!*\
  !*** ./src/app/home/logos/logos.component.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".logos {\n  background-color: #f6f6f6;\n  text-align: center;\n  padding: 10px;\n  margin-bottom: 70px;\n  box-shadow: 0px 22px 27px -6px rgba(0, 0, 0, 0.4);\n  -webkit-box-shadow: 0px 22px 27px -6px rgba(0, 0, 0, 0.4);\n  -moz-box-shadow: 0px 22px 27px -6px rgba(0, 0, 0, 0.4);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9ob21lL2xvZ29zL2xvZ29zLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9ob21lL2xvZ29zL2xvZ29zLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUEseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGlEQUFBO0VBQ0EseURBQUE7RUFDQSxzREFBQTtBQ0FBIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9sb2dvcy9sb2dvcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dvc3tcblxuYmFja2dyb3VuZC1jb2xvcjojZjZmNmY2O1xudGV4dC1hbGlnbjogY2VudGVyO1xucGFkZGluZzogMTBweDtcbm1hcmdpbi1ib3R0b206IDcwcHg7XG5ib3gtc2hhZG93OiAwcHggMjJweCAyN3B4IC02cHggcmdiYSgwLDAsMCwwLjQpO1xuLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMjJweCAyN3B4IC02cHggcmdiYSgwLDAsMCwwLjQpO1xuLW1vei1ib3gtc2hhZG93OiAwcHggMjJweCAyN3B4IC02cHggcmdiYSgwLDAsMCwwLjQpO1xuXG59XG4iLCIubG9nb3Mge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjZmNmY2O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDcwcHg7XG4gIGJveC1zaGFkb3c6IDBweCAyMnB4IDI3cHggLTZweCByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDIycHggMjdweCAtNnB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgLW1vei1ib3gtc2hhZG93OiAwcHggMjJweCAyN3B4IC02cHggcmdiYSgwLCAwLCAwLCAwLjQpO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/home/logos/logos.component.ts":
/*!***********************************************!*\
  !*** ./src/app/home/logos/logos.component.ts ***!
  \***********************************************/
/*! exports provided: LogosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogosComponent", function() { return LogosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var LogosComponent = /** @class */ (function () {
    function LogosComponent() {
    }
    LogosComponent.prototype.ngOnInit = function () {
    };
    LogosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'home-logos',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./logos.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/logos/logos.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./logos.component.scss */ "./src/app/home/logos/logos.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], LogosComponent);
    return LogosComponent;
}());



/***/ }),

/***/ "./src/app/home/newsletter/newsletter.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/home/newsletter/newsletter.component.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host .news-letter {\n  background-color: #000206;\n}\n:host .header-text {\n  display: block;\n  text-align: center;\n}\n:host .header-text p {\n  text-align: center;\n  font-size: 20px;\n  margin-top: 5px;\n  color: #FFF;\n}\n:host .header-text h4 {\n  font-size: 55px;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif !important;\n  text-transform: none !important;\n  color: #FFF;\n}\n:host .header-text h4 span {\n  color: #f3840d;\n}\n:host .news-letter form {\n  max-width: 770px;\n  margin: 0 auto;\n}\n:host .inputs {\n  display: flex;\n  justify-content: space-between;\n}\n:host .lema {\n  margin: 20px;\n  display: block;\n  text-align: center;\n  font-size: 14px;\n  font-weight: 100;\n  color: #a6a6a6;\n}\n:host .news-letter form input {\n  height: 53px;\n  width: 35%;\n  padding: 0 20px;\n  display: inline;\n  font-size: 12px;\n  float: left;\n  background-color: #a6a6a6;\n  border: 0px;\n  color: #FFF !important;\n}\n:host .news-letter form button {\n  height: 53px;\n  float: right;\n  border: 0px;\n  color: #fff;\n  font-size: 13px;\n  font-weight: bold;\n  background: none;\n  width: 25%;\n  background-color: #f3840d;\n}\n:host .news-letter form button:hover {\n  background: #fff;\n  color: #131920;\n}\n:host .news-letter.style-2 {\n  background: #5b53a6;\n}\n:host .news-letter.style-2 .heading span {\n  color: #fff;\n}\n:host .news-letter.style-2 form input {\n  border-radius: 50px;\n  border: none;\n}\n:host .news-letter.style-2 form button {\n  border-radius: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9ob21lL25ld3NsZXR0ZXIvbmV3c2xldHRlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvaG9tZS9uZXdzbGV0dGVyL25ld3NsZXR0ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQU87RUFFTCx5QkFBQTtBQ0FGO0FER0E7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7QUNERjtBREVFO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNBSjtBREtBO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsK0NBQUE7RUFDQSwrQkFBQTtFQUNBLFdBQUE7QUNIRjtBRElFO0VBQ0UsY0FBQTtBQ0ZKO0FET0E7RUFDQyxnQkFBQTtFQUNBLGNBQUE7QUNMRDtBRFFBO0VBQ0UsYUFBQTtFQUNBLDhCQUFBO0FDTkY7QURVQTtFQUNFLFlBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FDUkY7QURVQTtFQUNDLFlBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNDLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0FDUkY7QURVQTtFQUNDLFlBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLFVBQUE7RUFDQyx5QkFBQTtBQ1JGO0FEVUE7RUFDQyxnQkFBQTtFQUNBLGNBQUE7QUNSRDtBRFVBO0VBQ0MsbUJBQUE7QUNSRDtBRFVBO0VBQ0MsV0FBQTtBQ1JEO0FEVUE7RUFDQyxtQkFBQTtFQUNBLFlBQUE7QUNSRDtBRFVBO0VBQ0MsbUJBQUE7QUNSRCIsImZpbGUiOiJzcmMvYXBwL2hvbWUvbmV3c2xldHRlci9uZXdzbGV0dGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qgey5uZXdzLWxldHRlcntcblxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMjA2O1xufVxuXG4uaGVhZGVyLXRleHR7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgY29sb3I6ICNGRkY7XG4gIH1cblxufVxuXG4uaGVhZGVyLXRleHQgaDR7XG4gIGZvbnQtc2l6ZTogNTVweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmICFpbXBvcnRhbnQ7XG4gIHRleHQtdHJhbnNmb3JtOiBub25lICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjRkZGO1xuICBzcGFue1xuICAgIGNvbG9yOiAjZjM4NDBkO1xuICB9XG5cbn1cblxuLm5ld3MtbGV0dGVyIGZvcm0ge1xuXHRtYXgtd2lkdGg6IDc3MHB4O1xuXHRtYXJnaW46IDAgYXV0bztcbn1cblxuLmlucHV0c3tcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuXG59XG5cbi5sZW1he1xuICBtYXJnaW46IDIwcHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDEwMDtcbiAgY29sb3I6ICNhNmE2YTY7XG59XG4ubmV3cy1sZXR0ZXIgZm9ybSBpbnB1dCB7XG5cdGhlaWdodDogNTNweDtcblx0d2lkdGg6IDM1JTtcblx0cGFkZGluZzogMCAyMHB4O1xuXHRkaXNwbGF5OiBpbmxpbmU7XG5cdGZvbnQtc2l6ZTogMTJweDtcblx0ZmxvYXQ6IGxlZnQ7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhNmE2YTY7XG4gIGJvcmRlcjogMHB4O1xuICBjb2xvcjogI0ZGRiAhaW1wb3J0YW50O1xufVxuLm5ld3MtbGV0dGVyIGZvcm0gYnV0dG9uIHtcblx0aGVpZ2h0OiA1M3B4O1xuXHRmbG9hdDogcmlnaHQ7XG5cdGJvcmRlcjogMHB4O1xuXHRjb2xvcjogI2ZmZjtcblx0Zm9udC1zaXplOiAxM3B4O1xuXHRmb250LXdlaWdodDogYm9sZDtcblx0YmFja2dyb3VuZDogbm9uZTtcblx0d2lkdGg6IDI1JTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzODQwZDtcbn1cbi5uZXdzLWxldHRlciBmb3JtIGJ1dHRvbjpob3ZlciB7XG5cdGJhY2tncm91bmQ6ICNmZmY7XG5cdGNvbG9yOiAjMTMxOTIwO1xufVxuLm5ld3MtbGV0dGVyLnN0eWxlLTIge1xuXHRiYWNrZ3JvdW5kOiAjNWI1M2E2O1xufVxuLm5ld3MtbGV0dGVyLnN0eWxlLTIgLmhlYWRpbmcgc3BhbiB7XG5cdGNvbG9yOiAjZmZmO1xufVxuLm5ld3MtbGV0dGVyLnN0eWxlLTIgZm9ybSBpbnB1dCB7XG5cdGJvcmRlci1yYWRpdXM6IDUwcHg7XG5cdGJvcmRlcjogbm9uZTtcbn1cbi5uZXdzLWxldHRlci5zdHlsZS0yIGZvcm0gYnV0dG9uIHtcblx0Ym9yZGVyLXJhZGl1czogNTBweDtcbn1cbn1cbiIsIjpob3N0IC5uZXdzLWxldHRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAyMDY7XG59XG46aG9zdCAuaGVhZGVyLXRleHQge1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuOmhvc3QgLmhlYWRlci10ZXh0IHAge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBjb2xvcjogI0ZGRjtcbn1cbjpob3N0IC5oZWFkZXItdGV4dCBoNCB7XG4gIGZvbnQtc2l6ZTogNTVweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWYgIWltcG9ydGFudDtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmUgIWltcG9ydGFudDtcbiAgY29sb3I6ICNGRkY7XG59XG46aG9zdCAuaGVhZGVyLXRleHQgaDQgc3BhbiB7XG4gIGNvbG9yOiAjZjM4NDBkO1xufVxuOmhvc3QgLm5ld3MtbGV0dGVyIGZvcm0ge1xuICBtYXgtd2lkdGg6IDc3MHB4O1xuICBtYXJnaW46IDAgYXV0bztcbn1cbjpob3N0IC5pbnB1dHMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG46aG9zdCAubGVtYSB7XG4gIG1hcmdpbjogMjBweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogMTAwO1xuICBjb2xvcjogI2E2YTZhNjtcbn1cbjpob3N0IC5uZXdzLWxldHRlciBmb3JtIGlucHV0IHtcbiAgaGVpZ2h0OiA1M3B4O1xuICB3aWR0aDogMzUlO1xuICBwYWRkaW5nOiAwIDIwcHg7XG4gIGRpc3BsYXk6IGlubGluZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmbG9hdDogbGVmdDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2E2YTZhNjtcbiAgYm9yZGVyOiAwcHg7XG4gIGNvbG9yOiAjRkZGICFpbXBvcnRhbnQ7XG59XG46aG9zdCAubmV3cy1sZXR0ZXIgZm9ybSBidXR0b24ge1xuICBoZWlnaHQ6IDUzcHg7XG4gIGZsb2F0OiByaWdodDtcbiAgYm9yZGVyOiAwcHg7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDEzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBiYWNrZ3JvdW5kOiBub25lO1xuICB3aWR0aDogMjUlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjM4NDBkO1xufVxuOmhvc3QgLm5ld3MtbGV0dGVyIGZvcm0gYnV0dG9uOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgY29sb3I6ICMxMzE5MjA7XG59XG46aG9zdCAubmV3cy1sZXR0ZXIuc3R5bGUtMiB7XG4gIGJhY2tncm91bmQ6ICM1YjUzYTY7XG59XG46aG9zdCAubmV3cy1sZXR0ZXIuc3R5bGUtMiAuaGVhZGluZyBzcGFuIHtcbiAgY29sb3I6ICNmZmY7XG59XG46aG9zdCAubmV3cy1sZXR0ZXIuc3R5bGUtMiBmb3JtIGlucHV0IHtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgYm9yZGVyOiBub25lO1xufVxuOmhvc3QgLm5ld3MtbGV0dGVyLnN0eWxlLTIgZm9ybSBidXR0b24ge1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/home/newsletter/newsletter.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/home/newsletter/newsletter.component.ts ***!
  \*********************************************************/
/*! exports provided: NewsletterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsletterComponent", function() { return NewsletterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/replacement.service */ "./src/app/service/replacement.service.ts");



var NewsletterComponent = /** @class */ (function () {
    function NewsletterComponent(rs) {
        this.rs = rs;
        this.enviado = false;
    }
    NewsletterComponent.prototype.ngOnInit = function () {
    };
    NewsletterComponent.prototype.suscripcion = function (form) {
        var _this = this;
        form.value.contact_type = "Suscripcion";
        console.log(form.value);
        this.rs.enviarmensaje(form.value).subscribe(function (data) {
            form.reset();
            _this.enviado = true;
            //alert("Mensaje enviado")
        }, function (error) {
            alert("Intente mas tarde");
        });
    };
    NewsletterComponent.ctorParameters = function () { return [
        { type: src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"] }
    ]; };
    NewsletterComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-newsletter',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./newsletter.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/newsletter/newsletter.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./newsletter.component.scss */ "./src/app/home/newsletter/newsletter.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"]])
    ], NewsletterComponent);
    return NewsletterComponent;
}());



/***/ }),

/***/ "./src/app/home/popularproducts/popularproducts.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/home/popularproducts/popularproducts.component.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvcG9wdWxhcnByb2R1Y3RzL3BvcHVsYXJwcm9kdWN0cy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/home/popularproducts/popularproducts.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/home/popularproducts/popularproducts.component.ts ***!
  \*******************************************************************/
/*! exports provided: PopularproductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopularproductsComponent", function() { return PopularproductsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/replacement.service */ "./src/app/service/replacement.service.ts");



var PopularproductsComponent = /** @class */ (function () {
    function PopularproductsComponent(replacementService) {
        this.replacementService = replacementService;
    }
    PopularproductsComponent.prototype.obtenerDestacados = function () {
        var _this = this;
        var limit = 4;
        var offset = 0;
        this.replacementService.getFeatured(limit, offset).subscribe(function (resultado) {
            _this.productos = resultado;
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    PopularproductsComponent.prototype.ngOnInit = function () {
        this.obtenerDestacados();
    };
    PopularproductsComponent.ctorParameters = function () { return [
        { type: src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"] }
    ]; };
    PopularproductsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'popular-products',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./popularproducts.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/popularproducts/popularproducts.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./popularproducts.component.scss */ "./src/app/home/popularproducts/popularproducts.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"]])
    ], PopularproductsComponent);
    return PopularproductsComponent;
}());



/***/ }),

/***/ "./src/app/home/repuestos/repuestos.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/home/repuestos/repuestos.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host .repuestos {\n  text-align: center;\n}\n:host .repuestos img {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9ob21lL3JlcHVlc3Rvcy9yZXB1ZXN0b3MuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2hvbWUvcmVwdWVzdG9zL3JlcHVlc3Rvcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFRTtFQUNFLGtCQUFBO0FDREo7QURHSTtFQUNFLFdBQUE7QUNETiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvcmVwdWVzdG9zL3JlcHVlc3Rvcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0e1xuXG4gIC5yZXB1ZXN0b3N7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgaW1ne1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuXG4gIH1cblxuXG5cblxufVxuIiwiOmhvc3QgLnJlcHVlc3RvcyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbjpob3N0IC5yZXB1ZXN0b3MgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/home/repuestos/repuestos.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/home/repuestos/repuestos.component.ts ***!
  \*******************************************************/
/*! exports provided: RepuestosMujerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RepuestosMujerComponent", function() { return RepuestosMujerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var RepuestosMujerComponent = /** @class */ (function () {
    function RepuestosMujerComponent() {
    }
    RepuestosMujerComponent.prototype.ngOnInit = function () {
    };
    RepuestosMujerComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-repuestos-mujer',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./repuestos.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/repuestos/repuestos.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./repuestos.component.scss */ "./src/app/home/repuestos/repuestos.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], RepuestosMujerComponent);
    return RepuestosMujerComponent;
}());



/***/ }),

/***/ "./src/app/home/sedes/sedes.component.scss":
/*!*************************************************!*\
  !*** ./src/app/home/sedes/sedes.component.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".sedes {\n  background-color: #FFFFFF;\n  text-align: center;\n}\n.sedes img {\n  width: 90%;\n  max-width: 900px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9ob21lL3NlZGVzL3NlZGVzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9ob21lL3NlZGVzL3NlZGVzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UseUJBQUE7RUFDQSxrQkFBQTtBQ0NGO0FEQUU7RUFBSyxVQUFBO0VBQ0gsZ0JBQUE7QUNHSiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvc2VkZXMvc2VkZXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2VkZXN7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGRkZGRkY7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgaW1neyB3aWR0aDogOTAlO1xuICAgIG1heC13aWR0aDogOTAwcHg7XG4gIH1cbn1cbiIsIi5zZWRlcyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGRkZGRkY7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5zZWRlcyBpbWcge1xuICB3aWR0aDogOTAlO1xuICBtYXgtd2lkdGg6IDkwMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/home/sedes/sedes.component.ts":
/*!***********************************************!*\
  !*** ./src/app/home/sedes/sedes.component.ts ***!
  \***********************************************/
/*! exports provided: SedesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SedesComponent", function() { return SedesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var SedesComponent = /** @class */ (function () {
    function SedesComponent() {
    }
    SedesComponent.prototype.ngOnInit = function () {
    };
    SedesComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sedes',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./sedes.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/sedes/sedes.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./sedes.component.scss */ "./src/app/home/sedes/sedes.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], SedesComponent);
    return SedesComponent;
}());



/***/ }),

/***/ "./src/app/home/slider/slider.component.scss":
/*!***************************************************!*\
  !*** ./src/app/home/slider/slider.component.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".home-slider {\n  position: relative;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9ob21lL3NsaWRlci9zbGlkZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2hvbWUvc2xpZGVyL3NsaWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGtCQUFBO0FDQ0QiLCJmaWxlIjoic3JjL2FwcC9ob21lL3NsaWRlci9zbGlkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaG9tZS1zbGlkZXIge1xuXHRwb3NpdGlvbjogcmVsYXRpdmU7XG59IiwiLmhvbWUtc2xpZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/home/slider/slider.component.ts":
/*!*************************************************!*\
  !*** ./src/app/home/slider/slider.component.ts ***!
  \*************************************************/
/*! exports provided: SliderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SliderComponent", function() { return SliderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var SliderComponent = /** @class */ (function () {
    function SliderComponent() {
        this.customOptions = {
            loop: true,
            mouseDrag: false,
            touchDrag: false,
            pullDrag: false,
            dots: false,
            autoplay: true,
            navSpeed: 700,
            nav: false,
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                740: {
                    items: 1
                },
                940: {
                    items: 1
                }
            }
        };
    }
    SliderComponent.prototype.ngOnInit = function () {
    };
    SliderComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-slider',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./slider.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/slider/slider.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./slider.component.scss */ "./src/app/home/slider/slider.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], SliderComponent);
    return SliderComponent;
}());



/***/ }),

/***/ "./src/app/list-categoria/list-categoria.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/list-categoria/list-categoria.component.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xpc3QtY2F0ZWdvcmlhL2xpc3QtY2F0ZWdvcmlhLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/list-categoria/list-categoria.component.ts":
/*!************************************************************!*\
  !*** ./src/app/list-categoria/list-categoria.component.ts ***!
  \************************************************************/
/*! exports provided: ListCategoriaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListCategoriaComponent", function() { return ListCategoriaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var ListCategoriaComponent = /** @class */ (function () {
    function ListCategoriaComponent() {
    }
    /*obtenerCategorias(){
      this.replacementService.getCategories().subscribe( resultado => {
        this.categorias = resultado;
      },
      error => {
        console.log(JSON.stringify(error));
      });
    }*/
    ListCategoriaComponent.prototype.ngOnInit = function () {
        //this.obtenerCategorias();
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", String)
    ], ListCategoriaComponent.prototype, "titulo", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], ListCategoriaComponent.prototype, "categorias", void 0);
    ListCategoriaComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'list-categoria',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./list-categoria.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/list-categoria/list-categoria.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./list-categoria.component.scss */ "./src/app/list-categoria/list-categoria.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], ListCategoriaComponent);
    return ListCategoriaComponent;
}());



/***/ }),

/***/ "./src/app/list-oferta/list-oferta.component.scss":
/*!********************************************************!*\
  !*** ./src/app/list-oferta/list-oferta.component.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".arrival-block {\n  margin: 10px;\n  width: auto;\n  border: 0px;\n}\n\n.item {\n  margin-bottom: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9saXN0LW9mZXJ0YS9saXN0LW9mZXJ0YS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGlzdC1vZmVydGEvbGlzdC1vZmVydGEuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9saXN0LW9mZXJ0YS9saXN0LW9mZXJ0YS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hcnJpdmFsLWJsb2Nre1xuICAgIG1hcmdpbjogMTBweDsgXG4gICAgd2lkdGg6IGF1dG87XG4gICAgYm9yZGVyOiAwcHg7XG59XG5cbi5pdGVte1xuICAgIG1hcmdpbi1ib3R0b206IDBweDtcbn0iLCIuYXJyaXZhbC1ibG9jayB7XG4gIG1hcmdpbjogMTBweDtcbiAgd2lkdGg6IGF1dG87XG4gIGJvcmRlcjogMHB4O1xufVxuXG4uaXRlbSB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/list-oferta/list-oferta.component.ts":
/*!******************************************************!*\
  !*** ./src/app/list-oferta/list-oferta.component.ts ***!
  \******************************************************/
/*! exports provided: ListOfertaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOfertaComponent", function() { return ListOfertaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var ListOfertaComponent = /** @class */ (function () {
    function ListOfertaComponent() {
    }
    ListOfertaComponent.prototype.ngOnInit = function () {
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], ListOfertaComponent.prototype, "ofertas", void 0);
    ListOfertaComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'list-oferta',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./list-oferta.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/list-oferta/list-oferta.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./list-oferta.component.scss */ "./src/app/list-oferta/list-oferta.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], ListOfertaComponent);
    return ListOfertaComponent;
}());



/***/ }),

/***/ "./src/app/nosotros/nosotros.component.scss":
/*!**************************************************!*\
  !*** ./src/app/nosotros/nosotros.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25vc290cm9zL25vc290cm9zLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/nosotros/nosotros.component.ts":
/*!************************************************!*\
  !*** ./src/app/nosotros/nosotros.component.ts ***!
  \************************************************/
/*! exports provided: NosotrosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NosotrosComponent", function() { return NosotrosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var NosotrosComponent = /** @class */ (function () {
    function NosotrosComponent() {
    }
    NosotrosComponent.prototype.ngOnInit = function () {
    };
    NosotrosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-nosotros',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./nosotros.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/nosotros/nosotros.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./nosotros.component.scss */ "./src/app/nosotros/nosotros.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], NosotrosComponent);
    return NosotrosComponent;
}());



/***/ }),

/***/ "./src/app/oferta/oferta.component.scss":
/*!**********************************************!*\
  !*** ./src/app/oferta/oferta.component.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL29mZXJ0YS9vZmVydGEuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/oferta/oferta.component.ts":
/*!********************************************!*\
  !*** ./src/app/oferta/oferta.component.ts ***!
  \********************************************/
/*! exports provided: OfertaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfertaComponent", function() { return OfertaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm5/router.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");




var OfertaComponent = /** @class */ (function () {
    function OfertaComponent(rutaActiva, replacementService) {
        this.rutaActiva = rutaActiva;
        this.replacementService = replacementService;
    }
    OfertaComponent.prototype.obtenerOferta = function (idParam) {
        var _this = this;
        this.replacementService.getOffer(idParam).subscribe(function (resultado) {
            _this.oferta = resultado;
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    OfertaComponent.prototype.obtenerProductosOferta = function (idParam) {
        var _this = this;
        this.replacementService.getOfferProducts(idParam).subscribe(function (resultado) {
            _this.productos = resultado;
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    OfertaComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.idParam = this.rutaActiva.snapshot.params.id;
        this.rutaActiva.params.subscribe(function (params) {
            _this.idParam = params.id;
            _this.obtenerOferta(_this.idParam);
            _this.obtenerProductosOferta(_this.idParam);
        });
    };
    OfertaComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"] }
    ]; };
    OfertaComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-oferta',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./oferta.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/oferta/oferta.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./oferta.component.scss */ "./src/app/oferta/oferta.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"]])
    ], OfertaComponent);
    return OfertaComponent;
}());



/***/ }),

/***/ "./src/app/ofertas/ofertas.component.sass":
/*!************************************************!*\
  !*** ./src/app/ofertas/ofertas.component.sass ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL29mZXJ0YXMvb2ZlcnRhcy5jb21wb25lbnQuc2FzcyJ9 */");

/***/ }),

/***/ "./src/app/ofertas/ofertas.component.ts":
/*!**********************************************!*\
  !*** ./src/app/ofertas/ofertas.component.ts ***!
  \**********************************************/
/*! exports provided: OfertasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfertasComponent", function() { return OfertasComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");



var OfertasComponent = /** @class */ (function () {
    function OfertasComponent(replacementService) {
        this.replacementService = replacementService;
    }
    OfertasComponent.prototype.obtenerOfertas = function () {
        var _this = this;
        this.replacementService.getOffers().subscribe(function (response) {
            _this.ofertas = response;
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    OfertasComponent.prototype.ngOnInit = function () {
        this.obtenerOfertas();
    };
    OfertasComponent.ctorParameters = function () { return [
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"] }
    ]; };
    OfertasComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ofertas',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./ofertas.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/ofertas/ofertas.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./ofertas.component.sass */ "./src/app/ofertas/ofertas.component.sass")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"]])
    ], OfertasComponent);
    return OfertasComponent;
}());



/***/ }),

/***/ "./src/app/pipes/replace-thumb-for-original.pipe.ts":
/*!**********************************************************!*\
  !*** ./src/app/pipes/replace-thumb-for-original.pipe.ts ***!
  \**********************************************************/
/*! exports provided: ReplaceThumbForOriginalPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReplaceThumbForOriginalPipe", function() { return ReplaceThumbForOriginalPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var ReplaceThumbForOriginalPipe = /** @class */ (function () {
    function ReplaceThumbForOriginalPipe() {
    }
    ReplaceThumbForOriginalPipe.prototype.transform = function (value, change) {
        if (value == undefined) {
            value = 'assest/img/bg1.png';
        }
        if (value == '') {
            value = 'assest/img/bg1.png';
        }
        if (change == '') {
            change = 'original';
        }
        if (change == undefined) {
            change = 'original';
        }
        return value.replace('thumb', change);
    };
    ReplaceThumbForOriginalPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'replaceThumbForOriginal'
        })
    ], ReplaceThumbForOriginalPipe);
    return ReplaceThumbForOriginalPipe;
}());



/***/ }),

/***/ "./src/app/producto/description/description.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/producto/description/description.component.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RvL2Rlc2NyaXB0aW9uL2Rlc2NyaXB0aW9uLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/producto/description/description.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/producto/description/description.component.ts ***!
  \***************************************************************/
/*! exports provided: DescriptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescriptionComponent", function() { return DescriptionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var DescriptionComponent = /** @class */ (function () {
    function DescriptionComponent() {
    }
    DescriptionComponent.prototype.ngOnInit = function () {
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], DescriptionComponent.prototype, "descripcion", void 0);
    DescriptionComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-description',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./description.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/producto/description/description.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./description.component.scss */ "./src/app/producto/description/description.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], DescriptionComponent);
    return DescriptionComponent;
}());



/***/ }),

/***/ "./src/app/producto/detalle/detalle.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/producto/detalle/detalle.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RvL2RldGFsbGUvZGV0YWxsZS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/producto/detalle/detalle.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/producto/detalle/detalle.component.ts ***!
  \*******************************************************/
/*! exports provided: DetalleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleComponent", function() { return DetalleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var DetalleComponent = /** @class */ (function () {
    function DetalleComponent() {
    }
    DetalleComponent.prototype.ngOnInit = function () {
    };
    DetalleComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalle',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./detalle.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/producto/detalle/detalle.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./detalle.component.scss */ "./src/app/producto/detalle/detalle.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], DetalleComponent);
    return DetalleComponent;
}());



/***/ }),

/***/ "./src/app/producto/producto.component.scss":
/*!**************************************************!*\
  !*** ./src/app/producto/producto.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host h4 {\n  font-size: 1.75rem;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n}\n:host .sub-bnr {\n  background-image: url(\"/assets/img/bg2.png\");\n}\n:host #btn_submit {\n  padding: 5px;\n  width: 100%;\n}\n:host h5 {\n  margin-bottom: 10px;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n}\n:host h6 {\n  font-size: 14px;\n  letter-spacing: normal;\n  text-transform: none;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n}\n:host .btn-black {\n  background-color: #f3840d;\n  color: #FFF;\n  border-radius: 5px;\n  margin: 10px 0;\n  padding: 10px 30px;\n  border: 0px;\n  width: 80%;\n}\n:host .anio {\n  margin: 0;\n  font-weight: 500;\n  font-size: 16px;\n  color: #000;\n}\n:host .price {\n  text-align: left;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 30px;\n  margin-bottom: 30px;\n  margin-top: 0;\n  color: #f3840d;\n}\n:host .price span {\n  font-size: 14px;\n  color: #000206;\n  font-weight: normal;\n}\n:host .caracteristicas {\n  margin: 30px 0;\n}\n:host .caracteristicas h3 {\n  text-align: center;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 45px;\n  margin-bottom: 30px;\n}\n:host .caracteristicas h4 {\n  margin-top: 20px;\n  text-align: left;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 25px;\n}\n:host .caracteristicas .info {\n  display: flex;\n  justify-content: space-between;\n  font-family: \"Open Sans\", sans-serif;\n  font-weight: 500;\n  font-size: 17px;\n  color: #000206;\n  border-bottom: 1.5px solid #000206;\n}\n:host .caracteristicas .info span:first-child {\n  width: 50%;\n  text-align: left;\n}\n:host .caracteristicas .info span:last-child {\n  width: 50%;\n  text-align: right;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9wcm9kdWN0by9wcm9kdWN0by5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcHJvZHVjdG8vcHJvZHVjdG8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUU7RUFDRSxrQkFBQTtFQUVBLGdCQUFBO0VBQWtCLG9DQUFBO0FDRHRCO0FESUU7RUFDSSw0Q0FBQTtBQ0ZOO0FES0U7RUFDSSxZQUFBO0VBQ0EsV0FBQTtBQ0hOO0FET0U7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0VBQWtCLG9DQUFBO0FDSnhCO0FET0U7RUFDSSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQWtCLG9DQUFBO0FDSnhCO0FEU0U7RUFDRSx5QkFBQTtFQUNKLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FDUEE7QURVRTtFQUFNLFNBQUE7RUFBVyxnQkFBQTtFQUFrQixlQUFBO0VBQWlCLFdBQUE7QUNKdEQ7QURNRTtFQUNFLGdCQUFBO0VBQWtCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGVBQUE7RUFBaUIsbUJBQUE7RUFDM0YsYUFBQTtFQUNBLGNBQUE7QUNBSjtBREVJO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQ0FOO0FESUU7RUFFRSxjQUFBO0FDSEo7QURLSTtFQUFJLGtCQUFBO0VBQW9CLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGVBQUE7RUFBaUIsbUJBQUE7QUNFckc7QURFSTtFQUFJLGdCQUFBO0VBQWtCLGdCQUFBO0VBQWtCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGVBQUE7QUNLcEc7QURISTtFQUNFLGFBQUE7RUFDQSw4QkFBQTtFQUNBLG9DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtDQUFBO0FDS047QURITTtFQUFrQixVQUFBO0VBQVksZ0JBQUE7QUNPcEM7QUROTTtFQUFpQixVQUFBO0VBQVksaUJBQUE7QUNVbkMiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0by9wcm9kdWN0by5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0e1xuXG4gIGg0e1xuICAgIGZvbnQtc2l6ZTogMS43NXJlbTtcblxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7IGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcbiAgfVxuXG4gIC5zdWItYm5ye1xuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL2ltZy9iZzIucG5nJyk7XG4gIH1cblxuICAjYnRuX3N1Ym1pdHtcbiAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICB9XG5cblxuICBoNXtcbiAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICBmb250LXdlaWdodDogODAwOyBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XG4gIH1cblxuICBoNntcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gICAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgICAgIGZvbnQtd2VpZ2h0OiA4MDA7IGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcblxuICB9XG5cblxuICAuYnRuLWJsYWNre1xuICAgIGJhY2tncm91bmQtY29sb3I6I2YzODQwZDtcbmNvbG9yOiAjRkZGO1xuYm9yZGVyLXJhZGl1czogNXB4O1xubWFyZ2luOiAxMHB4IDA7XG5wYWRkaW5nOiAxMHB4IDMwcHg7XG5ib3JkZXI6IDBweDtcbndpZHRoOiA4MCU7XG5cbiAgfVxuICAuYW5pb3ttYXJnaW46IDA7IGZvbnQtd2VpZ2h0OiA1MDA7IGZvbnQtc2l6ZTogMTZweDsgY29sb3I6ICMwMDA7ICB9XG5cbiAgLnByaWNle1xuICAgIHRleHQtYWxpZ246IGxlZnQ7IGZvbnQtd2VpZ2h0OiA4MDA7IGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjsgZm9udC1zaXplOiAzMHB4OyBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgY29sb3I6ICNmMzg0MGQ7XG5cbiAgICBzcGFue1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgY29sb3I6IzAwMDIwNjtcbiAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgfVxuICB9XG5cbiAgLmNhcmFjdGVyaXN0aWNhc3tcblxuICAgIG1hcmdpbjogMzBweCAwO1xuXG4gICAgaDN7IHRleHQtYWxpZ246IGNlbnRlcjsgZm9udC13ZWlnaHQ6IDgwMDsgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmOyBmb250LXNpemU6IDQ1cHg7IG1hcmdpbi1ib3R0b206IDMwcHg7fVxuXG5cblxuICAgIGg0eyBtYXJnaW4tdG9wOiAyMHB4OyB0ZXh0LWFsaWduOiBsZWZ0OyBmb250LXdlaWdodDogODAwOyBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7IGZvbnQtc2l6ZTogMjVweDt9XG5cbiAgICAuaW5mb3tcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XG4gICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgICAgY29sb3I6ICMwMDAyMDY7XG4gICAgICBib3JkZXItYm90dG9tOiAxLjVweCBzb2xpZCAjMDAwMjA2O1xuXG4gICAgICBzcGFuOmZpcnN0LWNoaWxkeyB3aWR0aDogNTAlOyB0ZXh0LWFsaWduOiBsZWZ0O31cbiAgICAgIHNwYW46bGFzdC1jaGlsZHsgd2lkdGg6IDUwJTsgdGV4dC1hbGlnbjogcmlnaHQ7fVxuICAgIH1cblxuICB9XG5cbn1cbiIsIjpob3N0IGg0IHtcbiAgZm9udC1zaXplOiAxLjc1cmVtO1xuICBmb250LXdlaWdodDogODAwO1xuICBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjtcbn1cbjpob3N0IC5zdWItYm5yIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWcvYmcyLnBuZ1wiKTtcbn1cbjpob3N0ICNidG5fc3VibWl0IHtcbiAgcGFkZGluZzogNXB4O1xuICB3aWR0aDogMTAwJTtcbn1cbjpob3N0IGg1IHtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7XG59XG46aG9zdCBoNiB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xufVxuOmhvc3QgLmJ0bi1ibGFjayB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzg0MGQ7XG4gIGNvbG9yOiAjRkZGO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIG1hcmdpbjogMTBweCAwO1xuICBwYWRkaW5nOiAxMHB4IDMwcHg7XG4gIGJvcmRlcjogMHB4O1xuICB3aWR0aDogODAlO1xufVxuOmhvc3QgLmFuaW8ge1xuICBtYXJnaW46IDA7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgY29sb3I6ICMwMDA7XG59XG46aG9zdCAucHJpY2Uge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogODAwO1xuICBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICBtYXJnaW4tdG9wOiAwO1xuICBjb2xvcjogI2YzODQwZDtcbn1cbjpob3N0IC5wcmljZSBzcGFuIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzAwMDIwNjtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbjpob3N0IC5jYXJhY3RlcmlzdGljYXMge1xuICBtYXJnaW46IDMwcHggMDtcbn1cbjpob3N0IC5jYXJhY3RlcmlzdGljYXMgaDMge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDQ1cHg7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG46aG9zdCAuY2FyYWN0ZXJpc3RpY2FzIGg0IHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cbjpob3N0IC5jYXJhY3RlcmlzdGljYXMgLmluZm8ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xuICBmb250LXdlaWdodDogNTAwO1xuICBmb250LXNpemU6IDE3cHg7XG4gIGNvbG9yOiAjMDAwMjA2O1xuICBib3JkZXItYm90dG9tOiAxLjVweCBzb2xpZCAjMDAwMjA2O1xufVxuOmhvc3QgLmNhcmFjdGVyaXN0aWNhcyAuaW5mbyBzcGFuOmZpcnN0LWNoaWxkIHtcbiAgd2lkdGg6IDUwJTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cbjpob3N0IC5jYXJhY3RlcmlzdGljYXMgLmluZm8gc3BhbjpsYXN0LWNoaWxkIHtcbiAgd2lkdGg6IDUwJTtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/producto/producto.component.ts":
/*!************************************************!*\
  !*** ./src/app/producto/producto.component.ts ***!
  \************************************************/
/*! exports provided: ProductoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductoComponent", function() { return ProductoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm5/router.js");
/* harmony import */ var _forms_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../forms.service */ "./src/app/forms.service.ts");
/* harmony import */ var _shop_cart_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shop-cart.service */ "./src/app/shop-cart.service.ts");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");






var ProductoComponent = /** @class */ (function () {
    function ProductoComponent(rutaActiva, replacementService, shopCart, fs) {
        this.rutaActiva = rutaActiva;
        this.replacementService = replacementService;
        this.shopCart = shopCart;
        this.fs = fs;
        this.cantSelected = 1;
        this.showMessage = false;
        this.enviado = false;
    }
    ProductoComponent.prototype.obtenerProducto = function (id) {
        var _this = this;
        this.replacementService.getProduct(id).subscribe(function (resultado) {
            _this.producto = resultado;
            console.log(resultado);
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    ProductoComponent.prototype.obtenerImagenesProducto = function (id) {
        var _this = this;
        this.replacementService.getProductImages(id).subscribe(function (resultado) {
            _this.imagenes = resultado;
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    ProductoComponent.prototype.obtenerOfertas = function () {
        var _this = this;
        this.replacementService.getOffers().subscribe(function (response) {
            _this.ofertas = response;
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    ProductoComponent.prototype.addToCart = function () {
        var _this = this;
        this.shopCart.addToCart(this.producto, this.cantSelected);
        this.showMessage = true;
        setTimeout(function () {
            _this.showMessage = false;
        }, 2000);
        return false;
    };
    ProductoComponent.prototype.addToCartAndMove = function () {
        this.shopCart.addToCart(this.producto, this.cantSelected);
    };
    ProductoComponent.prototype.getCart = function () {
        this.shopCart.getProducts();
    };
    ProductoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.idParam = this.rutaActiva.snapshot.params.id;
        this.rutaActiva.params.subscribe(function (params) {
            _this.idParam = params.id;
            _this.obtenerProducto(params.id);
            _this.obtenerImagenesProducto(params.id);
            //this.obtenerOfertas();
        });
        this.getInfoContacto();
    };
    ProductoComponent.prototype.getOldPrice = function () {
        return this.producto.replacement_price;
    };
    ProductoComponent.prototype.isOffer = function () {
        return this.producto.offer_percentage ? true : false;
    };
    ProductoComponent.prototype.getNewPrice = function () {
        if (this.producto.offer_percentage)
            return this.producto.replacement_price * (100 - parseInt(this.producto.offer_percentage)) / 100;
        else
            return this.producto.replacement_price;
    };
    ProductoComponent.prototype.contacto = function (form) {
        var _this = this;
        form.value.contact_type = "Sin stock";
        form.value.contact_msj = "id repuesto: " + this.producto.replacement_id + " " + this.producto.replacement_name;
        console.log(form.value);
        this.replacementService.enviarmensaje(form.value).subscribe(function (data) {
            form.reset();
            _this.enviado = true;
            //alert("Mensaje enviado")
        }, function (error) {
            alert("Intente mas tarde");
        });
        var info = {
            "contact_name": form.value.contact_name,
            "contact_email": form.value.contact_email,
            "contact_phone": form.value.contact_phone,
            "contact_city": form.value.contact_city
        };
        this.fs.saveInfo(info);
    };
    ProductoComponent.prototype.getInfoContacto = function () {
        this.infoClient = this.fs.getInfo();
    };
    ProductoComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_5__["ReplacementService"] },
        { type: _shop_cart_service__WEBPACK_IMPORTED_MODULE_4__["ShopCartService"] },
        { type: _forms_service__WEBPACK_IMPORTED_MODULE_3__["FormsService"] }
    ]; };
    ProductoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-producto',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./producto.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/producto/producto.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./producto.component.scss */ "./src/app/producto/producto.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _service_replacement_service__WEBPACK_IMPORTED_MODULE_5__["ReplacementService"], _shop_cart_service__WEBPACK_IMPORTED_MODULE_4__["ShopCartService"], _forms_service__WEBPACK_IMPORTED_MODULE_3__["FormsService"]])
    ], ProductoComponent);
    return ProductoComponent;
}());



/***/ }),

/***/ "./src/app/repuestos/filtro-repuestos/filtro-repuestos.component.sass":
/*!****************************************************************************!*\
  !*** ./src/app/repuestos/filtro-repuestos/filtro-repuestos.component.sass ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlcHVlc3Rvcy9maWx0cm8tcmVwdWVzdG9zL2ZpbHRyby1yZXB1ZXN0b3MuY29tcG9uZW50LnNhc3MifQ== */");

/***/ }),

/***/ "./src/app/repuestos/filtro-repuestos/filtro-repuestos.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/repuestos/filtro-repuestos/filtro-repuestos.component.ts ***!
  \**************************************************************************/
/*! exports provided: FiltroRepuestosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltroRepuestosComponent", function() { return FiltroRepuestosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/replacement.service */ "./src/app/service/replacement.service.ts");




var FiltroRepuestosComponent = /** @class */ (function () {
    function FiltroRepuestosComponent(replacementService) {
        this.replacementService = replacementService;
        this.filtros = [];
        this.buscar = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    FiltroRepuestosComponent.prototype.ngOnInit = function () {
        this.obtenerCategorias();
        this.obtenerMarcas();
    };
    FiltroRepuestosComponent.prototype.obtenerCategorias = function () {
        var _this = this;
        this.replacementService.getCategories().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data.map(function (e) { return ({
                id: e.type_id,
                label: e.type_name
            }); });
        })).subscribe(function (data) { return _this.categorias = data; });
    };
    FiltroRepuestosComponent.prototype.obtenerMarcas = function () {
        var _this = this;
        this.replacementService.getBrands().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data.map(function (e) { return ({
                id: e.brand_id,
                label: e.brand_name
            }); });
        })).subscribe(function (data) { return _this.marcas = data; });
    };
    FiltroRepuestosComponent.prototype.addItem = function (filter) {
        var existe = this.filtros.find(function (e) { return e.type == filter.type; });
        if (existe) {
            existe.selected = filter.selected;
        }
        else {
            this.filtros.push(filter);
        }
        this.buscar.emit(this.filtros);
    };
    FiltroRepuestosComponent.ctorParameters = function () { return [
        { type: src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], FiltroRepuestosComponent.prototype, "buscar", void 0);
    FiltroRepuestosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'filtro-repuestos',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./filtro-repuestos.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/repuestos/filtro-repuestos/filtro-repuestos.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./filtro-repuestos.component.sass */ "./src/app/repuestos/filtro-repuestos/filtro-repuestos.component.sass")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [src_app_service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"]])
    ], FiltroRepuestosComponent);
    return FiltroRepuestosComponent;
}());



/***/ }),

/***/ "./src/app/repuestos/list-productos/list-productos.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/repuestos/list-productos/list-productos.component.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlcHVlc3Rvcy9saXN0LXByb2R1Y3Rvcy9saXN0LXByb2R1Y3Rvcy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/repuestos/list-productos/list-productos.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/repuestos/list-productos/list-productos.component.ts ***!
  \**********************************************************************/
/*! exports provided: ListProductosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListProductosComponent", function() { return ListProductosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var ListProductosComponent = /** @class */ (function () {
    function ListProductosComponent() {
    }
    ListProductosComponent.prototype.ngOnInit = function () {
    };
    ListProductosComponent.prototype.cantProductos = function () {
        if (this.productos === undefined)
            return 0;
        else
            return this.productos.length;
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Array)
    ], ListProductosComponent.prototype, "productos", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Boolean)
    ], ListProductosComponent.prototype, "showFiltros", void 0);
    ListProductosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'list-productos',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./list-productos.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/repuestos/list-productos/list-productos.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./list-productos.component.scss */ "./src/app/repuestos/list-productos/list-productos.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], ListProductosComponent);
    return ListProductosComponent;
}());



/***/ }),

/***/ "./src/app/repuestos/repuestos.component.scss":
/*!****************************************************!*\
  !*** ./src/app/repuestos/repuestos.component.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".buscador-repuestos {\n  background-color: #3374db;\n  width: 100%;\n  padding: 10px;\n  display: flex;\n  justify-items: center;\n  flex-direction: column;\n  align-items: center;\n  text-align: center;\n}\n\n.buscador-repuestos input {\n  width: 100%;\n  padding: 14px;\n  max-width: 1320px;\n  display: flex;\n  align-self: center;\n  font-size: 16px;\n  font-weight: 100;\n  border-radius: 5px;\n  border: 0;\n}\n\n.showhidefilter {\n  font-weight: bold;\n  font-size: 13px;\n  cursor: pointer;\n}\n\n.item-name p {\n  font-size: 18px;\n}\n\n.sub-bnr {\n  background-position: bottom;\n}\n\n.btn-load-more {\n  background-color: #000;\n  color: #FFF;\n  border-radius: 5px;\n  margin: 10px;\n  padding: 10px 30px;\n  border: 0px;\n}\n\n.moreelements {\n  background-color: #EFEFEF;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9yZXB1ZXN0b3MvcmVwdWVzdG9zLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9yZXB1ZXN0b3MvcmVwdWVzdG9zLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBRUMseUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNHLGFBQUE7RUFDQSxxQkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ0RKOztBRElBO0VBRUMsV0FBQTtFQUNHLGFBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtBQ0ZKOztBRE1BO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQ0hKOztBRE1BO0VBQ0ksZUFBQTtBQ0hKOztBRE1BO0VBQVMsMkJBQUE7QUNGVDs7QURLQTtFQUNFLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0ZGOztBREtBO0VBQ0UseUJBQUE7QUNGRiIsImZpbGUiOiJzcmMvYXBwL3JlcHVlc3Rvcy9yZXB1ZXN0b3MuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5idXNjYWRvci1yZXB1ZXN0b3N7XG5cblx0YmFja2dyb3VuZC1jb2xvcjogIzMzNzRkYjtcblx0d2lkdGg6IDEwMCU7XG5cdHBhZGRpbmc6IDEwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmJ1c2NhZG9yLXJlcHVlc3RvcyBpbnB1dHtcblxuXHR3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAxNHB4O1xuICAgIG1heC13aWR0aDogMTMyMHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBmb250LXdlaWdodDogMTAwO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBib3JkZXI6IDA7XG5cbn1cblxuLnNob3doaWRlZmlsdGVye1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5pdGVtLW5hbWUgcHtcbiAgICBmb250LXNpemU6IDE4cHg7XG59XG5cbi5zdWItYm5ye2JhY2tncm91bmQtcG9zaXRpb246IGJvdHRvbTt9XG5cblxuLmJ0bi1sb2FkLW1vcmV7XG4gIGJhY2tncm91bmQtY29sb3I6IzAwMDtcbiAgY29sb3I6ICNGRkY7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luOiAxMHB4O1xuICBwYWRkaW5nOiAxMHB4IDMwcHg7XG4gIGJvcmRlcjogMHB4O1xufVxuXG4ubW9yZWVsZW1lbnRze1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUZFRkVGO1xufVxuIiwiLmJ1c2NhZG9yLXJlcHVlc3RvcyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzMzc0ZGI7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmJ1c2NhZG9yLXJlcHVlc3RvcyBpbnB1dCB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAxNHB4O1xuICBtYXgtd2lkdGg6IDEzMjBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24tc2VsZjogY2VudGVyO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYm9yZGVyOiAwO1xufVxuXG4uc2hvd2hpZGVmaWx0ZXIge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5pdGVtLW5hbWUgcCB7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cblxuLnN1Yi1ibnIge1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBib3R0b207XG59XG5cbi5idG4tbG9hZC1tb3JlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcbiAgY29sb3I6ICNGRkY7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luOiAxMHB4O1xuICBwYWRkaW5nOiAxMHB4IDMwcHg7XG4gIGJvcmRlcjogMHB4O1xufVxuXG4ubW9yZWVsZW1lbnRzIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0VGRUZFRjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/repuestos/repuestos.component.ts":
/*!**************************************************!*\
  !*** ./src/app/repuestos/repuestos.component.ts ***!
  \**************************************************/
/*! exports provided: RepuestosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RepuestosComponent", function() { return RepuestosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm5/router.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");




var RepuestosComponent = /** @class */ (function () {
    function RepuestosComponent(rutaActiva, replacementService) {
        this.rutaActiva = rutaActiva;
        this.replacementService = replacementService;
        this.offset = 0;
        this.cantidad = 12;
        this.idParam = '';
        this.showFiltros = true;
        this.filtros = [];
        this.productos = [];
        this.getMore = true;
        this.loading = false;
        this.nextBreakpoint = 0;
    }
    RepuestosComponent.prototype.ngOnInit = function () {
        this.obtenerProductos(this.filtros);
    };
    /*
  
    @HostListener('window:scroll', ['$event'])
    handleScroll(event) {
      if(this.productos.length > 0){
      if(this.nextBreakpoint == 0)
        this.nextBreakpoint = parseInt(this.moreElementsDiv.nativeElement.offsetTop);
    
      let positionScroll = parseInt(document.documentElement.scrollTop.toFixed(0));
      let viewportHeight = parseInt(window.innerWidth.toFixed(0));
  
      if (( viewportHeight + positionScroll  ) > this.nextBreakpoint){
       this.loadmore();
       this.nextBreakpoint = parseInt(this.moreElementsDiv.nativeElement.offsetTop);
      }
    }}
  
    */
    RepuestosComponent.prototype.loadmore = function () {
        var _this = this;
        if (this.getMore && !this.loading) {
            this.loading = true;
            this.offset = this.offset + this.cantidad;
            this.replacementService.getProducts(this.filtros, this.cantidad, this.offset).subscribe(function (response) {
                //       response.forEach(element => {
                //        this.productos.push(element);
                //     });
                var _a;
                (_a = _this.productos).push.apply(_a, response);
                _this.loading = false;
                if (response.length < _this.cantidad) {
                    _this.getMore = false;
                }
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
    };
    RepuestosComponent.prototype.obtenerProductos = function (filtros) {
        var _this = this;
        //if(filtros.length > 0){      
        this.filtros = filtros;
        this.replacementService.getProducts(filtros, this.cantidad, this.offset).subscribe(function (response) {
            _this.productos = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(response);
            _this.loading = false;
        }, function (error) {
            console.log(JSON.stringify(error));
        });
        //}
    };
    RepuestosComponent.prototype.removeFilter = function (element) {
        var found = this.filtros.find(function (e) { return e.type == element.type; });
        if (found) {
            found.selected = found.selected.filter(function (f) { return f.id !== element.combo.id; });
            this.obtenerProductos(this.filtros);
        }
    };
    RepuestosComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('moreElementsDiv'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], RepuestosComponent.prototype, "moreElementsDiv", void 0);
    RepuestosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-repuestos',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./repuestos.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/repuestos/repuestos.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./repuestos.component.scss */ "./src/app/repuestos/repuestos.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"]])
    ], RepuestosComponent);
    return RepuestosComponent;
}());



/***/ }),

/***/ "./src/app/rutaclub/mispuntos/mispuntos.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/rutaclub/mispuntos/mispuntos.component.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3J1dGFjbHViL21pc3B1bnRvcy9taXNwdW50b3MuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/rutaclub/mispuntos/mispuntos.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/rutaclub/mispuntos/mispuntos.component.ts ***!
  \***********************************************************/
/*! exports provided: MispuntosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MispuntosComponent", function() { return MispuntosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var MispuntosComponent = /** @class */ (function () {
    function MispuntosComponent() {
    }
    MispuntosComponent.prototype.ngOnInit = function () {
    };
    MispuntosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-mispuntos',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./mispuntos.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rutaclub/mispuntos/mispuntos.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./mispuntos.component.scss */ "./src/app/rutaclub/mispuntos/mispuntos.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], MispuntosComponent);
    return MispuntosComponent;
}());



/***/ }),

/***/ "./src/app/rutaclub/rutaclub.component.scss":
/*!**************************************************!*\
  !*** ./src/app/rutaclub/rutaclub.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".yellow-box {\n  background-color: #F9AE41;\n}\n\n.buscador {\n  font-size: 30px;\n  width: 80%;\n  padding: 10px;\n  margin: 10px;\n  text-align: center;\n  border-radius: 10px;\n  border: 0px;\n}\n\n.subtitle {\n  font-weight: bold;\n  color: #2d3a4b;\n  font-size: 16px;\n  margin: 0px;\n  margin-bottom: 20px;\n  letter-spacing: 0px;\n  max-width: 100%;\n  font-family: \"Montserrat\", sans-serif;\n}\n\n.body-rutaclub {\n  margin-top: 50px;\n  margin-bottom: 50px;\n}\n\n.body-rutaclub h4 {\n  text-align: center;\n  font-size: 16px;\n  margin-top: 40px;\n}\n\n.body-rutaclub p {\n  max-width: 80%;\n  margin: 0% 10% 0% 10%;\n  text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9ydXRhY2x1Yi9ydXRhY2x1Yi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcnV0YWNsdWIvcnV0YWNsdWIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBQTtBQ0NKOztBREVBO0VBQ0ksZUFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0FDQ0o7O0FERUE7RUFDSSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EscUNBQUE7QUNDSjs7QURHQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNBSjs7QURHQTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQUo7O0FER0E7RUFDSSxjQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtBQ0FKIiwiZmlsZSI6InNyYy9hcHAvcnV0YWNsdWIvcnV0YWNsdWIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIueWVsbG93LWJveHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjlBRTQxO1xufVxuXG4uYnVzY2Fkb3J7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIHdpZHRoOiA4MCU7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBtYXJnaW46IDEwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgYm9yZGVyOiAwcHg7XG59XG5cbi5zdWJ0aXRsZXtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBjb2xvcjogIzJkM2E0YjtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbWFyZ2luOiAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQnLCBzYW5zLXNlcmlmO1xuXG59XG5cbi5ib2R5LXJ1dGFjbHVie1xuICAgIG1hcmdpbi10b3A6NTBweDtcbiAgICBtYXJnaW4tYm90dG9tOjUwcHg7XG59XG5cbi5ib2R5LXJ1dGFjbHViIGg0e1xuICAgIHRleHQtYWxpZ246Y2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBtYXJnaW4tdG9wOjQwcHg7XG59XG5cbi5ib2R5LXJ1dGFjbHViIHB7XG4gICAgbWF4LXdpZHRoOiA4MCU7XG4gICAgbWFyZ2luOjAlIDEwJSAwJSAxMCU7XG4gICAgdGV4dC1hbGlnbjpqdXN0aWZ5OyBcbn1cbiIsIi55ZWxsb3ctYm94IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0Y5QUU0MTtcbn1cblxuLmJ1c2NhZG9yIHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICB3aWR0aDogODAlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBtYXJnaW46IDEwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYm9yZGVyOiAwcHg7XG59XG5cbi5zdWJ0aXRsZSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzJkM2E0YjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBtYXJnaW46IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDBweDtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCIsIHNhbnMtc2VyaWY7XG59XG5cbi5ib2R5LXJ1dGFjbHViIHtcbiAgbWFyZ2luLXRvcDogNTBweDtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLmJvZHktcnV0YWNsdWIgaDQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbWFyZ2luLXRvcDogNDBweDtcbn1cblxuLmJvZHktcnV0YWNsdWIgcCB7XG4gIG1heC13aWR0aDogODAlO1xuICBtYXJnaW46IDAlIDEwJSAwJSAxMCU7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG59Il19 */");

/***/ }),

/***/ "./src/app/rutaclub/rutaclub.component.ts":
/*!************************************************!*\
  !*** ./src/app/rutaclub/rutaclub.component.ts ***!
  \************************************************/
/*! exports provided: RutaclubComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RutaclubComponent", function() { return RutaclubComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_2__);



var RutaclubComponent = /** @class */ (function () {
    function RutaclubComponent() {
        this.consultado = false;
        this.puntos = 0;
    }
    RutaclubComponent.prototype.ngOnInit = function () {
    };
    RutaclubComponent.prototype.agregar = function () {
        var movimientos = {
            fecha: Date.now(),
            puntos: "-1100"
        };
        //    const things = this.db.collection('club').add(movimientos);
        // let dasd =  this.db.collection('club').doc('oeJT5NwSY5Xl9XCmYKid').collection('movimientos').add(movimientos);
        //console.log(dasd);
        // .valueChanges();
        // things.subscribe(console.log);
    };
    RutaclubComponent.prototype.consultar = function () {
        /*
              let puntos = 0;
        
              let movs = this.db.collection('club').doc('oeJT5NwSY5Xl9XCmYKid').collection('movimientos').get().toPromise().then((querySnapshot) => {
                querySnapshot.forEach((doc : any) => {
        
                    this.puntos = this.puntos + parseInt(doc.data().puntos);
                    //console.log(this.puntos);
                    
                });
                this.consultado = true;
                
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
            });*/
    };
    RutaclubComponent.prototype.consultarId = function () {
        console.log(this.idSocio);
    };
    RutaclubComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-rutaclub',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./rutaclub.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rutaclub/rutaclub.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./rutaclub.component.scss */ "./src/app/rutaclub/rutaclub.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], RutaclubComponent);
    return RutaclubComponent;
}());



/***/ }),

/***/ "./src/app/service/contactos.service.ts":
/*!**********************************************!*\
  !*** ./src/app/service/contactos.service.ts ***!
  \**********************************************/
/*! exports provided: ContactosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactosService", function() { return ContactosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm5/http.js");



var ContactosService = /** @class */ (function () {
    function ContactosService(httpClient) {
        this.httpClient = httpClient;
        this.API_URL = "https://api.rutasurtrucks.com.ar/contactos";
    }
    ContactosService.prototype.sendContact = function (postData) {
        return this.httpClient.post(this.API_URL + '/contacto/', postData);
    };
    ContactosService.prototype.sendFinanciacion = function (postData) { };
    ContactosService.prototype.sendCarrito = function (postData) {
        return this.httpClient.post(this.API_URL + '/carrito/', postData);
    };
    ContactosService.prototype.sendVender = function (postData) { };
    ContactosService.prototype.sendComprar = function (postData) { };
    ContactosService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    ContactosService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ContactosService);
    return ContactosService;
}());



/***/ }),

/***/ "./src/app/service/filtro.service.ts":
/*!*******************************************!*\
  !*** ./src/app/service/filtro.service.ts ***!
  \*******************************************/
/*! exports provided: FiltroService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltroService", function() { return FiltroService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var FiltroService = /** @class */ (function () {
    function FiltroService() {
        this.filtros = [];
        this.otrosFiltrosSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.currentFilters = this.otrosFiltrosSubject.asObservable();
    }
    FiltroService.prototype.remove = function (type, combo) {
        var found = this.filtros.find(function (e) { return e.type == type; });
        if (found) {
            found.selected = found.selected.filter(function (f) { return f.id !== combo.id; });
        }
        this.update();
    };
    FiltroService.prototype.addItem = function (filter) {
        this.filtros.push(filter);
        this.update();
    };
    FiltroService.prototype.update = function () {
        this.otrosFiltrosSubject.next(this.filtros);
    };
    FiltroService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], FiltroService);
    return FiltroService;
}());



/***/ }),

/***/ "./src/app/service/replacement.service.ts":
/*!************************************************!*\
  !*** ./src/app/service/replacement.service.ts ***!
  \************************************************/
/*! exports provided: ReplacementService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReplacementService", function() { return ReplacementService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm5/http.js");



var ReplacementService = /** @class */ (function () {
    function ReplacementService(httpClient) {
        this.httpClient = httpClient;
    }
    ReplacementService.prototype.getCategories = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/repuestos/categorias";
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getBrands = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/repuestos/marcas";
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getCategory = function (id) {
        var url_api = "https://api.rutasurtrucks.com.ar/repuestos/categorias/" + id;
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getProducts = function (filtros, limit, offset) {
        var params = filtros.map(function (f) {
            return f.type + '=' + f.selected.map(function (e) { return e.id; }).join(',');
        }).join('&');
        var url_api = "https://api.rutasurtrucks.com.ar/repuestos/?" + params + "&limit=" + limit + "&offset=" + offset;
        console.log(url_api);
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getProduct = function (id) {
        var url_api = "https://api.rutasurtrucks.com.ar/repuestos/" + id;
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getProductImages = function (id) {
        var url_api = "https://api.rutasurtrucks.com.ar/repuestos/imagenesRepuesto/" + id;
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getOffers = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/repuestos/ofertas";
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getOffer = function (id) {
        var url_api = "https://api.rutasurtrucks.com.ar/repuestos/ofertas/" + id;
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getOfferProducts = function (id) {
        var url_api = "https://api.rutasurtrucks.com.ar/repuestos/ofertas/" + id + "/productos";
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getFeatured = function (limit, offset) {
        var url_api = "https://api.rutasurtrucks.com.ar/repuestos/?limit=" + limit + "&offset=" + offset + "&featured=1";
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getSearch = function (search, limit, offset) {
        var url_api = "https://api.rutasurtrucks.com.ar/repuestos/?query=" + search + "&limit=" + limit + "&offset=" + offset;
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getDestacado = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/vehiculos/?destacado=1";
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getUsado = function (id) {
        var url_api = "https://api.rutasurtrucks.com.ar/vehiculos/" + id;
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.getUsadoImagenes = function (id) {
        var url_api = "https://api.rutasurtrucks.com.ar/vehiculos/" + id + "/imagenes/";
        return this.httpClient.get(url_api);
    };
    ReplacementService.prototype.enviarmensaje = function (contacto) {
        var url_api = "https://api.rutasurtrucks.com.ar/contactos";
        return this.httpClient.post(url_api, contacto);
    };
    ReplacementService.prototype.sendContact = function (postData) {
        var API_URL = "https://api.rutasurtrucks.com.ar/";
        return this.httpClient.post(API_URL + 'vehiculos/contacto/', postData);
    };
    ReplacementService.prototype.sendService = function (service) {
        var url_api = "https://api.rutasurtrucks.com.ar/servicios";
        return this.httpClient.post(url_api, service);
    };
    ReplacementService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    ReplacementService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ReplacementService);
    return ReplacementService;
}());



/***/ }),

/***/ "./src/app/service/usados.service.ts":
/*!*******************************************!*\
  !*** ./src/app/service/usados.service.ts ***!
  \*******************************************/
/*! exports provided: UsadosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsadosService", function() { return UsadosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm5/http.js");



var UsadosService = /** @class */ (function () {
    function UsadosService(httpClient) {
        this.httpClient = httpClient;
    }
    UsadosService.prototype.getUsados = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/vehiculos/";
        return this.httpClient.get(url_api);
    };
    UsadosService.prototype.getProducts = function (filtros, limit, offset) {
        var params = filtros.map(function (f) {
            return f.type + '=' + f.selected.map(function (e) { return e.id; }).join(',');
        }).join('&');
        var url_api = "https://api.rutasurtrucks.com.ar/vehiculos/?" + params + "&limit=" + limit + "&offset=" + offset;
        console.log(url_api);
        return this.httpClient.get(url_api);
    };
    UsadosService.prototype.getCategories = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/categorias";
        return this.httpClient.get(url_api);
    };
    UsadosService.prototype.getBrands = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/categorias/marcas";
        return this.httpClient.get(url_api);
    };
    UsadosService.prototype.getColor = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/categorias/colores";
        return this.httpClient.get(url_api);
    };
    UsadosService.prototype.getYears = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/categorias/anios";
        return this.httpClient.get(url_api);
    };
    UsadosService.prototype.getLong = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/categorias/largos";
        return this.httpClient.get(url_api);
    };
    UsadosService.prototype.getModel = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/categorias/modelos";
        return this.httpClient.get(url_api);
    };
    UsadosService.prototype.getDestacado = function () {
        var url_api = "https://api.rutasurtrucks.com.ar/vehiculos/?destacado=1";
        return this.httpClient.get(url_api);
    };
    UsadosService.prototype.getUsado = function (id) {
        var url_api = "https://api.rutasurtrucks.com.ar/vehiculos/" + id;
        return this.httpClient.get(url_api);
    };
    UsadosService.prototype.getUsadoImagenes = function (id) {
        var url_api = "https://api.rutasurtrucks.com.ar/vehiculos/" + id + "/imagenes/";
        return this.httpClient.get(url_api);
    };
    UsadosService.prototype.enviarmensaje = function (contacto) {
        var url_api = "https://api.rutasurtrucks.com.ar/contactos";
        return this.httpClient.post(url_api, contacto);
    };
    UsadosService.prototype.sendContact = function (postData) {
        var API_URL = "https://api.rutasurtrucks.com.ar/";
        return this.httpClient.post(API_URL + 'vehiculos/contacto/', postData);
    };
    UsadosService.prototype.sendService = function (service) {
        var url_api = "https://api.rutasurtrucks.com.ar/servicios";
        return this.httpClient.post(url_api, service);
    };
    UsadosService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    UsadosService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UsadosService);
    return UsadosService;
}());



/***/ }),

/***/ "./src/app/servicios/servicios.component.sass":
/*!****************************************************!*\
  !*** ./src/app/servicios/servicios.component.sass ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlcnZpY2lvcy9zZXJ2aWNpb3MuY29tcG9uZW50LnNhc3MifQ== */");

/***/ }),

/***/ "./src/app/servicios/servicios.component.ts":
/*!**************************************************!*\
  !*** ./src/app/servicios/servicios.component.ts ***!
  \**************************************************/
/*! exports provided: ServiciosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciosComponent", function() { return ServiciosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");
/* harmony import */ var _forms_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../forms.service */ "./src/app/forms.service.ts");




var ServiciosComponent = /** @class */ (function () {
    function ServiciosComponent(rs, fs) {
        this.rs = rs;
        this.fs = fs;
        this.enviado = false;
    }
    ServiciosComponent.prototype.ngOnInit = function () {
        this.getInfoContacto();
    };
    ServiciosComponent.prototype.contacto = function (form) {
        var _this = this;
        //console.log(form.value);
        this.rs.sendService(form.value).subscribe(function (data) {
            form.reset();
            _this.enviado = true;
            //alert("Mensaje enviado")
        }, function (error) {
            console.log(error);
            alert("Intente mas tarde");
        });
        var info = {
            "contact_name": form.value.contact_name,
            "contact_email": form.value.contact_email,
            "contact_phone": form.value.contact_phone,
            "contact_city": form.value.contact_city
        };
        this.fs.saveInfo(info);
    };
    ServiciosComponent.prototype.getInfoContacto = function () {
        this.infoClient = this.fs.getInfo();
    };
    ServiciosComponent.ctorParameters = function () { return [
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"] },
        { type: _forms_service__WEBPACK_IMPORTED_MODULE_3__["FormsService"] }
    ]; };
    ServiciosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-servicios',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./servicios.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/servicios/servicios.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./servicios.component.sass */ "./src/app/servicios/servicios.component.sass")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_replacement_service__WEBPACK_IMPORTED_MODULE_2__["ReplacementService"], _forms_service__WEBPACK_IMPORTED_MODULE_3__["FormsService"]])
    ], ServiciosComponent);
    return ServiciosComponent;
}());



/***/ }),

/***/ "./src/app/shop-cart.service.ts":
/*!**************************************!*\
  !*** ./src/app/shop-cart.service.ts ***!
  \**************************************/
/*! exports provided: ShopCartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopCartService", function() { return ShopCartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm5/http.js");



var ShopCartService = /** @class */ (function () {
    function ShopCartService(httpClient) {
        this.httpClient = httpClient;
        this.cart = JSON.parse(window.localStorage.getItem('cart'));
        if (this.cart == null) {
            this.cart = [];
        }
    }
    ShopCartService.prototype.addToCart = function (product, cant) {
        var found = 0;
        for (var _i = 0, _a = this.cart; _i < _a.length; _i++) {
            var elem = _a[_i];
            if (elem.info.replacement_id == product.replacement_id) {
                elem.amount += cant;
                elem.total = parseFloat(elem.total) + parseFloat(product.replacement_price) * cant;
                found = 1;
                break;
            }
        }
        if (found == 0) {
            this.cart.push({
                "info": product,
                "amount": cant,
                "total": product.replacement_price * cant
            });
        }
        window.localStorage.setItem('cart', JSON.stringify(this.cart));
    };
    ShopCartService.prototype.modifyCantProd = function (index, amount) {
        if (this.cart != null) {
            if (this.cart[index]) {
                this.cart[index].amount = amount;
                this.cart[index].total = parseFloat(this.cart[index].info.replacement_price) * amount;
                window.localStorage.setItem('cart', JSON.stringify(this.cart));
            }
        }
    };
    ShopCartService.prototype.removeFromCart = function (index) {
        //eliminar del arreglo dada el index
        if (this.cart[index]) {
            this.cart.splice(index, 1);
        }
        window.localStorage.setItem('cart', JSON.stringify(this.cart));
    };
    ShopCartService.prototype.getProducts = function () {
        //retornar todos los elementos del carrito
        this.cart = JSON.parse(window.localStorage.getItem('cart'));
        return this.cart;
    };
    ShopCartService.prototype.getCantidad = function () {
        if (this.cart != undefined)
            return this.cart.length;
        else
            return 0;
    };
    ShopCartService.prototype.getTotal = function () {
        this.cart = JSON.parse(localStorage.getItem('cart'));
        var total = 0;
        if (this.cart != undefined) {
            for (var _i = 0, _a = this.cart; _i < _a.length; _i++) {
                var elem = _a[_i];
                total += total + elem.amount * elem.info.replacement_price;
            }
        }
        return total;
    };
    ShopCartService.prototype.reset = function () {
        this.cart = undefined;
        window.localStorage.removeItem('cart');
    };
    ShopCartService.prototype.enviarpedido = function (postData) {
        var API_URL = "https://api.rutasurtrucks.com.ar/";
        return this.httpClient.post(API_URL + 'pedido/', postData);
    };
    ShopCartService.prototype.enviarcamion = function (postData) {
        var API_URL = "https://api.rutasurtrucks.com.ar/";
        return this.httpClient.post(API_URL + 'contactos/vender/', postData);
    };
    ShopCartService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    ShopCartService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ShopCartService);
    return ShopCartService;
}());



/***/ }),

/***/ "./src/app/singles/one-categoria/one-categoria.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/singles/one-categoria/one-categoria.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NpbmdsZXMvb25lLWNhdGVnb3JpYS9vbmUtY2F0ZWdvcmlhLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/singles/one-categoria/one-categoria.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/singles/one-categoria/one-categoria.component.ts ***!
  \******************************************************************/
/*! exports provided: OneCategoriaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OneCategoriaComponent", function() { return OneCategoriaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var OneCategoriaComponent = /** @class */ (function () {
    function OneCategoriaComponent() {
    }
    OneCategoriaComponent.prototype.ngOnInit = function () {
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], OneCategoriaComponent.prototype, "categoria", void 0);
    OneCategoriaComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'one-categoria',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./one-categoria.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-categoria/one-categoria.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./one-categoria.component.scss */ "./src/app/singles/one-categoria/one-categoria.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], OneCategoriaComponent);
    return OneCategoriaComponent;
}());



/***/ }),

/***/ "./src/app/singles/one-destacado-card/one-destacado-card.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/singles/one-destacado-card/one-destacado-card.component.scss ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  width: 100%;\n  display: inline-block;\n  margin-bottom: 20px;\n  background-color: #FFF;\n  border-radius: 8px;\n  /*box-shadow: 0 1px 5px 0 rgb(70 70 70 / 15%), 0 2px 2px 0 rgb(70 70 70 / 15%), 0 3px 2px -2px rgb(70 70 70 / 15%);*/\n  cursor: pointer;\n}\n:host .img {\n  border-top-left-radius: 8px;\n  border-top-right-radius: 8px;\n  overflow: hidden;\n  background-color: #f5f5f5;\n  width: 100%;\n  height: 190px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n:host .img img {\n  width: 100%;\n  height: auto;\n}\n:host .item .descipcion {\n  padding: 10px;\n}\n:host .item .descipcion h2 {\n  font-size: 16px;\n  line-height: 20px;\n  height: 20px;\n  overflow: hidden;\n  margin: 0;\n}\n:host .item .descipcion p {\n  color: #898989;\n  font-size: 14px;\n  margin: 10px 0;\n  line-height: 17px;\n  height: 17px;\n  font-weight: bold;\n  overflow: hidden;\n}\n:host .item .price {\n  padding: 0 10px 10px 10px;\n  color: #f3840d;\n  font-size: 20px;\n  line-height: 20px;\n  font-weight: 800;\n  text-align: left;\n  margin: 0;\n}\n:host:hover {\n  /*box-shadow: 0 6px 10px 1px rgb(70 70 70 / 15%), 0 4px 5px -2px rgb(70 70 70 / 15%);*/\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9zaW5nbGVzL29uZS1kZXN0YWNhZG8tY2FyZC9vbmUtZGVzdGFjYWRvLWNhcmQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NpbmdsZXMvb25lLWRlc3RhY2Fkby1jYXJkL29uZS1kZXN0YWNhZG8tY2FyZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFFQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLG9IQUFBO0VBQ0EsZUFBQTtBQ0FKO0FERUk7RUFFRSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNETjtBREdNO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUNEUjtBRFdJO0VBQ0UsYUFBQTtBQ1ROO0FEVU07RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0FDUlI7QURXTTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNUUjtBRGFJO0VBQ0kseUJBQUE7RUFDQSxjQUFBO0VBRUEsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7QUNaUjtBRG1CQTtFQUNFLHNGQUFBO0FDaEJGIiwiZmlsZSI6InNyYy9hcHAvc2luZ2xlcy9vbmUtZGVzdGFjYWRvLWNhcmQvb25lLWRlc3RhY2Fkby1jYXJkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3R7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGO1xuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAvKmJveC1zaGFkb3c6IDAgMXB4IDVweCAwIHJnYig3MCA3MCA3MCAvIDE1JSksIDAgMnB4IDJweCAwIHJnYig3MCA3MCA3MCAvIDE1JSksIDAgM3B4IDJweCAtMnB4IHJnYig3MCA3MCA3MCAvIDE1JSk7Ki9cbiAgICBjdXJzb3I6IHBvaW50ZXI7XG5cbiAgICAuaW1ne1xuXG4gICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA4cHg7XG4gICAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogOHB4O1xuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmNWY1ZjU7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogMTkwcHg7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuXG4gICAgICBpbWd7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICB9XG5cbiAgfVxuXG5cblxuXG4gIC5pdGVte1xuXG4gICAgLmRlc2NpcGNpb257XG4gICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgaDJ7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgbWFyZ2luOiAwO1xuXG4gICAgICB9XG4gICAgICBwe1xuICAgICAgICBjb2xvcjogcmdiKDEzNywgMTM3LCAxMzcpO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIG1hcmdpbjogMTBweCAwO1xuICAgICAgICBsaW5lLWhlaWdodDogMTdweDtcbiAgICAgICAgaGVpZ2h0OiAxN3B4O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAucHJpY2V7XG4gICAgICAgIHBhZGRpbmc6IDAgMTBweCAxMHB4IDEwcHg7XG4gICAgICAgIGNvbG9yOiAjZjM4NDBkO1xuXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICB9XG5cbiAgfVxufVxuXG5cbjpob3N0OmhvdmVyIHtcbiAgLypib3gtc2hhZG93OiAwIDZweCAxMHB4IDFweCByZ2IoNzAgNzAgNzAgLyAxNSUpLCAwIDRweCA1cHggLTJweCByZ2IoNzAgNzAgNzAgLyAxNSUpOyovXG59XG4iLCI6aG9zdCB7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGRkY7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgLypib3gtc2hhZG93OiAwIDFweCA1cHggMCByZ2IoNzAgNzAgNzAgLyAxNSUpLCAwIDJweCAycHggMCByZ2IoNzAgNzAgNzAgLyAxNSUpLCAwIDNweCAycHggLTJweCByZ2IoNzAgNzAgNzAgLyAxNSUpOyovXG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbjpob3N0IC5pbWcge1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA4cHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA4cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNWY1ZjU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDE5MHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbjpob3N0IC5pbWcgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbn1cbjpob3N0IC5pdGVtIC5kZXNjaXBjaW9uIHtcbiAgcGFkZGluZzogMTBweDtcbn1cbjpob3N0IC5pdGVtIC5kZXNjaXBjaW9uIGgyIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgaGVpZ2h0OiAyMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBtYXJnaW46IDA7XG59XG46aG9zdCAuaXRlbSAuZGVzY2lwY2lvbiBwIHtcbiAgY29sb3I6ICM4OTg5ODk7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luOiAxMHB4IDA7XG4gIGxpbmUtaGVpZ2h0OiAxN3B4O1xuICBoZWlnaHQ6IDE3cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuOmhvc3QgLml0ZW0gLnByaWNlIHtcbiAgcGFkZGluZzogMCAxMHB4IDEwcHggMTBweDtcbiAgY29sb3I6ICNmMzg0MGQ7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIG1hcmdpbjogMDtcbn1cblxuOmhvc3Q6aG92ZXIge1xuICAvKmJveC1zaGFkb3c6IDAgNnB4IDEwcHggMXB4IHJnYig3MCA3MCA3MCAvIDE1JSksIDAgNHB4IDVweCAtMnB4IHJnYig3MCA3MCA3MCAvIDE1JSk7Ki9cbn0iXX0= */");

/***/ }),

/***/ "./src/app/singles/one-destacado-card/one-destacado-card.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/singles/one-destacado-card/one-destacado-card.component.ts ***!
  \****************************************************************************/
/*! exports provided: OneDestacadoCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OneDestacadoCardComponent", function() { return OneDestacadoCardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var OneDestacadoCardComponent = /** @class */ (function () {
    function OneDestacadoCardComponent() {
    }
    OneDestacadoCardComponent.prototype.ngOnInit = function () {
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], OneDestacadoCardComponent.prototype, "destacado", void 0);
    OneDestacadoCardComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'one-destacado-card',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./one-destacado-card.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-destacado-card/one-destacado-card.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./one-destacado-card.component.scss */ "./src/app/singles/one-destacado-card/one-destacado-card.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], OneDestacadoCardComponent);
    return OneDestacadoCardComponent;
}());



/***/ }),

/***/ "./src/app/singles/one-destacado/one-destacado.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/singles/one-destacado/one-destacado.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".item-usado {\n  border-radius: 5px;\n  background-position: 50% 50%;\n  background-size: cover;\n  background-repeat: no-repeat;\n  width: 100%;\n  height: 400px;\n  background-color: #FFFFFF;\n  border: 1px #EFEFEF solid;\n  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.12);\n  margin-bottom: 10px;\n}\n\n.item-usado-titulo {\n  width: 40%;\n  margin: 50px;\n  position: absolute;\n  height: 300px;\n  background-color: rgba(0, 0, 0, 0.6);\n  color: #FFFFFF;\n  border-radius: 5px;\n  padding: 20px;\n}\n\n.item-usado-titulo .item-img-anio img {\n  width: 60px;\n  height: 60px;\n  display: inline-block;\n}\n\n.item-usado-titulo .item-img-anio div {\n  display: inline-block;\n  color: #FFFFFF;\n  padding-left: 10px;\n}\n\n.item-usado-titulo .price {\n  display: block;\n  font-size: 20px;\n  font-weight: bold;\n  letter-spacing: 2px;\n  margin: 10px 0 10px 0;\n}\n\n.item-usado-titulo .item-info {\n  font-size: 10px;\n  display: block;\n}\n\n.item-usado-titulo .item-info p {\n  border-bottom: 1px solid rgba(255, 255, 255, 0.3);\n  display: block;\n  color: #EFEFEF;\n  margin: 0px 0 0 0;\n  padding: 5px 0 5px 0;\n}\n\n.item-usado-titulo .item-info p:last-child {\n  border-bottom: none;\n}\n\n.item-usado-titulo .btn-mas-info {\n  position: absolute;\n  bottom: 0px;\n  background-color: #F9AE41;\n  color: #222;\n  font-weight: bolder;\n  padding: 10px 30px 10px 30px;\n  margin-bottom: 20px;\n  border-radius: 5px;\n}\n\n@media (max-width: 767px) {\n  .item-usado {\n    height: 320px;\n  }\n\n  .item-usado-titulo {\n    width: calc(100% - 40px);\n    margin: 5px;\n    padding: 5px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9zaW5nbGVzL29uZS1kZXN0YWNhZG8vb25lLWRlc3RhY2Fkby5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2luZ2xlcy9vbmUtZGVzdGFjYWRvL29uZS1kZXN0YWNhZG8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtFQUVBLDJDQUFBO0VBQ0EsbUJBQUE7QUNBSjs7QURJQTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esb0NBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0FDREo7O0FES1E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0FDSFo7O0FETVE7RUFDSSxxQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQ0paOztBRFVJO0VBQ0MsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7QUNSTDs7QURXSTtFQUNJLGVBQUE7RUFFQSxjQUFBO0FDVlI7O0FEWVE7RUFDSSxpREFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtBQ1ZaOztBRFlRO0VBQ0ksbUJBQUE7QUNWWjs7QURjSTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsNEJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDWlI7O0FEb0JBO0VBQ0k7SUFDSSxhQUFBO0VDakJOOztFRG9CRTtJQUNJLHdCQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7RUNqQk47QUFDRiIsImZpbGUiOiJzcmMvYXBwL3NpbmdsZXMvb25lLWRlc3RhY2Fkby9vbmUtZGVzdGFjYWRvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLml0ZW0tdXNhZG97XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IDUwJSA1MCU7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNDAwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbiAgICBib3JkZXI6IDFweCAjRUZFRkVGIHNvbGlkO1xuICAgIFxuICAgIGJveC1zaGFkb3c6IDAgMXB4IDJweCAwIHJnYmEoMCwwLDAsLjEyKTtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG5cbi5pdGVtLXVzYWRvLXRpdHVsb3tcbiAgICB3aWR0aDogNDAlO1xuICAgIG1hcmdpbjogNTBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKCRjb2xvcjogIzAwMDAwMCwgJGFscGhhOiAwLjYpO1xuICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBwYWRkaW5nOiAyMHB4O1xuXG4gICAgLml0ZW0taW1nLWFuaW97XG5cbiAgICAgICAgaW1ne1xuICAgICAgICAgICAgd2lkdGg6IDYwcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIH1cblxuICAgICAgICBkaXZ7XG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgICAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICAgICAgfVxuICAgIFxuXG4gICAgfVxuXG4gICAgLnByaWNle1xuICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgbGV0dGVyLXNwYWNpbmc6IDJweDtcbiAgICAgbWFyZ2luOiAxMHB4IDAgMTBweCAwO1xuICAgIH1cblxuICAgIC5pdGVtLWluZm97XG4gICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICAgICAgXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBcbiAgICAgICAgcHtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDI1NSwyNTUsMjU1LDAuMyk7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIGNvbG9yOiAjRUZFRkVGO1xuICAgICAgICAgICAgbWFyZ2luOiAwcHggMCAwIDA7XG4gICAgICAgICAgICBwYWRkaW5nOiA1cHggMCA1cHggMFxuICAgICAgICB9XG4gICAgICAgIHA6bGFzdC1jaGlsZHtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuYnRuLW1hcy1pbmZve1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGJvdHRvbTogMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjlBRTQxO1xuICAgICAgICBjb2xvcjogIzIyMjtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICAgICAgcGFkZGluZzogMTBweCAzMHB4IDEwcHggMzBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuXG5cbiAgICB9XG5cbn1cblxuXG5AbWVkaWEobWF4LXdpZHRoOjc2N3B4KSB7XG4gICAgLml0ZW0tdXNhZG97XG4gICAgICAgIGhlaWdodDogMzIwcHg7XG4gICAgfVxuXG4gICAgLml0ZW0tdXNhZG8tdGl0dWxve1xuICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XG4gICAgICAgIG1hcmdpbjogNXB4O1xuICAgICAgICBwYWRkaW5nOiA1cHg7XG5cbiAgICB9XG5cbn1cbiIsIi5pdGVtLXVzYWRvIHtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCUgNTAlO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA0MDBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbiAgYm9yZGVyOiAxcHggI0VGRUZFRiBzb2xpZDtcbiAgYm94LXNoYWRvdzogMCAxcHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuLml0ZW0tdXNhZG8tdGl0dWxvIHtcbiAgd2lkdGg6IDQwJTtcbiAgbWFyZ2luOiA1MHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMzAwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC42KTtcbiAgY29sb3I6ICNGRkZGRkY7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZzogMjBweDtcbn1cbi5pdGVtLXVzYWRvLXRpdHVsbyAuaXRlbS1pbWctYW5pbyBpbWcge1xuICB3aWR0aDogNjBweDtcbiAgaGVpZ2h0OiA2MHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG4uaXRlbS11c2Fkby10aXR1bG8gLml0ZW0taW1nLWFuaW8gZGl2IHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBjb2xvcjogI0ZGRkZGRjtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xufVxuLml0ZW0tdXNhZG8tdGl0dWxvIC5wcmljZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICBtYXJnaW46IDEwcHggMCAxMHB4IDA7XG59XG4uaXRlbS11c2Fkby10aXR1bG8gLml0ZW0taW5mbyB7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4uaXRlbS11c2Fkby10aXR1bG8gLml0ZW0taW5mbyBwIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4zKTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNvbG9yOiAjRUZFRkVGO1xuICBtYXJnaW46IDBweCAwIDAgMDtcbiAgcGFkZGluZzogNXB4IDAgNXB4IDA7XG59XG4uaXRlbS11c2Fkby10aXR1bG8gLml0ZW0taW5mbyBwOmxhc3QtY2hpbGQge1xuICBib3JkZXItYm90dG9tOiBub25lO1xufVxuLml0ZW0tdXNhZG8tdGl0dWxvIC5idG4tbWFzLWluZm8ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjlBRTQxO1xuICBjb2xvcjogIzIyMjtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgcGFkZGluZzogMTBweCAzMHB4IDEwcHggMzBweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgLml0ZW0tdXNhZG8ge1xuICAgIGhlaWdodDogMzIwcHg7XG4gIH1cblxuICAuaXRlbS11c2Fkby10aXR1bG8ge1xuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcbiAgICBtYXJnaW46IDVweDtcbiAgICBwYWRkaW5nOiA1cHg7XG4gIH1cbn0iXX0= */");

/***/ }),

/***/ "./src/app/singles/one-destacado/one-destacado.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/singles/one-destacado/one-destacado.component.ts ***!
  \******************************************************************/
/*! exports provided: OneDestacadoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OneDestacadoComponent", function() { return OneDestacadoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var OneDestacadoComponent = /** @class */ (function () {
    function OneDestacadoComponent() {
    }
    OneDestacadoComponent.prototype.ngOnInit = function () { };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], OneDestacadoComponent.prototype, "destacado", void 0);
    OneDestacadoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'one-destacado',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./one-destacado.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-destacado/one-destacado.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./one-destacado.component.scss */ "./src/app/singles/one-destacado/one-destacado.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], OneDestacadoComponent);
    return OneDestacadoComponent;
}());



/***/ }),

/***/ "./src/app/singles/one-oferta/one-oferta.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/singles/one-oferta/one-oferta.component.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NpbmdsZXMvb25lLW9mZXJ0YS9vbmUtb2ZlcnRhLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/singles/one-oferta/one-oferta.component.ts":
/*!************************************************************!*\
  !*** ./src/app/singles/one-oferta/one-oferta.component.ts ***!
  \************************************************************/
/*! exports provided: OneOfertaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OneOfertaComponent", function() { return OneOfertaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var OneOfertaComponent = /** @class */ (function () {
    function OneOfertaComponent() {
    }
    OneOfertaComponent.prototype.ngOnInit = function () {
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], OneOfertaComponent.prototype, "oferta", void 0);
    OneOfertaComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'one-oferta',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./one-oferta.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-oferta/one-oferta.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./one-oferta.component.scss */ "./src/app/singles/one-oferta/one-oferta.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], OneOfertaComponent);
    return OneOfertaComponent;
}());



/***/ }),

/***/ "./src/app/singles/one-producto/one-producto.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/singles/one-producto/one-producto.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  width: 100%;\n  display: inline-block;\n  margin-bottom: 20px;\n  background-color: #FFF;\n  border-radius: 8px;\n  /*box-shadow: 0 1px 5px 0 rgb(70 70 70 / 15%), 0 2px 2px 0 rgb(70 70 70 / 15%), 0 3px 2px -2px rgb(70 70 70 / 15%);*/\n  cursor: pointer;\n}\n:host .img {\n  border-top-left-radius: 8px;\n  border-top-right-radius: 8px;\n  overflow: hidden;\n  background-color: #f5f5f5;\n  width: 100%;\n  height: 190px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n:host .img img {\n  width: 100%;\n  height: auto;\n}\n:host .item .descipcion {\n  padding: 10px;\n}\n:host .item .descipcion h2 {\n  font-size: 16px;\n  line-height: 20px;\n  height: 20px;\n  overflow: hidden;\n  margin: 0;\n}\n:host .item .descipcion p {\n  color: #898989;\n  font-size: 14px;\n  margin: 10px 0;\n  line-height: 17px;\n  height: 17px;\n  font-weight: bold;\n  overflow: hidden;\n}\n:host .item .price {\n  padding: 0 10px 10px 10px;\n  color: #f3840d;\n  font-size: 20px;\n  line-height: 20px;\n  font-weight: 800;\n  text-align: left;\n  margin: 0;\n}\n:host:hover {\n  /*box-shadow: 0 6px 10px 1px rgb(70 70 70 / 15%), 0 4px 5px -2px rgb(70 70 70 / 15%);*/\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC9zaW5nbGVzL29uZS1wcm9kdWN0by9vbmUtcHJvZHVjdG8uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NpbmdsZXMvb25lLXByb2R1Y3RvL29uZS1wcm9kdWN0by5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7RUFFQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLG9IQUFBO0VBQ0EsZUFBQTtBQ0FGO0FERUU7RUFFRSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNESjtBREdJO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUNETjtBRFdFO0VBQ0UsYUFBQTtBQ1RKO0FEVUk7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0FDUk47QURXSTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNUTjtBRGFFO0VBQ0kseUJBQUE7RUFDQSxjQUFBO0VBRUEsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7QUNaTjtBRG1CQTtFQUNBLHNGQUFBO0FDaEJBIiwiZmlsZSI6InNyYy9hcHAvc2luZ2xlcy9vbmUtcHJvZHVjdG8vb25lLXByb2R1Y3RvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3R7XG4gIHdpZHRoOiAxMDAlO1xuXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRjtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAvKmJveC1zaGFkb3c6IDAgMXB4IDVweCAwIHJnYig3MCA3MCA3MCAvIDE1JSksIDAgMnB4IDJweCAwIHJnYig3MCA3MCA3MCAvIDE1JSksIDAgM3B4IDJweCAtMnB4IHJnYig3MCA3MCA3MCAvIDE1JSk7Ki9cbiAgY3Vyc29yOiBwb2ludGVyO1xuXG4gIC5pbWd7XG5cbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA4cHg7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDhweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmNWY1ZjU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxOTBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cbiAgICBpbWd7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogYXV0bztcbiAgICB9XG5cbn1cblxuXG5cblxuLml0ZW17XG5cbiAgLmRlc2NpcGNpb257XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBoMntcbiAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgIG1hcmdpbjogMDtcblxuICAgIH1cbiAgICBwe1xuICAgICAgY29sb3I6IHJnYigxMzcsIDEzNywgMTM3KTtcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgIG1hcmdpbjogMTBweCAwO1xuICAgICAgbGluZS1oZWlnaHQ6IDE3cHg7XG4gICAgICBoZWlnaHQ6IDE3cHg7XG4gICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgfVxuICB9XG5cbiAgLnByaWNle1xuICAgICAgcGFkZGluZzogMCAxMHB4IDEwcHggMTBweDtcbiAgICAgIGNvbG9yOiAjZjM4NDBkO1xuXG4gICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgbWFyZ2luOiAwO1xuICB9XG5cbn1cbn1cblxuXG46aG9zdDpob3ZlciB7XG4vKmJveC1zaGFkb3c6IDAgNnB4IDEwcHggMXB4IHJnYig3MCA3MCA3MCAvIDE1JSksIDAgNHB4IDVweCAtMnB4IHJnYig3MCA3MCA3MCAvIDE1JSk7Ki9cbn1cbiIsIjpob3N0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRjtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAvKmJveC1zaGFkb3c6IDAgMXB4IDVweCAwIHJnYig3MCA3MCA3MCAvIDE1JSksIDAgMnB4IDJweCAwIHJnYig3MCA3MCA3MCAvIDE1JSksIDAgM3B4IDJweCAtMnB4IHJnYig3MCA3MCA3MCAvIDE1JSk7Ki9cbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuOmhvc3QgLmltZyB7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDhweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDhweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjVmNTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTkwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuOmhvc3QgLmltZyBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuOmhvc3QgLml0ZW0gLmRlc2NpcGNpb24ge1xuICBwYWRkaW5nOiAxMHB4O1xufVxuOmhvc3QgLml0ZW0gLmRlc2NpcGNpb24gaDIge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIG1hcmdpbjogMDtcbn1cbjpob3N0IC5pdGVtIC5kZXNjaXBjaW9uIHAge1xuICBjb2xvcjogIzg5ODk4OTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW46IDEwcHggMDtcbiAgbGluZS1oZWlnaHQ6IDE3cHg7XG4gIGhlaWdodDogMTdweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG46aG9zdCAuaXRlbSAucHJpY2Uge1xuICBwYWRkaW5nOiAwIDEwcHggMTBweCAxMHB4O1xuICBjb2xvcjogI2YzODQwZDtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgbWFyZ2luOiAwO1xufVxuXG46aG9zdDpob3ZlciB7XG4gIC8qYm94LXNoYWRvdzogMCA2cHggMTBweCAxcHggcmdiKDcwIDcwIDcwIC8gMTUlKSwgMCA0cHggNXB4IC0ycHggcmdiKDcwIDcwIDcwIC8gMTUlKTsqL1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/singles/one-producto/one-producto.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/singles/one-producto/one-producto.component.ts ***!
  \****************************************************************/
/*! exports provided: OneProductoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OneProductoComponent", function() { return OneProductoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _shop_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shop-cart.service */ "./src/app/shop-cart.service.ts");



var OneProductoComponent = /** @class */ (function () {
    function OneProductoComponent(cartService) {
        this.cartService = cartService;
    }
    OneProductoComponent.prototype.ngOnInit = function () { };
    OneProductoComponent.prototype.getOldPrice = function () {
        return this.producto.replacement_price;
    };
    OneProductoComponent.prototype.isOffer = function () {
        return this.producto.offer_percentage ? true : false;
    };
    OneProductoComponent.prototype.offer = function () {
        return this.producto.offer_percentage;
    };
    OneProductoComponent.prototype.getNewPrice = function () {
        if (this.producto.offer_percentage)
            return (this.producto.replacement_price * (100 - parseInt(this.producto.offer_percentage)) / 100).toFixed(2);
        else
            return parseInt(this.producto.replacement_price).toFixed(2);
    };
    OneProductoComponent.ctorParameters = function () { return [
        { type: _shop_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShopCartService"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], OneProductoComponent.prototype, "producto", void 0);
    OneProductoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'one-producto',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./one-producto.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/singles/one-producto/one-producto.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./one-producto.component.scss */ "./src/app/singles/one-producto/one-producto.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_shop_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShopCartService"]])
    ], OneProductoComponent);
    return OneProductoComponent;
}());



/***/ }),

/***/ "./src/app/terminos/terminos.component.sass":
/*!**************************************************!*\
  !*** ./src/app/terminos/terminos.component.sass ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Rlcm1pbm9zL3Rlcm1pbm9zLmNvbXBvbmVudC5zYXNzIn0= */");

/***/ }),

/***/ "./src/app/terminos/terminos.component.ts":
/*!************************************************!*\
  !*** ./src/app/terminos/terminos.component.ts ***!
  \************************************************/
/*! exports provided: TerminosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TerminosComponent", function() { return TerminosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var TerminosComponent = /** @class */ (function () {
    function TerminosComponent() {
    }
    TerminosComponent.prototype.ngOnInit = function () {
    };
    TerminosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-terminos',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./terminos.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/terminos/terminos.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./terminos.component.sass */ "./src/app/terminos/terminos.component.sass")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], TerminosComponent);
    return TerminosComponent;
}());



/***/ }),

/***/ "./src/app/usado/usado.component.scss":
/*!********************************************!*\
  !*** ./src/app/usado/usado.component.scss ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host h4 {\n  font-size: 1.75rem;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n}\n:host .sub-bnr {\n  background-image: url(\"/assets/img/bg2.png\");\n}\n:host #btn_submit {\n  padding: 5px;\n  width: 100%;\n}\n:host h5 {\n  margin-bottom: 10px;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n}\n:host h6 {\n  font-size: 14px;\n  letter-spacing: normal;\n  text-transform: none;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n}\n:host .btn-black {\n  background-color: #f3840d;\n  color: #FFF;\n  border-radius: 5px;\n  margin: 10px 0;\n  padding: 10px 30px;\n  border: 0px;\n  width: 80%;\n}\n:host .anio {\n  margin: 0;\n  font-weight: 500;\n  font-size: 16px;\n  color: #000;\n}\n:host .price {\n  text-align: left;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 30px;\n  margin-bottom: 30px;\n  margin-top: 0;\n  color: #f3840d;\n}\n:host .price span {\n  font-size: 14px;\n  color: #000206;\n  font-weight: normal;\n}\n:host .caracteristicas {\n  margin: 30px 0;\n}\n:host .caracteristicas h3 {\n  text-align: center;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 45px;\n  margin-bottom: 30px;\n}\n:host .caracteristicas h4 {\n  margin-top: 20px;\n  text-align: left;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 25px;\n}\n:host .caracteristicas .info {\n  display: flex;\n  justify-content: space-between;\n  font-family: \"Open Sans\", sans-serif;\n  font-weight: 500;\n  font-size: 17px;\n  color: #000206;\n  border-bottom: 1.5px solid #000206;\n}\n:host .caracteristicas .info span:first-child {\n  width: 50%;\n  text-align: left;\n}\n:host .caracteristicas .info span:last-child {\n  width: 50%;\n  text-align: right;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC91c2Fkby91c2Fkby5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdXNhZG8vdXNhZG8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUk7RUFDRSxrQkFBQTtFQUVBLGdCQUFBO0VBQWtCLG9DQUFBO0FDRHhCO0FESUk7RUFDSSw0Q0FBQTtBQ0ZSO0FES0k7RUFDSSxZQUFBO0VBQ0EsV0FBQTtBQ0hSO0FET0k7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0VBQWtCLG9DQUFBO0FDSjFCO0FET0k7RUFDSSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQWtCLG9DQUFBO0FDSjFCO0FEU0k7RUFDRSx5QkFBQTtFQUNKLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FDUEY7QURVSTtFQUFNLFNBQUE7RUFBVyxnQkFBQTtFQUFrQixlQUFBO0VBQWlCLFdBQUE7QUNKeEQ7QURNSTtFQUNFLGdCQUFBO0VBQWtCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGVBQUE7RUFBaUIsbUJBQUE7RUFDM0YsYUFBQTtFQUNBLGNBQUE7QUNBTjtBREVNO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQ0FSO0FESUk7RUFFRSxjQUFBO0FDSE47QURLTTtFQUFJLGtCQUFBO0VBQW9CLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGVBQUE7RUFBaUIsbUJBQUE7QUNFdkc7QURFTTtFQUFJLGdCQUFBO0VBQWtCLGdCQUFBO0VBQWtCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGVBQUE7QUNLdEc7QURITTtFQUNFLGFBQUE7RUFDQSw4QkFBQTtFQUNBLG9DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtDQUFBO0FDS1I7QURIUTtFQUFrQixVQUFBO0VBQVksZ0JBQUE7QUNPdEM7QUROUTtFQUFpQixVQUFBO0VBQVksaUJBQUE7QUNVckMiLCJmaWxlIjoic3JjL2FwcC91c2Fkby91c2Fkby5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0e1xuXG4gICAgaDR7XG4gICAgICBmb250LXNpemU6IDEuNzVyZW07XG5cbiAgICAgIGZvbnQtd2VpZ2h0OiA4MDA7IGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcbiAgICB9XG5cbiAgICAuc3ViLWJucntcbiAgICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL2ltZy9iZzIucG5nJyk7XG4gICAgfVxuXG4gICAgI2J0bl9zdWJtaXR7XG4gICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuXG5cbiAgICBoNXtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDgwMDsgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xuICAgIH1cblxuICAgIGg2e1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICAgICAgICBmb250LXdlaWdodDogODAwOyBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XG5cbiAgICB9XG5cblxuICAgIC5idG4tYmxhY2t7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiNmMzg0MGQ7XG4gIGNvbG9yOiAjRkZGO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIG1hcmdpbjogMTBweCAwO1xuICBwYWRkaW5nOiAxMHB4IDMwcHg7XG4gIGJvcmRlcjogMHB4O1xuICB3aWR0aDogODAlO1xuXG4gICAgfVxuICAgIC5hbmlve21hcmdpbjogMDsgZm9udC13ZWlnaHQ6IDUwMDsgZm9udC1zaXplOiAxNnB4OyBjb2xvcjogIzAwMDsgIH1cblxuICAgIC5wcmljZXtcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7IGZvbnQtd2VpZ2h0OiA4MDA7IGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjsgZm9udC1zaXplOiAzMHB4OyBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICAgICAgbWFyZ2luLXRvcDogMDtcbiAgICAgIGNvbG9yOiAjZjM4NDBkO1xuXG4gICAgICBzcGFue1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGNvbG9yOiMwMDAyMDY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLmNhcmFjdGVyaXN0aWNhc3tcblxuICAgICAgbWFyZ2luOiAzMHB4IDA7XG5cbiAgICAgIGgzeyB0ZXh0LWFsaWduOiBjZW50ZXI7IGZvbnQtd2VpZ2h0OiA4MDA7IGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjsgZm9udC1zaXplOiA0NXB4OyBtYXJnaW4tYm90dG9tOiAzMHB4O31cblxuXG5cbiAgICAgIGg0eyBtYXJnaW4tdG9wOiAyMHB4OyB0ZXh0LWFsaWduOiBsZWZ0OyBmb250LXdlaWdodDogODAwOyBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7IGZvbnQtc2l6ZTogMjVweDt9XG5cbiAgICAgIC5pbmZve1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgICAgICBjb2xvcjogIzAwMDIwNjtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMS41cHggc29saWQgIzAwMDIwNjtcblxuICAgICAgICBzcGFuOmZpcnN0LWNoaWxkeyB3aWR0aDogNTAlOyB0ZXh0LWFsaWduOiBsZWZ0O31cbiAgICAgICAgc3BhbjpsYXN0LWNoaWxkeyB3aWR0aDogNTAlOyB0ZXh0LWFsaWduOiByaWdodDt9XG4gICAgICB9XG5cbiAgICB9XG5cbn1cbiIsIjpob3N0IGg0IHtcbiAgZm9udC1zaXplOiAxLjc1cmVtO1xuICBmb250LXdlaWdodDogODAwO1xuICBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjtcbn1cbjpob3N0IC5zdWItYm5yIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWcvYmcyLnBuZ1wiKTtcbn1cbjpob3N0ICNidG5fc3VibWl0IHtcbiAgcGFkZGluZzogNXB4O1xuICB3aWR0aDogMTAwJTtcbn1cbjpob3N0IGg1IHtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7XG59XG46aG9zdCBoNiB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xufVxuOmhvc3QgLmJ0bi1ibGFjayB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMzg0MGQ7XG4gIGNvbG9yOiAjRkZGO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIG1hcmdpbjogMTBweCAwO1xuICBwYWRkaW5nOiAxMHB4IDMwcHg7XG4gIGJvcmRlcjogMHB4O1xuICB3aWR0aDogODAlO1xufVxuOmhvc3QgLmFuaW8ge1xuICBtYXJnaW46IDA7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgY29sb3I6ICMwMDA7XG59XG46aG9zdCAucHJpY2Uge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogODAwO1xuICBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICBtYXJnaW4tdG9wOiAwO1xuICBjb2xvcjogI2YzODQwZDtcbn1cbjpob3N0IC5wcmljZSBzcGFuIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzAwMDIwNjtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbjpob3N0IC5jYXJhY3RlcmlzdGljYXMge1xuICBtYXJnaW46IDMwcHggMDtcbn1cbjpob3N0IC5jYXJhY3RlcmlzdGljYXMgaDMge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDQ1cHg7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG46aG9zdCAuY2FyYWN0ZXJpc3RpY2FzIGg0IHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cbjpob3N0IC5jYXJhY3RlcmlzdGljYXMgLmluZm8ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xuICBmb250LXdlaWdodDogNTAwO1xuICBmb250LXNpemU6IDE3cHg7XG4gIGNvbG9yOiAjMDAwMjA2O1xuICBib3JkZXItYm90dG9tOiAxLjVweCBzb2xpZCAjMDAwMjA2O1xufVxuOmhvc3QgLmNhcmFjdGVyaXN0aWNhcyAuaW5mbyBzcGFuOmZpcnN0LWNoaWxkIHtcbiAgd2lkdGg6IDUwJTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cbjpob3N0IC5jYXJhY3RlcmlzdGljYXMgLmluZm8gc3BhbjpsYXN0LWNoaWxkIHtcbiAgd2lkdGg6IDUwJTtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/usado/usado.component.ts":
/*!******************************************!*\
  !*** ./src/app/usado/usado.component.ts ***!
  \******************************************/
/*! exports provided: UsadoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsadoComponent", function() { return UsadoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/replacement.service */ "./src/app/service/replacement.service.ts");




var UsadoComponent = /** @class */ (function () {
    function UsadoComponent(rs, route) {
        this.rs = rs;
        this.route = route;
    }
    UsadoComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = this.route.snapshot.paramMap.get('id');
        this.rs.getUsado(id).subscribe(function (data) {
            _this.usado = data;
        }, function (error) { console.log(error); });
        this.rs.getUsadoImagenes(id).subscribe(function (data) {
            _this.imagenes = data;
        }, function (error) { console.log(error); });
    };
    UsadoComponent.prototype.contacto = function (form) {
        console.log(form.value);
        form.value.unidad = this.usado.brand_name + ' ' + this.usado.vehicle_model + ' (' + this.usado.vehicle_year + ')';
        this.rs.sendContact(form.value)
            .subscribe(function (data) {
            form.reset();
            alert("Mensaje enviado");
        }, function (error) {
            alert("Intente más tarde");
        });
    };
    UsadoComponent.ctorParameters = function () { return [
        { type: _service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] }
    ]; };
    UsadoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-usado',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./usado.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/usado/usado.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./usado.component.scss */ "./src/app/usado/usado.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_replacement_service__WEBPACK_IMPORTED_MODULE_3__["ReplacementService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], UsadoComponent);
    return UsadoComponent;
}());



/***/ }),

/***/ "./src/app/usados/filtro-usados/filtro-usados.component.sass":
/*!*******************************************************************!*\
  !*** ./src/app/usados/filtro-usados/filtro-usados.component.sass ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzYWRvcy9maWx0cm8tdXNhZG9zL2ZpbHRyby11c2Fkb3MuY29tcG9uZW50LnNhc3MifQ== */");

/***/ }),

/***/ "./src/app/usados/filtro-usados/filtro-usados.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/usados/filtro-usados/filtro-usados.component.ts ***!
  \*****************************************************************/
/*! exports provided: FiltroUsadosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltroUsadosComponent", function() { return FiltroUsadosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var src_app_service_usados_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/usados.service */ "./src/app/service/usados.service.ts");




var FiltroUsadosComponent = /** @class */ (function () {
    function FiltroUsadosComponent(us) {
        this.us = us;
        this.categorias = [];
        this.marcas = [];
        this.colores = [];
        this.largo = [];
        this.modelos = [];
        this.anios = [];
    }
    FiltroUsadosComponent.prototype.ngOnInit = function () {
        this.obtenerCategorias();
        this.obtenerMarcas();
        this.obtenerColores();
        this.obtenerLargo();
        this.obtenerAnio();
        this.obtenerModelo();
    };
    FiltroUsadosComponent.prototype.obtenerCategorias = function () {
        var _this = this;
        this.us.getCategories().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data.map(function (e) { return ({
                id: e.type_id,
                label: e.type_name
            }); });
        })).subscribe(function (data) { return _this.categorias = data; });
    };
    FiltroUsadosComponent.prototype.obtenerMarcas = function () {
        var _this = this;
        this.us.getBrands().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data.map(function (e) { return ({
                id: e.brand_id,
                label: e.brand_name
            }); });
        })).subscribe(function (data) { return _this.marcas = data; });
    };
    FiltroUsadosComponent.prototype.obtenerColores = function () {
        var _this = this;
        this.us.getColor().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data.map(function (e) { return ({
                id: e.vehicle_color,
                label: e.vehicle_color
            }); });
        })).subscribe(function (data) { return _this.colores = data; });
    };
    FiltroUsadosComponent.prototype.obtenerLargo = function () {
        var _this = this;
        this.us.getLong().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data.map(function (e) { return ({
                id: e.vehicle_long,
                label: e.vehicle_long
            }); });
        })).subscribe(function (data) { return _this.largo = data; });
    };
    FiltroUsadosComponent.prototype.obtenerModelo = function () {
        var _this = this;
        this.us.getModel().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data.map(function (e) { return ({
                id: e.vehicle_model,
                label: e.vehicle_model
            }); });
        })).subscribe(function (data) { return _this.modelos = data; });
    };
    FiltroUsadosComponent.prototype.obtenerAnio = function () {
        var _this = this;
        this.us.getYears().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            return data.map(function (e) { return ({
                id: e.vehicle_year,
                label: e.vehicle_year
            }); });
        })).subscribe(function (data) { return _this.anios = data; });
    };
    FiltroUsadosComponent.ctorParameters = function () { return [
        { type: src_app_service_usados_service__WEBPACK_IMPORTED_MODULE_3__["UsadosService"] }
    ]; };
    FiltroUsadosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'filtro-usados',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./filtro-usados.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/usados/filtro-usados/filtro-usados.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./filtro-usados.component.sass */ "./src/app/usados/filtro-usados/filtro-usados.component.sass")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [src_app_service_usados_service__WEBPACK_IMPORTED_MODULE_3__["UsadosService"]])
    ], FiltroUsadosComponent);
    return FiltroUsadosComponent;
}());



/***/ }),

/***/ "./src/app/usados/lista-usados/lista-usados.component.sass":
/*!*****************************************************************!*\
  !*** ./src/app/usados/lista-usados/lista-usados.component.sass ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzYWRvcy9saXN0YS11c2Fkb3MvbGlzdGEtdXNhZG9zLmNvbXBvbmVudC5zYXNzIn0= */");

/***/ }),

/***/ "./src/app/usados/lista-usados/lista-usados.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/usados/lista-usados/lista-usados.component.ts ***!
  \***************************************************************/
/*! exports provided: ListaUsadosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaUsadosComponent", function() { return ListaUsadosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");


var ListaUsadosComponent = /** @class */ (function () {
    function ListaUsadosComponent() {
    }
    ListaUsadosComponent.prototype.ngOnInit = function () {
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Array)
    ], ListaUsadosComponent.prototype, "datos", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Boolean)
    ], ListaUsadosComponent.prototype, "showFiltros", void 0);
    ListaUsadosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'lista-usados',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./lista-usados.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/usados/lista-usados/lista-usados.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./lista-usados.component.sass */ "./src/app/usados/lista-usados/lista-usados.component.sass")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], ListaUsadosComponent);
    return ListaUsadosComponent;
}());



/***/ }),

/***/ "./src/app/usados/usados.component.scss":
/*!**********************************************!*\
  !*** ./src/app/usados/usados.component.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".item-name p {\n  font-size: 18px;\n}\n\n.sub-bnr {\n  background-position: bottom;\n}\n\n.btn-load-more {\n  background-color: #000;\n  color: #FFF;\n  border-radius: 5px;\n  margin: 10px;\n  padding: 10px 30px;\n  border: 0px;\n}\n\n.showhidefilter {\n  font-weight: bold;\n  font-size: 13px;\n  cursor: pointer;\n}\n\n.moreelements {\n  background-color: #EFEFEF;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC91c2Fkb3MvdXNhZG9zLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC91c2Fkb3MvdXNhZG9zLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtBQ0NKOztBREVBO0VBQVMsMkJBQUE7QUNFVDs7QURDQTtFQUNFLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0VGOztBREtBO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQ0ZGOztBREtBO0VBQ0UseUJBQUE7QUNGRiIsImZpbGUiOiJzcmMvYXBwL3VzYWRvcy91c2Fkb3MuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaXRlbS1uYW1lIHB7XG4gICAgZm9udC1zaXplOiAxOHB4O1xufVxuXG4uc3ViLWJucntiYWNrZ3JvdW5kLXBvc2l0aW9uOiBib3R0b207fVxuXG5cbi5idG4tbG9hZC1tb3Jle1xuICBiYWNrZ3JvdW5kLWNvbG9yOiMwMDA7XG4gIGNvbG9yOiAjRkZGO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIG1hcmdpbjogMTBweDtcbiAgcGFkZGluZzogMTBweCAzMHB4O1xuICBib3JkZXI6IDBweDtcbn1cblxuXG5cblxuXG4uc2hvd2hpZGVmaWx0ZXJ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDEzcHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLm1vcmVlbGVtZW50c3tcbiAgYmFja2dyb3VuZC1jb2xvcjogI0VGRUZFRjtcbn1cbiIsIi5pdGVtLW5hbWUgcCB7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cblxuLnN1Yi1ibnIge1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBib3R0b207XG59XG5cbi5idG4tbG9hZC1tb3JlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcbiAgY29sb3I6ICNGRkY7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luOiAxMHB4O1xuICBwYWRkaW5nOiAxMHB4IDMwcHg7XG4gIGJvcmRlcjogMHB4O1xufVxuXG4uc2hvd2hpZGVmaWx0ZXIge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5tb3JlZWxlbWVudHMge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUZFRkVGO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/usados/usados.component.ts":
/*!********************************************!*\
  !*** ./src/app/usados/usados.component.ts ***!
  \********************************************/
/*! exports provided: UsadosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsadosComponent", function() { return UsadosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _service_usados_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/usados.service */ "./src/app/service/usados.service.ts");
/* harmony import */ var _service_filtro_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/filtro.service */ "./src/app/service/filtro.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var UsadosComponent = /** @class */ (function () {
    function UsadosComponent(us, fs) {
        this.us = us;
        this.fs = fs;
        this.filtros = [];
        this.showFiltros = true;
        this.loading = false;
        this.offset = 0;
        this.cantidad = 12;
        this.getMore = true;
        this.usados = [];
    }
    UsadosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fs.currentFilters.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (x) { return console.log(x); }))
            .subscribe(function (filtros) {
            _this.filtros = filtros;
            _this.obtenerProductos();
        });
    };
    UsadosComponent.prototype.loadmore = function () {
        var _this = this;
        if (this.getMore && !this.loading) {
            this.loading = true;
            this.offset = this.offset + this.cantidad;
            this.us.getProducts(this.filtros, this.cantidad, this.offset).subscribe(function (response) {
                var _a;
                (_a = _this.usados).push.apply(_a, response);
                _this.loading = false;
                if (response.length < _this.cantidad) {
                    _this.getMore = false;
                }
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }
    };
    UsadosComponent.prototype.obtenerProductos = function () {
        //if(filtros.length > 0){
        var _this = this;
        this.us.getProducts(this.filtros, this.cantidad, this.offset).subscribe(function (response) {
            _this.usados = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(response);
            _this.loading = false;
        }, function (error) {
            console.log(JSON.stringify(error));
        });
        //}
    };
    UsadosComponent.prototype.updateObserver = function () {
        this.fs.update();
    };
    UsadosComponent.ctorParameters = function () { return [
        { type: _service_usados_service__WEBPACK_IMPORTED_MODULE_2__["UsadosService"] },
        { type: _service_filtro_service__WEBPACK_IMPORTED_MODULE_3__["FiltroService"] }
    ]; };
    UsadosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-usados',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./usados.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/usados/usados.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./usados.component.scss */ "./src/app/usados/usados.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_usados_service__WEBPACK_IMPORTED_MODULE_2__["UsadosService"],
            _service_filtro_service__WEBPACK_IMPORTED_MODULE_3__["FiltroService"]])
    ], UsadosComponent);
    return UsadosComponent;
}());

/*
vehicle_id: "1274"
vehicle_type_id: "1"
vehicle_brand_id: "61"
vehicle_state: "Usado"
vehicle_model: "PREMIUM 440 DXI OPTI-DRIVER"
vehicle_year: "2014"
vehicle_fuel: "DIESEL"
vehicle_brake: "DISCO ABS EBS "
vehicle_traction: "4X2"
vehicle_direction: "Asistida "
vehicle_city: "1"
vehicle_color: "BLANCO"
vehicle_km: "772.167"
vehicle_motor: "11 LITROS "
vehicle_power: "440"
vehicle_description: ""
vehicle_equipment: ""
vehicle_entry: "2020-03-16 17:37:34"
vehicle_thumbnail_url: "http://admin.usados.rutasurtrucks.com.ar/uploads/thumb/2020-03-16-05-42-18da9d3085-1a13-45c2-a1f2-bda90737e465.jpg"
vehicle_view: "ONLINE"
vehicle_notification: "1"
vehicle_long: "TRACTOR"
vehicle_destacado: "1"
company_id: "1"
company_name: "BAHIA BLANCA"
company_description: "Somos concesionario oficial Volvo Trucks, Renault Trucks, remolques Cormetal, Lambert y Randon. Contamos con concesionarios en las ciudades de Bahia Blanca, Quequén, Comodoro Rivadavia, Allen y Mar del Plata"
company_phone: "02262-15411046"
company_address: "Ruta 3 km 696"
company_lat: ""
company_long: ""
company_email: "info@rutasurtrucks.com.ar"
company_schedule: "08:30 a 19:00 hs. Lunes a Viernes."
company_whasapp: "542915077534"
brand_id: "61"
brand_name: "RENAULT"
brand_logo: null
brand_position: null
type_id: "1"
type_name: "CAMIONES"
type_image: "https://i.ibb.co/BGWdGPq/c-VUkssss3.png"
type_position: "1"
type_top: null
*/


/***/ }),

/***/ "./src/app/vender/vender.component.scss":
/*!**********************************************!*\
  !*** ./src/app/vender/vender.component.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".cart-ship-info ul.row {\n  margin-bottom: 50px;\n}\n\n.camiones {\n  display: flex;\n  flex-direction: column;\n  width: 100%;\n}\n\n.camiones .col {\n  display: flex;\n}\n\n.camiones .col .custom-file-upload {\n  background-color: #d4d3d3;\n  padding: 12px;\n  border-radius: 15px;\n  margin: 10px 5px;\n  width: auto;\n}\n\n.camiones .col .custom-file-upload span {\n  background-color: #fa8303;\n  color: #FFFFFF;\n  padding: 10px;\n  border-radius: 10px;\n}\n\n.camiones .col .selected {\n  background-color: #3dd596;\n}\n\n.camiones .col img {\n  height: 120px;\n}\n\n.camiones .col div:first-child {\n  text-align: center;\n  width: 70%;\n}\n\n.camiones .col div:last-child {\n  text-align: center;\n  width: 30%;\n}\n\n.camiones input[type=file] {\n  display: none;\n}\n\nh2 {\n  text-align: center;\n  color: #fa8303;\n  margin: 30px 0;\n}\n\nh3 {\n  color: #fa8303;\n}\n\nh4 {\n  color: #6788a2;\n  font-size: 18px;\n}\n\n.btn-primary {\n  background-color: #fa8303;\n  color: #FFFFFF;\n  border-radius: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9icmF2b2VkdS9Eb2N1bWVudHMvQlBTaXN0ZW1hcy9ydXRhU3VyL21lcmNhZG8tY2FtaW9uZXMvc3JjL2FwcC92ZW5kZXIvdmVuZGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC92ZW5kZXIvdmVuZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtFO0VBQ0UsbUJBQUE7QUNKSjs7QURRQTtFQUdBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7QUNQQTs7QURVRTtFQUNFLGFBQUE7QUNSSjs7QURVSTtFQUVFLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBRUEsZ0JBQUE7RUFDQSxXQUFBO0FDVk47O0FEWU07RUFDRSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUNWUjs7QURpQkk7RUFDRSx5QkFBQTtBQ2ZOOztBRGtCSTtFQUNFLGFBQUE7QUNoQk47O0FEbUJJO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0FDakJOOztBRG1CSTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtBQ2pCTjs7QUR3QkU7RUFDRSxhQUFBO0FDdEJKOztBRDJCQTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUN4QkY7O0FEMkJBO0VBQ0UsY0FBQTtBQ3hCRjs7QUQyQkE7RUFDRSxjQUFBO0VBQ0EsZUFBQTtBQ3hCRjs7QUQyQkE7RUFDRSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQ3hCRiIsImZpbGUiOiJzcmMvYXBwL3ZlbmRlci92ZW5kZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FydC1zaGlwLWluZm97XG5cblxuXG5cbiAgdWwucm93e1xuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XG4gIH1cbn1cblxuLmNhbWlvbmVze1xuXG5cbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xud2lkdGg6IDEwMCU7XG5cblxuICAuY29se1xuICAgIGRpc3BsYXk6IGZsZXg7XG5cbiAgICAuY3VzdG9tLWZpbGUtdXBsb2Fke1xuXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDRkM2QzO1xuICAgICAgcGFkZGluZzogMTJweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG5cbiAgICAgIG1hcmdpbjogMTBweCA1cHg7XG4gICAgICB3aWR0aDogYXV0bztcblxuICAgICAgc3BhbntcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZhODMwMztcbiAgICAgICAgY29sb3I6ICNGRkZGRkY7XG4gICAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG5cbiAgICAgIH1cblxuXG5cbiAgICB9XG4gICAgLnNlbGVjdGVke1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzNkZDU5NjtcbiAgICB9XG5cbiAgICBpbWd7XG4gICAgICBoZWlnaHQ6IDEyMHB4O1xuICAgIH1cblxuICAgIGRpdjpmaXJzdC1jaGlsZHtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIHdpZHRoOiA3MCU7XG4gICAgfVxuICAgIGRpdjpsYXN0LWNoaWxke1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgd2lkdGg6IDMwJTtcbiAgICB9XG5cblxuICB9XG5cblxuICBpbnB1dFt0eXBlPVwiZmlsZVwiXSB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG59XG5cbmgye1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjZmE4MzAzO1xuICBtYXJnaW46IDMwcHggMDtcbn1cblxuaDN7XG4gIGNvbG9yOiAjZmE4MzAzO1xufVxuXG5oNHtcbiAgY29sb3I6ICM2Nzg4YTI7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cblxuLmJ0bi1wcmltYXJ5e1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmE4MzAzO1xuICBjb2xvcjogI0ZGRkZGRjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cbiIsIi5jYXJ0LXNoaXAtaW5mbyB1bC5yb3cge1xuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xufVxuXG4uY2FtaW9uZXMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB3aWR0aDogMTAwJTtcbn1cbi5jYW1pb25lcyAuY29sIHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5jYW1pb25lcyAuY29sIC5jdXN0b20tZmlsZS11cGxvYWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDRkM2QzO1xuICBwYWRkaW5nOiAxMnB4O1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBtYXJnaW46IDEwcHggNXB4O1xuICB3aWR0aDogYXV0bztcbn1cbi5jYW1pb25lcyAuY29sIC5jdXN0b20tZmlsZS11cGxvYWQgc3BhbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmYTgzMDM7XG4gIGNvbG9yOiAjRkZGRkZGO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuLmNhbWlvbmVzIC5jb2wgLnNlbGVjdGVkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNkZDU5Njtcbn1cbi5jYW1pb25lcyAuY29sIGltZyB7XG4gIGhlaWdodDogMTIwcHg7XG59XG4uY2FtaW9uZXMgLmNvbCBkaXY6Zmlyc3QtY2hpbGQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiA3MCU7XG59XG4uY2FtaW9uZXMgLmNvbCBkaXY6bGFzdC1jaGlsZCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDMwJTtcbn1cbi5jYW1pb25lcyBpbnB1dFt0eXBlPWZpbGVdIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuaDIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjZmE4MzAzO1xuICBtYXJnaW46IDMwcHggMDtcbn1cblxuaDMge1xuICBjb2xvcjogI2ZhODMwMztcbn1cblxuaDQge1xuICBjb2xvcjogIzY3ODhhMjtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuXG4uYnRuLXByaW1hcnkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmE4MzAzO1xuICBjb2xvcjogI0ZGRkZGRjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/vender/vender.component.ts":
/*!********************************************!*\
  !*** ./src/app/vender/vender.component.ts ***!
  \********************************************/
/*! exports provided: VenderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VenderComponent", function() { return VenderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm5/forms.js");
/* harmony import */ var _shop_cart_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shop-cart.service */ "./src/app/shop-cart.service.ts");




var VenderComponent = /** @class */ (function () {
    function VenderComponent(shopCart) {
        this.shopCart = shopCart;
        this.enviado = false;
        this.myForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            marca: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            anio: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            kilometros: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            observaciones: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
            foto1: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
            foto2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
            foto3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
            foto4: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
            fileSource1: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
            fileSource2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
            fileSource3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', []),
            fileSource4: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [])
        });
    }
    Object.defineProperty(VenderComponent.prototype, "f", {
        get: function () {
            return this.myForm;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VenderComponent.prototype, "foto1", {
        get: function () {
            if (this.myForm.get('fileSource1').value)
                return true;
            else
                return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VenderComponent.prototype, "foto2", {
        get: function () {
            if (this.myForm.get('fileSource2').value)
                return true;
            else
                return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VenderComponent.prototype, "foto3", {
        get: function () {
            if (this.myForm.get('fileSource3').value)
                return true;
            else
                return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VenderComponent.prototype, "foto4", {
        get: function () {
            if (this.myForm.get('fileSource4').value)
                return true;
            else
                return false;
        },
        enumerable: true,
        configurable: true
    });
    VenderComponent.prototype.ngOnInit = function () {
        console.log(this.f.status);
    };
    VenderComponent.prototype.status = function () {
        console.log(this.f);
        return this.f.valid;
    };
    VenderComponent.prototype.enviarCamion = function () {
        var _this = this;
        this.shopCart.enviarcamion(this.myForm.value).subscribe(function (data) {
            //console.log(data);
            _this.myForm.reset();
            _this.enviado = true;
            alert("Mensaje enviado correctamente");
        }, function (error) {
            //console.log(error);
            alert("Intente mas tarde");
        });
    };
    VenderComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            var file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = function () {
                _this.imageSrc = reader.result;
                var foto = 'fileSource' + event.target.ariaLabel;
                _this.myForm.get(foto).patchValue(reader.result);
                console.log(_this.myForm.value);
            };
        }
    };
    VenderComponent.ctorParameters = function () { return [
        { type: _shop_cart_service__WEBPACK_IMPORTED_MODULE_3__["ShopCartService"] }
    ]; };
    VenderComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-vender',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./vender.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/vender/vender.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./vender.component.scss */ "./src/app/vender/vender.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_shop_cart_service__WEBPACK_IMPORTED_MODULE_3__["ShopCartService"]])
    ], VenderComponent);
    return VenderComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyCFm7PhIuouaenao-cQbbK14wJAmECRFL0",
        authDomain: "mercado-camiones.firebaseapp.com",
        databaseURL: "https://mercado-camiones.firebaseio.com",
        projectId: "mercado-camiones",
        storageBucket: "mercado-camiones.appspot.com",
        messagingSenderId: "108660176462",
        appId: "1:108660176462:web:2b96163d63be678aad751f",
        measurementId: "G-MZWGP4B5P4"
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/__ivy_ngcc__/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/bravoedu/Documents/BPSistemas/rutaSur/mercado-camiones/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map