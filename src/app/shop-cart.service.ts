import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShopCartService {

  //estructura del carrito
  cart: any;

  constructor(private httpClient: HttpClient) {
    this.cart = JSON.parse(window.localStorage.getItem('cart'));
    if (this.cart == null){
      this.cart = [];
    }
  }

  addToCart(product, cant){
    let found = 0;
      for(let elem of this.cart){
        if(elem.info.replacement_id == product.replacement_id){
          elem.amount += cant;
          elem.total = parseFloat(elem.total) + parseFloat(product.replacement_price)*cant;
          found = 1;
          break;
        }
      }
    if(found==0){
      this.cart.push({
        "info": product,
        "amount": cant,
        "total": product.replacement_price*cant
      });
    }
    window.localStorage.setItem('cart',JSON.stringify(this.cart));

  }

  modifyCantProd(index,amount){
    if (this.cart != null){
      if(this.cart[index]){
        this.cart[index].amount = amount;
        this.cart[index].total = parseFloat(this.cart[index].info.replacement_price)*amount;
        window.localStorage.setItem('cart',JSON.stringify(this.cart));
      }
    }
  }

  removeFromCart(index){
    //eliminar del arreglo dada el index
    if(this.cart[index]){
      this.cart.splice(index,1);
    }
    window.localStorage.setItem('cart',JSON.stringify(this.cart));
  }

  getProducts(){
    //retornar todos los elementos del carrito
    this.cart = JSON.parse(window.localStorage.getItem('cart'));
    return this.cart;

  }
  getCantidad(){
    if(this.cart != undefined)
      return this.cart.length;
    else
      return 0;
  }

  getTotal(){
    this.cart = JSON.parse(localStorage.getItem('cart'));
    let total=0;
    if(this.cart != undefined){
      for(let elem of this.cart){
          total += total + elem.amount*elem.info.replacement_price;
      }
    }
    return total;
  }

  reset(){
    this.cart = undefined;
    window.localStorage.removeItem('cart');
  }

  enviarpedido(postData: any){
      let API_URL : string = "https://api.rutasurtrucks.com.ar/";
      return this.httpClient.post(API_URL + 'pedido/',postData);
  }

  enviarcamion(postData: any){
    let API_URL : string = "https://api.rutasurtrucks.com.ar/";
    return this.httpClient.post(API_URL + 'contactos/vender/',postData);
}

}
