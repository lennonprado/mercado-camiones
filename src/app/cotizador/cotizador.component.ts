import { FormsService } from './../forms.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ReplacementService } from '../service/replacement.service';

@Component({
  selector: 'app-cotizador',
  templateUrl: './cotizador.component.html',
  styleUrls: ['./cotizador.component.scss']
})
export class CotizadorComponent implements OnInit {

  enviado : boolean = false;
  infoClient: any;

  constructor(private rs : ReplacementService, private fs : FormsService) { }

  ngOnInit() {
    this.getInfoContacto();
  }

  contacto(form: NgForm) {
    form.value.contact_type = "Seguro";
    //console.log(form.value);
    this.rs.enviarmensaje(form.value).subscribe(
      data => {
        form.reset();
        this.enviado = true
        //alert("Mensaje enviado")
      },
      error => {
        console.log(error);
        alert("Intente mas tarde");
      }
    );
    
    let info = {
      "contact_name": form.value.contact_name,
      "contact_email": form.value.contact_email,
      "contact_phone": form.value.contact_phone,
      "contact_city": form.value.contact_city
    };

    this.fs.saveInfo(info);
  }

  getInfoContacto(){
    this.infoClient = this.fs.getInfo();
  }

}
