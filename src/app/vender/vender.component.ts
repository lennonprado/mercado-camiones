import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';

import { ShopCartService } from '../shop-cart.service';

@Component({
  selector: 'app-vender',
  templateUrl: './vender.component.html',
  styleUrls: ['./vender.component.scss']
})
export class VenderComponent implements OnInit {




  enviado : boolean = false;
  imageSrc: string;

  myForm = new FormGroup({

    nombre: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    telefono: new FormControl('', [Validators.required]),

    marca: new FormControl('', [Validators.required]),
    anio: new FormControl('', [Validators.required]),
    kilometros: new FormControl('', [Validators.required]),

    observaciones: new FormControl('',[]),

    foto1: new FormControl('',[]),
    foto2: new FormControl('',[]),
    foto3: new FormControl('',[]),
    foto4: new FormControl('',[]),

    fileSource1: new FormControl('',[]),
    fileSource2: new FormControl('',[]),
    fileSource3: new FormControl('',[]),
    fileSource4: new FormControl('',[])

  });


  constructor(private shopCart: ShopCartService) {
  }

  get f(){
    return this.myForm
  }

  get foto1(){
    if(this.myForm.get('fileSource1').value)
      return true
    else
      return false
  }
  get foto2(){
    if(this.myForm.get('fileSource2').value)
      return true
    else
      return false
  }
  get foto3(){
    if(this.myForm.get('fileSource3').value)
      return true
    else
      return false
  }
  get foto4(){
    if(this.myForm.get('fileSource4').value)
      return true
    else
      return false
  }

  ngOnInit(): void {
    console.log(this.f.status);
  }

  status(){
    console.log(this.f);

    return this.f.valid
  }

  enviarCamion() {
    this.shopCart.enviarcamion(this.myForm.value).subscribe(
      data => {
        //console.log(data);
        this.myForm.reset();
        this.enviado = true;
        alert("Mensaje enviado correctamente")
      },
      error => {
        //console.log(error);
        alert("Intente mas tarde");
      }
    );
  }

  onFileChange(event) {
    const reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageSrc = reader.result as string;
        const foto = 'fileSource' +  event.target.ariaLabel

        this.myForm.get(foto).patchValue(reader.result)
        console.log(this.myForm.value);

      };
    }

  }
}
