import { Component, OnInit } from '@angular/core';
import { ReplacementService } from '../service/replacement.service';

@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.component.html',
  styleUrls: ['./ofertas.component.sass']
})
export class OfertasComponent implements OnInit {

  ofertas: any;

  constructor(private replacementService: ReplacementService) { }



  
  obtenerOfertas(){
    this.replacementService.getOffers().subscribe( response => {
      this.ofertas = response;
    },
    error => {
      console.log(JSON.stringify(error));
    });
  }


  ngOnInit() {
    this.obtenerOfertas();
  }

}
