import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Filtro } from '../models/filtro';

@Injectable({
  providedIn: 'root'
})
export class ReplacementService {

  constructor(private httpClient: HttpClient) { }

  getCategories(): Observable<any>{
    const url_api = "https://api.rutasurtrucks.com.ar/repuestos/categorias";
    return this.httpClient.get(url_api);
  }

  getBrands(): Observable<any>{
    const url_api = "https://api.rutasurtrucks.com.ar/repuestos/marcas";
    return this.httpClient.get(url_api);
  }

  getCategory(id: string): Observable<any>{
    const url_api = `https://api.rutasurtrucks.com.ar/repuestos/categorias/${id}`;
    return  this.httpClient.get(url_api);
  }

  getProducts(filtros: Filtro[], limit, offset): Observable<any>{

    const params = filtros.map( f => {
      return f.type + '=' + f.selected.map(e => e.id ).join(',')
    }).join('&')

    const url_api = `https://api.rutasurtrucks.com.ar/repuestos/?${params}&limit=${limit}&offset=${offset}`;
    console.log(url_api);

    return this.httpClient.get(url_api);
  }


  getProduct(id: string): Observable<any>{
    const url_api = `https://api.rutasurtrucks.com.ar/repuestos/${id}`;
    return this.httpClient.get(url_api);
  }

  getProductImages(id: string): Observable<any>{
    const url_api = `https://api.rutasurtrucks.com.ar/repuestos/imagenesRepuesto/${id}`;
    return this.httpClient.get(url_api);
  }

  getOffers(): Observable<any>{
    const url_api = "https://api.rutasurtrucks.com.ar/repuestos/ofertas";
    return this.httpClient.get(url_api);
  }

  getOffer(id:string): Observable<any>{
    const url_api = `https://api.rutasurtrucks.com.ar/repuestos/ofertas/${id}`;
    return this.httpClient.get(url_api);
  }

  getOfferProducts(id:string): Observable<any>{
    const url_api = `https://api.rutasurtrucks.com.ar/repuestos/ofertas/${id}/productos`;
    return this.httpClient.get(url_api);
  }

  getFeatured(limit,offset): Observable<any>{

    const url_api = `https://api.rutasurtrucks.com.ar/repuestos/?limit=${limit}&offset=${offset}&featured=1`;
    return this.httpClient.get(url_api);
  }

  getSearch(search:string,limit,offset): Observable<any>{

    const url_api = `https://api.rutasurtrucks.com.ar/repuestos/?query=${search}&limit=${limit}&offset=${offset}`;
    return this.httpClient.get(url_api);
  }

  getDestacado(){
    const url_api = `https://api.rutasurtrucks.com.ar/vehiculos/?destacado=1`;
    return this.httpClient.get(url_api);
  }

  getUsado(id){
    const url_api = `https://api.rutasurtrucks.com.ar/vehiculos/${id}`;
    return this.httpClient.get(url_api);
  }

  getUsadoImagenes(id){
    const url_api = `https://api.rutasurtrucks.com.ar/vehiculos/${id}/imagenes/`;
    return this.httpClient.get(url_api);
  }

  enviarmensaje(contacto){
    const url_api = `https://api.rutasurtrucks.com.ar/contactos`;
    return this.httpClient.post(url_api,contacto);
  }

  sendContact(postData: any){
    let API_URL : string = "https://api.rutasurtrucks.com.ar/";
    return this.httpClient.post(API_URL + 'vehiculos/contacto/',postData);
  }

  sendService(service){
    const url_api = `https://api.rutasurtrucks.com.ar/servicios`;
    return this.httpClient.post(url_api,service);
  }

  enviarFinanciamiento(data){
    const url_api = `https://api.rutasurtrucks.com.ar/financiamiento`;
    return this.httpClient.post(url_api,data);
  }

}
