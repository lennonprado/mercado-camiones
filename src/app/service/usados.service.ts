import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Filtro } from '../models/filtro';

@Injectable({
  providedIn: 'root'
})
export class UsadosService {

  constructor(private httpClient: HttpClient) { }

    getUsados(){
      const url_api = `https://api.rutasurtrucks.com.ar/vehiculos/`;
      return this.httpClient.get(url_api);
    }

    getProducts(filtros: Filtro[], limit, offset,grupo): Observable<any>{

      const params = filtros.map( f => {
        return f.type + '=' + f.selected.map(e => e.id ).join(',')
      }).join('&')

      const url_api = `https://api.rutasurtrucks.com.ar/vehiculos/?${params}&group=${grupo}&limit=${limit}&offset=${offset}`;
      //console.log(url_api);

      return this.httpClient.get(url_api);
    }

    getGrupo(grupo : string): Observable<any>{
      const url_api = `https://api.rutasurtrucks.com.ar/grupos/${grupo}`;
      return this.httpClient.get(url_api);
    }

    getCategories(): Observable<any>{
      const url_api = `https://api.rutasurtrucks.com.ar/categorias`;
      return this.httpClient.get(url_api);
    }

    getBrands(grupo: string ): Observable<any>{
      const url_api = `https://api.rutasurtrucks.com.ar/grupos/marcas/${grupo}`;
      return this.httpClient.get(url_api);
    }

    getColor(grupo: string ): Observable<any>{
      const url_api = `https://api.rutasurtrucks.com.ar/grupos/colores/${grupo}`;
      return this.httpClient.get(url_api);
    }

    getYears(grupo: string ): Observable<any>{
      const url_api = `https://api.rutasurtrucks.com.ar/grupos/anios/${grupo}`;
      return this.httpClient.get(url_api);
    }

    getLong(grupo: string ): Observable<any>{
      const url_api = `https://api.rutasurtrucks.com.ar/grupos/largos/${grupo}`;
      return this.httpClient.get(url_api);
    }

    getModel(grupo: string ): Observable<any>{
      const url_api = `https://api.rutasurtrucks.com.ar/grupos/modelos/${grupo}`;
      return this.httpClient.get(url_api);
    }

    getDestacado(){
      const url_api = `https://api.rutasurtrucks.com.ar/vehiculos/?destacado=1`;
      return this.httpClient.get(url_api);
    }

    getUsado(id){
      const url_api = `https://api.rutasurtrucks.com.ar/vehiculos/${id}`;
      return this.httpClient.get(url_api);
    }

    getUsadoImagenes(id){
      const url_api = `https://api.rutasurtrucks.com.ar/vehiculos/${id}/imagenes/`;
      return this.httpClient.get(url_api);
    }

    enviarmensaje(contacto){
      const url_api = `https://api.rutasurtrucks.com.ar/contactos`;
      return this.httpClient.post(url_api,contacto);
    }

    sendContact(postData: any){
      let API_URL : string = "https://api.rutasurtrucks.com.ar/";
      return this.httpClient.post(API_URL + 'vehiculos/contacto/',postData);
    }

    sendService(service){
      const url_api = `https://api.rutasurtrucks.com.ar/servicios`;
      return this.httpClient.post(url_api,service);
    }


}
