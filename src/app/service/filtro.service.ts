import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Combo, Filtro } from '../models/filtro';

@Injectable({
  providedIn: 'root'
})
export class FiltroService {

  filtros : Filtro[] = [];

  private otrosFiltrosSubject : BehaviorSubject<Filtro[]> = new BehaviorSubject([])

  public currentFilters : Observable<Filtro[]> = this.otrosFiltrosSubject.asObservable();

  constructor() {}

  remove(type : string, combo: Combo){
    const found = this.filtros.find( e => e.type == type )
    if(found){
      found.selected = found.selected.filter( f => f.id !== combo.id)
      this.update()
    }
  }

  addItem(filter:Filtro){
    this.filtros.push(filter);



    /*const found = this.filtros.find( e => e.type == filter.type )
    if(!found){
      this.filtros.push(filter);
      this.update();
    }
    else{
      found.selected = filter.selected
      this.update()
    }*/
  }

  reset(){
    if(this.filtros.length > 0){
      this.filtros = []
      this.update()
    }
  }

  update(){
    this.otrosFiltrosSubject.next(this.filtros)
  }

}
