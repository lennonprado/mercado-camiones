import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Filtro } from '../models/filtro';

@Injectable({
  providedIn: 'root'
})
export class ContactosService {

  API_URL = "https://api.rutasurtrucks.com.ar/contactos";

  constructor(private httpClient: HttpClient){}

  sendContact(postData: any){
    return this.httpClient.post(this.API_URL + '/contacto/',postData);
  }

  sendFinanciacion(postData: any){}

  sendCarrito(postData: any){
    return this.httpClient.post(this.API_URL + '/carrito/',postData);
  }

  sendVender(postData: any){}

  sendComprar(postData: any){}

}
