import { Component, ViewChild, Directive, ElementRef, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ViewportScroller } from '@angular/common';
import { ReplacementService } from '../service/replacement.service';


@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.scss']
})
export class BusquedaComponent implements OnInit {

  @ViewChild('moreElementsDiv') moreElementsDiv: ElementRef;
  @ViewChild('allElements') allElements: ElementRef;

  
  stringParam: string;
  calls = 0;
  offset = 0;
  cantidad = 24;
  productos: any[] = [];
  getMore = true;
  loading = false;
  nextBreakpoint = 0;
  heigthDiv = 0;

  constructor(
    private rutaActiva: ActivatedRoute, 
    private replacementService: ReplacementService,
    private router: Router 
  ){}

  @HostListener('window:scroll', ['$event'])
  handleScroll(event) {
    if(this.nextBreakpoint == 0){    
      this.nextBreakpoint = parseInt(this.moreElementsDiv.nativeElement.offsetTop);
      this.heigthDiv =  parseInt(this.allElements.nativeElement.offsetHeight );
    }
    let positionScroll = parseInt(document.documentElement.scrollTop.toFixed(0));
    let viewportHeight = parseInt(window.innerWidth.toFixed(0));
    
      if (( viewportHeight + positionScroll  ) > this.nextBreakpoint){
      this.loadmore();
      this.nextBreakpoint = this.nextBreakpoint + this.heigthDiv;
      }
    
  }
  

  loadmore(){
    this.loading = true;
    if(this.getMore && !this.loading ){
      this.offset = this.offset + this.cantidad;
      this.obtenerResultado();  
    }
  }

  obtenerResultado(){
    this.calls++;
    console.log(this.calls + '. Next bp: '  + this.nextBreakpoint);
    this.replacementService.getSearch(this.stringParam,this.cantidad,this.offset).subscribe( response => {
      this.productos.push(...response);
      this.loading = false; 
     if(response.length < this.cantidad)
        this.getMore = false;        
    },
    error => {
      this.loading = false; 
      this.getMore = false;
      console.log(JSON.stringify(error));
    });
  }

  ngOnInit() {
    this.stringParam = this.rutaActiva.snapshot.queryParams.query;
    this.rutaActiva.queryParams.subscribe(
      (params: Params) => {
        this.stringParam = params.query;
        this.productos = [];
        this.offset = 0;
        this.loading = false;
        this.getMore = true;
        this.obtenerResultado();
      })
  }

}
