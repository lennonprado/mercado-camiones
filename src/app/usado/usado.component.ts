import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ReplacementService } from '../service/replacement.service';


@Component({
  selector: 'app-usado',
  templateUrl: './usado.component.html',
  styleUrls: ['./usado.component.scss']
})
export class UsadoComponent implements OnInit {

  constructor(public rs : ReplacementService,private route: ActivatedRoute) { }

  usado: any;
  imagenes:any

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');

    this.rs.getUsado(id).subscribe(
      data  => { this.usado = data;
       },
      error => { console.log( error );}
    );

    this.rs.getUsadoImagenes(id).subscribe(
      data  => { this.imagenes = data;
       },
      error => { console.log( error );}
    );

  }

  sendToWhat(){
    let msj = 'Hola, me interesa el usado ' + this.usado.brand_name + ' ' + this.usado.vehicle_model + ' (' + this.usado.vehicle_year + ')';
    window.open('https://wa.me/5492262411046?text=' + msj, "_blank");
  }

  contacto(form: NgForm){
    console.log(form.value);

    form.value.unidad = this.usado.brand_name + ' ' + this.usado.vehicle_model + ' (' + this.usado.vehicle_year + ')';

    this.rs.sendContact(form.value)
    .subscribe(
      data => {
        form.reset();
        alert("Mensaje enviado")
      },
      error => {

        alert("Intente más tarde");

      }
    )
  }


}
