import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { from, of, pipe } from 'rxjs';
import { map, tap, flatMap } from 'rxjs/operators';
import { ReplacementService } from 'src/app/service/replacement.service';
import { Filtro } from '../../models/filtro';


@Component({
  selector: 'filtro-repuestos',
  templateUrl: './filtro-repuestos.component.html',
  styleUrls: ['./filtro-repuestos.component.sass']
})
export class FiltroRepuestosComponent implements OnInit {
  categorias:any;
  marcas:any;

  filtros: Filtro[] = [];

  @Output() buscar = new EventEmitter<Filtro[]>();

  constructor(private replacementService: ReplacementService) { }

  ngOnInit(): void {
    this.obtenerCategorias();
    this.obtenerMarcas();
  }

  obtenerCategorias(){
    this.replacementService.getCategories().pipe(
      map( (data:any[]) => {
        return data.map( e => ({
          id : e.type_id,
          label : e.type_name
        }))
      })
    ).subscribe(
      data => this.categorias = data
    )   
  }

  obtenerMarcas(){
    this.replacementService.getBrands().pipe(
      map( (data:any[]) => {
        return data.map( e => ({
          id : e.brand_id,
          label : e.brand_name
        }))
      })
    ).subscribe(
      data => this.marcas = data
    )   
  }

  addItem(filter:Filtro){


    const existe = this.filtros.find( e => e.type == filter.type)
    if(existe){
      existe.selected = filter.selected
    }
    else{
      this.filtros.push(filter);
    }

    this.buscar.emit(this.filtros)
  }

}
