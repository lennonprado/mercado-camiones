import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroRepuestosComponent } from './filtro-repuestos.component';

describe('FiltroRepuestosComponent', () => {
  let component: FiltroRepuestosComponent;
  let fixture: ComponentFixture<FiltroRepuestosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroRepuestosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroRepuestosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
