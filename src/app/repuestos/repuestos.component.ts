import { Component, ViewChild, Directive, ElementRef, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Filtro } from '../models/filtro';
import { ReplacementService } from '../service/replacement.service';


@Component({
  selector: 'app-repuestos',
  templateUrl: './repuestos.component.html',
  styleUrls: ['./repuestos.component.scss']
})
export class RepuestosComponent implements OnInit {

  @ViewChild('moreElementsDiv') moreElementsDiv: ElementRef;

  offset = 0;
  cantidad = 12;
  idParam: any='';

  showFiltros = false;

  filtros: Filtro[] = [];
  categoria: any;
  productos: any[] = [];
  tituloSubcat: string;
  getMore = true;
  loading = false;
  nextBreakpoint = 0;


  constructor(
    private rutaActiva: ActivatedRoute,
    private replacementService: ReplacementService
  ){}

  ngOnInit() {

    this.obtenerProductos(this.filtros)
  }


  loadmore(){
    if(this.getMore && !this.loading ){
      this.loading = true;
      this.offset = this.offset + this.cantidad;


      this.replacementService.getProducts(this.filtros,this.cantidad,this.offset).subscribe( (response:any[]) => {

//       response.forEach(element => {
//        this.productos.push(element);
//     });

        this.productos.push(... response);
        this.loading = false;

        if(response.length < this.cantidad ) {
            this.getMore = false;
          }
        },
      error => {
        console.log(JSON.stringify(error));
      });


    }
  }



  obtenerProductos(filtros: Filtro[]){
    //if(filtros.length > 0){
      this.filtros = filtros
      this.replacementService.getProducts(filtros,this.cantidad,this.offset).subscribe( response => {

        this.productos = [... response];
        this.loading = false;
        if(response.length < this.cantidad ) {
            this.getMore = false;
         }

        },

      error => {
        console.log(JSON.stringify(error));
      });
    //}
  }



  removeFilter(element : any){
    const found = this.filtros.find( e => e.type == element.type )
    if(found){
      found.selected = found.selected.filter( f => f.id !== element.combo.id)
      this.obtenerProductos(this.filtros)
    }
  }

}
