import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'list-productos',
  templateUrl: './list-productos.component.html',
  styleUrls: ['./list-productos.component.scss']
})
export class ListProductosComponent implements OnInit {
  
  @Input() productos: any[];

  @Input() showFiltros: boolean;

  

  constructor() { }

  ngOnInit() {
  }

  cantProductos(){
    if(this.productos === undefined)
      return 0;
    else
      return this.productos.length;
  }

}
