import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSeguroComponent } from './home-seguro.component';

describe('HomeSeguroComponent', () => {
  let component: HomeSeguroComponent;
  let fixture: ComponentFixture<HomeSeguroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeSeguroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSeguroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
