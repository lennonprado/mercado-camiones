import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ReplacementService } from 'src/app/service/replacement.service';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {

  enviado : boolean = false;

  constructor(private rs : ReplacementService) { }

  ngOnInit() {
  }


  suscripcion(form: NgForm){

      form.value.contact_type = "Suscripcion";
      console.log(form.value);
      this.rs.enviarFinanciamiento(form.value).subscribe(
        data => {
          form.reset();
          this.enviado = true
          //alert("Mensaje enviado")
        },
        error => {
          alert("Intente mas tarde");
        }
      );
  }

}
