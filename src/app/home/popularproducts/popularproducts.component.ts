import { Component, OnInit } from '@angular/core';
import { ReplacementService } from 'src/app/service/replacement.service';

@Component({
  selector: 'popular-products',
  templateUrl: './popularproducts.component.html',
  styleUrls: ['./popularproducts.component.scss']
})
export class PopularproductsComponent implements OnInit {

  productos: any;

  constructor(private replacementService: ReplacementService) { }

  obtenerDestacados(){
    let limit = 4;
    let offset = 0;
    this.replacementService.getFeatured(limit,offset).subscribe( resultado => {
      this.productos = resultado;
    },
    error => {
      console.log(JSON.stringify(error));
    });
  }

  ngOnInit() {
    this.obtenerDestacados();
  }

}
