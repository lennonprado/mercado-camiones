import { Component, OnInit } from '@angular/core';
import { ReplacementService } from 'src/app/service/replacement.service';

@Component({
  selector: 'app-frontproducts',
  templateUrl: './frontproducts.component.html',
  styleUrls: ['./frontproducts.component.scss']
})
export class FrontproductsComponent implements OnInit {
  destacados: any;
  

  constructor(private rs: ReplacementService) { }

  ngOnInit() {
    this.rs.getDestacado().subscribe(
      data  => { 
        this.destacados = data;  
        
      },
      error => { console.log( error );}
    )
  }


}
