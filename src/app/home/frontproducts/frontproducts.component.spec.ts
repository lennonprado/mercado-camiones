import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontproductsComponent } from './frontproducts.component';

describe('FrontproductsComponent', () => {
  let component: FrontproductsComponent;
  let fixture: ComponentFixture<FrontproductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontproductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontproductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
