import { Component, ViewChild, Directive, ElementRef, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ViewportScroller } from '@angular/common';
import { ReplacementService } from '../service/replacement.service';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.scss'],
})



export class CategoriaComponent implements OnInit {

  @ViewChild('moreElementsDiv') moreElementsDiv: ElementRef;

  offset = 0;
  cantidad = 24;
  idParam: any=null;
  categoria: any;
  productos: any[] = [];
  tituloSubcat: string;
  getMore = true;
  loading = false;
  nextBreakpoint = 0;


  constructor(
    private rutaActiva: ActivatedRoute, 
    private replacementService: ReplacementService
  ){}


  @HostListener('window:scroll', ['$event'])
  handleScroll(event) {
    if(this.nextBreakpoint == 0)    
      this.nextBreakpoint = parseInt(this.moreElementsDiv.nativeElement.offsetTop);
  
    let positionScroll = parseInt(document.documentElement.scrollTop.toFixed(0));
    let viewportHeight = parseInt(window.innerWidth.toFixed(0));

    if (( viewportHeight + positionScroll  ) > this.nextBreakpoint){
     this.loadmore();
     this.nextBreakpoint = parseInt(this.moreElementsDiv.nativeElement.offsetTop);
    }


  }

  loadmore(){
    if(this.getMore && !this.loading ){
      this.loading = true;
      this.offset = this.offset + this.cantidad;
      this.obtenerProductos(this.idParam);  
    }
  }


  ngOnInit() {
    this.rutaActiva.params.subscribe(
      (params: Params) => {
        if(params.id != undefined){
          this.idParam = params.id;        
          this.obtenerCategoria(this.idParam);
        }
        this.obtenerProductos(this.idParam);
        this.tituloSubcat = "SUBCATEGORIAS";
      }
    );
  }


  obtenerCategoria(idParam){
    this.replacementService.getCategory(idParam).subscribe( resultado => {
      this.categoria = resultado;
    },
    error => {
      console.log(JSON.stringify(error));
    });
  }

  obtenerProductos(idParam){    
    this.replacementService.getProducts(idParam,this.cantidad,this.offset).subscribe( response => {
      this.productos.push(response);

      console.log(this.productos);
      
      this.loading = false;      
      if(response.length == 0)
        this.getMore = false;
    },
    error => {
      console.log(JSON.stringify(error));
    });
  }

}
