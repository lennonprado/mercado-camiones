import { TestBed } from '@angular/core/testing';

import { RutaclubService } from './rutaclub.service';

describe('RutaclubService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RutaclubService = TestBed.get(RutaclubService);
    expect(service).toBeTruthy();
  });
});
