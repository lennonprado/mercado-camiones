import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderBreadcumpComponent } from './header-breadcump.component';

describe('HeaderBreadcumpComponent', () => {
  let component: HeaderBreadcumpComponent;
  let fixture: ComponentFixture<HeaderBreadcumpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderBreadcumpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderBreadcumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
