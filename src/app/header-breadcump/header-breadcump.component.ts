import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'header-breadcump',
  templateUrl: './header-breadcump.component.html',
  styleUrls: ['./header-breadcump.component.scss']
})
export class HeaderBreadcumpComponent implements OnInit {

  @Input() titulo: any;
  @Input() bg: any;
  @Input() descripcion: any;

  mybg;

  constructor() { }

  ngOnInit() {

    this.mybg = this.bg;

  }

}
