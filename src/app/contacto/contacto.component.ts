import { FormsService } from './../forms.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ContactosService } from '../service/contactos.service';
import { ActivatedRoute } from '@angular/router';
import { ReplacementService } from '../service/replacement.service';


@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit {

  enviado : boolean = false;
  infoClient: any;
  unidad: boolean = false;
  
  usado: any;
  imagenes:any

  msj = '';


  constructor(public rs : ReplacementService, private cs : ContactosService, private fs : FormsService,private route: ActivatedRoute) { }

  ngOnInit() {
    this.getInfoContacto();
    const id = this.route.snapshot.paramMap.get('id');
    
    if(id){
      this.rs.getUsado(id).subscribe(
        data  => { 

          this.usado = data; 
          this.unidad = true;
          this.msj = 'Me interesa la siguiente unidad: ' + this.usado.vehicle_model + ', me podrian pasar información? \nMuchas gracias.'
          
        },
        error => { console.log( error );}
      );
      
    }
  }

  contacto(form: NgForm){
    form.value.contact_type = "Contacto"; 
    form.value.contact_desde = "Contacto desde Mercado Camiones";

    if(this.unidad){
      form.value.contact_unidad = this.usado.vehicle_id;
    }
    
    this.cs.sendContact(form.value).subscribe(
      data => {
        form.reset();
        this.enviado = true
        //alert("Mensaje enviado correctamente");
      },
      error => {
        console.log(error);
        alert("Intente mas tarde");
      }
    );

    let info = {
      "contact_name": form.value.contact_name,
      "contact_email": form.value.contact_email,
      "contact_phone": form.value.contact_phone,
      "contact_city": form.value.contact_city
    };

    this.fs.saveInfo(info);
  }

  getInfoContacto(){
    this.infoClient = this.fs.getInfo();
  }
}
