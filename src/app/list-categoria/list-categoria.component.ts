import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'list-categoria',
  templateUrl: './list-categoria.component.html',
  styleUrls: ['./list-categoria.component.scss']
})
export class ListCategoriaComponent implements OnInit {

  @Input() titulo: string;
  @Input() categorias: any;

  constructor() {}

  /*obtenerCategorias(){
    this.replacementService.getCategories().subscribe( resultado => {
      this.categorias = resultado;
    },
    error => {
      console.log(JSON.stringify(error));
    });
  }*/

  ngOnInit() {
    //this.obtenerCategorias();
  }

}
