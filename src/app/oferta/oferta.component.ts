import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ReplacementService } from '../service/replacement.service';

@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.component.html',
  styleUrls: ['./oferta.component.scss']
})
export class OfertaComponent implements OnInit {

  idParam: string;
  oferta: any;
  productos: any;

  constructor(private rutaActiva: ActivatedRoute, private replacementService: ReplacementService) { }

  obtenerOferta(idParam){
    this.replacementService.getOffer(idParam).subscribe( resultado => {
      this.oferta = resultado;
    },
    error => {
      console.log(JSON.stringify(error));
    });
  }

  obtenerProductosOferta(idParam){
    this.replacementService.getOfferProducts(idParam).subscribe( resultado => {
      this.productos = resultado;
    },
    error => {
      console.log(JSON.stringify(error));
    });
  }

  ngOnInit() {
    this.idParam = this.rutaActiva.snapshot.params.id;
    this.rutaActiva.params.subscribe(
      (params: Params) => {
        this.idParam = params.id;
        this.obtenerOferta(this.idParam);
        this.obtenerProductosOferta(this.idParam);
      }
    );
  }

}
