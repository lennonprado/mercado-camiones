import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { ShopCartService } from '../../shop-cart.service';

@Component({
  selector: 'one-producto',
  templateUrl: './one-producto.component.html',
  styleUrls: ['./one-producto.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OneProductoComponent implements OnInit {

  @Input() producto: any;

  constructor(private cartService: ShopCartService) { }

  ngOnInit() {}

  getOldPrice(){
    return this.producto.replacement_price;
  }

  isOffer(){
    return this.producto.offer_percentage ? true : false;
  }

  offer(){
    return this.producto.offer_percentage;
  }

  getNewPrice() : any {
    if( this.producto.offer_percentage )
     return (this.producto.replacement_price * (100 - parseInt(this.producto.offer_percentage)) / 100).toFixed(2) ;
     else
     return parseInt(this.producto.replacement_price).toFixed(2);
  }
}
