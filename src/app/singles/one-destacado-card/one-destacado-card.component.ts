import { ChangeDetectionStrategy, Component,Input, OnInit } from '@angular/core';

@Component({
  selector: 'one-destacado-card',
  templateUrl: './one-destacado-card.component.html',
  styleUrls: ['./one-destacado-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})


export class OneDestacadoCardComponent implements OnInit {

  @Input() destacado: any;

  constructor() { }

  ngOnInit(): void {
  }

}
