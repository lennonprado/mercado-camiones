import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneCategoriaComponent } from './one-categoria.component';

describe('OneCategoriaComponent', () => {
  let component: OneCategoriaComponent;
  let fixture: ComponentFixture<OneCategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneCategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
