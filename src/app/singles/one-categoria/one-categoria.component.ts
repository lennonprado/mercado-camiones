import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'one-categoria',
  templateUrl: './one-categoria.component.html',
  styleUrls: ['./one-categoria.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OneCategoriaComponent implements OnInit {
  @Input() categoria: any;

  constructor() { }

  ngOnInit() {
  }

}
