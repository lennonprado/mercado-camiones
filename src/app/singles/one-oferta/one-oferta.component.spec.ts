import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneOfertaComponent } from './one-oferta.component';

describe('OneOfertaComponent', () => {
  let component: OneOfertaComponent;
  let fixture: ComponentFixture<OneOfertaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneOfertaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneOfertaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
