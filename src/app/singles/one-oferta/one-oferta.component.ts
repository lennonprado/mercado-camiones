import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'one-oferta',
  templateUrl: './one-oferta.component.html',
  styleUrls: ['./one-oferta.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OneOfertaComponent implements OnInit {

  @Input() oferta: any;

  constructor() { }

  ngOnInit() {
  }

}
