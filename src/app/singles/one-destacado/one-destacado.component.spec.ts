import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneDestacadoComponent } from './one-destacado.component';

describe('OneDestacadoComponent', () => {
  let component: OneDestacadoComponent;
  let fixture: ComponentFixture<OneDestacadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneDestacadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneDestacadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
