import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'one-destacado',
  templateUrl: './one-destacado.component.html',
  styleUrls: ['./one-destacado.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OneDestacadoComponent implements OnInit {
  @Input() destacado: any;

  constructor() { }

  ngOnInit() {}

}
