import { ActivatedRoute, Router } from '@angular/router';
import { ShopCartService } from './../shop-cart.service';
import { Component, OnInit } from '@angular/core';
import { ReplacementService } from '../service/replacement.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  hiddenSearchBar: boolean;
  textoDeInput : string = '';
  statusMenu: boolean = true;
  statusCategoria: boolean = false;
  categorias : any;

  constructor(
    private scs: ShopCartService, 
    private rs: ReplacementService, 
    private router: Router 
    ) { }

  ngOnInit() {
    this.statusMenu = false;
    this.statusCategoria = false;
    this.getCategorias();
    this.router.events.subscribe( val => this.closeDropDowns(val)   )  
  }

  closeDropDowns(val){
    this.statusMenu = false;
    this.statusCategoria = false;
  }

  clickEventMenu(){
    this.statusMenu = !this.statusMenu;           
  }
  clickEventCategorias(){
    this.statusCategoria = !this.statusCategoria;       
  }

  getCantidad(){
    return this.scs.getCantidad();
  }

  getCategorias(){
    this.rs.getCategories().subscribe(
      data => this.categorias = data,
      error => console.log(error)    
    )
  }

  onSubmit(f){
    this.router.navigateByUrl('/busqueda/?query=' + this.textoDeInput);
  }

  clickEventNosotros(){
    
  }
  
}
