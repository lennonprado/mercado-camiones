import { Component, OnInit } from '@angular/core';
import { ShopCartService } from '../shop-cart.service';
import { NgForm } from '@angular/forms';
import { ReplacementService } from '../service/replacement.service';
import { ContactosService } from '../service/contactos.service';

@Component({
  selector: 'app-carro-compra',
  templateUrl: './carro-compra.component.html',
  styleUrls: ['./carro-compra.component.scss']
})
export class CarroCompraComponent implements OnInit {

  products: any;
  totalSinIva: any;
  total: any;
  amounts: any;
  enviado : boolean = false;

  constructor(private cs : ContactosService, private shopCart: ShopCartService, private replacServ: ReplacementService) {
    this.amounts = [];
  }

  ngOnInit() {
    this.getProducts();
    this.shopCart.getProducts();
    this.getTotal();
    //console.log(this.products);
  }

  getCantidad(){
    return this.shopCart.getCantidad();
  }

  getProducts(){
    this.products = this.shopCart.getProducts();
    if(this.products != undefined){
      for(let elem of this.products){
        this.amounts.push(elem);
      }
    }
  }

  getTotal(){
    this.totalSinIva = this.shopCart.getTotal();
    this.total = this.totalSinIva + (this.totalSinIva * 21 / 100);
    this.total = this.total.toFixed(2);
  }

  removeProd(index){
    this.products.splice(index,1);
    this.shopCart.removeFromCart(index);
    this.getTotal();
    return false;
  }

  modifyProd(index){
    this.products[index].total = parseFloat(this.products[index].info.replacement_price) * this.products[index].amount;
    this.shopCart.modifyCantProd(index,this.products[index].amount);
    this.getTotal();
  }

  enviarpedido(form: NgForm) {

    form.value.contact_type = "Pedido";
    form.value.contact_desde = "Pedido desde Mercado Camiones";
    form.value.productos = this.products;
    form.value.subtotal = this.totalSinIva;
    form.value.total = this.total;
    
    //console.log(form.value);

    this.cs.sendCarrito(form.value).subscribe(
      data => { 
        form.reset();
        this.enviado = true;
        this.shopCart.reset();
        alert("Pedido enviado correctamente")
      },
      error => {
        console.log(error);
        alert("Intente mas tarde");
      }    
    );
  }

}
