import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormsService {

  //infoClient: any;

  constructor() { }

  saveInfo(info){
    window.localStorage.setItem('infoClient',JSON.stringify(info));
  }

  getInfo(){
    let info = JSON.parse(window.localStorage.getItem('infoClient'));
    //console.log(info);
    return info;
  }
}
