import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormsService } from './../forms.service';
import { ShopCartService } from '../shop-cart.service';
import { NgForm } from '@angular/forms';
import { ReplacementService } from '../service/replacement.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent implements OnInit {

  idParam: string;
  producto: any;
  imagenes: any;
  ofertas: any;
  cantSelected = 1;
  showMessage = false;
  enviado : boolean = false;
  infoClient: any;

  constructor(
    private rutaActiva: ActivatedRoute, 
    private replacementService: ReplacementService, 
    private shopCart: ShopCartService, 
    private fs : FormsService) {

  }

  obtenerProducto(id: string){
    this.replacementService.getProduct(id).subscribe( resultado => {
      this.producto = resultado;
      console.log(resultado);
      
    },
    error => {
      console.log(JSON.stringify(error));
    });
  }

  obtenerImagenesProducto(id: string){
    this.replacementService.getProductImages(id).subscribe( resultado => {
      this.imagenes = resultado;
    }, error => {
      console.log(JSON.stringify(error));
    });
  }

  obtenerOfertas(){
    this.replacementService.getOffers().subscribe( response => {
      this.ofertas = response;
    },
    error => {
      console.log(JSON.stringify(error));
    });
  }

  addToCart(){
    this.shopCart.addToCart(this.producto,this.cantSelected);
    this.showMessage = true;
    setTimeout(() => {
      this.showMessage = false;

  }, 2000);
    return false;
  }

  addToCartAndMove(){
    this.shopCart.addToCart(this.producto,this.cantSelected);
  }

  getCart(){
    this.shopCart.getProducts();
  }

  ngOnInit() {
    this.idParam = this.rutaActiva.snapshot.params.id;
    this.rutaActiva.params.subscribe(
      (params: Params) => {
        this.idParam = params.id;
        this.obtenerProducto(params.id);
        this.obtenerImagenesProducto(params.id);
        //this.obtenerOfertas();
      }
    );
    this.getInfoContacto();
  }

  getOldPrice(){
    return this.producto.replacement_price;
  }

  isOffer(){
    return this.producto.offer_percentage ? true : false;
  }

  getNewPrice(){
    if( this.producto.offer_percentage )
     return this.producto.replacement_price * (100 - parseInt(this.producto.offer_percentage)) / 100 ;
     else
     return this.producto.replacement_price;
  }

  contacto(form: NgForm) {
    form.value.contact_type = "Sin stock";
    form.value.contact_msj = "id repuesto: " + this.producto.replacement_id + " " + this.producto.replacement_name;
    console.log(form.value);
    this.replacementService.enviarmensaje(form.value).subscribe(
      data => {
        form.reset();
        this.enviado = true
        //alert("Mensaje enviado")
      },
      error => {
        alert("Intente mas tarde");
      }
    );

    let info = {
      "contact_name": form.value.contact_name,
      "contact_email": form.value.contact_email,
      "contact_phone": form.value.contact_phone,
      "contact_city": form.value.contact_city
    };

    this.fs.saveInfo(info);
  }
 
  sendToWhat(){
    let msj = 'Hola, me interesa el producto ' + this.producto.replacement_name + ' (' + this.producto.replacement_part_number + ') cantidad:  '+this.cantSelected;
    window.open('https://wa.me/5492262411046?text=' + msj, "_blank");
  }

  getInfoContacto(){
    this.infoClient = this.fs.getInfo();
  }

}
