import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';


//import { OwlCarouselDemoComponent } from './components/owl-carousel-demo/owl-carousel-demo.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselModule } from 'ngx-owl-carousel-o';



import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SliderComponent } from './home/slider/slider.component';
import { FrontproductsComponent } from './home/frontproducts/frontproducts.component';
import { PopularproductsComponent } from './home/popularproducts/popularproducts.component';
import { GaleryComponent } from './common/galery/galery.component';
import { DescriptionComponent } from './producto/description/description.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { OfertaComponent } from './oferta/oferta.component';
import { ListCategoriaComponent } from './list-categoria/list-categoria.component';
import { ListOfertaComponent } from './list-oferta/list-oferta.component';
import { HomeSeguroComponent } from './home/home-seguro/home-seguro.component';
import { DetalleComponent } from './producto/detalle/detalle.component';
import { ListProductosComponent } from './repuestos/list-productos/list-productos.component';
import { OneOfertaComponent } from './singles/one-oferta/one-oferta.component';
import { OneCategoriaComponent } from './singles/one-categoria/one-categoria.component';
import { OneProductoComponent } from './singles/one-producto/one-producto.component';
import { OneDestacadoComponent } from './singles/one-destacado/one-destacado.component';
import { NewsletterComponent } from './home/newsletter/newsletter.component';
import { RutaclubComponent } from './rutaclub/rutaclub.component';
import { MispuntosComponent } from './rutaclub/mispuntos/mispuntos.component';
import { CotizadorComponent } from './cotizador/cotizador.component';
import { VehiculoComponent } from './cotizador/vehiculo/vehiculo.component';
import { AjusteComponent } from './cotizador/ajuste/ajuste.component';
import { CotizacionesComponent } from './cotizador/cotizaciones/cotizaciones.component';
import { SolicitudComponent } from './cotizador/solicitud/solicitud.component';
import { HomeComponent } from './home/home.component';
import { ProductoComponent } from './producto/producto.component';
import { ContactoComponent } from './contacto/contacto.component';
import { BuscadorComponent } from './header/buscador/buscador.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { CarroCompraComponent } from './carro-compra/carro-compra.component';
import { UsadoComponent } from './usado/usado.component';
import { UsadosComponent } from './usados/usados.component';
import { HeaderBreadcumpComponent } from './header-breadcump/header-breadcump.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { OfertasComponent } from './ofertas/ofertas.component';
import { TerminosComponent } from './terminos/terminos.component';
import { ReplaceThumbForOriginalPipe } from './pipes/replace-thumb-for-original.pipe';
import { OneDestacadoCardComponent } from './singles/one-destacado-card/one-destacado-card.component';

import { LogosComponent } from './home/logos/logos.component';
import { BuyTruckComponent } from './home/buy-truck/buy-truck.component';
import { BuyorsellComponent } from './home/buyorsell/buyorsell.component';
import { SedesComponent } from './home/sedes/sedes.component';
import { RepuestosMujerComponent } from './home/repuestos/repuestos.component';
import { RepuestosComponent } from './repuestos/repuestos.component';


import { FinanciamientoComponent } from './financiamiento/financiamiento.component';
import { VenderComponent } from './vender/vender.component';
import { FiltroRepuestosComponent } from './repuestos/filtro-repuestos/filtro-repuestos.component';
import { FiltroSelectComponent } from './common/filtro-select/filtro-select.component';
import { FiltroElegidosComponent } from './common/filtro-elegidos/filtro-elegidos.component';
import { FiltroUsadosComponent } from './usados/filtro-usados/filtro-usados.component';
import { ListaUsadosComponent } from './usados/lista-usados/lista-usados.component';
import { NosotrosComponent } from './nosotros/nosotros.component';





@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    SliderComponent,
    FrontproductsComponent,
    PopularproductsComponent,
    GaleryComponent,
    DescriptionComponent,
    CategoriaComponent,
    OfertaComponent,
    ListCategoriaComponent,
    ListOfertaComponent,
    HomeSeguroComponent,
    DetalleComponent,
    ListProductosComponent,
    OneOfertaComponent,
    OneCategoriaComponent,
    OneProductoComponent,
    OneDestacadoComponent,
    NewsletterComponent,
    RutaclubComponent,
    MispuntosComponent,
    CotizadorComponent,
    VehiculoComponent,
    AjusteComponent,
    CotizacionesComponent,
    SolicitudComponent,
    HomeComponent,
    ProductoComponent,
    ContactoComponent,
    BuscadorComponent,
    BusquedaComponent,
    CarroCompraComponent,
    UsadoComponent,
    UsadosComponent,
    HeaderBreadcumpComponent,
    ServiciosComponent,
    OfertasComponent,
    TerminosComponent,
    ReplaceThumbForOriginalPipe,
    OneDestacadoCardComponent,
    RepuestosComponent,
    LogosComponent,
    BuyTruckComponent,
    BuyorsellComponent,
    SedesComponent,


    FinanciamientoComponent,
    VenderComponent,
    RepuestosMujerComponent,
    FiltroRepuestosComponent,
    FiltroSelectComponent,
    FiltroElegidosComponent,
    FiltroUsadosComponent,
    ListaUsadosComponent,
    NosotrosComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    CarouselModule
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
