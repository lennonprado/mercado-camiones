import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaclubComponent } from './rutaclub.component';

describe('RutaclubComponent', () => {
  let component: RutaclubComponent;
  let fixture: ComponentFixture<RutaclubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaclubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaclubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
