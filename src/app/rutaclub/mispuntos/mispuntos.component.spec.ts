import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MispuntosComponent } from './mispuntos.component';

describe('MispuntosComponent', () => {
  let component: MispuntosComponent;
  let fixture: ComponentFixture<MispuntosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MispuntosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MispuntosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
