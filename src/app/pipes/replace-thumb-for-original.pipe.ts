import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceThumbForOriginal'
})
export class ReplaceThumbForOriginalPipe implements PipeTransform {

  transform(value: string, change : string): string { 
    if(value == undefined){
      value = 'assest/img/bg1.png';
    }   
    
    if(value == ''){
      value = 'assest/img/bg1.png';
    }
    
    if(change == ''){
      change = 'original';
    }

    if(change == undefined){
      change = 'original';
    }
    
      return value.replace('thumb',change);

  }

}
