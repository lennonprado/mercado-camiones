import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'list-oferta',
  templateUrl: './list-oferta.component.html',
  styleUrls: ['./list-oferta.component.scss']
})
export class ListOfertaComponent implements OnInit {

  @Input() ofertas: any; 

  constructor() { }

  ngOnInit() {
  }

}
