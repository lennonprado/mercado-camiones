export interface Filtro{
    type : string;
    selected : Combo[];  
  }
  
  export interface Combo{
    id : number,
    label : string
  }