import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroUsadosComponent } from './filtro-usados.component';

describe('FiltroUsadosComponent', () => {
  let component: FiltroUsadosComponent;
  let fixture: ComponentFixture<FiltroUsadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroUsadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroUsadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
