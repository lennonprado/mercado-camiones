import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { map,tap } from 'rxjs/operators';
import { FiltroService } from 'src/app/service/filtro.service';
import { ReplacementService } from 'src/app/service/replacement.service';
import { UsadosService } from 'src/app/service/usados.service';


@Component({
  selector: 'filtro-usados',
  templateUrl: './filtro-usados.component.html',
  styleUrls: ['./filtro-usados.component.sass']
})
export class FiltroUsadosComponent implements OnInit, OnChanges  {

  @Input() grupo;

  categorias : any = []
  marcas : any = []
  colores: any = [];
  largo: any = [];

  modelos: any = [];
  anios: any = [];

  cargados = 0
  completados = 0

  selects: []

  constructor(private us: UsadosService, private fs:FiltroService) {}

  ngOnInit(): void {
    this.fs.currentFilters.subscribe(
      f => this.completados = f.length
    )
  }

  ngOnChanges(){
    this.cargados = 0
    this.obtenerCategorias();
    this.obtenerMarcas();
    this.obtenerColores();
    this.obtenerLargo();
    this.obtenerAnio();
    this.obtenerModelo();
  }

  obtenerCategorias(){
    this.us.getGrupo(this.grupo).pipe(
      map( (data:any[]) => {
        return data.map( e => ({
          id : e.type_id,
          label : e.type_name
        }))
      })
    ).subscribe(
      data => { this.categorias = data
        this.cargados++
      }
    )
  }

  obtenerMarcas(){
    this.us.getBrands(this.grupo).pipe(
      map( (data:any[]) => {
        return data.map( e => ({
          id : e.brand_id,
          label : e.brand_name
        }))
      })
    ).subscribe(
      data => {this.marcas = data
        this.cargados++
      }
    )
  }

  obtenerColores(){
    this.us.getColor(this.grupo).pipe(
      map( (data:any[]) => {
        return data.map( e => ({
          id : e.vehicle_color,
          label : e.vehicle_color
        }))
      })
    ).subscribe(
      data => {this.colores = data
        this.cargados++
      }
    )
  }

  obtenerLargo(){
    this.us.getLong(this.grupo).pipe(
      map( (data:any[]) => {
        return data.map( e => ({
          id : e.vehicle_long,
          label : e.vehicle_long
        }))
      })
    ).subscribe(
      data => {
        this.largo = data
        this.cargados++
      }
    )
  }

  obtenerModelo(){
    this.us.getModel(this.grupo).pipe(
      map( (data:any[]) => {
        return data.map( e => ({
          id : e.vehicle_model,
          label : e.vehicle_model
        }))
      })
    ).subscribe(
      data => {this.modelos = data
        this.cargados++}
    )
  }

  obtenerAnio(){
    this.us.getYears(this.grupo).pipe(
      map( (data:any[]) => {
        return data.map( e => ({
          id : e.vehicle_year,
          label : e.vehicle_year
        }))
      })
    ).subscribe(
      data => {this.anios = data
        this.cargados++}
    )
  }

}


export interface select{
  type: string;
  label:string
    datos: [{
      id : string,
      label : string
    }]
}
