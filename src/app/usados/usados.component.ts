import { Component, OnInit } from '@angular/core';

import { UsadosService } from '../service/usados.service';
import { FiltroService } from '../service/filtro.service';
import { Combo, Filtro } from '../models/filtro';
import { ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-usados',
  templateUrl: './usados.component.html',
  styleUrls: ['./usados.component.scss']
})

export class UsadosComponent implements OnInit {


  filtros
  grupo:string;
  destacados: any;
  showFiltros = true
  loading = false;
  offset = 0;
  cantidad = 12;
  getMore = true;
  usados : any[] = []

  constructor(
    private us: UsadosService,
    private fs: FiltroService,
    private router:ActivatedRoute) { }

  ngOnInit() {
    this.router.params.subscribe(
      params => {
        if(params.hasOwnProperty('group')){
          console.clear()
          console.warn("----------------------------------------------RUTA CAMBIO");
          console.warn("----------------------------------------------RUTA CAMBIO");
          console.warn("----------------------------------------------RUTA CAMBIO");
          this.grupo = params.group
          this.fs.reset()
          this.loadFilters()
        }
      }
    )
  }

  loadFilters(){
    this.fs.currentFilters.subscribe(
      filtros => {
        this.filtros = filtros
        this.obtenerProductos()
      }
    );
  }



  obtenerProductos(){

    this.us.getProducts(this.filtros,this.cantidad,this.offset,this.grupo).subscribe( response => {

        this.usados = [...response];
          this.loading = false;
        },
      error => {
        console.log(JSON.stringify(error));
      });
    //}
  }

  updateObserver(){
    this.fs.update();
  }

  loadmore(){
    if(this.getMore && !this.loading ){
      this.loading = true;
      this.offset = this.offset + this.cantidad;


      this.us.getProducts(this.filtros,this.cantidad,this.offset,this.grupo).subscribe( (response:any[]) => {

        this.usados.push(... response);
        this.loading = false;
        if(response.length < this.cantidad ) {
            this.getMore = false;
          }
        },
      error => {
        console.log(JSON.stringify(error));
      });


    }
  }

}
