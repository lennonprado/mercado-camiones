import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaUsadosComponent } from './lista-usados.component';

describe('ListaUsadosComponent', () => {
  let component: ListaUsadosComponent;
  let fixture: ComponentFixture<ListaUsadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaUsadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaUsadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
