import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'lista-usados',
  templateUrl: './lista-usados.component.html',
  styleUrls: ['./lista-usados.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListaUsadosComponent implements OnInit {
  @Input() datos: any[];
  @Input() showFiltros: boolean;


  constructor() { }

  ngOnInit(): void {
  }

  trackByFn(index, item) {
    if(item.hasOwnProperty('vehicle_id'))
    return item.vehicle_id; // or item.id
  }

}
