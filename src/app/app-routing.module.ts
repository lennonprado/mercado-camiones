import { OfertasComponent } from './ofertas/ofertas.component';
import { UsadosComponent } from './usados/usados.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfertaComponent } from './oferta/oferta.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { ProductoComponent } from './producto/producto.component';
import { RutaclubComponent } from './rutaclub/rutaclub.component';
import { CotizadorComponent } from './cotizador/cotizador.component';
import { ContactoComponent } from './contacto/contacto.component';
import { HomeComponent } from './home/home.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { CarroCompraComponent } from './carro-compra/carro-compra.component'
import { UsadoComponent } from './usado/usado.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { TerminosComponent } from './terminos/terminos.component';
import { FinanciamientoComponent } from './financiamiento/financiamiento.component';
import { VenderComponent } from './vender/vender.component';
import { RepuestosComponent } from './repuestos/repuestos.component';
import { NosotrosComponent } from './nosotros/nosotros.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  //{ path: 'oferta/:id', component: OfertaComponent },
  //{ path: 'categoria', component: CategoriaComponent },
  //{ path: 'categoria/:id', component: CategoriaComponent },
  { path: 'producto/:id', component: ProductoComponent },
  { path: 'contacto', component: ContactoComponent },
  { path: 'contacto/:id', component: ContactoComponent },
  { path: 'busqueda/', component: BusquedaComponent},
  { path: 'carrito', component: CarroCompraComponent },
  { path: 'usado/:id', component: UsadoComponent },
  { path: 'comprar/:group', component: UsadosComponent },
  { path: 'vender', component: VenderComponent  },
  { path: 'cubiertas', component: RepuestosComponent  },
  { path: '**', redirectTo:'/'  },

  //{ path: 'comprar/:grupo', component: UsadosComponent },
  //{ path: 'cotizador', component: CotizadorComponent },
  //{ path: 'seguros', component: CotizadorComponent },
  //{ path: 'rutaclub', component: RutaclubComponent },
  //{ path: 'servicios', component: ServiciosComponent },
  //{ path: 'ofertas', component: OfertasComponent },
  //{ path: 'terminos', component: TerminosComponent  },
  //{ path: 'financiamiento', component: FinanciamientoComponent  },
  //{ path: 'nosotros', component: NosotrosComponent  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,  {
    scrollPositionRestoration: 'enabled', // Add options right here
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
