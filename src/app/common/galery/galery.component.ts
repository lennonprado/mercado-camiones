import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-galery',
  templateUrl: './galery.component.html',
  styleUrls: ['./galery.component.scss']
})
export class GaleryComponent implements OnInit {

  @Input() imagenes: any[];
  
  indice: number;

  constructor() {}

  ngOnInit() {
    this.indice = 0;
  }

  changePrevFocus(){
    let size = this.imagenes.length;
    this.indice = this.indice - 1;
    if(this.indice < 0){
      this.indice = size-1;
    }
  }

  changeNextFocus(){
    let size = this.imagenes.length;
    this.indice = this.indice + 1;
    if(this.indice == size){
      this.indice = 0;
    }
  }

  setThisFocus(i){
    let size = this.imagenes.length;
    this.indice = i;
  }

}
