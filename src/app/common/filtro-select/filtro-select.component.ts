import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FiltroService } from 'src/app/service/filtro.service';
import { Combo, Filtro } from '../../models/filtro';


@Component({
  selector: 'filtro-select',
  templateUrl: './filtro-select.component.html',
  styleUrls: ['./filtro-select.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        animate('200ms ease-in', style({ overflow:'hidden', height: 'auto' }))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({ overflow:'hidden', height: '0px' }))
      ])
    ])
  ]
})
export class FiltroSelectComponent implements OnInit, OnChanges {

  @Input() datos: any[];
  @Input() label: string;
  @Input() type: string;

  filtro : Filtro

  visible = false

  //@Output() filtrar = new EventEmitter<any>();

  constructor(private fs : FiltroService){

  }

  ngOnInit(): void {
  }

  ngOnChanges(){
    this.filtro = {
      type : this.type,
      selected : []
    }
    this.fs.addItem(this.filtro)
  }


  select(dato : Combo){
    if((this.filtro.selected.find( (e:Combo) => (e.id === dato.id) && (e.label == dato.label) ) ) === undefined) {
      this.filtro.selected.push(dato)
      //this.filtrar.emit(this.filtro)
      this.updateObserver()
    }
  }

  isSelected(dato){
    const found = this.filtro.selected.find( e => e.id == dato.id && e.label == dato.label)
    return found ? true : false
  }

  updateObserver(){
      this.fs.update();
  }


}
