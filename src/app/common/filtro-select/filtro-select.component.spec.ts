import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroSelectComponent } from './filtro-select.component';

describe('FiltroSelectComponent', () => {
  let component: FiltroSelectComponent;
  let fixture: ComponentFixture<FiltroSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
