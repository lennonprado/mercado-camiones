import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FiltroService } from 'src/app/service/filtro.service';
import { Combo, Filtro } from '../../models/filtro';

@Component({
  selector: 'filtro-elegidos',
  templateUrl: './filtro-elegidos.component.html',
  styleUrls: ['./filtro-elegidos.component.scss']
})
export class FiltroElegidosComponent implements OnInit {

 filtros: Filtro[] = [];

 display = false;

  @Output() remove = new EventEmitter<Combo>();

  constructor(private fs: FiltroService){}

  ngOnInit(): void {

    this.fs.currentFilters.subscribe(
      (f:Filtro[]) => {
        this.display = false
        for(let i=0; i<f.length;i++){
          if(f[i].selected.length > 0){
            this.display = true
            break
          }
        }
        this.filtros = f
      }
    )
  }

  removeThisFilter(combo : Combo, type : string){
    const element:any = {
      type : type,
      combo : combo
    }
    //this.remove.emit(element)
    this.fs.remove(type, combo)
  }

  mostrar(){
     this.fs.currentFilters.subscribe(
      f => {
        console.log(f);

        this.filtros = f
      }
    )
  }

  borrar(){

  }


}
