import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroElegidosComponent } from './filtro-elegidos.component';

describe('FiltroElegidosComponent', () => {
  let component: FiltroElegidosComponent;
  let fixture: ComponentFixture<FiltroElegidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroElegidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroElegidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
